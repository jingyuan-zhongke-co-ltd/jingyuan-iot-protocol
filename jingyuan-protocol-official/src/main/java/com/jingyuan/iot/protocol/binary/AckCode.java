package com.jingyuan.iot.protocol.binary;


/**
 * 应答消息状态码
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:47
 * @Version: V1.0
 */
public enum AckCode {
    ok,
    noAuth,
    unsupportedMessage
}
