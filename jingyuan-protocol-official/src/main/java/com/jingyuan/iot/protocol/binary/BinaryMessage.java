package com.jingyuan.iot.protocol.binary;

import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.DeviceMessage;
import reactor.core.publisher.Flux;


/**
 * 二进制消息抽象接口
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:51
 * @Version: V1.0
 */
public interface BinaryMessage<T extends DeviceMessage> {

    BinaryMessageType getType();

    void read(ByteBuf buf);

    void write(ByteBuf buf);

    void setMessage(T message);

    T getMessage();

}
