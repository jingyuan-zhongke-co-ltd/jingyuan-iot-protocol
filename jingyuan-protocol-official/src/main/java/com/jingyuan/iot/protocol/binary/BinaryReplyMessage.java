package com.jingyuan.iot.protocol.binary;

import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.DeviceMessageReply;

import java.util.Map;

/**
 * 回复
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:51
 * @Version: V1.0
 */
public abstract class BinaryReplyMessage<T extends DeviceMessageReply> implements BinaryMessage<T> {

    /**
     * 原消息
     */
    private T message;

    /**
     * 新消息
     *
     * @return T
     */
    protected abstract T newMessage();

    /**
     * 读数据
     *
     * @param buf 写入数据
     */
    @Override
    public final void read(ByteBuf buf) {
        message = newMessage();
        boolean success = buf.readBoolean();
        if (success) {
            doReadSuccess(message, buf);
        } else {
            message.success(false);
            message.code(String.valueOf(DataType.readFrom(buf)));
            message.message(String.valueOf(DataType.readFrom(buf)));
        }
    }

    /**
     * 读成功
     *
     * @param msg 消息
     * @param buf 写入数据
     */
    protected abstract void doReadSuccess(T msg, ByteBuf buf);

    /**
     * 写数据
     *
     * @param buf 写出数据
     */
    @Override
    public final void write(ByteBuf buf) {
        buf.writeBoolean(message.isSuccess());

        if (message.isSuccess()) {
            doWriteSuccess(message, buf);
        } else {
            DataType.writeTo(message.getCode(), buf);
            DataType.writeTo(message.getMessage(), buf);
        }
    }

    /**
     * 写成功
     *
     * @param msg 消息
     * @param buf 写出数据
     */
    protected abstract void doWriteSuccess(T msg, ByteBuf buf);

    @Override
    public void setMessage(T message) {
        this.message = message;
    }

    @Override
    public T getMessage() {
        return message;
    }
}
