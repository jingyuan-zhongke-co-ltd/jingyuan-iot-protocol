package com.jingyuan.iot.protocol.binary;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetlinks.core.message.property.ReadPropertyMessageReply;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.Map;

/**
 * 读取属性回复
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:51
 * @Version: V1.0
 */
public class BinaryReadPropertyMessageReply extends BinaryReplyMessage<ReadPropertyMessageReply> {

    @Override
    public BinaryMessageType getType() {
        return BinaryMessageType.readPropertyReply;
    }

    @Override
    protected ReadPropertyMessageReply newMessage() {
        return new ReadPropertyMessageReply();
    }

    @Override
    protected void doWriteSuccess(ReadPropertyMessageReply msg, ByteBuf buf) {
        DataType.OBJECT.write(buf, msg.getProperties());
    }

    @Override
    protected void doReadSuccess(ReadPropertyMessageReply msg, ByteBuf buf) {
        @SuppressWarnings("all")
        Map<String, Object> map = (Map<String, Object>) DataType.OBJECT.read(buf);
        msg.setProperties(map);

    }


}
