package com.jingyuan.iot.protocol.functional;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TimeSyncRequest {
    private String messageId;
}
