package com.jingyuan.iot.protocol.binary;

import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetlinks.core.message.function.FunctionInvokeMessageReply;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.Map;

/**
 * 功能调用回复
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:50
 * @Version: V1.0
 */
public class BinaryFunctionInvokeMessageReply extends BinaryReplyMessage<FunctionInvokeMessageReply> {

    @Override
    public BinaryMessageType getType() {
        return BinaryMessageType.functionReply;
    }

    @Override
    protected FunctionInvokeMessageReply newMessage() {
        return new FunctionInvokeMessageReply();
    }

    @Override
    protected void doReadSuccess(FunctionInvokeMessageReply msg, ByteBuf buf) {
        msg.setFunctionId((String) DataType.readFrom(buf));
        msg.setOutput(DataType.readFrom(buf));
    }

    @Override
    protected void doWriteSuccess(FunctionInvokeMessageReply msg, ByteBuf buf) {
        DataType.writeTo(getMessage().getFunctionId(), buf);
        DataType.writeTo(msg.getOutput(), buf);
    }


}
