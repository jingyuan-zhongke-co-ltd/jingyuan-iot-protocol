package com.jingyuan.iot.protocol.binary;

import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.Map;

/**
 * 修改属性回复
 *
 * @author: zhouhao
 * @date: 2024/3/27 16:51
 * @Version: V1.0
 */
public class BinaryWritePropertyMessageReply extends BinaryReplyMessage<WritePropertyMessageReply> {

    @Override
    public BinaryMessageType getType() {
        return BinaryMessageType.writePropertyReply;
    }

    @Override
    protected WritePropertyMessageReply newMessage() {
        return new WritePropertyMessageReply();
    }

    @Override
    protected void doReadSuccess(WritePropertyMessageReply msg, ByteBuf buf) {
        @SuppressWarnings("all")
        Map<String, Object> map = (Map<String, Object>) DataType.OBJECT.read(buf);
        msg.setProperties(map);
    }

    @Override
    protected void doWriteSuccess(WritePropertyMessageReply msg, ByteBuf buf) {
        DataType.OBJECT.write(buf, msg.getProperties());
    }
}
