package com.jingyuan.iot.protocol.tcp.wac.gauge.conf;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfRemoteConfBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.BcdAndPad;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.revertBCD;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.genRead;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.writeGeneralParma;

/**
 * 远程开度模式
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
public class WacConfRemoteCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值，表示50开度。
        properties.put("valveAngular", Convert.toInt(revertBCD(buf, 2)));

        genRead(buf, properties);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacConfRemoteConfBo bean = BeanUtil.toBean(properties, WacConfRemoteConfBo.class);
        // 目标调节值
        buf.writeBytes(BcdAndPad(bean.getAdjustOpening(), 2));

        writeGeneralParma(buf, bean);
    }
}
