package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * FB 查询流量差量值
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpQueryFlowDifferenceMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        流量差量值	Int 	0~65535	2
        byte[] flowDifference = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(flowDifference);
        properties.put("flowDifference", BytesUtils.bytesToShort(flowDifference));

    }
}
