package com.jingyuan.iot.protocol.tcp.cct.afn.afn04;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn04.CctContValveCloseBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 遥控关阀（跳闸门）
 *
 * @author: cc
 * @date: 2024/7/26 10:08
 * @Version: V1.0
 */
@Slf4j
public class CctContValveCloseMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        contValve(buf, properties);
    }

    public static void contValve(ByteBuf buf, Map<String, Object> properties) {
        CctContValveCloseBo valveCloseBo = BeanUtil.toBean(properties, CctContValveCloseBo.class);

        // 计量表类型
        buf.writeByte(valveCloseBo.getMeterType());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(valveCloseBo.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
    }
}
