package com.jingyuan.iot.protocol.tcp.hlm.afn.afnff;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.hlm.afn03.HlmSetAllBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils.replenishByte;

/**
 * 配置计量表运行参数
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmSetRunningParamMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        HlmSetAllBo runningParamBo = BeanUtil.toBean(properties, HlmSetAllBo.class);
//        01 			// 数据包标识。
        buf.writeByte(1);
//        00 			// 上传模式为采后传。周期或定时采集时间到后，采集存储完数据立即上传。
        buf.writeByte(runningParamBo.getUploadMode());
//        01 00 		// 开始时间01:00
        for (String s : runningParamBo.getStartTimeText().split(":")) {
            buf.writeBytes(BytesUtils.hexToBytes(s));
        }
//        03 00 		// 结束时间03:00
        for (String s : runningParamBo.getEndTimeText().split(":")) {
            buf.writeBytes(BytesUtils.hexToBytes(s));
        }
//        00 			// 采集模式为周期采集，即采集周期到，设备采集存储数据
        int collectionMode = runningParamBo.getCollectionMode();
        buf.writeByte(collectionMode);
//        3 C 00 		// 采集周期60分钟。
        byte[] bytes = BytesUtils.intToBytes(runningParamBo.getCollectionInterval());
        bytes = replenishByte(bytes, 2);
        buf.writeBytes(new byte[]{bytes[0], bytes[1]});
//        16 00 		// 定时时间1，即采集模式为定时采集时，到了16:00设备采集存储一次数据。
        for (String s : runningParamBo.getSchedule1Text().split(":")) {
            buf.writeBytes(BytesUtils.hexToBytes(s));
        }
//        06 00 		// 定时时间2，即采集模式为定时采集时，到了06:00设备采集存储一次数据。
        for (String s : runningParamBo.getSchedule2Text().split(":")) {
            buf.writeBytes(BytesUtils.hexToBytes(s));
        }
//        31 38 30 2E 31 30 31 2E 31 34 37 2e 31 31 35 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//// 服务器IP，180.101.147.115。
        byte[] bytes1 = BytesUtils.hexToBytes(BytesUtils.acsStrToHex(runningParamBo.getIp()));
        bytes1 = replenishByte(bytes1, 32);
        buf.writeBytes(bytes1);
//        33 16 		// 服务器端口，5683。
        byte[] bytes3 = BytesUtils.shortToBytes(runningParamBo.getPort());
        bytes3 = replenishByte(bytes3, 2);
        buf.writeBytes(new byte[]{bytes3[1], bytes3[0]});

    }
}
