package com.jingyuan.iot.protocol.tcp.cct.afn.afn0a;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn04.CctQueryInnerTableFileBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0b.CctRtMeterDataMessage.readGroupAndMeterCount;

/**
 * F13：查询集采器内表档案
 *
 * @author: dr
 * @date: 2024-12-24
 * @Version: V1.0
 */
@Slf4j
public class CctQueryInnerTableFileMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        总组数（单表召测时数据为0，组召测时>0）	数据格式08	2
//        当前组（单表召测时数据为0，组召测时>0）	数据格式08	2
//        组内表数（单表召测时数据为0，组召测时>0）	BIN	1
//        表类型及厂商代码	BIN	1
        int meterCount = readGroupAndMeterCount(buf, properties);

        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        readMeterData(buf, meterDataVos, count);
        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));
    }

    /**
     * 读取表数据
     *
     * @param buf          写入缓冲区
     * @param meterDataVos 计量表数据
     * @param count        组内表数
     */
    private void readMeterData(ByteBuf buf, List<CctMeterDataVo> meterDataVos, int count) {
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
            // 计量表序号
            int i1 = buf.readByte();
            cctMeterDataVo.setMeterSerialNum(i1);
            // 计量表通信号
            byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
            BytesUtils.bytesReverse(nbAddr);
            cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
            // 运行标志
            int runFlag = buf.readByte();
            cctMeterDataVo.setRunFlag(runFlag);
            // 边界值
            int boundary = buf.readByte();
            cctMeterDataVo.setBoundary(boundary);
            // 采集器号
            byte[] coolAddr = ByteBufUtils.readBytes(buf, 6);
            cctMeterDataVo.setCoolAddr(Integer.valueOf(BytesUtils.binary(10, coolAddr[5])));
            // 已下载标志
            int downloadedFlag = buf.readByte();
            cctMeterDataVo.setBoundary(downloadedFlag);
            // 中继深度
            String relayDepth = BytesUtils.byteToHex(buf.readByte());
            if (relayDepth.equals("直接通讯")) {
                relayDepth = "";
            } else if (relayDepth.equals("0b")) {
                relayDepth = "尚未搜寻路径";
            } else if (relayDepth.equals("ee")) {
                relayDepth = "未搜到路径";
            } else {
                relayDepth = "中继深度:" + Convert.toInt(relayDepth);
            }
            cctMeterDataVo.setRelayDepth(relayDepth);
            // 中继路径
            byte[] bytes1 = ByteBufUtils.readBytes(buf, 10);
            BytesUtils.bytesReverse(bytes1);
            String s = BytesUtils.bytesToHex(bytes1);
            StringBuilder stringBuilder = new StringBuilder();
            for (int j = 0; j < 5; j = j + 2) {
                String s1 = s.substring(j, j + 2);
                if ("ee".equals(s1)) break;
                stringBuilder.append(s1).append("->");
            }
            cctMeterDataVo.setRelayPath(Convert.toStr(stringBuilder.substring(0, stringBuilder.length() - 2)));

            meterDataVos.add(cctMeterDataVo);
        }
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctQueryInnerTableFileBo bean = BeanUtil.toBean(properties, CctQueryInnerTableFileBo.class);

        // 计量表类型
        buf.writeByte(bean.getMeterType());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(bean.getMeterAddr());
        if (ArrayUtil.isEmpty(nbAddrBytes) || nbAddrBytes.length != 6) {
            // 表号地址不匹配，默认按照批抄处理
            buf.writeBytes(new byte[]{(byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa, (byte) 0xaa});
        } else {
            BytesUtils.bytesReverse(nbAddrBytes);
            buf.writeBytes(nbAddrBytes);
        }
    }
}
