package com.jingyuan.iot.protocol.tcp.lcp.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn03.LcpLargeMeterBase;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设定大表底数
 *
 * @author: dr
 * @date: 2024-10-31
 * @Version: V1.0
 */
@Slf4j
public class LcpSetLargeMeterReadingMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpLargeMeterBase bean = BeanUtil.toBean(properties, LcpLargeMeterBase.class);
        byte[] bytes1 = BytesUtils.intToBytes(bean.getNegativePulseCount());
        BytesUtils.bytesReverse(bytes1);
        buf.writeBytes(bytes1);

        byte[] bytes2 = BytesUtils.intToBytes(bean.getPositivePulseCount());
        BytesUtils.bytesReverse(bytes2);
        buf.writeBytes(bytes2);
    }
}
