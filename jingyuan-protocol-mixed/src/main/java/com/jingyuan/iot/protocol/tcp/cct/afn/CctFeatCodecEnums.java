package com.jingyuan.iot.protocol.tcp.cct.afn;

import com.jingyuan.common.protocol.enums.cct.fn.*;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn03.CctDownloadMeterProfileMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn03.CctSetHeartbeatMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn03.CctSetIpPortMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn03.CctSetRunningParamMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn04.CctContMeterCloseMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn04.CctContMeterOpenMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn04.CctContValveCloseMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn04.CctContValveOpenMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0a.CctQueryHeartbeatMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0a.CctQueryInnerTableFileMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0a.CctQueryIpPortMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0a.CctQueryRunningParamMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0b.CctRtMeterDataMessage;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.*;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 设置命令
 *
 * @author: cc
 * @date: 2024/6/21 11:03
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum CctFeatCodecEnums implements FeatCodec {
    /**
     * ===================================      查询命令      =======================================
     * 查询集采器心跳周期
     */
    query_cct_heartbeat(CctAfnQueryCommRwEnums.query_cct_heartbeat, new CctQueryHeartbeatMessage()),
    /**
     * 查询集采器IP和端口
     */
    query_cct_ip_port(CctAfnQueryCommRwEnums.query_cct_ip_port, new CctQueryIpPortMessage()),
    /**
     * 查询计量表运行参数
     */
    query_running_param(CctAfnQueryCommRwEnums.query_running_param, new CctQueryRunningParamMessage()),
    /**
     * F13：查询集采器内表档案
     */
    query_meter_profile_cct(CctAfnQueryCommRwEnums.query_meter_profile_cct, new CctQueryInnerTableFileMessage()),


    /**
     * ===================================      请求1类数据(实时数据)命令      =======================================
     * 请求计量表当前数据
     */
    rt_meter_data(CctAfnRealtimeRwEnums.rt_meter_data, new CctRtMeterDataMessage()),

    /**
     * ===================================      请求2类数据(历史数据)命令  0c    =======================================
     * F1日零点冻结数据
     */
    ht_0_day_frozen(CctAfnHistoryRwEnums.ht_0_day_frozen, new CctRt0DayFrozenMessage()),
    /**
     * F2：抄表日冻结数据
     */
    ht_read_day_frozen(CctAfnHistoryRwEnums.ht_read_day_frozen, new CctRtReadDayFrozenMessage()),
    /**
     * F3：请求节点设备日冻结数据
     */
    ht_node_day_frozen(CctAfnHistoryRwEnums.ht_node_day_frozen, new CctRtNodeDayFrozenMessage()),
    /**
     * F4：请求节点设备月冻结数据
     */
    ht_node_month_frozen(CctAfnHistoryRwEnums.ht_node_month_frozen, new CctRtNodeMonthFrozenMessage()),
    /**
     * F5：读计量表内月冻结数据
     */
    ht_meter_month_frozen(CctAfnHistoryRwEnums.ht_meter_month_frozen, new CctRtMeterMonthFrozenMessage()),
    /**
     * F6：读集采器内月冻结数据
     */
    ht_cct_month_frozen(CctAfnHistoryRwEnums.ht_cct_month_frozen, new CctRtConcentratorMeterMonthFrozenMessage()),


    /**
     * ===================================      设置命令      =======================================
     * 设置集采器心跳周期
     */
    set_cct_heartbeat(CctAfnSetCommWriteEnums.set_cct_heartbeat, new CctSetHeartbeatMessage()),
    /**
     * 设置集采器IP和端口
     */
    set_cct_ip_port(CctAfnSetCommWriteEnums.set_cct_ip_port, new CctSetIpPortMessage()),
    /**
     * 配置计量表运行参数
     */
    set_running_param(CctAfnSetCommWriteEnums.set_running_param, new CctSetRunningParamMessage()),
    /**
     * 向集采器下载表档案
     */
    download_meter_profile_cct(CctAfnSetCommWriteEnums.download_meter_profile_cct, new CctDownloadMeterProfileMessage()),

    /**
     * ===================================      控制命令      =======================================
     * 遥控关阀（跳闸门）
     */
    cont_valve_close(CctAfnContCommWriteEnums.cont_valve_close, new CctContValveCloseMessage()),
    /**
     * 遥控开阀（合闸）
     */
    cont_valve_open(CctAfnContCommWriteEnums.cont_valve_open, new CctContValveOpenMessage()),
    /**
     * 遥控计量表关阀
     */
    cont_meter_close(CctAfnContCommWriteEnums.cont_meter_close, new CctContMeterCloseMessage()),
    /**
     * 遥控计量表开阀
     */
    cont_meter_open(CctAfnContCommWriteEnums.cont_meter_open, new CctContMeterOpenMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
