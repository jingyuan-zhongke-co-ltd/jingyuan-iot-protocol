package com.jingyuan.iot.protocol.tcp.runda.codec;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 心跳帧：用于维持链路，和实时更新电脑板工作状态，默认3分钟一次心跳。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaKeepaliveMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 设备状态
        properties.put("wpStatus", String.valueOf(buf.readByte()));
        // 屏幕状态
        properties.put("screenStatus", String.valueOf(buf.readByte()));
        // 工作模式
        properties.put("workMode", String.valueOf(buf.readByte()));
        // 剩余流量
        byte[] flowRemaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("flowRemaining", BytesUtils.binary(10, flowRemaining));
        // 剩余天数
        byte[] daysRemaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("daysRemaining", BytesUtils.binary(10, daysRemaining));
        // 已用流量
        byte[] flowUsed = ByteBufUtils.readBytes(buf, 2);
        properties.put("flowUsed", BytesUtils.binary(10, flowUsed));
        // 已用天数
        byte[] daysUsed = ByteBufUtils.readBytes(buf, 2);
        properties.put("daysUsed", BytesUtils.binary(10, daysUsed));

        // 净水TDS
        byte[] purTds = ByteBufUtils.readBytes(buf, 2);
        properties.put("purTds", BytesUtils.bytesToHex(purTds));
        // 原水TDS
        byte[] rawTds = ByteBufUtils.readBytes(buf, 2);
        properties.put("rawTds", BytesUtils.bytesToHex(rawTds));

        // 第一级滤芯剩余值
        byte[] level1Remaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("level1Remaining", BytesUtils.binary(10, level1Remaining));
        // 第二级滤芯剩余值
        byte[] level2Remaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("level2Remaining", BytesUtils.binary(10, level2Remaining));
        // 第三级滤芯剩余值
        byte[] level3Remaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("level3Remaining", BytesUtils.binary(10, level3Remaining));
        // 第四级滤芯剩余值
        byte[] level4Remaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("level4Remaining", BytesUtils.binary(10, level4Remaining));
        // 第五级滤芯剩余值
        byte[] level5Remaining = ByteBufUtils.readBytes(buf, 2);
        properties.put("level5Remaining", BytesUtils.binary(10, level5Remaining));

        // 各级滤芯最大值读取
        eachLevelMaxRead(buf, properties);

        // 信号强度值
        byte[] signalValue = ByteBufUtils.readBytes(buf, 1);
        properties.put("signalValue", BytesUtils.binary(10, signalValue));

        // LAC值
        byte[] lac = ByteBufUtils.readBytes(buf, 2);
        properties.put("lac", BytesUtils.bytesToHex(lac));
        // CID值
        byte[] cid = ByteBufUtils.readBytes(buf, 2);
        properties.put("cid", BytesUtils.bytesToHex(cid));
    }


    /**
     * 各级滤芯最大值读取
     *
     * @param buf        ByteBuf
     * @param properties 属性 Map
     */
    public static void eachLevelMaxRead(ByteBuf buf, Map<String, Object> properties) {
        // 第一级滤芯最大值
        byte[] level1Max = ByteBufUtils.readBytes(buf, 2);
        properties.put("level1Max", BytesUtils.binary(10, level1Max));
        // 第二级滤芯最大值
        byte[] level2Max = ByteBufUtils.readBytes(buf, 2);
        properties.put("level2Max", BytesUtils.binary(10, level2Max));
        // 第三级滤芯最大值
        byte[] level3Max = ByteBufUtils.readBytes(buf, 2);
        properties.put("level3Max", BytesUtils.binary(10, level3Max));
        // 第四级滤芯最大值
        byte[] level4Max = ByteBufUtils.readBytes(buf, 2);
        properties.put("level4Max", BytesUtils.binary(10, level4Max));
        // 第五级滤芯最大值
        byte[] level5Max = ByteBufUtils.readBytes(buf, 2);
        properties.put("level5Max", BytesUtils.binary(10, level5Max));
    }
}
