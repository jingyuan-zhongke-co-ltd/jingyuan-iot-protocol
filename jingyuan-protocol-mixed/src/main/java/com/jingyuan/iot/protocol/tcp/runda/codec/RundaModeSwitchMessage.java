package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpModeSwitchBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 模式切换
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaModeSwitchMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 工作模式
        properties.put("workMode", String.valueOf(buf.readByte()));
        // 流量与时间转换系数
        byte[] convertFactor = ByteBufUtils.readBytes(buf, 2);
        properties.put("convertFactor", BytesUtils.binary(10, convertFactor));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x03);

        WpModeSwitchBo wpModeSwitchBo = BeanUtil.toBean(properties, WpModeSwitchBo.class);
        String workMode = wpModeSwitchBo.getWorkMode();
        buf.writeByte(BytesUtils.intToByte(Integer.parseInt(workMode)));
        Short convertFactor = wpModeSwitchBo.getConvertFactor();
        buf.writeBytes(BytesUtils.shortToBytes(convertFactor));
    }
}
