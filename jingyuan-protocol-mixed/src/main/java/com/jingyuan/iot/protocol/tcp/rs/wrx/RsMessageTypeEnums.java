package com.jingyuan.iot.protocol.tcp.rs.wrx;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.rs.RsFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.rs.holder.RsTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * 乌鲁木齐 NB 协议类型枚举
 *
 * @author: 李立强
 * @date: 2024/10/08 15:34
 * @Version: V1.0
 */
@Slf4j
public enum RsMessageTypeEnums {
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), RsReportPropertyMessage::new),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), RsWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), RsWritePropertyMessageReply::new),
    ;

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    private static final Map<String, RsMessageTypeEnums> MAPS = new HashMap<>(16);

    static {
        Arrays.stream(values()).forEach(item -> MAPS.put(item.messageType, item));
    }

    RsMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, RsMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (RsMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf) {
        DeviceMessage message = encodeHolder.getDeviceMessage();
        int messageId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getDeviceId()));
        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        message.messageId(Convert.toStr(messageId));
        RsMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);
        // 写入帧头 68
        buf.writeBytes(BytesUtils.hexToBytes("68"));
        // 插入 1 字节的占位符 lenH（表号后面总长度的高位）
        int lenHIndex = buf.writerIndex();
        buf.writeByte(0x00); // 插入占位符（1 字节）
        // 插入 1 字节的占位符 lenL（帧标志2后面长度的低位）
        int lenLIndex = buf.writerIndex();
        buf.writeByte(0x00); // 插入占位符（1 字节）
        // 写入帧头 68
        buf.writeBytes(BytesUtils.hexToBytes("68"));
        buf.writeBytes(BytesUtils.hexToBytes(encodeHolder.getFeature().getFeatCode()));
        buf.writeBytes(BytesUtils.hexToBytes(message.getDeviceId()));
        // 写入其他内容
        tcp.write(buf, encodeHolder);
        // 计算 lenH（表号后面的数据长度，包括固定部分的 10 个字节）
        int dataLength = buf.writerIndex() - 14;
        int len = 10 + dataLength;
        // 将 len 拆分成两个字节
        int lenH = (len >> 8) & 0xFF;
        int lenL = len & 0xFF;
        // 将 lenH 和 lenL 分别放入 buf 中
        buf.setByte(lenHIndex, lenH);
        buf.setByte(lenLIndex, lenL);
        // 校验和
        String bytesStr = ByteBufUtils.getBytesToHex(buf, 4, buf.writerIndex() - 4); // 修正提取范围
        byte[] bytes = BytesUtils.hexToBytes(bytesStr);
        int calculatedCheckSum = 0;
        for (byte b : bytes) {
            calculatedCheckSum += b & 0xFF; // 确保无符号加法
        }
        calculatedCheckSum %= 256; // 保持校验和在 1 字节范围内
        buf.writeByte(calculatedCheckSum & 0xFF); // 写入低 1 字节校验和
        buf.writeBytes(BytesUtils.hexToBytes("16"));
        MsgIdCacheUtil.cacheRsReplyMsgId(message.getDeviceId(), encodeHolder.getFeature().getFeatCode(), messageId);
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static RsTcpDecodeHolder read(ByteBuf buf) {
        RsTcpDecodeHolder decodeHolder = RsTcpDecodeHolder.of();
        // 原始数据帧
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        String deviceId = ByteBufUtils.getBytesToHex(buf, 6, 8);
        decodeHolder.setDeviceId(deviceId);
        // 功能码
        RsFeatEnums feature = RsFeatEnums.getByFeatCode(ByteBufUtils.getBytesToHex(buf, 4, 2).toUpperCase());
        decodeHolder.setFeature(feature);
        // 指令校验
        feature.getCheckResult(decodeHolder.getRawDataHex());
        // 设备消息日志添加功能信息
        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        feature.setMessageHeaders(deviceMessage);
        decodeHolder.setDeviceMessage(deviceMessage);
        decodeHolder.setVendor(JoyoIotVendor.JOYO_RS);
        return decodeHolder;
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf,
                             TcpDecodeHolder decodeHolder,
                             BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        IotMessageType decodeType = feature.getDecodeType();
        Assert.notNull(decodeType, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        RsMessageTypeEnums type = MAPS.get(decodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        // 跳过表号前的内容
        buf.readBytes(6);
        // 创建消息对象
        TcpMessageAdapter<? extends DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        Integer replyMsgId = MsgIdCacheUtil.getRsReplyMsgId(decodeHolder.getDeviceId(), feature.getFeatCode());
        return handler.apply(message, replyMsgId);
    }
}
