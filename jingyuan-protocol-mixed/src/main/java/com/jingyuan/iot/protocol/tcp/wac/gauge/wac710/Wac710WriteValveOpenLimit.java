package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteValveOpenLimitBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写阀门开度上下限
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteValveOpenLimit implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteValveOpenLimitBo limitBo = BeanUtil.toBean(properties, Wac710WriteValveOpenLimitBo.class);
        // 阀门开度上限
        writeOpening(buf, limitBo.getOpenLimitUpper());
        // 阀门开度下限
        writeOpening(buf, limitBo.getOpenLimitLower());
    }


    public static void writeOpening(ByteBuf buf, Short opening) {
        if (opening == 100) {
            buf.writeBytes(new byte[]{0x00, 0x10});
        } else {
            buf.writeBytes(BCD.strToBcd(Convert.toStr(opening)));
            buf.writeByte(0);
        }
    }
}
