package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 终端日历时钟
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpTerminalClockMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        String time = time6ByteRead(buf);
        properties.put("calendarClock", DateUtils.parseLocalDateTime(time));//日历时钟

    }

    private String time6ByteRead(ByteBuf buf) {
        byte[] moduleTime = ByteBufUtils.readBytes(buf, 5);
        BytesUtils.bytesReverse(moduleTime);
        String output = BytesUtils.bcdToStr(moduleTime);

        return String.format("%s-%s-%s %s:%s",
                "20" + output.substring(0, 2), // 年
                output.substring(2, 4), // 月
                output.substring(4, 6), // 日
                output.substring(6, 8), // 时
                output.substring(8, 10));
    }

}
