package com.jingyuan.iot.protocol.tcp.hlm.holder;

import com.jingyuan.common.enums.JingyuanBaseEnum;
import com.jingyuan.common.protocol.enums.hlm.HlmAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmPrmEnums;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息编码包装类
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
public class HlmTcpEncodeHolder extends TcpEncodeHolder {

    private int l;

    /**
     * 启动表示位
     */
    private HlmPrmEnums prm;
    private JingyuanBaseEnum<Integer> prmFeature;
    /**
     * 协议id位
     */
    private JingyuanBaseEnum<String> protocolCode;

    /**
     * 行政区划码
     */
    private String adcode;
    private String deviceId;

    /**
     * 应用层功能码（AFN）
     */
    private HlmAfnFeatureEnums afn;
    private String afnCode;
}
