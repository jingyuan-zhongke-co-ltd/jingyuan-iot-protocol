package com.jingyuan.iot.protocol.tcp.wac.gauge;

import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.wac.adjust.WacAdjustFeatEnums;
import com.jingyuan.common.protocol.enums.wac.conf.WacConfFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.stream.StreamUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import reactor.core.Disposable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能与编解码处理类映射
 *
 * @author: cc
 * @date: 2024/9/23 11:18
 * @Version: V1.0
 */
public class WacFeatCodecs {

    private static final Map<JoyoIotVendor, Map<IotFeat, TcpMessageCodec>> GAUGES_MAP = new ConcurrentHashMap<>();
    /**
     * 各仪表支持的写目标调节值子功能
     */
    private static final Map<JoyoIotVendor, List<WacAdjustFeatEnums>> ADJUST_SUPPORT_FEATS = new HashMap<>();


    /**
     * 注册编解码器
     *
     * @param featCodecs 功能编解码器
     * @return reactor.core.Disposable
     */
    public static Disposable register(JoyoIotVendor gaugeType, Collection<FeatCodec> featCodecs) {
        GAUGES_MAP.put(gaugeType, StreamUtils.toMap(featCodecs, FeatCodec::getFeat, FeatCodec::getCodec));
        return () -> GAUGES_MAP.remove(gaugeType);
    }


    public static Map<JoyoIotVendor, Map<IotFeat, TcpMessageCodec>> get() {
        return GAUGES_MAP;
    }

    public static TcpMessageCodec lookup(JoyoIotVendor vendor, IotFeat feat) {
        return Optional.ofNullable(GAUGES_MAP.get(vendor))
                       .map(map -> map.get(feat))
                       .orElseThrow(() -> new IllegalArgumentException("不支持的仪表: " + vendor + "，功能类型: " + feat));
    }

    static {
        // 注册各类调节阀(WAC300, WAC710)功能
        WacFeatCodecs.register(JoyoIotVendor.JOYO_WAC710, Arrays.asList(Wac710CodecEnums.values()));
        WacFeatCodecs.register(JoyoIotVendor.JOYO_WAC300, Arrays.asList(Wac300CodecEnums.values()));

        // 注册各类调节阀(WAC300, WAC710)写目标调节值功能
        ADJUST_SUPPORT_FEATS.put(JoyoIotVendor.JOYO_WAC300, ListUtil.of(
            WacAdjustFeatEnums.REMOTE_CONT, WacAdjustFeatEnums.RETURN_CONT, WacAdjustFeatEnums.TEMP_DIFF_CONT, WacAdjustFeatEnums.TIME_SHARING_MODE));
        ADJUST_SUPPORT_FEATS.put(JoyoIotVendor.JOYO_WAC710, ListUtil.of(
            WacAdjustFeatEnums.REMOTE_CONT, WacAdjustFeatEnums.RETURN_CONT, WacAdjustFeatEnums.TEMP_DIFF_CONT, WacAdjustFeatEnums.TIME_SHARING_MODE));
    }

    public static void read(WacTcpDecodeHolder decodeHolder, ByteBuf buf, Map<String, Object> properties) {
        IotFeat feature = decodeHolder.getFeature();
        TcpMessageCodec messageCodec = lookup(decodeHolder.getVendor(), feature);
        messageCodec.read(buf, properties);
    }

    public static void write(WacTcpEncodeHolder encodeHolder, ByteBuf buf, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        // 数据标识(功能码)
        buf.writeBytes(BytesUtils.hexToBytes(feature.getFeatCode()));
        // 序号 SER
        buf.writeByte(encodeHolder.getSerId());
        // 数据
        TcpMessageCodec messageCodec;
        if (feature instanceof WacAdjustFeatEnums) {
            WacAdjustFeatEnums adjustFeat = (WacAdjustFeatEnums) feature;
            // 校验
            supportWriteTarget(encodeHolder.getVendor(), adjustFeat);
            // 适配写目标调节值
            messageCodec = Wac710CodecEnums.W_TARGET_ADJUST_VALUE.getCodec();
            buf.writeByte(Convert.toInt(((WacAdjustFeatEnums) feature).getCode()));
        } else if (feature instanceof WacConfFeatEnums) {
            // 适配写目标调节值
            messageCodec = Wac710CodecEnums.W_WAC_ALL_CONFIG.getCodec();
            properties.put("adjustmentMode",Convert.toInt(((WacConfFeatEnums) feature).getCode()));
        } else {
            messageCodec = lookup(encodeHolder.getVendor(), feature);
        }
        messageCodec.write(buf, properties);
    }


    /**
     * 校验仪表是否支持写目标调节值
     *
     * @param vendor 仪表（厂商）类型
     * @param feat   写目标调节值子功能
     */
    private static void supportWriteTarget(JoyoIotVendor vendor, WacAdjustFeatEnums feat) {
        List<WacAdjustFeatEnums> wacAdjustFeatEnums = ADJUST_SUPPORT_FEATS.get(vendor);
        Assert.notEmpty(wacAdjustFeatEnums, "不支持的仪表: " + vendor + "，功能类型: " + feat);
        Assert.isTrue(wacAdjustFeatEnums.contains(feat), "仪表: " + vendor + "不支持的功能类型: " + feat.getDesc());
    }
}
