package com.jingyuan.iot.protocol.tcp.wac.gauge.wac300;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.wac300.Wac300WriteProbeScreenConfParamBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写探头与屏幕配置参数(生产 AC/09)
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac300WriteProbeScreenConfParam implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac300WriteProbeScreenConfParamBo bean = BeanUtil.toBean(properties, Wac300WriteProbeScreenConfParamBo.class);
        buf.writeBytes(BytesUtils.hexToBytes(bean.getScreenConf()));
        buf.writeBytes(BytesUtils.hexToBytes(bean.getProbeConf()));
    }
}
