package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnConfirmRwEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnConfirmRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 确认∕否认
 *
 * @author: dr
 * @date: 2024-10-24
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnConfirmDenyMessage implements HlmAfnMessage {

    @Override
    public void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = String.valueOf(buf.readByte());
        HlmAfnConfirmRwEnums confirmEnum = HlmAfnConfirmRwEnums.getByFeatCode(fn);
        decodeHolder.setFeature(confirmEnum);
    }

    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnConfirmRwEnums;
        buf.writeByte(0b01111111);
    }
}
