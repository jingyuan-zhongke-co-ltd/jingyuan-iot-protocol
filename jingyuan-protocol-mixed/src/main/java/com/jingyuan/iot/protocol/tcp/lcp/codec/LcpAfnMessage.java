package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;

import java.util.Map;

/**
 * 应用层功能码基础消息接口
 *
 * @author: cc
 * @date: 2024/3/27 16:29
 * @Version: V1.0
 */
public interface LcpAfnMessage {

    default void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
    }

    default void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        throw new IllegalArgumentException("暂不支持的功能类型");
    }
}
