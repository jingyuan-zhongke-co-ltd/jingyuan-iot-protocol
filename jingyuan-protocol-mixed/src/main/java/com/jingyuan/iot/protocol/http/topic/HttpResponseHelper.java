package com.jingyuan.iot.protocol.http.topic;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.jingyuan.common.utils.json.JsonUtils;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetlinks.core.message.codec.http.SimpleHttpResponseMessage;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.io.Serializable;

/**
 * Http响应帮助类
 *
 * @author: cc
 * @date: 2024/9/12 16:43
 * @Version: V1.0
 */
public class HttpResponseHelper {
    public static SimpleHttpResponseMessage unauthorized(String message) {
        return instance("401", message);
    }

    public static SimpleHttpResponseMessage unauthorized() {
        return instance("401", "unauthorized");
    }

    public static SimpleHttpResponseMessage badRequest(String message) {
        return instance("400", message);
    }

    public static SimpleHttpResponseMessage notFound(String message) {
        return instance("404", message);
    }

    public static SimpleHttpResponseMessage ok(String message) {
        return instance("200", message);
    }

    public static SimpleHttpResponseMessage ok() {
        return instance("200", "success");
    }

    public static SimpleHttpResponseMessage instance(String code, String message) {
        return SimpleHttpResponseMessage
            .builder()
            .contentType(MediaType.APPLICATION_JSON)
            .body(JsonUtils.toJsonString(R.of(code, message)))
            .status(HttpStatus.OK.value())
            .build();
    }

    public static SimpleHttpResponseMessage instance(String message) {
        return SimpleHttpResponseMessage
            .builder()
            .contentType(MediaType.APPLICATION_JSON)
            .body(message)
            .status(HttpStatus.OK.value())
            .build();
    }

    @Data
    @NoArgsConstructor(staticName = "of")
    @AllArgsConstructor(staticName = "of")
    @JsonInclude(JsonInclude.Include.NON_NULL)
    private static class R implements Serializable {
        private static final long serialVersionUID = 1L;
        private String code;
        private String message;
    }
}
