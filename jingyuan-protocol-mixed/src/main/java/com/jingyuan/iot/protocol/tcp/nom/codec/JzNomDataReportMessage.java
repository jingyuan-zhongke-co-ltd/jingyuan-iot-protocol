package com.jingyuan.iot.protocol.tcp.nom.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.nom.JzNomEncryptFlagEnums;
import com.jingyuan.common.protocol.enums.nom.JzNomFeatTagEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;
import java.util.Objects;

/**
 * 京兆无磁-数据上报
 *
 * @author: cc
 * @date: 2024/9/23 10:30
 * @Version: V1.0
 */
@Slf4j
public class JzNomDataReportMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 加密标识
        byte encryptFlag = buf.readByte();
        JzNomEncryptFlagEnums encryptEnum = JzNomEncryptFlagEnums.getByCode(encryptFlag);

        // 数据域
        String dataDomains = ByteBufUtils.readBytesToHex(buf, buf.writerIndex() - buf.readerIndex() - 3);

        String decryptHex = decrypt(encryptEnum, dataDomains);
        properties.put("decryptHex", decryptHex);

        ByteBuf byteBuf = Unpooled.buffer().writeBytes(BytesUtils.hexToBytes(decryptHex));

        recursionDecode(byteBuf.readShortLE(), byteBuf, properties);
    }


    /**
     * 递归解析数据域
     *
     * @param buf        buf
     * @param properties 结果
     */
    public void recursionDecode(short dataLength, ByteBuf buf, Map<String, Object> properties) {
        if (dataLength <= 0) {
            return;
        }
        String tag = BytesUtils.bytesToHex(ByteBufUtils.readBytes(buf, 1));
        JzNomFeatTagEnums featTagEnum = JzNomFeatTagEnums.getByTag(tag);
        if (ObjUtil.isNull(featTagEnum)) {
            dataLength = 0;
            return;
        }

        dataLength -= BytesUtils.bytesToShort(ByteBufUtils.getBytes(buf, buf.readerIndex(), 2), ByteOrder.LITTLE_ENDIAN);
        FeatCodecs.read(featTagEnum, buf, properties);

        recursionDecode(dataLength, buf, properties);
    }


    public static String decrypt(JzNomEncryptFlagEnums encryptEnum, String dataDomains) {
        String decryptHex = encryptEnum.decrypt(dataDomains, false);
        String tag = BytesUtils.binary(10, BytesUtils.hexToBytes(StrUtil.subWithLength(decryptHex, 4, 2)));

        if (!StrUtil.isNumeric(tag) || Convert.toInt(tag) > 15 ||
            !Objects.equals(StrUtil.subWithLength(decryptHex, 48, 2), StrUtil.subWithLength(decryptHex, 62, 2))) {
            decryptHex = encryptEnum.decrypt(dataDomains, true);
            tag = BytesUtils.binary(10, BytesUtils.hexToBytes(StrUtil.subWithLength(decryptHex, 4, 2)));
            Assert.isFalse(!StrUtil.isNumeric(tag) || Convert.toInt(tag) > 15 ||
                               !Objects.equals(StrUtil.subWithLength(decryptHex, 48, 2), StrUtil.subWithLength(decryptHex, 62, 2)),
                           "数据域解析失败，解析前：【" + dataDomains + "】, 解析后：【" + decryptHex + "】");
        }
        return decryptHex;
    }
}
