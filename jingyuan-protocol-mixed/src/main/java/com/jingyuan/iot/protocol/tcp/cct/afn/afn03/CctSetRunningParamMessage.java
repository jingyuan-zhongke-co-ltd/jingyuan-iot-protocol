package com.jingyuan.iot.protocol.tcp.cct.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn03.CctSetRunningParamBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 配置计量表运行参数
 *
 * @author: cc
 * @date: 2024/6/27 9:46
 * @Version: V1.0
 */
@Slf4j
public class CctSetRunningParamMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctSetRunningParamBo runningParamBo = BeanUtil.toBean(properties, CctSetRunningParamBo.class);
        // 计量表类型
        buf.writeByte(runningParamBo.getMeterType());
        // 数据纠错标志
        buf.writeByte(runningParamBo.getCorrectionFlag());
        buf.writeByte(0x00);
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(runningParamBo.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
        // 采集器通信地址
        buf.writeBytes(new byte[]{runningParamBo.getCoolAddr().byteValue(), 0x00, 0x00, 0x00, 0x00, 0x00});
        // 运行标志
        buf.writeByte(runningParamBo.getRunFlag());
        // 数据项
        buf.writeByte(runningParamBo.getDataItem());
    }
}
