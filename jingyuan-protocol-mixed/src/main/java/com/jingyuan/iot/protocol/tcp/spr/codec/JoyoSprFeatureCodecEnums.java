package com.jingyuan.iot.protocol.tcp.spr.codec;

import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 京源分体阀控协议消息类型枚举
 * Split valve-regulated protocol
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum JoyoSprFeatureCodecEnums implements FeatCodec {
    /**
     * 数据上报
     */
    REPORT_DATA(JoyoSprFeatureEnums.REPORT_DATA, new JoyoSprDataReportMessage()),

    /**
     * 控阀
     */
    VALVE_STATE(JoyoSprFeatureEnums.VALVE_STATE, new JoyoSprValveStateMessage()),
    /**
     * 控阀应答
     */
    VALVE_STATE_REPLY(JoyoSprFeatureEnums.VALVE_STATE_REPLY, new JoyoSprValveStateMessageReply()),

    /**
     * 离线通知
     */
    OFFLINE(JoyoSprFeatureEnums.OFFLINE, new JoyoSprOfflineMessage()),

    /**
     * IP和端口设置
     */
    IP_PORT_CONFIG(JoyoSprFeatureEnums.IP_PORT_CONFIG, new JoyoSprIpPortConfigMessage()),
    /**
     * IP和端口设置应答
     */
    IP_PORT_CONFIG_REPLY(JoyoSprFeatureEnums.IP_PORT_CONFIG_REPLY, new JoyoSprIpPortConfigMessageReply()),

    /**
     * 上报周期设置与查询
     */
    REPORT_CYCLE(JoyoSprFeatureEnums.REPORT_CYCLE, new JoyoSprReportCycleMessage()),
    /**
     * 上报周期应答
     */
    REPORT_CYCLE_REPLY(JoyoSprFeatureEnums.REPORT_CYCLE_REPLY, new JoyoSprReportCycleMessageReply()),

    /**
     * 历史数据
     */
    HISTORY(JoyoSprFeatureEnums.HISTORY, new JoyoSprHistoryMessage()),
    /**
     * 历史数据应答
     */
    HISTORY_REPLY(JoyoSprFeatureEnums.HISTORY_REPLY, new JoyoSprHistoryMessageReply()),

    /**
     * 报警帧数据
     */
    ALARM_REPLY(JoyoSprFeatureEnums.ALARM_REPLY, new JoyoSprAlarmMessageReply()),

    /**
     * =================================== NB升级程序帧 ===================================
     */
    UPGRADE(JoyoSprFeatureEnums.UPGRADE, new JoyoSprUpgradeMessage()),
    /**
     * NB升级程序帧应答
     */
    UPGRADE_REPLY(JoyoSprFeatureEnums.UPGRADE_REPLY, new JoyoSprUpgradeMessageReply()),
    /**
     * NB升级请求升级包
     */
    UPGRADE_QUERY_REPLY(JoyoSprFeatureEnums.UPGRADE_QUERY_REPLY, new JoyoSprUpgradeQueryMessageReply()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
