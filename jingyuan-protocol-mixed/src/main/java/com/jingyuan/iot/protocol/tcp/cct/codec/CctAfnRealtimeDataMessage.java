package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnRealtimeRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 请求1类数据（实时数据）
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnRealtimeDataMessage implements CctAfnMessage {

    @Override
    public void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String afn = String.valueOf(buf.readByte());
        CctAfnRealtimeRwEnums realtimeRwEnum = CctAfnRealtimeRwEnums.getByFeatCode(afn);
        decodeHolder.setFeature(realtimeRwEnum);

        FeatCodecs.read(realtimeRwEnum, buf, properties);
    }

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnRealtimeRwEnums;
        CctAfnRealtimeRwEnums featureEnum = (CctAfnRealtimeRwEnums) feature;

        FeatCodecs.write(featureEnum, buf, properties);
    }
}
