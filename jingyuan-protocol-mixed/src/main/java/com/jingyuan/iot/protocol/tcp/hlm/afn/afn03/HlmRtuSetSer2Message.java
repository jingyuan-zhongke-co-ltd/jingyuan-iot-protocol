package com.jingyuan.iot.protocol.tcp.hlm.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.hlm.afn03.HlmRtuSetSer1Bo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils.replenishByte;

/**
 * 设置服务器2信息
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmRtuSetSer2Message implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        HlmRtuSetSer1Bo runningParamBo = BeanUtil.toBean(properties, HlmRtuSetSer1Bo.class);
//        01 			// 数据包标识。
        buf.writeByte(1);
//        31 38 30 2E 31 30 31 2E 31 34 37 2e 31 31 35 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//// 服务器IP，180.101.147.115。
        byte[] bytes1 = BytesUtils.hexToBytes(BytesUtils.acsStrToHex(runningParamBo.getIp()));
        bytes1 = replenishByte(bytes1, 32);
        buf.writeBytes(bytes1);
//        33 16 		// 服务器端口，5683。
        byte[] bytes3 = BytesUtils.shortToBytes(runningParamBo.getPort());
        bytes3 = replenishByte(bytes3, 2);
        buf.writeBytes(new byte[]{bytes3[1], bytes3[0]});
//        标识 Bin 0：不用 1：启用 1
        byte logotype = runningParamBo.getLogotype();
        buf.writeByte(logotype);
    }


}
