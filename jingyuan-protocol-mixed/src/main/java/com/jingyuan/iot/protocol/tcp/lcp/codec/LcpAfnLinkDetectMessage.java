package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnLinkDetectReadEnums;
import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnLinkDetectionReadEnums;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 链路接口检测（登录包，心跳包）
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnLinkDetectMessage implements LcpAfnMessage {

    @Override
    public void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = String.valueOf(buf.readByte());
        LcpAfnLinkDetectionReadEnums linkDetectUpEnum = LcpAfnLinkDetectionReadEnums.getByFeatCode(fn);
        decodeHolder.setFeature(linkDetectUpEnum);

        FeatCodecs.read(linkDetectUpEnum, buf, properties);
    }
}
