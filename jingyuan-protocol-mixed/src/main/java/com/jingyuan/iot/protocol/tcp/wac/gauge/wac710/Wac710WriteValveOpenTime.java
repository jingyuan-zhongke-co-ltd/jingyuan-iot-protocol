package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteValveOpenTimeBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 写累计开阀时间
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteValveOpenTime implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteValveOpenTimeBo bean = BeanUtil.toBean(properties, Wac710WriteValveOpenTimeBo.class);
        buf.writeBytes(BytesUtils.intToBytes(bean.getCumulateValveOpenTime(), ByteOrder.LITTLE_ENDIAN));
    }
}
