package com.jingyuan.iot.protocol.tcp.hlm.afn;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnControlRwEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnQueryCommRwEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnSetCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn03.*;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn05.*;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c.HlmContDormancyMessage;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c.HlmQurTrafficReportMessage;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c.HlmSetTrafficReportMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 设置命令
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum HlmFeatCodecEnums implements FeatCodec {
    /**
     * ===================================      查询命令      =======================================
     * 查询设备地址
     */
    hlm_query_device_address(HlmAfnQueryCommRwEnums.hlm_query_device_address, new HlmQueryDeviceAddress()),
    /**
     * 查询设备信号值
     */
    hlm_query_device_signal(HlmAfnQueryCommRwEnums.hlm_query_device_signal, new HlmQueryDeviceSignal()),
    /**
     * 查询ICCID
     */
    hlm_query_iccid(HlmAfnQueryCommRwEnums.hlm_query_iccid, new HlmQueryIccid()),
    /**
     * 查询软件版本
     */
    hlm_query_software_version(HlmAfnQueryCommRwEnums.hlm_query_software_version, new HlmQuerySoftwareVersion()),
    /**
     * 查询电池电压
     */
    hlm_query_battery_voltage(HlmAfnQueryCommRwEnums.hlm_query_battery_voltage, new HlmQueryBatteryVoltage()),
    /**
     * 查询IMEI
     */
    hlm_query_imei(HlmAfnQueryCommRwEnums.hlm_query_imei, new HlmQueryImei()),
    /**
     * 查询配置下发参数
     */
    hlm_query_configuration_params(HlmAfnQueryCommRwEnums.hlm_query_configuration_params, new HlmQueryConfigurationParams()),

    /**
     * ===================================      设置命令      =======================================
     * 配置计量表运行参数
     */
    set_running_param(HlmAfnSetCommWriteEnums.hlm_set_param, new HlmSetRunningParamMessage()),
    /**
     * 设置设备地址和抄表协议
     */
    hlm_set_add(HlmAfnSetCommWriteEnums.hlm_set_add, new HlmSetAddProtocolMessage()),
    /**
     * 设置服务器1信息
     */
    hlm_rtu_set_ser1(HlmAfnSetCommWriteEnums.hlm_rtu_set_ser1, new HlmRtuSetSer1Message()),
    /**
     * 设置服务器2信息
     */
    hlm_rtu_set_ser2(HlmAfnSetCommWriteEnums.hlm_rtu_set_ser2, new HlmRtuSetSer2Message()),
    /**
     * 采集周期
     */
    hlm_rtu_set_acquisition_cycle(HlmAfnSetCommWriteEnums.hlm_rtu_set_acquisition_cycle, new HlmRtuSetAcquisitionCycleMessage()),


    /**
     * ===================================      控制命令      =======================================
     * 遥控关阀（跳闸门）
     */

    cont_valve_close(HlmAfnControlRwEnums.dormancy, new HlmContDormancyMessage()),
    /**
     * 设置开启流量异常上报
     */
    cont_valve_open(HlmAfnControlRwEnums.SetTrafficReport, new HlmSetTrafficReportMessage()),
    /**
     * 查询关闭流量异常上报
     */
    cont_meter_close(HlmAfnControlRwEnums.QurTrafficReport, new HlmQurTrafficReportMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
