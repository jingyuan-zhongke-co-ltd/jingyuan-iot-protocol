package com.jingyuan.iot.protocol.tcp.cct.wrx;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cct.codec.CctAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessage;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class CctWritePropertyMessage implements TcpMessageAdapter<WritePropertyMessage> {

    private WritePropertyMessage message;


    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
        CctAfnFeatCodecEnums.write(buf, (CctTcpEncodeHolder) encodeHolder, message.getProperties());
    }


    @Override
    public void setMessage(WritePropertyMessage message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessage getMessage() {
        return message;
    }
}
