package com.jingyuan.iot.protocol.tcp.spr.wrx;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import com.jingyuan.iot.protocol.tcp.spr.SprChildVendorEnums;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 京源消息类型枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
public enum SprMessageTypeEnums {

    //0x01
    online(IotMessageType.ONLINE.getCode(), null),

    //0x02
    ack(IotMessageType.ACKNOWLEDGE.getCode(), null),
    //0x03
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), SprReportPropertyMessage::new),

    //0x04
    readProperty(IotMessageType.READ_PROPERTY.getCode(), null),

    //0x05
    readPropertyReply(IotMessageType.READ_PROPERTY_REPLY.getCode(), null),

    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), SprWritePropertyMessage::new),

    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), SprWritePropertyMessageReply::new),

    function(IotMessageType.INVOKE_FUNCTION.getCode(), null),

    functionReply(IotMessageType.INVOKE_FUNCTION_REPLY.getCode(), null),

    event(IotMessageType.EVENT.getCode(), null);

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    SprMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, SprMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (SprMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(SprTcpEncodeHolder encodeHolder, ByteBuf buf) {
        DeviceMessage message = encodeHolder.getDeviceMessage();
        SprChildVendorEnums vendorEnum = encodeHolder.getChildVendorEnum();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        SprMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);
        // 前置命令帧填充
        doWritePrefix(buf, vendorEnum, message.getDeviceId());
        // 下发功能指令填充
        tcp.write(buf, encodeHolder);
        // 后置置命令帧填充
        doWriteSuffix(buf, vendorEnum);
        // 缓存消息ID，供设备回复时匹配，通过 deviceId + messageType
        MsgIdCacheUtil.cacheSprReplyMsgId(message.getDeviceId(), vendorEnum
            .getFeatureType()
            .apply(buf), msgId);
    }


    /**
     * 向 ByteBuf 写入设备ID
     *
     * @param buf         buf
     * @param deviceId    设备ID
     * @param childVendor 厂商信息枚举
     */
    private static void doWritePrefix(ByteBuf buf, SprChildVendorEnums childVendor, String deviceId) {
        buf.writeBytes(childVendor.getFrameId());
        String prefix = StrUtil.sub(deviceId, 0, 2);
        String suffix = StrUtil.sub(deviceId, 2, deviceId.length() + 1);
        String s;
        if (deviceId.length() == 12) {
            s = prefix + "0000" + suffix;
        } else {
            s = prefix + "00" + suffix;
        }
        byte[] nbMeterAddrBytes = BytesUtils.hexToBytes(s);
        buf.writeBytes(nbMeterAddrBytes);
    }

    /**
     * 向 ByteBuf 写入校验信息
     *
     * @param buf         写出数据
     * @param childVendor 厂商信息枚举
     */
    private static void doWriteSuffix(ByteBuf buf, SprChildVendorEnums childVendor) {
        int computeSum = ChecksumUtils.computeSumSingle(buf, 1, buf.writerIndex());
        buf.writeByte((byte) computeSum);
        buf.writeBytes(childVendor.getTerminator());
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static SprTcpDecodeHolder read(ByteBuf buf) {
        // 厂商类型
        SprTcpDecodeHolder decodeHolder = SprChildVendorEnums.baseDecode(buf);
        IotFeat feature = decodeHolder.getFeature();

        // 指令校验
//        feature.getCheckResult(decodeHolder.getRawDataHex());

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);
        MessageHelper.addHeadVendor(deviceMessage, decodeHolder.getChildVendorEnum().getCode());

        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf,
                             SprTcpDecodeHolder decodeHolder,
                             BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        IotMessageType decodeType = feature.getDecodeType();
        Assert.notNull(decodeType, "暂不支持的功能类型：" + feature);
        SprMessageTypeEnums type = maps.get(decodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature);

        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        message.thingId(DeviceThingType.device, decodeHolder.getDeviceId());

        Integer replyMsgId = MsgIdCacheUtil.getSprReplyMsgId(message.getDeviceId(), feature.getFeatCode());
        return handler.apply(message, replyMsgId);
    }
}
