package com.jingyuan.iot.protocol.tcp.cts.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.date.DateTime;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.extern.slf4j.Slf4j;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Map;

/**
 * NB数据上传
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CtsDataUploadsMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        ByteBufUtils.readBytes(buf, 1);
        // 表数据
        byte[] tableData = ByteBufUtils.readBytes(buf, 23);
        tableDataRead(tableData, properties);
        // 49字节
        byte[] bytes49 = ByteBufUtils.readBytes(buf, 49);
        bytes49Read(bytes49, properties);
        // 模组版本号
        byte[] moduleVersion = ByteBufUtils.readBytes(buf, 9);
        properties.put("modVersion", BytesUtils.bytesToAscStr(moduleVersion));
        // 版本号
        byte[] version = ByteBufUtils.readBytes(buf, 9);
        properties.put("programVersion", BytesUtils.bytesToAscStr(version));
        // 模块状态
        byte[] moduleStatus = ByteBufUtils.readBytes(buf, 2);
        moduleStatusRead(moduleStatus, properties);
        // 模块电压
        byte[] moduleVoltage = ByteBufUtils.readBytes(buf, 2);
        // 将两个字节合并为一个无符号整数
        int voltageRaw = ((moduleVoltage[0] & 0xFF) << 8) | (moduleVoltage[1] & 0xFF);
        // 转换为实际电池电压
        double batteryVoltage = voltageRaw / 100.0;
        properties.put("moduleVoltage", batteryVoltage);
        // CIMI
        byte[] cimi = ByteBufUtils.readBytes(buf, 15);
        properties.put("cimi", BytesUtils.bytesToAscStr(cimi));
        // IMEI
        byte[] imei = ByteBufUtils.readBytes(buf, 15);
        properties.put("imei", BytesUtils.bytesToAscStr(imei));
        // CSQ
        csqRead(buf, properties);

    }

    private void csqRead(ByteBuf buf, Map<String, Object> properties) {
        // 读取 CSQ 值
        String csq = BytesUtils.byteToHex(buf.readByte());
        int signalStrength = 0;
        if ("99".equals(csq)) {
            signalStrength = -1;
        } else {
            int csqValue = Integer.parseInt(csq, 16);

            if (csqValue >= 2 && csqValue <= 30) {
                // 线性映射公式：信号强度 = -113 + (CSQ * 2)
                signalStrength = -113 + (csqValue * 2);
            } else if (csqValue == 31) {
                signalStrength = -51;
            } else if (csqValue == 0) {
                signalStrength = -113;
            } else if (csqValue == 1) {
                signalStrength = -111;
            }
        }
        properties.put("csq", signalStrength);
    }

    private void moduleStatusRead(byte[] moduleStatus, Map<String, Object> properties) {
        // 读取高字节和低字节，将 byte 转换为无符号整数
        int highByte = moduleStatus[0] & 0xFF;
        int lowByte = moduleStatus[1] & 0xFF;

        // 解析高字节
        // 攻击状态(模块)
        properties.put("moldAttackStatus", (highByte & 0b10000000) >> 7);
        // 电池状态(模块)
        properties.put("moldBatteryStatus", (highByte & 0b01000000) >> 6);
        // 霍尔故障(模块)
        properties.put("moldHallFault", (highByte & 0b00100000) >> 5);
        // 保留(模块)
        // properties.put("moldReserved1", (highByte & 0b00010000) >> 4);
        // 保留(模块)
        // properties.put("moldReserved2", (highByte & 0b00001000) >> 3);
        // 阀门故障(模块)
        properties.put("moldValveFault", (highByte & 0b00000100) >> 2);
        // 保留(模块)
        // properties.put("moldReserved3", (highByte & 0b00000010) >> 1);
        // 阀门状态(模块)
        properties.put("moldValveStatus", (highByte & 0b00000001));

        // 解析低字节
        // 上传地址(模块)
        properties.put("uploadPlatform", (lowByte & 0b00000010) >> 1);
        // 其他位保留(模块)
        // properties.put("moldReserved4", (lowByte & 0b00000001));
    }

    private void bytes49Read(byte[] bytes49, Map<String, Object> properties) {
        ByteBuf buf = Unpooled.wrappedBuffer(bytes49);
        // 6字节时间数据
        timeRead(properties, buf);
        // ICCID 集成电路卡标识码
        iccidRead(properties, buf);
        // 采样错误代码
        byte[] samplingErrorCode = ByteBufUtils.readBytes(buf, 1);
        properties.put("samplingErrorCode", BytesUtils.bytesToHex(samplingErrorCode));
        // NB故障代码
        byte[] nbFaultCode = ByteBufUtils.readBytes(buf, 1);
        properties.put("nbFaultCode", BytesUtils.bytesToHex(nbFaultCode));
        // NB上传周期
        byte[] nbUploadPeriod = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(nbUploadPeriod);
        properties.put("reportCycle", Integer.parseInt(BytesUtils.binary(10, nbUploadPeriod)) / 60 + "");
    }

    private void iccidRead(Map<String, Object> properties, ByteBuf buf) {
        byte[] iccid = ByteBufUtils.readBytes(buf, 20);
        String asciiString = BytesUtils.bytesToAscStr(iccid);
        properties.put("iccid", asciiString);
    }

    private void timeRead(Map<String, Object> properties, ByteBuf buf) {
        String input = ByteBufUtils.readBytesToHex(buf, 6);
        StringBuilder reversed = new StringBuilder();

        // 每两位逆序
        for (int i = input.length(); i > 0; i -= 2) {
            reversed.append(input, i - 2, i);
        }

        // 格式化成“21.08.31 13:33:45”
        String output = reversed.toString();
        String formatted = String.format("%s-%s-%s %s:%s:%s",
                                         "20" + output.substring(0, 2),
                                         output.substring(2, 4),
                                         output.substring(4, 6),
                                         output.substring(6, 8),
                                         output.substring(8, 10),
                                         output.substring(10, 12));
        DateTime dateTime = DateUtils.parseDate(formatted);
        Date date = DateUtils.addDays(new Date(), -10);
        if (dateTime.before(date)) {
            properties.put("currentTime", LocalDateTime.now());
        } else {
            properties.put("currentTime", DateUtils.parseLocalDateTime(formatted));
        }
    }

    private void tableDataRead(byte[] bytesTableData, Map<String, Object> properties) {
        ByteBuf buf = Unpooled.wrappedBuffer(bytesTableData);
        ByteBufUtils.readBytes(buf, 1);
        // 表类型
        String nbType = BytesUtils.bytesToHex(ByteBufUtils.readBytes(buf, 1));
        // properties.put("meterType", nbType.equals("10") ? "水表（nb 无阀）" : "未知");
        properties.put("meterType", nbType.equals("10") ? "水表(nb)" : "未知");
        // 目标表地址
        ByteBufUtils.readBytes(buf, 12);
        // 表数据
        byte[] tableDataHighLow = ByteBufUtils.readBytes(buf, 3);
        byte[] tableDataSmallByte = ByteBufUtils.readBytes(buf, 1);
        byte[] unit = ByteBufUtils.readBytes(buf, 1);
        properties.put("reading", BytesUtils.bytesToHex(tableDataHighLow).replaceFirst("^0+(?!$)", "")
            + "." + BytesUtils.bytesToHex(tableDataSmallByte)
            + (BytesUtils.bytesToHex(unit).equals("2c") ? "" : "未知单位"));
        // 表状态
        byte[] tableStatus = ByteBufUtils.readBytes(buf, 1);
        tableStatusRead(tableStatus, properties);
        // 电池电压
        byte[] batteryVoltage = ByteBufUtils.readBytes(buf, 1);
        double doubleValue = Double.parseDouble(BytesUtils.binary(10, batteryVoltage)) + 200;
        double transformedValue = doubleValue / 100.0;
        properties.put("voltage", transformedValue);

    }

    private void tableStatusRead(byte[] tableStatus, Map<String, Object> properties) {
        // 读取 statusValue，将 byte 转换为无符号整数
        int statusValue = tableStatus[0] & 0xFF;

        // 按位与操作每一位，并将结果存储到 properties 中
        // 攻击状态: attackStatus
        properties.put("attackState", (statusValue & 0b00000001) >> 0);
        // 电池状态: batteryStatus
        properties.put("voltageState", (statusValue & 0b00000010) >> 1);
        // 霍尔故障: hallFault
        properties.put("hallFailure", (statusValue & 0b00000100) >> 2);
        // 保留: reserved1
        // properties.put("nbReserved1", (statusValue & 0b00001000) >> 3);
        // 掉电: powerLoss
        properties.put("powerLoss", (statusValue & 0b00010000) >> 4);
        // 阀门故障: valveFault
        properties.put("valveFailure", (statusValue & 0b00100000) >> 5);
        // 保留: reserved2
        // properties.put("nbReserved2", (statusValue & 0b01000000) >> 6);
        // 阀门状态: valveStatus
        properties.put("valveState", (statusValue & 0b10000000) >> 7);

    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x04);
        Object wpId = properties.get(IotConstants.RUNDA_WP_ID);
        if (ObjectUtil.isNull(wpId)) {
            buf.writeBytes(new byte[]{0x00, 0x00, 0x00, 0x00});
        } else {
            String wpIdHex = StrUtil.fillBefore(HexUtils.toHex(Convert.toInt(wpId)), '0', 8);
            buf.writeBytes(BytesUtils.hexToBytes(wpIdHex));
        }
    }
}
