package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 读modbus本机串口属性
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadModbusSerialPort implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 2);
        properties.put("serialBaudRate", BytesUtils.bytesToShort(bytes, ByteOrder.LITTLE_ENDIAN));
        properties.put("serialDataBits",buf.readByte());
        properties.put("serialCheckBits",buf.readByte());
        properties.put("serialStopBits",buf.readByte());

    }
}
