package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnContCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控制命令
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnContCommMessage implements CctAfnMessage {

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnContCommWriteEnums;

        FeatCodecs.write(feature, buf, properties);
    }
}
