package com.jingyuan.iot.protocol.tcp.runda.codec;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 关机：用于关闭，并关闭制水功能。
 * 必须绑定套餐后操作，否则无响应。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaShutdownMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 设备状态
        properties.put("wpStatus", String.valueOf(buf.readByte()));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x01);
        buf.writeByte(0x00);
    }
}
