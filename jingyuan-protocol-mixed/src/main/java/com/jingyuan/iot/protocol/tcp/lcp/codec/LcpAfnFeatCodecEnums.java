package com.jingyuan.iot.protocol.tcp.lcp.codec;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.lcp.LcpAfnFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用层功能码指令处理
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
@AllArgsConstructor
public enum LcpAfnFeatCodecEnums {
    /**
     * 确认∕否认
     */
    confirm(LcpAfnFeatureEnums.confirm, new LcpAfnConfirmDenyMessage()),
    link_detect(LcpAfnFeatureEnums.link_detect, new LcpAfnLinkDetectMessage()),
    set_comm(LcpAfnFeatureEnums.set_comm, new LcpAfnSetCommMessage()),
    cont_comm(LcpAfnFeatureEnums.cont_comm, new LcpAfnContCommMessage()),
    query_comm(LcpAfnFeatureEnums.query_comm, new LcpAfnQueryCommMessage()),
    realtime_data(LcpAfnFeatureEnums.realtime_data, new LcpAfnRealtimeDataMessage()),
    history_data(LcpAfnFeatureEnums.history_data, new LcpAfnHistoryDataMessage()),
    data_uploads(LcpAfnFeatureEnums.data_uploads, new LcpAfnDataUploadsMessage()),
    ;

    private final LcpAfnFeatureEnums afnFeat;

    private final LcpAfnMessage codec;

    private static final Map<String, LcpAfnFeatCodecEnums> VALUES = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(val -> VALUES.put(val.afnFeat.getCode(), val));
    }

    public static void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        LcpAfnFeatCodecEnums afn = VALUES.get(decodeHolder.getAfn().getCode());
        Assert.notNull(afn, "不支持的应用层功能码:" + afn + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        afn.codec.read(buf, decodeHolder, properties);
    }

    public static void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        LcpAfnFeatureEnums afnFeatureEnum = encodeHolder.getAfn();
        LcpAfnFeatCodecEnums afnCodecEnum = VALUES.get(afnFeatureEnum.getCode());
        Assert.notNull(afnCodecEnum, "不支持的应用层功能码:" + afnCodecEnum);

        // 应用层功能码AFN
        buf.writeByte(HexUtils.decodeHex(afnFeatureEnum.getCode())[0]);
        // 帧序列域SEQ
        IotFeat feature = encodeHolder.getFeature();
        if (feature.isNeedCon()) {
            buf.writeByte(0b01110000);
        } else {
            buf.writeByte(0b01100000);
        }

        // 应用层功能码
        buf.writeByte(encodeHolder.getFeature().getFeatCodeByte());

        // 数据单元
        afnCodecEnum.codec.write(buf, encodeHolder, properties);

        // 计算校验和
        int computeSum = ChecksumUtils.computeSumSingle(buf, 6, buf.writerIndex());
        buf.writeByte((byte) computeSum);
        buf.writeByte(0x16);
    }
}
