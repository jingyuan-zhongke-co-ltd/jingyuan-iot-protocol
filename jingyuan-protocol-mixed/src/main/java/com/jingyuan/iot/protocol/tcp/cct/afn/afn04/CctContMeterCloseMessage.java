package com.jingyuan.iot.protocol.tcp.cct.afn.afn04;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn04.CctContMeterCloseBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 遥控计量表关阀
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctContMeterCloseMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        contValve(buf, properties);
    }

    public static void contValve(ByteBuf buf, Map<String, Object> properties) {
        CctContMeterCloseBo contMeterCloseBo = BeanUtil.toBean(properties, CctContMeterCloseBo.class);
        // 采集器通信地址
        buf.writeBytes(new byte[]{contMeterCloseBo.getCoolAddr().byteValue(), 0x00, 0x00, 0x00, 0x00, 0x00});
        // 计量表序号
        buf.writeByte(contMeterCloseBo.getMeterSerialNum().byteValue());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(contMeterCloseBo.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
        // 中继深度
        buf.writeByte(contMeterCloseBo.getRelayDepth());
        // 中继路径
        byte[] relayPathBytes = BytesUtils.hexToBytes(contMeterCloseBo.getRelayPath());
        Assert.isTrue(ArrayUtil.isNotEmpty(relayPathBytes) && relayPathBytes.length == 10, "中继路径错误");
        BytesUtils.bytesReverse(relayPathBytes);
        buf.writeBytes(relayPathBytes);
    }
}
