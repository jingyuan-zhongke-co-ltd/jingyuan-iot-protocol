package com.jingyuan.iot.protocol.tcp.hlm.afn;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAAfnDataUploadEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn0a.HlmHeatAfnDataUploadsMessage;
import com.jingyuan.iot.protocol.tcp.hlm.afn.afn0a.HlmUltAfnDataUploadsMessage;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Map;

/**
 * 调节阀 目标调节 编解码枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum HlmDatauploadEnums implements FeatCodec {

    /**
     * 超声波水表
     */
    ULTRASONIC(HlmAAfnDataUploadEnums.ULTRASONIC, new HlmUltAfnDataUploadsMessage()),
    /**
     * 热表
     */
    HEAT(HlmAAfnDataUploadEnums.HEAT, new HlmHeatAfnDataUploadsMessage()),
    ;

    private final IotFeat feat;
    private final TcpMessageCodec codec;

    public static TcpMessageCodec lookup(HlmAAfnDataUploadEnums dataUploadEnums) {
        return Arrays.stream(values())
                     .filter(e -> e.feat.equals(dataUploadEnums))
                     .findFirst()
                     .map(val -> val.codec)
                     .orElseThrow(() -> new IllegalArgumentException("不支持的内层解析: " + dataUploadEnums));
    }

    public static void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        HlmAAfnDataUploadEnums byCode = HlmAAfnDataUploadEnums.getByCode(decodeHolder.getInnerLength());
        TcpMessageCodec messageCodec = lookup(byCode);
        // 调节模式
        properties.put("innerType", byCode.getDesc());
        messageCodec.read(buf, properties);
    }

}
