package com.jingyuan.iot.protocol.tcp.wac.gauge.conf;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfInstantFlowPidBo;
import com.jingyuan.common.protocol.model.wac.conf.WacConfPidBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.validator.ValidatorUtils;
import com.jingyuan.iot.protocol.tcp.wac.gauge.wac710.Wac710WriteValveOpenLimit;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 瞬时流量PID/热功率PID调节
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
public class WacConfPid implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        List<Map<String, Object>> maps = new ArrayList<>(3);
        int count = buf.readByte();
        for (int i = 1; i <= count; i++) {
            Map<String, Object> map = new HashMap<>(4);
            // 是否启用自调节
            map.put("enSelfAdjust" + i, buf.readByte());
            // 起始时间
            byte[] beginBytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(beginBytes);
            String beginTime = BCD.bcdToStr(beginBytes);
            map.put("beginTime" + i, beginTime);
            // 结束时间
            byte[] endBytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(endBytes);
            String endTime = BCD.bcdToStr(endBytes);
            map.put("endTime" + i, endTime);
            // 目标调节值
            map.put("adjustValue" + i, buf.readFloatLE());
            maps.add(map);
        }
        properties.put("adjustPid", maps);
        // 调节周期
        properties.put("adjustCycle", buf.readIntLE());
        // 调节死区
        properties.put("adjustDeadZone", buf.readFloatLE());
        // P 参数
        byte[] bytes1 = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes1);
        properties.put("p", BigDecimal.valueOf(Convert.toInt(BytesUtils.bytesToHex(bytes1))).movePointLeft(2));
        // I 参数
        byte[] bytes2 = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes2);
        properties.put("i", BigDecimal.valueOf(Convert.toInt(BytesUtils.bytesToHex(bytes2))).movePointLeft(2));
        // D 参数
        byte[] bytes3 = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes3);
        properties.put("d", BigDecimal.valueOf(Convert.toInt(BytesUtils.bytesToHex(bytes3))).movePointLeft(2));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacConfInstantFlowPidBo instantFlowPidBo = BeanUtil.toBean(properties, WacConfInstantFlowPidBo.class);
        List<WacConfPidBo> wacPidBos = instantFlowPidBo.getAdjustPid();
        // 由于长度由实际参数数量决定，所以此处需根据实际值计算
        buf.setByte(13, 5 + wacPidBos.size() * 13);
        // 调节个数
        buf.writeByte(wacPidBos.size());
        wacPidBos.forEach(item -> {
            ValidatorUtils.validateEntity(item);
            // 是否启用自调节
            buf.writeByte(item.getEnSelfAdjust() ? 0 : 1);
            // 起始时间
            String beginTime = DateUtils.format(item.getBeginTime(), DatePatterns.MONTH_DAY_HOUR_MINUTE_PATTERN);
            buf.writeBytes(BytesUtils.hexToBytes(beginTime));
            // 结束时间
            String endTime = DateUtils.format(item.getBeginTime(), DatePatterns.MONTH_DAY_HOUR_MINUTE_PATTERN);
            buf.writeBytes(BytesUtils.hexToBytes(endTime));
            // todo 思维还是两位 目标调节值
            buf.writeFloatLE(item.getAdjustValue());
        });

        // 调节周期
        buf.writeIntLE(instantFlowPidBo.getAdjustCycle());
        // 调节死区
        buf.writeFloatLE(instantFlowPidBo.getAdjustDeadZone());
        // P 参数
        short p = (short) new BigDecimal(Convert.toStr(instantFlowPidBo.getP()))
            .movePointRight(2)
            .intValue();
        Wac710WriteValveOpenLimit.writeOpening(buf, p);
        // I 参数
        short i = (short) new BigDecimal(Convert.toStr(instantFlowPidBo.getI()))
            .movePointRight(2)
            .intValue();
        Wac710WriteValveOpenLimit.writeOpening(buf, i);
        // D 参数
        short d = (short) new BigDecimal(Convert.toStr(instantFlowPidBo.getD()))
            .movePointRight(2)
            .intValue();
        Wac710WriteValveOpenLimit.writeOpening(buf, d);
    }
}
