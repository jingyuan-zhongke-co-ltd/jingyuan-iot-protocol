package com.jingyuan.iot.protocol.tcp.spr;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import com.jingyuan.common.constant.redis.PrefixIotConstants;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.model.FirmwareFrameBo;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.cached.CachedMessageHelper;
import com.jingyuan.iot.protocol.tcp.spr.codec.JoyoSprUpgradeIssuedMessage;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.spr.wrx.SprMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.property.WritePropertyMessage;
import org.jetlinks.core.message.property.WritePropertyMessageReply;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.Objects;


/**
 * 京源NB分体阀控协议
 *
 * @author: cc
 * @date: 2024/3/27 14:15
 * @Version: V1.0
 */
@Slf4j
public class SprDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata JOYO_NB_CONFIG = new DefaultConfigMetadata(
        "京源分体阀控水表TCP配置"
        , "京源分体阀控水表TCP配置");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }

    private final ReactiveStringRedisTemplate redisTemplate;

    public SprDeviceMessageCodec(ReactiveStringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return doDecode(context);
    }


    /**
     * 执行解码操作
     *
     * @param context 待解码消息上下文
     * @return org.jetlinks.core.message.DeviceMessage
     */
    private Publisher<? extends Message> doDecode(MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        SprTcpDecodeHolder decodeHolder = SprMessageTypeEnums.read(payload);
        if (ObjectUtil.isNull(decodeHolder.getChildVendorEnum()) || ObjectUtil.isNull(decodeHolder.getDeviceMessage())) {
            return Mono.empty();
        }

        DeviceMessage message = decodeHolder.getDeviceMessage();
        return context.getDevice(message.getDeviceId())
                      .flatMapMany(deviceOperator ->
                                       setConfigs(decodeHolder, deviceOperator)
                                           .thenMany(message
                                                         .getHeader(IotSprConstants.UPGRADE_N)
                                                         .map(Convert::toShort)
                                                         // 升级指令下发
                                                         .map(aShort -> upgradeIssued(message, decodeHolder.getChildVendorEnum(), aShort, deviceOperator, context))
                                                         // 缓存指令下发
                                                         .orElseGet(() -> cachedIssued(message, decodeHolder.getChildVendorEnum(), deviceOperator, context)))
                      );
    }


    /**
     * 设置厂商等信息
     *
     * @param decodeHolder 解码持有者
     * @param operator     设备操作类
     * @return reactor.core.publisher.Mono<java.lang.Boolean>
     */
    private Mono<Boolean> setConfigs(SprTcpDecodeHolder decodeHolder, DeviceOperator operator) {
        IotFeat headFeat = MessageHelper.getHeadFeat(decodeHolder.getDeviceMessage());
        if (JoyoSprFeatureEnums.UPGRADE_REPLY.equals(headFeat) || JoyoSprFeatureEnums.UPGRADE_QUERY_REPLY.equals(headFeat)) {
            return Mono.just(true);
        }
        return operator
            // 设置厂商信息
            .setConfig(IotConstants.VENDOR_CODE, decodeHolder
                .getChildVendorEnum()
                .getCode());
    }


    /**
     * NB 水表下发命令
     *
     * @param context 消息解码上下文
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    public Flux<DeviceMessage> cachedIssued(DeviceMessage message, SprChildVendorEnums childVendorEnum, DeviceOperator deviceOperator, MessageDecodeContext context) {
        return CachedMessageHelper
            .getMessage(deviceOperator)
            .flatMapMany(messages -> {
                if (CollUtil.isEmpty(messages)) {
                    return Flux.just(message);
                } else {
                    return Flux
                        .fromIterable(messages)
                        .flatMap(toSendMessage -> {
                            IotFeat feature = MessageHelper.getHeadFeatThrow(toSendMessage);
                            SprTcpEncodeHolder encodeHolder = SprTcpEncodeHolder.of();
                            encodeHolder.setChildVendorEnum(childVendorEnum);
                            encodeHolder.setFeature(feature);
                            encodeHolder.setDeviceMessage(toSendMessage);
                            return Flux.just(encodeHolder)
                                       .flatMap(holder -> doEncode(holder)
                                           .flatMapMany(buf -> ((FromDeviceMessageContext) context)
                                               .getSession()
                                               .send(buf)
                                               .flatMapMany(res -> {
                                                   if (res) {
                                                       return Flux.just(message, encodeHolder.getDeviceMessage());
                                                   } else {
                                                       return Flux.just(message);
                                                   }
                                               })));
                        });
                }
            })
            .switchIfEmpty(Flux.defer(() -> Flux.just(message)));
    }

    /**
     * 升级指令下发
     *
     * @param message 设备消息
     * @param context 消息解码上下文
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    public Flux<DeviceMessage> upgradeIssued(DeviceMessage message, SprChildVendorEnums childVendorEnum, Short upgradeN, DeviceOperator deviceOperator, MessageDecodeContext context) {
        return deviceOperator
            .getConfigs(IotConstants.FIRMWARE_ID)
            .map(values -> MessageHelper.getValueStr(values, IotConstants.FIRMWARE_ID, "设备升级固件信息不存在"))
            .flatMapMany(firmwareId -> redisTemplate
                .opsForValue()
                .get(PrefixIotConstants.FIRMWARE_FRAME + firmwareId)
                .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("设备升级固件信息不存在"))))
                .map(firmware -> JsonUtils.parseObject(firmware, FirmwareFrameBo.class))
                .flatMapMany(frameBo -> {
                    ByteBuf buf = Unpooled.buffer();
                    return JoyoSprUpgradeIssuedMessage
                        .write(buf, childVendorEnum, upgradeN, frameBo)
                        .flatMapMany(upgradeProperties -> ((FromDeviceMessageContext) context)
                            .getSession()
                            .send(EncodedMessage.simple(buf))
                            .flatMapMany(res -> {
                                if (res) {
                                    return mergeUpgradeProperties(message, upgradeProperties);
                                }
                                return Flux.just(message);
                            }));
                })
            );
    }


    public static Flux<DeviceMessage> mergeUpgradeProperties(DeviceMessage message, Map<String, Object> upgradeProperties) {
        if (message instanceof WritePropertyMessage) {
            WritePropertyMessage writePropertyMessage = (WritePropertyMessage) message;
            Map<String, Object> properties = writePropertyMessage.getProperties();
            if (CollUtil.isNotEmpty(properties)) {
                upgradeProperties.putAll(properties);
            }
            writePropertyMessage.setProperties(upgradeProperties);
            return Flux.just(writePropertyMessage);
        } else if (message instanceof WritePropertyMessageReply) {
            WritePropertyMessageReply writePropertyMessageReply = (WritePropertyMessageReply) message;
            Map<String, Object> properties = writePropertyMessageReply.getProperties();
            if (CollUtil.isNotEmpty(properties)) {
                upgradeProperties.putAll(properties);
            }
            writePropertyMessageReply.setProperties(upgradeProperties);
            return Flux.just(writePropertyMessageReply);
        }
        return Flux.just(message);
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        return context.getDevice(deviceMessage.getDeviceId())
                      .switchIfEmpty(Mono.just(Objects.requireNonNull(context.getDevice())))
                      .flatMap(deviceOperator ->
                                   deviceOperator.getConfig(IotConstants.VENDOR_CODE)
                                                 .map(vendorCode -> {
                                                     SprChildVendorEnums vendorEnum = SprChildVendorEnums.getByCode(String.valueOf(vendorCode.get()));
                                                     IotFeat feature = MessageHelper.getHeadFeatThrow(deviceMessage);
                                                     SprTcpEncodeHolder encodeHolder = SprTcpEncodeHolder.of();
                                                     encodeHolder.setChildVendorEnum(vendorEnum);
                                                     encodeHolder.setFeature(feature);
                                                     encodeHolder.setDeviceMessage(deviceMessage);
                                                     return encodeHolder;
                                                 })
                                                 .flatMap(this::doEncode));
    }


    /**
     * 执行编码操作
     *
     * @param encodeHolder 待编码消息
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(SprTcpEncodeHolder encodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        SprMessageTypeEnums.write(encodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
