package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 终端版本信息
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpTerminalVersionMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        终端硬件版本	ASCII	32
        byte[]  HardwareVersion = ByteBufUtils.readBytes(buf, 32);
        properties.put("HardwareVersion", BytesUtils.bytesToAscStr(HardwareVersion));
//        终端软件版本号	ASCII	32
        byte[]  SoftwareVersion = ByteBufUtils.readBytes(buf, 32);
        properties.put("SoftwareVersion", BytesUtils.bytesToAscStr(SoftwareVersion));
//        终端IMSI号	ASCII	32
        byte[]  imsi = ByteBufUtils.readBytes(buf, 32);
        properties.put("imsi", BytesUtils.bytesToAscStr(imsi));
    }


}
