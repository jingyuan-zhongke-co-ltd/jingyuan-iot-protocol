package com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.hlm.afn0c.HlmSetTrafficReportBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils.replenishByte;

/**
 * 休眠
 *
 * @author: dr
 * @date: 2024-10-24
 * @Version: V1.0
 */
@Slf4j
public class HlmSetTrafficReportMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        HlmSetTrafficReportBo params = BeanUtil.toBean(properties, HlmSetTrafficReportBo.class);
//        开启或关闭流量异常上报	BIN	0~1	1	0x00关闭，0x01开启。
        buf.writeByte(params.getTrafficReportExSta());
//        监控时段小于多少流量L值上报	BIN		2	x0.1表示实际监控L值。
        byte[] bytes = BytesUtils.shortToBytes((short) (params.getMonitorLValue()*10));
        bytes = replenishByte(bytes, 2);
        buf.writeBytes(new byte[]{bytes[1], bytes[0]});
//        监控流量时间点1234	BCD		2	时分。
        writeDateHm(buf,  params.getMonitoringTimePoint1());
        writeDateHm(buf,  params.getMonitoringTimePoint2());
        writeDateHm(buf,  params.getMonitoringTimePoint3());
        writeDateHm(buf,  params.getMonitoringTimePoint4());
    }

    private static void writeDateHm(ByteBuf buf, Date time) {
        String hour = Convert.toStr(DateUtils.hour(time, true));
        String minute = Convert.toStr(DateUtils.minute(time));
        buf.writeBytes(BCD.strToBcd(hour));
        buf.writeBytes(BCD.strToBcd(minute));
    }
}
