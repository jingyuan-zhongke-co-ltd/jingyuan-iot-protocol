package com.jingyuan.iot.protocol.http.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.ObjectUtil;
import com.ctg.ag.sdk.biz.AepDeviceCommandClient;
import com.ctg.ag.sdk.biz.AepDeviceCommandLwmProfileClient;
import com.ctg.ag.sdk.biz.aep_device_command.CreateCommandRequest;
import com.ctg.ag.sdk.biz.aep_device_command.CreateCommandResponse;
import com.ctg.ag.sdk.biz.aep_device_command_lwm_profile.CreateCommandLwm2mProfileRequest;
import com.ctg.ag.sdk.biz.aep_device_command_lwm_profile.CreateCommandLwm2mProfileResponse;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.constant.IotNorthConstants;
import com.jingyuan.common.flowable.enums.NorthMessageTypes;
import com.jingyuan.common.flowable.enums.CtwingServiceEnums;
import com.jingyuan.common.flowable.model.ctwing.CtwingCacheCommandResponse;
import com.jingyuan.common.flowable.model.ctwing.CtwingCreateCommandDto;
import com.jingyuan.common.flowable.model.ctwing.CtwingNorthParamDto;
import com.jingyuan.common.flowable.model.onenet.OnenetCacheLwm2mResponse;
import com.jingyuan.common.flowable.model.onenet.OnenetNorthParamDto;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.common.utils.log.LogUtils;
import com.jingyuan.common.utils.log.pojo.NorthLogVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

/**
 * HTTP 指令下发辅助类
 *
 * @author: cc
 * @date: 2024/12/19 15:13
 * @Version: V1.0
 */
@Slf4j
public class HttpCommandHelper {

    /**
     * 向 AEP 发送指令
     *
     * @param encodeHex 编码后指令
     * @param paramDto  写入的属性值
     */
    public static void createCommand2Ctwing(String encodeHex, CtwingNorthParamDto paramDto) {
        paramDto.setEncodeHex(encodeHex);
        CtwingCreateCommandDto createCommandDto = CtwingCreateCommandDto.of(paramDto);

        if (CtwingServiceEnums.TOU_CHUAN.codeEquals(paramDto.getServiceId())) {
            createCommandTcp2Ctwing(paramDto, createCommandDto);
        } else {
            createCommandLwm2m2Ctwing(paramDto, createCommandDto);
        }
    }

    private static void createCommandTcp2Ctwing(CtwingNorthParamDto paramDto, CtwingCreateCommandDto createCommandDto) {
        CreateCommandRequest request = new CreateCommandRequest();
        request.setParamMasterKey(paramDto.getMasterKey());
        request.setBody(JsonUtils.toJsonString(createCommandDto).getBytes(StandardCharsets.UTF_8));

        try {
            CreateCommandResponse response = AepDeviceCommandClient
                .newClient()
                .appKey(paramDto.getAppKey())
                .appSecret(paramDto.getAppSecret())
                .build()
                .CreateCommand(request);
            LogUtils.northLog(log, NorthLogVo.nleVo(NorthChannelEnums.ctwing, true, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, response, paramDto.getImei(), paramDto.getNprId()));
        } catch (Exception e) {
            LogUtils.northLog(log, NorthLogVo.nleVo(NorthChannelEnums.ctwing, false, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, LogUtils.formatStackCause(e), paramDto.getImei(), paramDto.getNprId()));
        }
    }

    private static void createCommandLwm2m2Ctwing(CtwingNorthParamDto paramDto, CtwingCreateCommandDto createCommandDto) {
        CreateCommandLwm2mProfileRequest request = new CreateCommandLwm2mProfileRequest();
        request.setParamMasterKey(paramDto.getMasterKey());
        request.setBody(JsonUtils.toJsonString(createCommandDto).getBytes(StandardCharsets.UTF_8));
        try {
            CreateCommandLwm2mProfileResponse response = AepDeviceCommandLwmProfileClient
                .newClient()
                .appKey(paramDto.getAppKey())
                .appSecret(paramDto.getAppSecret())
                .build()
                .CreateCommandLwm2mProfile(request);

            CtwingCacheCommandResponse commandResponse = JsonUtils.parseObject(new String(response.getBody(), StandardCharsets.UTF_8), CtwingCacheCommandResponse.class);
            if (ObjUtil.isNotNull(commandResponse) && "0".equals(commandResponse.getCode())) {
                LogUtils.northLog(log, NorthLogVo.nleVo(NorthChannelEnums.ctwing, true, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, commandResponse,
                                                        paramDto.getImei(), paramDto.getNprId()));
            } else {
                LogUtils.northLog(log, NorthLogVo.noNorVo(NorthChannelEnums.ctwing, false, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, commandResponse,
                                                          paramDto.getImei(), ObjectUtil.isNotNull(commandResponse) ? commandResponse.getMsg() : "未知错误"));
            }
        } catch (Exception e) {
            LogUtils.northLog(log, NorthLogVo.nleVo(NorthChannelEnums.ctwing, false, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, LogUtils.formatStackCause(e), paramDto.getImei(), paramDto.getNprId()));
        }
    }


    /**
     * 向 Onenet 发送指令
     *
     * @param encodeHex 编码后指令
     * @param paramDto  写入的属性值
     */
    public static void createCommand2Onenet(String encodeHex, OnenetNorthParamDto paramDto) {
        String expiredTime = DateUtils.format(DateUtils.addDays(DateUtils.date(), 2), DatePatterns.UTC_SIMPLE_PATTERN);
        String url = String.format(IotNorthConstants.ONENET_CACHED_COMMAND_LWM2M_FORMAT,
                                   paramDto.getNorthUrl(), paramDto.getImei(),
                                   IotNorthConstants.ONENET_OBJ_ID, IotNorthConstants.ONENET_OBJ_INST_ID,
                                   IotNorthConstants.ONENET_READ_RES_ID, expiredTime);

        HashMap<String, Object> map = new HashMap<>();
        map.put("args", encodeHex);

        Flux.just(map)
            .publishOn(Schedulers.boundedElastic())
            .flatMap(body -> WebClient
                .create(url)
                .post()
                .contentType(MediaType.APPLICATION_JSON)
                .header("api-key", Convert.toStr(paramDto.getApiKey()))
                .bodyValue(JsonUtils.toJsonString(map))
                .retrieve()
                .toEntity(OnenetCacheLwm2mResponse.class)
                .flatMap(response -> {
                    Map<String, String> request = new HashMap<>();
                    request.put("bytesToHex", encodeHex);
                    request.put("url", url);
                    OnenetCacheLwm2mResponse cacheLwm2mResponse = response.getBody();
                    if (ObjectUtil.isNotNull(cacheLwm2mResponse) && "0".equals(cacheLwm2mResponse.getCode())) {
                        LogUtils.northLog(log, NorthLogVo.noNorVo(NorthChannelEnums.onenet, true, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, cacheLwm2mResponse,
                                                                  paramDto.getImei()));
                    } else {
                        LogUtils.northLog(log, NorthLogVo.noNorVo(NorthChannelEnums.onenet, false, NorthMessageTypes.COMMAND_ISSUED.getCode(), request, cacheLwm2mResponse,
                                                                  paramDto.getImei(), ObjectUtil.isNotNull(cacheLwm2mResponse) ? cacheLwm2mResponse.getMsg() : "未知错误"));
                    }
                    return Mono.empty();
                }))
            .subscribe();
    }
}
