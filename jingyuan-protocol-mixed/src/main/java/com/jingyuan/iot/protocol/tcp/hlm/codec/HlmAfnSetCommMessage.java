package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnSetCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置参数
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnSetCommMessage implements HlmAfnMessage {

    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof HlmAfnSetCommWriteEnums;

        FeatCodecs.write(feature, buf, properties);
    }
}
