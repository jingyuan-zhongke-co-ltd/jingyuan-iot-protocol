package com.jingyuan.iot.protocol.tcp.wac.gauge.wac300;

import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.wac.WacTempSensorState;
import com.jingyuan.common.protocol.enums.wac.WacValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 读阀门数据
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac300ReadValveData implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 预留
        properties.put("valveAd", buf.readShortLE());
        // 设备唤醒到连上服务器的时间
        properties.put("wakesUpSeconds", buf.readShortLE());
        // 最近2小时平均温度
        properties.put("avgTemp", read2BytesLeBcdDot(buf));
        // 当前进水温度，58.60℃，(BCD码,2字节)
        properties.put("inletTemp", read2BytesLeBcdDot(buf));
        // 当前回水温度，38.50℃，(BCD码,2字节)
        properties.put("returnTemp", read2BytesLeBcdDot(buf));

        // 累积工作时间，12345658分钟，(BCD码,4字节)
        properties.put("accrueWorkingTime", read4BytesLeBcd(buf));
        // 累积阀开时间，12133656分钟，(BCD码,4字节)
        properties.put("accrueValveOpenTime", read4BytesLeBcd(buf));
        // 阀门开关次数(BCD码,4字节)，1234次；
        properties.put("valveContNum", read4BytesLeBcd(buf));
        // 累积热量6678kwh
        properties.put("accrueHeat", read4BytesLeBcd(buf));
        // 热量单位，05 代表 kwh
        properties.put("heatUnit", Convert.toInt(buf.readByte()));
        // 阀门开度
        short valveAngular = buf.readShortLE();
        properties.put("valveAngular", Convert.toInt(HexUtils.toHex(valveAngular)));
        // 实时时间
        byte[] bytes = ByteBufUtils.readBytes(buf, 7);
        BytesUtils.bytesReverse(bytes);
        properties.put("currentTime", DateUtils.parse(BCD.bcdToStr(bytes), DatePatterns.PURE_DATETIME_PATTERN));

        // 读取 ST 状态字
        readSt(buf, properties);
    }


    /**
     * 读取 ST 状态字
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    public static void readSt(ByteBuf buf, Map<String, Object> properties) {
        // ST(状态)
        byte st1 = buf.readByte();
        // 阀门状态
        properties.put("valveState", WacValveStateEnums.getByCode(st1 & 0x03).getCode());
        // 阀门超时故障动作状态
        properties.put("valveTimeoutState", st1 & 0x04);
        // 阀门堵转故障动作状态
        properties.put("valveStallState", st1 & 0x08);
        // 温度传感器状态
        properties.put("tempSenState", WacTempSensorState.getByStCode(st1).getCode());
        // 电池欠压故障状态
        properties.put("batteryVoltageState", st1 & 0x40);


        byte st2 = buf.readByte();
        // 阀门控制
        properties.put("valveControl", st2 & 0x03);
        // 外供电状态
        properties.put("extSupplyPowerState", st2 & 0x10);
        // 外接电源故障
        properties.put("extInsPowerState", st2 & 0x20);
        // 开盖报警状态
        properties.put("ocAlarmState", st2 & 0x80);
    }

    /**
     * 从 ByteBuf 读取两位 BCD 转为小数，50 25 -> 25.50
     *
     * @param buf ByteBuf
     * @return java.math.BigDecimal
     */
    public static BigDecimal read2BytesLeBcdDot(ByteBuf buf) {
        String avgTemp1 = BytesUtils.byteToHex(buf.readByte());
        String avgTemp2 = BytesUtils.byteToHex(buf.readByte());
        return new BigDecimal(avgTemp2).add(new BigDecimal(avgTemp1).movePointLeft(2));
    }

    /**
     * 从 ByteBuf 读取两位 BCD 转为整数，58 56 34 12 -> 12345658
     *
     * @param buf ByteBuf
     * @return java.math.BigDecimal
     */
    public static Integer read4BytesLeBcd(ByteBuf buf) {
        int accrueWorkingTime = buf.readIntLE();
        return Convert.toInt(HexUtils.toHex(accrueWorkingTime));
    }
}
