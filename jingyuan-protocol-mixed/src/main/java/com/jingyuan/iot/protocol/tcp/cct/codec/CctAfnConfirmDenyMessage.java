package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnConfirmRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 确认∕否认
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnConfirmDenyMessage implements CctAfnMessage {

    @Override
    public void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = String.valueOf(buf.readByte());
        CctAfnConfirmRwEnums confirmEnum = CctAfnConfirmRwEnums.getByFeatCode(fn);
        decodeHolder.setFeature(confirmEnum);
    }

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnConfirmRwEnums;
    }
}
