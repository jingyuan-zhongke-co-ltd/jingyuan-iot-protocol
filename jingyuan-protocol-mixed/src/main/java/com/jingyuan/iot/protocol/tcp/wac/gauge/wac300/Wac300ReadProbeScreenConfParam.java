package com.jingyuan.iot.protocol.tcp.wac.gauge.wac300;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 *  退出自检(生产 DD/AA)
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac300ReadProbeScreenConfParam implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        properties.put("screenConf", BytesUtils.byteToHex(buf.readByte()));
        properties.put("probeConf", BytesUtils.byteToHex(buf.readByte()));
    }

}
