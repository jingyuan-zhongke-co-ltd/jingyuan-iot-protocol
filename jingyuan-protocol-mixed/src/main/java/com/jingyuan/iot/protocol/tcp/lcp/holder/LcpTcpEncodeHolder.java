package com.jingyuan.iot.protocol.tcp.lcp.holder;

import com.jingyuan.common.protocol.enums.lcp.LcpAfnFeatureEnums;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息编码包装类
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
public class LcpTcpEncodeHolder extends TcpEncodeHolder {

    private int l;

    private String deviceId;

    /**
     * 应用层子功能码（FN）
     */
    private String fn;

    /**
     * 应用层功能码（AFN）
     */
    private LcpAfnFeatureEnums afn;
}
