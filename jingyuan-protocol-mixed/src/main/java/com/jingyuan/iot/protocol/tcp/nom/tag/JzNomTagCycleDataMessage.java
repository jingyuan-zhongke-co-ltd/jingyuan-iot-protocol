package com.jingyuan.iot.protocol.tcp.nom.tag;

import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.math.ArithmeticUtil;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 京兆无磁-水表周期数据
 *
 * @author: cc
 * @date: 2024/9/23 10:30
 * @Version: V1.0
 */
@Slf4j
public class JzNomTagCycleDataMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int length = buf.readShortLE();

        // 起始时间
        properties.put("cycleStartTime", JzNomTagRealTimeDataMessage.readDate(buf));
        // 数据记录间隔
        properties.put("cycleInterval", (int) buf.readByte());
        // 记录个数
        int cycleRecordNum = buf.readByte();
        properties.put("cycleRecordNum", cycleRecordNum);

        // 记录值
        byte[] cycleDataBytes = ByteBufUtils.readBytes(buf, 2 * cycleRecordNum);
        List<Double> cycleDataList = new ArrayList<>();
        for (byte[] bytes : ArrayUtil.split(cycleDataBytes, 2)) {
            cycleDataList.add(ArithmeticUtil.div(BytesUtils.bytesToShort(bytes, ByteOrder.LITTLE_ENDIAN), 1000, 3));
        }
        properties.put("cycleData", cycleDataList);
    }
}
