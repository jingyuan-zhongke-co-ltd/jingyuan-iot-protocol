package com.jingyuan.iot.protocol.tcp.cct.wrx;

import com.jingyuan.common.protocol.constant.IotCctConstants;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cct.codec.CctAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.HashMap;
import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class CctWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder tcpDecodeHolder) {
        message = new WritePropertyMessageReply();
        CctTcpDecodeHolder decodeHolder = (CctTcpDecodeHolder) tcpDecodeHolder;
        Map<String, Object> map = initReadMap(decodeHolder);
        CctAfnFeatCodecEnums.read(buf, decodeHolder, map);
        map.put(IotCctConstants.ADCODE, decodeHolder.getAdcode());
        message.setProperties(map);
    }

    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }


    public static Map<String, Object> initReadMap(CctTcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(16);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JOYO_CCT.getCode());
        return map;
    }
}
