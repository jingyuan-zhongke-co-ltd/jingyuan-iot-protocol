package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpSyncTimeBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 用水同步：当工作在时长模式时，服务器端记录设备累计使用时间，并下发时间到电脑板，电脑板只做时长显示。
 * 必须绑定套餐后操作，否则无响应。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaSyncTimeMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 已用天数
        byte[] usedDays = ByteBufUtils.readBytes(buf, 2);
        properties.put("usedDays", BytesUtils.binary(10, usedDays));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpSyncTimeBo wpSyncTimeBo = BeanUtil.toBean(properties, WpSyncTimeBo.class);
        Short usedDays = wpSyncTimeBo.getUsedDays();
        buf.writeBytes(BytesUtils.shortToBytes(usedDays));
    }
}
