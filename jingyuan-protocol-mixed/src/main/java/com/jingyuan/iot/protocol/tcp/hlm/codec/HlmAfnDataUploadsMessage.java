package com.jingyuan.iot.protocol.tcp.hlm.codec;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnQueryCommRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.iot.protocol.tcp.hlm.afn.HlmDatauploadEnums;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils.timeByteRead;

/**
 * 属性上报
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnDataUploadsMessage implements HlmAfnMessage {

    @Override
    public void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        decodeHolder.setFn(BytesUtils.byteToHex(buf.readByte()));
//        数据包标识
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
        decodeHolder.setPacketId(logotype);
//                程序版本
        int Version = BytesUtils.byteToUnInt(buf.readByte());
        properties.put("programVersion", Version);
//      读取 ICCID
        String iccid = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 20));
        properties.put("iccid", iccid);

//      读取模组时间
        String moduleTime = timeByteRead(buf, 6);
        properties.put("modTime", DateUtils.parseLocalDateTime(moduleTime));

// 读取信号
        int signal = buf.readByte();
        properties.put("csq", signal);

// 读取电压
        byte[] voltage = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(voltage);
        properties.put("voltage", BytesUtils.bytesToShort(voltage) * 0.01);

// 读取采集时间
        String acquisitionTime = timeByteRead(buf, 6);
        properties.put("freezeTime", DateUtils.parseLocalDateTime(acquisitionTime));

// 读取包数目
        int packageNumber = buf.readByte();
        // 读取包序号
        int packetOrdinal = buf.readByte();
        //todo 判断分包粘包 暂不处理
        if (packageNumber == packetOrdinal) {
//            break;
        }
        // 有效数据长度
        int validLength = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2), ByteOrder.LITTLE_ENDIAN);
        decodeHolder.setInnerLength(Convert.toStr(validLength));
        HlmDatauploadEnums.read(buf, decodeHolder, properties);
    }

    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof HlmAfnQueryCommRwEnums;
    }


}
