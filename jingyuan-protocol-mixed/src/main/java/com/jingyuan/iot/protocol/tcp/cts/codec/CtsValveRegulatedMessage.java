package com.jingyuan.iot.protocol.tcp.cts.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cts.CtsValveStateBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * NB数据上传
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CtsValveRegulatedMessage implements TcpMessageCodec {
    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        String valveState = ByteBufUtils.getBytesToHex(buf, 14, 1);
        valveState = IotValveStateEnums.getByByteHex(valveState).getCode();
        properties.put("valveState", valveState);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CtsValveStateBo ctsValveStateBo = BeanUtil.toBean(properties, CtsValveStateBo.class);
        String valveState = ctsValveStateBo.getValveState();
        String hex = IotValveStateEnums.getByCode(valveState).getName();
        buf.writeBytes(BytesUtils.hexToBytes(hex));
    }
}
