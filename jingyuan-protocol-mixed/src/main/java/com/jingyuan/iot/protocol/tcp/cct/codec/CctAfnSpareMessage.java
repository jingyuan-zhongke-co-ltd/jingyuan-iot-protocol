package com.jingyuan.iot.protocol.tcp.cct.codec;

import lombok.extern.slf4j.Slf4j;

/**
 * 备用
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnSpareMessage implements CctAfnMessage {

}
