package com.jingyuan.iot.protocol.tcp.lcp.wrx;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.lcp.codec.LcpAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.HashMap;
import java.util.Map;

/**
 * 京源测控终端协议 - 数据上报
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.01.0
 */
@Slf4j
public class LcpWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder tcpDecodeHolder) {
        message = new WritePropertyMessageReply();
        LcpTcpDecodeHolder decodeHolder = (LcpTcpDecodeHolder) tcpDecodeHolder;
        Map<String, Object> map = initReadMap(decodeHolder);
        LcpAfnFeatCodecEnums.read(buf, decodeHolder, map);
        message.setProperties(map);
    }

    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }


    public static Map<String, Object> initReadMap(LcpTcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(16);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JOYO_LCP.getCode());
        map.put(IotConstants.METER_ADDR, decodeHolder.getDeviceId());
        return map;
    }
}
