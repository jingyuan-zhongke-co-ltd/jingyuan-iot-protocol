package com.jingyuan.iot.protocol.http.topic;

import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.IotMessageType;
import lombok.Getter;
import lombok.SneakyThrows;
import org.jetlinks.core.route.MqttRoute;

import java.util.StringJoiner;
import java.util.function.Function;

/**
 * http topic 枚举
 *
 * @author: cc
 * @date: 2024/10/11 17:03
 * @Version: V1.0
 */
@Getter
public enum HttpTopicEnums {
    // 上报属性数据
    reportProperty("/*/properties/report",
                   IotMessageType.REPORT_PROPERTY,
                   route -> route
                       .upstream(true)
                       .downstream(false)
                       .group("属性上报_V1")
                       .description("上报物模型属性数据")
                       .example("{\"properties\":{\"属性ID\":\"属性值\"}}")),
    reportPropertyV2("/*/properties/report/*",
                     IotMessageType.REPORT_PROPERTY,
                     route -> route
                         .upstream(true)
                         .downstream(false)
                         .group("属性上报_V2")
                         .description("上报物模型属性数据")
                         .example("{\"properties\":{\"属性ID\":\"属性值\"}}")),

    //修改属性
    writeProperty("/*/properties/write",
                  IotMessageType.WRITE_PROPERTY,
                  route -> route
                      .upstream(false)
                      .downstream(true)
                      .group("修改属性")
                      .description("平台下发修改物模型属性数据指令")
                      .example("{\"messageId\":\"消息ID,回复时需要一致.\",\"properties\":{\"属性ID\":\"属性值\"}}")),

    //事件上报
    event("/*/event/*",
          IotMessageType.EVENT,
          route -> route
              .upstream(true)
              .downstream(false)
              .group("事件上报")
              .description("AEP平台上报物模型事件数据")
              .example("{\"data\":{\"key\":\"value\"}}")) {
        @Override
        protected void transMqttTopic(String[] topic) {
            topic[topic.length - 1] = "{eventId:事件ID}";
        }
    },

    // 设备上下线
    online_offline("/*/online/offline",
                   IotMessageType.ONLINE,
                   builder -> builder
                       .upstream(true)
                       .group("状态管理")
                       .description("设备状态变更")),
    ;

    HttpTopicEnums(String topic,
                   IotMessageType messageType,
                   Function<MqttRoute.Builder, MqttRoute.Builder> routeCustom) {
        if (topic.startsWith("/")) {
            topic = StrUtil.sub(topic, 1, topic.length());
        }
        this.pattern = topic.split("/");
        this.messageType = messageType;
        this.route = routeCustom.apply(toRoute()).build();
    }

    private final String[] pattern;
    private final MqttRoute route;
    private final IotMessageType messageType;

    @SneakyThrows
    private MqttRoute.Builder toRoute() {
        String[] topics = new String[pattern.length];
        System.arraycopy(pattern, 0, topics, 0, pattern.length);
        topics[0] = "{vendorCode:厂商编号}";
        transMqttTopic(topics);
        StringJoiner joiner = new StringJoiner("/", "/", "");
        for (String topic : topics) {
            joiner.add(topic);
        }
        return MqttRoute
            .builder(joiner.toString())
            .qos(1);
    }

    protected void transMqttTopic(String[] topic) {

    }
}
