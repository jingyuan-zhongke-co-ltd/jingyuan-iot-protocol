package com.jingyuan.iot.protocol.tcp.cct.holder;

import com.jingyuan.common.enums.JingyuanBaseEnum;
import com.jingyuan.common.protocol.enums.cct.CctAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.cct.CctPrmEnums;
import com.jingyuan.common.protocol.enums.cct.fn.IotCctFunc;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息编码包装类
 *
 * @author: cc
 * @date: 2024/4/22 11:28
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
public class CctTcpEncodeHolder extends TcpEncodeHolder {

    private int l;

    /**
     * 启动表示位
     */
    private CctPrmEnums prm;
    private JingyuanBaseEnum<Integer> prmFeature;

    /**
     * 行政区划码
     */
    private String adcode;
    private String deviceId;

    /**
     * 应用层功能码（AFN）
     */
    private CctAfnFeatureEnums afn;
    /**
     * 应用层数据单元功能
     */
    private IotCctFunc feature;
}
