package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpRechargeUpBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 充正值：用于充值流量或天数。
 * 必须绑定套餐后操作，否则无响应。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaRechargeUpMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 充值流量或天数
        byte[] rechargeAmount = ByteBufUtils.readBytes(buf, 2);
        properties.put("rechargeAmount", BytesUtils.binary(10, rechargeAmount));
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpRechargeUpBo wpRechargeBo = BeanUtil.toBean(properties, WpRechargeUpBo.class);
        Short amount = wpRechargeBo.getAmount();
        buf.writeBytes(BytesUtils.shortToBytes(amount));
    }
}
