package com.jingyuan.iot.protocol.tcp.wac.holder;

import com.jingyuan.common.protocol.enums.DataDirectionEnums;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息编码包装类
 *
 * @author: cc
 * @date: 2024/12/4 16:15
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class WacTcpEncodeHolder extends TcpEncodeHolder {
    /**
     * 仪表类型
     */
    private JoyoIotVendor vendor;
    /**
     * 传输方向位
     */
    private DataDirectionEnums dir;
    /**
     * 序号 SER
     */
    private Integer serId;
}
