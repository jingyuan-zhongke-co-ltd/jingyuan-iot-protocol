package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;

import java.util.Map;

/**
 * 模组联网IP和端口设置远程应答帧
 * 帧长度：37字节
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
public class JoyoSprIpPortConfigMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // IP和PORT地址, IP：17.158.134.219  端口：1111，B12~B34
        // 那么B12~B34为：0x22 0x31 0x37 0x2E 0x31 0x35 0x38 0x2E 0x31 0x33 0x34 0x2E 0x32 0x31 0x39 0x22 0x2C 0x31 0x31 0x31 0x31 0x3A 0x15
        // 其中0x22为双引号的ASCII码，0x2E为.号的ASCII码，0x2C为，的ASCII码，
        // 0x3A为填充字符，如果字节数不够，则填充0x3A；0x15=21，为IP和端的长度（从0x22到0x31），总共21字节。
        // B12固定为0x22(ASIC 对应为 ")，B34为IP和端口的长度
        // 去除第一位 "
        buf.readByte();
        String ipPortsStr = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 22));
        String ip = StrUtil.sub(ipPortsStr, 0, StrUtil.indexOf(ipPortsStr, '"'));
        String port = StrUtil.sub(ipPortsStr, StrUtil.indexOf(ipPortsStr, ',') + 1, StrUtil.indexOf(ipPortsStr, ':'));

        properties.put("ip", ip);
        properties.put("port", port);
    }
}