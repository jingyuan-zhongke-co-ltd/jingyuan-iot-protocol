package com.jingyuan.iot.protocol.tcp.cz.wrx;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.czdzp.CzDzpFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.CRC16CzDzp;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * 沧州定制消息类型枚举
 *
 * @author: 李立强
 * @date: 2024/10/08 15:34
 * @Version: V1.0
 */
@Slf4j
public enum CzDzpMessageTypeEnums {
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), CzDzpReportPropertyMessage::new),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), CzDzpWritePropertyMessage::new),
    ;

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    private static final Map<String, CzDzpMessageTypeEnums> MAPS = new HashMap<>(16);

    static {
        Arrays.stream(values()).forEach(item -> MAPS.put(item.messageType, item));
    }

    CzDzpMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, CzDzpMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (CzDzpMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf) {
        DeviceMessage message = encodeHolder.getDeviceMessage();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));
        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        CzDzpMessageTypeEnums type = maps.get(messageType.getCode());
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);
        // 填充68a1
        buf.writeBytes(BytesUtils.hexToBytes("68a1"));
        // 填充表号
        String deviceId = message.getDeviceId();
        StringBuilder reversedDeviceId = new StringBuilder();
        for (int i = deviceId.length(); i > 0; i -= 2) {
            reversedDeviceId.append(deviceId, i - 2, i);
        }
        buf.writeBytes(BytesUtils.hexToBytes(reversedDeviceId.toString()));
        // 下发功能指令填充
        tcp.write(buf, encodeHolder);
        // 后置命令帧填充
        doWriteSuffix(buf);
        // 添加帧结尾，1 字节 (0x16)
        buf.writeByte(0x16);
        // 添加 MID，假设是 2 字节的低位在前
        byte[] midBytes = new byte[2];
        // 低位字节
        midBytes[0] = (byte) (msgId & 0xFF);
        // 高位字节
        midBytes[1] = (byte) ((msgId >> 8) & 0xFF);
        buf.writeBytes(midBytes);
    }

    /**
     * 向 ByteBuf 写入校验信息
     *
     * @param buf         写出数据
     */
    private static void doWriteSuffix(ByteBuf buf) {
        // 预留 ZXKF
        buf.writeBytes(BytesUtils.hexToBytes("00"));
        // 预留 NC
        buf.writeBytes(BytesUtils.hexToBytes("000000000000000000"));
        // 提取要计算 CRC 的数据部分
        String dataToCheck = ByteBufUtils.getBytesToHex(buf, 0, buf.writerIndex());
        byte[] dataBytes = BytesUtils.hexToBytes(dataToCheck);
        // 校验帧
        CRC16CzDzp crc16CzDzp = new CRC16CzDzp();
        crc16CzDzp.reset();
        crc16CzDzp.update(dataBytes, 0, dataBytes.length);
        int calculatedSum = crc16CzDzp.getCRC();
        // 确保写入校验和为小端格式
        byte[] crcBytes = new byte[2];
        // 低位字节
        crcBytes[0] = (byte) (calculatedSum & 0xFF);
        // 高位字节
        crcBytes[1] = (byte) ((calculatedSum >> 8) & 0xFF);
        // 写入校验和
        buf.writeBytes(crcBytes);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static TcpDecodeHolder read(ByteBuf buf) {
        TcpDecodeHolder decodeHolder = TcpDecodeHolder.of();
        // 原始数据帧
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        // 设备ID
        String bytesToHex = ByteBufUtils.getBytesToHex(buf, 2, 7);
        StringBuilder reversedDeviceId = new StringBuilder();
        for (int i = 0; i < bytesToHex.length(); i += 2) {
            reversedDeviceId.insert(0, bytesToHex.substring(i, i + 2));
        }
        decodeHolder.setDeviceId(reversedDeviceId.toString());
        // 功能码
        CzDzpFeatEnums feature = CzDzpFeatEnums.getByFeatCode(ByteBufUtils.getBytesToHex(buf, 9, 2).toUpperCase());
        decodeHolder.setFeature(feature);
        // 指令校验
        feature.getCheckResult(decodeHolder.getRawDataHex());
        // 设备消息日志添加功能信息
        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        feature.setMessageHeaders(deviceMessage);
        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf,
                             TcpDecodeHolder decodeHolder,
                             BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        IotMessageType decodeType = feature.getDecodeType();
        Assert.notNull(decodeType, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        CzDzpMessageTypeEnums type = MAPS.get(decodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        // 68A1
        buf.readBytes(2);
        // 创建消息对象
        TcpMessageAdapter<? extends DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        Integer replyMsgId = MsgIdCacheUtil.getReplyMsgId(message.getDeviceId(), feature.getFeatCode());
        return handler.apply(message, replyMsgId);
    }

}
