package com.jingyuan.iot.protocol.tcp.lcp.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn03.LcpPressureLimits;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 设置压力上下限值
 *
 * @author: dr
 * @date: 2024-10-31
 * @Version: V1.0
 */
@Slf4j
public class LcpSetPressureLimitsMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpPressureLimits bean = BeanUtil.toBean(properties, LcpPressureLimits.class);

        byte[] bytes1 = BytesUtils.shortToBytes(bean.getPressureLowerLimit(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(bytes1);

        byte[] bytes2 = BytesUtils.shortToBytes(bean.getPressureUpperLimit(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(bytes2);

    }
}
