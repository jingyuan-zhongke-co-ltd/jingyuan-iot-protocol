package com.jingyuan.iot.protocol.tcp.codec;

import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;

/**
 * 功能与编解码处理类映射
 *
 * @author: cc
 * @date: 2024/9/23 11:23
 * @Version: V1.0
 */
public interface FeatCodec {
    IotFeat getFeat();

    TcpMessageCodec getCodec();
}
