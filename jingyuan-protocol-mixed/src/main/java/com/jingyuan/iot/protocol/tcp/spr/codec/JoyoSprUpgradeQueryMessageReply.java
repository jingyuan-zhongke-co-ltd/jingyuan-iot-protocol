package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.Crc16Utils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * NB升级 模块请求升级包（适用于TCP直连模式）
 *
 * @author: cc
 * @date: 2024/10/14 14:07
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprUpgradeQueryMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 第 N 帧
        long n = ByteBufUtils.readBytesToUnLong(buf, 2);
        properties.put(IotSprConstants.UPGRADE_N, n);

        // CRC校验(B0-B13)
        long receivedSum = ByteBufUtils.readBytesToUnLong(buf, 2);

        String rawDataHex = ByteBufUtils.getBytesToHex(buf, 0, buf.writerIndex());
        byte[] toCheckBytes = BytesUtils.hexToBytes(StrUtil.subPre(rawDataHex, 28));
        long computeSum = Crc16Utils.crc16Ccitt(toCheckBytes, 0, toCheckBytes.length);
        // Assert.isTrue(receivedSum == computeSum, "数据帧校验和校验失败，原始数据帧：【" + rawDataHex + "】");
    }
}
