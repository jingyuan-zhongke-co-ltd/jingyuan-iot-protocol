package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpBindPackageBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 绑定套餐：用于客户选定套餐类型后，将套餐值下发给该电脑板。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaBindPackageMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 工作模式
        properties.put("workMode", String.valueOf(buf.readByte()));
        // 各级滤芯最大值读取
        RundaKeepaliveMessage.eachLevelMaxRead(buf, properties);
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x0b);

        WpBindPackageBo wpBindPackageBo = BeanUtil.toBean(properties, WpBindPackageBo.class);

        byte workMode = (byte) Integer.parseInt(wpBindPackageBo.getWorkMode());
        buf.writeByte(workMode);

        Short level1Max = wpBindPackageBo.getLevel1Max();
        buf.writeBytes(BytesUtils.shortToBytes(level1Max));
        Short level2Max = wpBindPackageBo.getLevel2Max();
        buf.writeBytes(BytesUtils.shortToBytes(level2Max));
        Short level3Max = wpBindPackageBo.getLevel3Max();
        buf.writeBytes(BytesUtils.shortToBytes(level3Max));
        Short level4Max = wpBindPackageBo.getLevel4Max();
        buf.writeBytes(BytesUtils.shortToBytes(level4Max));
        Short level5Max = wpBindPackageBo.getLevel5Max();
        buf.writeBytes(BytesUtils.shortToBytes(level5Max));
    }
}
