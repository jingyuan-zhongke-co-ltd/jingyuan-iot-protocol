package com.jingyuan.iot.protocol.tcp.spr;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Function;

/**
 * IOT厂商类型枚举类
 *
 * @author: cc
 * @date: 2024/3/27 14:33
 * @Version: V1.0
 */
@Getter
@AllArgsConstructor
public enum SprChildVendorEnums {

    /**
     * 厂商类型枚举值
     **/
    JOYO_SPR(
        JoyoIotVendor.JOYO_SPR.getCode(),
        SprChildVendorEnums::joyoSprBaseDecode,
        buf -> StrUtil.padPre(ByteBufUtils.getBytesToHex(buf, 10, 2), 4, '0'),
        new byte[]{(byte) 0xa1, 0x68},
        "a168",
        "00",
        new byte[]{(byte) 0x16}
    ),
    JOYO_SPR_THIRD(
        JoyoIotVendor.JOYO_SPR_THIRD.getCode(),
        SprChildVendorEnums::joyoSprBaseDecode,
        buf -> StrUtil.padPre(ByteBufUtils.getBytesToHex(buf, 10, 2), 4, '0'),
        new byte[]{(byte) 0xa2, 0x68},
        "a268",
        "00",
        new byte[]{(byte) 0x16}
    ),
    ;

    /**
     * 编号
     */
    private final String code;
    /**
     * 解码的基础数据
     */
    private final Function<ByteBuf, SprTcpDecodeHolder> decodedBaseMessage;
    /**
     * ByteBuf 读取功能类型
     */
    private final Function<ByteBuf, String> featureType;
    /**
     * 帧标识
     */
    private final byte[] frameId;
    /**
     * 帧标识
     */
    private final String frameHex;
    /**
     * 设备编号填充
     */
    private final String deviceIdAppendStr;
    /**
     * 终止符
     */
    private final byte[] terminator;

    public static SprChildVendorEnums getByCode(String code) {
        Assert.notBlank(code, "不支持的厂商类型");
        AtomicReference<SprChildVendorEnums> vendorEnums = new AtomicReference<>(null);
        Arrays.asList(values()).forEach(vendor -> {
            if (vendor.getCode().equals(code)) {
                vendorEnums.set(vendor);
            }
        });
        Assert.notNull(vendorEnums.get(), "不支持的厂商类型");
        return vendorEnums.get();
    }

    /**
     * 获取厂商、功能码等基础信息
     *
     * @param buf ByteBuf
     * @return com.jingyuan.common.protocol.enums.IotVendorEnums
     */
    public static SprTcpDecodeHolder baseDecode(ByteBuf buf) {
        String rawDataHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf));
        Assert.notBlank(rawDataHex, "原始数据帧不能为空");

        SprChildVendorEnums iotVendorEnum = vendorMatchByRawData(rawDataHex);
        SprTcpDecodeHolder messageDecodeHolder = iotVendorEnum.decodedBaseMessage.apply(buf);
        messageDecodeHolder.setRawDataHex(rawDataHex);
        messageDecodeHolder.setChildVendorEnum(iotVendorEnum);
        return messageDecodeHolder;
    }


    private static SprChildVendorEnums vendorMatchByRawData(String rawDataHex) {
        for (SprChildVendorEnums value : values()) {
            if (StrUtil.startWithIgnoreCase(rawDataHex, value.getFrameHex())) {
                return value;
            }
        }
        throw new IllegalArgumentException("不支持的厂商类型, 原始数据帧: 【" + rawDataHex + "】");
    }


    public static SprTcpDecodeHolder joyoSprBaseDecode(ByteBuf buf) {
        ByteBufUtils.readBytes(buf, 2);
        SprTcpDecodeHolder decodeHolder = SprTcpDecodeHolder.of();
        decodeHolder.setDeviceId(doReadDeviceId(buf));
        String featCode = ByteBufUtils.readBytesToHex(buf, 2);
        JoyoSprFeatureEnums feature = JoyoSprFeatureEnums.getByFeatCode(featCode);
        Assert.notNull(feature, "功能类型解析异常：【" + featCode + "】，原始数据帧：【" + BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)) + "】");
        decodeHolder.setFeature(feature);
        return decodeHolder;
    }


    /**
     * 获取设备ID
     *
     * @param buf buf
     * @return java.lang.String
     */
    private static String doReadDeviceId(ByteBuf buf) {
        // 表号8字节
        String hexString = ByteBufUtils.readBytesToHex(buf, 8);
        // 表类型，固定1字节
        String prefix = StrUtil.sub(hexString, 0, 2);

        // 表类型后面截取掉 00 或 0000
        int code = Integer.parseInt(StrUtil.sub(hexString, 2, 6));
        int sufLength = code == 0 ? 10 : 12;
        String suffix = StrUtil.subSufByLength(hexString, sufLength);
        return prefix + suffix;
    }
}
