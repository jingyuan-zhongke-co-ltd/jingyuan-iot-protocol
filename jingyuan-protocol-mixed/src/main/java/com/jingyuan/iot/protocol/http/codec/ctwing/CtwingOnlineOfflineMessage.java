package com.jingyuan.iot.protocol.http.codec.ctwing;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.constant.IotNorthConstants;
import com.jingyuan.common.flowable.enums.NorthLifecycleState;
import com.jingyuan.common.flowable.enums.NorthMessageTypes;
import com.jingyuan.common.flowable.model.north.event.NorthLogEventBo;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotTopicConstants;
import com.jingyuan.common.protocol.holder.http.HttpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.HttpMessageAdapter;
import com.jingyuan.common.utils.json.JsonUtils;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;
import reactor.core.publisher.Flux;

import java.util.Map;

/**
 * AEP 平台设备上下线消息
 *
 * @author: cc
 * @date: 2024/10/17 9:23
 * @Version: V1.0
 */
@Slf4j
public class CtwingOnlineOfflineMessage implements HttpMessageAdapter {

    @Override
    @SuppressWarnings("unchecked")
    public Flux<DeviceMessage> read(HttpDecodeHolder decodeHolder) {
        Map<String, Object> payloadMap = JsonUtils.parseObject(decodeHolder.getPayload(), Map.class);
        if (CollUtil.isEmpty(payloadMap)) {
            return Flux.empty();
        }
        return Flux
            .just(payloadMap)
            .flatMap(map -> {
                if (!"deviceOnlineOfflineReport".equals(Convert.toStr(map.get("messageType")))) {
                    return Flux.error(new IllegalArgumentException("设备上下线消息解析异常"));
                }
                String imei = Convert.toStr(map.get(IotConstants.IMEI_LOWER));
                String deviceId = Convert.toStr(map.get(IotConstants.DEVICE_ID));
                String productId = Convert.toStr(map.get(IotConstants.PRODUCT_ID));
                String eventType = Convert.toStr(map.get("eventType"));
                DeviceMessage deviceMessage = NorthLifecycleState.messageInstance(imei, deviceId, eventType, NorthChannelEnums.ctwing);
                deviceMessage.addHeader(IotNorthConstants.NPR_ID, productId);

                return decodeHolder
                    .getEventBus()
                    .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS,
                             NorthLogEventBo.nleEvent(NorthChannelEnums.ctwing, true, NorthMessageTypes.ONLINE_OFFLINE.getCode(),
                                                      JsonUtils.toJsonString(map), JsonUtils.toJsonString(deviceMessage), imei, productId))
                    .thenReturn(deviceMessage);
            });
    }
}
