package com.jingyuan.iot.protocol.tcp.lcp.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn03.CctSetIpPortBo;
import com.jingyuan.common.protocol.model.lcp.afn03.LcpPulseRatio;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置脉冲变比
 *
 * @author: dr
 * @date: 2024-10-31
 * @Version: V1.0
 */
@Slf4j
public class LcpSetPulseRatioMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpPulseRatio bean = BeanUtil.toBean(properties, LcpPulseRatio.class);
        buf.writeByte(bean.getPulseRatio());
    }
}
