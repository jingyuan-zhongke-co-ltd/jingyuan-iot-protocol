package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.spr.SprIpPortConfigBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.charset.StandardCharsets;
import java.util.Map;

/**
 * 模组联网IP和端口设置远程
 * 帧长度：37字节
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprIpPortConfigMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(JoyoSprFeatureEnums.IP_PORT_CONFIG.getFeatCode()));
        SprIpPortConfigBo sprIpPortConfigBo = BeanUtil.toBean(properties, SprIpPortConfigBo.class);

        byte[] bytes = String
            .format(IotSprConstants.IP_PORT_FORMAT_STR, sprIpPortConfigBo.getIp(), sprIpPortConfigBo.getPort())
            .getBytes(StandardCharsets.UTF_8);
        byte[] fillBytes = BytesUtils.bytesResize(bytes, IotSprConstants.IP_PORT_FILL_BYTE, IotSprConstants.IP_PORT_BYTES_LENGTH);
        fillBytes[IotSprConstants.IP_PORT_BYTES_LENGTH - 1] = (byte) bytes.length;
        buf.writeBytes(fillBytes);
    }
}
