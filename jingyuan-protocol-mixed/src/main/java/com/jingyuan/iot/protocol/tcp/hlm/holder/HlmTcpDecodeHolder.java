package com.jingyuan.iot.protocol.tcp.hlm.holder;

import com.jingyuan.common.enums.JingyuanBaseEnum;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.cct.CctSeqFfEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmDirEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmPrmEnums;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息解码包装类
 *
 * @author: dr
 * @date: 2024-10-23
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class HlmTcpDecodeHolder extends TcpDecodeHolder {
    /**
     * 传输方向位
     */
    private HlmDirEnums dir;
    /**
     * 启动表示位
     */
    private HlmPrmEnums prm;
    /**
     * 协议id位
     */
    private JingyuanBaseEnum<String> protocol;

    /**
     * 应用层功能码（AFN）
     */
    private HlmAfnFeatureEnums afn;

    /**
     * 应用层子功能码（FN）
     */
    private String fn;

    /**
     * 数据包标识
     */
    private int packetId;
    /**
     * 帧序列域 FIR、FIN
     */
    private CctSeqFfEnums seqFf;
    /**
     * 请求确认标志位
     * 帧序列域 CON
     */
    private CctSeqConEnums seqCon;

    /**
     * 内层有效数据长度
     */
    private String innerLength;
}
