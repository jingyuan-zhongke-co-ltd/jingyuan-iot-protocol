package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读阀门开度上下限
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadValveOpenLimit implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 阀门开度上限
        byte[] upperBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(upperBytes);
        properties.put("openLimitUpper", Convert.toInt(BCD.bcdToStr(upperBytes)));

        // 阀门开度下限
        byte[] lowerBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(lowerBytes);
        properties.put("openLimitLower", Convert.toInt(BCD.bcdToStr(upperBytes)));
    }
}
