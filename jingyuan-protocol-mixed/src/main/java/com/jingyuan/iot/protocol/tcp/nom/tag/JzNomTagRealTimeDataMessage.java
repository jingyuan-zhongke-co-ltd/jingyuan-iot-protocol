package com.jingyuan.iot.protocol.tcp.nom.tag;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.math.ArithmeticUtil;
import com.jingyuan.common.utils.string.StrUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

/**
 * 京兆无磁-水表实时数据
 *
 * @author: cc
 * @date: 2024/9/23 10:30
 * @Version: V1.0
 */
@Slf4j
public class JzNomTagRealTimeDataMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        recursionReal(buf.readShortLE(), buf, properties);
    }


    /**
     * 递归处理报警数据
     */
    public static void recursionReal(short lessLength, ByteBuf buf, Map<String, Object> properties) {
        if (lessLength <= 0) {
            return;
        }
        // 数据ID
        int dataId = buf.readByte();
        switch (dataId) {
            case 0:
                // 当前累计流量
                properties.put("reading", ArithmeticUtil.div(buf.readUnsignedIntLE(), 1000, 3));
                lessLength -= 5;
                break;
            case 1:
                // 累计正流量
                properties.put("forwardFlow", ArithmeticUtil.div(buf.readUnsignedIntLE(), 1000, 3));
                lessLength -= 5;
                break;
            case 2:
                // 累计逆流量
                properties.put("reverseFlow", ArithmeticUtil.div(buf.readUnsignedIntLE(), 1000, 3));
                lessLength -= 5;
                break;
            case 3:
                // 日最高流量
                properties.put("dailyHighestFlow", ArithmeticUtil.div(buf.readUnsignedShortLE(), 1000, 3));
                lessLength -= 3;
                break;
            case 4:
                // 日最高流量时间
                properties.put("dailyHighestTime", readDate(buf));
                lessLength -= 7;
                break;
            case 5:
                // 水表采集时间
                properties.put("currentTime", readDate(buf));
                lessLength -= 7;
                break;
            case 6:
                // 电池电压
                properties.put("voltage", ArithmeticUtil.div(buf.readShortLE(), 100, 2));
                lessLength -= 3;
                break;
            case 7:
                // 阀门状态
                properties.put("valveState", (int) buf.readByte());
                lessLength -= 2;
                break;
            case 8:
                // 应急状态
                properties.put("emergencyState", (int) buf.readByte());
                lessLength -= 2;
                break;
            default:
                lessLength -= 1;
                break;
        }
        recursionReal(lessLength, buf, properties);
    }


    public static String readDate(ByteBuf buf) {
        String dateStr = StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2) +
            StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2) +
            StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2) +
            StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2) +
            StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2) +
            StrUtils.fillBefore(Convert.toStr(buf.readByte()), '0', 2);
        return "000000000000".equals(dateStr) ? DateUtils.formatDateTime(DateUtils.beginOfDay(new Date())) :
            DateUtils.formatDateTime(DateUtils.parse("20" + dateStr, DatePatterns.PURE_DATETIME_PATTERN));
    }
}
