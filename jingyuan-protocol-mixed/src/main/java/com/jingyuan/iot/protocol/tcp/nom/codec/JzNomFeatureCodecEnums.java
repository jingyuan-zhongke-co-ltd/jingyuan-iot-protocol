package com.jingyuan.iot.protocol.tcp.nom.codec;

import com.jingyuan.common.protocol.enums.nom.JzNomFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 京兆无磁协议消息类型枚举
 *
 * @author: cc
 * @date: 2024/9/23 10:18
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum JzNomFeatureCodecEnums implements FeatCodec {
    /**
     * 数据上报
     */
    DATA_REPORT(JzNomFeatEnums.DATA_REPORT, new JzNomDataReportMessage()),
    PARAM_SET(JzNomFeatEnums.PARAM_SET, new JzNomDataReportMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
