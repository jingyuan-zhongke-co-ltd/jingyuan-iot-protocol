package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpHeartbeatModifyBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读取和修改心跳周期
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaHeartbeatModifyMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 心跳周期
        byte[] heartbeat = ByteBufUtils.readBytes(buf, 2);
        properties.put("heartbeat", BytesUtils.binary(10, heartbeat));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpHeartbeatModifyBo wpHeartbeatModifyBo = BeanUtil.toBean(properties, WpHeartbeatModifyBo.class);
        Short heartbeat = wpHeartbeatModifyBo.getHeartbeat();
        buf.writeBytes(BytesUtils.shortToBytes(heartbeat));
    }
}
