package com.jingyuan.iot.protocol.tcp.spr.wrx;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class SprWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new WritePropertyMessageReply();
        Map<String, Object> map = SprReportPropertyMessage.doRead(buf, decodeHolder);
        message.setProperties(map);

        String n = Convert.toStr(map.get(IotSprConstants.UPGRADE_N));
        if (StrUtil.isNotBlank(n)) {
            message.addHeader(IotSprConstants.UPGRADE_N, n);
        }
    }

    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }
}
