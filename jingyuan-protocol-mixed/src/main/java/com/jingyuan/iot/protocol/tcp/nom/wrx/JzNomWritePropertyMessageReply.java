package com.jingyuan.iot.protocol.tcp.nom.wrx;

import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JzNomWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new WritePropertyMessageReply();
        message.setDeviceId(decodeHolder.getDeviceId());
        Map<String, Object> map = JzNomReportPropertyMessage.initReadMap(buf, decodeHolder);

        FeatCodecs.read(decodeHolder, buf, map);
        message.setProperties(map);
    }

    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }
}
