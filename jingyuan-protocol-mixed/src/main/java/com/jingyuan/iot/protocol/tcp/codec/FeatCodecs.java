package com.jingyuan.iot.protocol.tcp.codec;

import cn.hutool.core.util.ObjUtil;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.cct.afn.CctFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.cz.codec.CzDzpFeatureCodecEnums;
import com.jingyuan.iot.protocol.tcp.lcp.afn.LcpFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.nom.codec.JzNomFeatureCodecEnums;
import com.jingyuan.iot.protocol.tcp.nom.tag.JzNomTagCodecEnums;
import com.jingyuan.iot.protocol.tcp.hlm.afn.HlmFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.rs.codec.RsFeatureCodecEnums;
import com.jingyuan.iot.protocol.tcp.runda.codec.RundaFeatureCodecEnums;
import com.jingyuan.iot.protocol.tcp.spr.codec.JoyoSprFeatureCodecEnums;
import io.netty.buffer.ByteBuf;
import reactor.core.Disposable;

import java.util.Arrays;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能与编解码处理类映射
 *
 * @author: cc
 * @date: 2024/9/23 11:18
 * @Version: V1.0
 */
public class FeatCodecs {

    private static final Map<IotFeat, TcpMessageCodec> ALL = new ConcurrentHashMap<>();

    /**
     * 注册编解码器
     *
     * @param featCodecs 功能编解码器
     * @return reactor.core.Disposable
     */
    public static Disposable register(Collection<FeatCodec> featCodecs) {
        featCodecs.forEach(FeatCodecs::register);
        return () -> {
            for (FeatCodec t : featCodecs) {
                ALL.remove(t.getFeat());
            }
        };
    }

    /**
     * 注册编解码器
     *
     * @param featCodec 功能编解码器
     * @return reactor.core.Disposable
     */
    public static Disposable register(FeatCodec featCodec) {
        if (ObjUtil.isNull(featCodec.getFeat()) || ObjUtil.isNull(featCodec.getCodec())) {
            return () -> {
            };
        }
        ALL.put(featCodec.getFeat(), featCodec.getCodec());
        return () -> ALL.remove(featCodec.getFeat());
    }

    public static Map<IotFeat, TcpMessageCodec> get() {
        return ALL;
    }

    public static Optional<TcpMessageCodec> lookup(IotFeat feat) {
        return Optional.ofNullable(ALL.get(feat));
    }

    static {
        // 分体阀控
        FeatCodecs.register(Arrays.asList(JoyoSprFeatureCodecEnums.values()));
        // 京兆无磁
        FeatCodecs.register(Arrays.asList(JzNomFeatureCodecEnums.values()));
        FeatCodecs.register(Arrays.asList(JzNomTagCodecEnums.values()));
        // 润达净水机
        FeatCodecs.register(Arrays.asList(RundaFeatureCodecEnums.values()));
        // 集采器
        FeatCodecs.register(Arrays.asList(CctFeatCodecEnums.values()));
        // 沧州定制协议
        FeatCodecs.register(Arrays.asList(CzDzpFeatureCodecEnums.values()));
        // 海龙马超声波协议
        FeatCodecs.register(Arrays.asList(HlmFeatCodecEnums.values()));
        // 京源 低功耗远程测控终端协议&自研测控终端
        FeatCodecs.register(Arrays.asList(LcpFeatCodecEnums.values()));
        // 乌鲁木齐 NB 协议
        FeatCodecs.register(Arrays.asList(RsFeatureCodecEnums.values()));
    }

    public static void read(TcpDecodeHolder decodeHolder, ByteBuf buf, Map<String, Object> properties) {
        read(decodeHolder.getFeature(), buf, properties);
    }

    public static void read(IotFeat feature, ByteBuf buf, Map<String, Object> properties) {
        TcpMessageCodec messageCodec = lookup(feature)
            .orElseThrow(() -> new IllegalArgumentException("不支持的消息类型" + feature));
        messageCodec.read(buf, properties);
    }

    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf, Map<String, Object> properties) {
        write(encodeHolder.getFeature(), buf, properties);
    }

    public static void write(IotFeat feature, ByteBuf buf, Map<String, Object> properties) {
        TcpMessageCodec messageCodec = lookup(feature)
            .orElseThrow(() -> new IllegalArgumentException("不支持的消息类型" + feature));
        messageCodec.write(buf, properties);
    }
}
