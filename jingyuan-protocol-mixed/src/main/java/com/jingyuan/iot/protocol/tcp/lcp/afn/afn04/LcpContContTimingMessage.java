package com.jingyuan.iot.protocol.tcp.lcp.afn.afn04;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn04.LcpTimingParameterBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Date;
import java.util.Map;

/**
 * 对时命令
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpContContTimingMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpTimingParameterBo bean = BeanUtil.toBean(properties, LcpTimingParameterBo.class);
        Date time = bean.getTime();
        String yyyyMMddHHmm = DateUtils.format(time, "yyMMddHHmm");
        buf.writeBytes(BytesUtils.hexToBytes(yyyyMMddHHmm));

    }

}

