package com.jingyuan.iot.protocol.tcp.cts.wxr;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cts.codec.CtsFeatureCodecEnums;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.HashMap;
import java.util.Map;

public class CtsWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    /**
     * 设置消息
     *
     * @param message 消息
     */
    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }

    /**
     * 获取消息
     *
     * @return T 消息
     */
    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }
    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new WritePropertyMessageReply();
        message.setDeviceId(decodeHolder.getDeviceId());
        Map<String, Object> map = initReadMap(decodeHolder);
        CtsFeatureCodecEnums.read(decodeHolder, buf, map);
        message.setProperties(map);
    }
    public static Map<String, Object> initReadMap(TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JOYO_CTS.getCode());
        return map;
    }
}
