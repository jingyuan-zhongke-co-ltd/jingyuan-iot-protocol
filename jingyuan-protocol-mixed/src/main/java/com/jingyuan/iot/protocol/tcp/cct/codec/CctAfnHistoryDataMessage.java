package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnHistoryRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 请求2类数据（历史数据）
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnHistoryDataMessage implements CctAfnMessage {

    @Override
    public void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String afn = String.valueOf(buf.readByte());
        CctAfnHistoryRwEnums historyRwEnum = CctAfnHistoryRwEnums.getByFeatCode(afn);
        decodeHolder.setFeature(historyRwEnum);

        FeatCodecs.read(historyRwEnum, buf, properties);
    }

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnHistoryRwEnums;
        FeatCodecs.write(feature, buf, properties);
    }
}
