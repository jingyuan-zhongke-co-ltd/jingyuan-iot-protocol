package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnSetCommWriteEnums;
import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnSetCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置参数
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnSetCommMessage implements LcpAfnMessage {

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof LcpAfnSetCommWriteEnums;

        FeatCodecs.write(feature, buf, properties);
    }
}
