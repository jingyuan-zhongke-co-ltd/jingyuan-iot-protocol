package com.jingyuan.iot.protocol.tcp.nom.tag;

import com.jingyuan.common.protocol.enums.nom.JzNomFeatTagEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 京兆无磁协议 水表周期数据
 *
 * @author: cc
 * @date: 2024/9/26 14:03
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum JzNomTagCodecEnums implements FeatCodec {

    /**
     * 水表周期数据
     */
    RESULT_CODE(JzNomFeatTagEnums.RESULT_CODE, new JzNomTagResultCodeMessage()),
    ALARM_DATA(JzNomFeatTagEnums.ALARM_DATA, new JzNomTagAlarmDataMessage()),
    CYCLE_DATA(JzNomFeatTagEnums.CYCLE_DATA, new JzNomTagCycleDataMessage()),
    REAL_TIME_DATA(JzNomFeatTagEnums.REAL_TIME_DATA, new JzNomTagRealTimeDataMessage()),
    INTENSIVE_DATA(JzNomFeatTagEnums.INTENSIVE_DATA, new JzNomTagIntensiveDataMessage()),
    TERMINAL_PARAM(JzNomFeatTagEnums.TERMINAL_PARAM, new JzNomTagTerminalParamMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
