package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 软重启
 *
 * @author: dr
 * @date: 2024-12-12
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteSoftReboot implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
    }
}
