package com.jingyuan.iot.protocol.tcp.wac.gauge.adjust;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 面板调节模式
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class WacAdjustPanelsCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 无意义，默认填充0x00 0x00
        buf.readBytes(2);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        // 无意义，默认填充0x00 0x00
        buf.writeBytes(new byte[]{0x00, 0x00});
    }
}
