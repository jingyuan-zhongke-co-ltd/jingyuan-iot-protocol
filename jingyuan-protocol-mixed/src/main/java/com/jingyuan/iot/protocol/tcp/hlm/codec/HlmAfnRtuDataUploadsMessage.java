package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnQueryCommRwEnums;
import com.jingyuan.common.protocol.enums.spr.JoyoSprUnitEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 低功耗 属性上报
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnRtuDataUploadsMessage implements HlmAfnMessage {

    @Override
    public void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        decodeHolder.setFn(BytesUtils.byteToHex(buf.readByte()));
//        数据包标识	Bin	1
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
        decodeHolder.setPacketId(logotype);
//        参数长度	Bin	1
        int parameterL = BytesUtils.byteToUnInt(buf.readByte());
//        Imei	Ascii	15
        byte[] imei = ByteBufUtils.readBytes(buf, 15);
        properties.put("imei", BytesUtils.bytesToAscStr(imei));
//        Iccid	Ascii	20
        byte[] iccid = ByteBufUtils.readBytes(buf, 20);
        properties.put("iccid", BytesUtils.bytesToAscStr(iccid));
//        Csq	Bin	1
        int signal = buf.readByte();
        properties.put("csq", signal);
//        电池电压	Bin	2	0.01V
        byte[] voltage = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(voltage); // 读取2字节电压
        properties.put("voltage", BytesUtils.bytesToShort(voltage) * 0.01); // 转换为实际电压值
//        版本号	Bin	4
        byte[] VersionNumber = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(VersionNumber);
//        BytesUtils.bytesToHex(VersionNumber)
        properties.put("version", BytesUtils.bytesToUnLong(VersionNumber));
//        抄表时间	Bcd	7	秒分时日月年
//        String freezeTime = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 7));
        properties.put("freezeTime", HlmDecodeUtils.timeByteRead(buf, 7));
//        表数据长度	Bin	1
        int L = BytesUtils.byteToUnInt(buf.readByte());
//        累计流量	float	4	M3
        byte[] readingBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(readingBytes);
        String reading = BytesUtils.binary(10, readingBytes);
        properties.put("reading", JoyoSprUnitEnums.KG_1.calculateToTon(reading));
//        瞬时流量	Float	4	M3/h
        float instantaneousFlow = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
        properties.put("instantFlowUnit", instantaneousFlow);
//        状态字	Int	4
        byte[] ST = ByteBufUtils.readBytes(buf, 4);
//        表时间	Bcd	7	秒分时日月年
//        String time = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 7));
        properties.put("deviceTime", HlmDecodeUtils.timeByteRead(buf, 7));
    }


    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof HlmAfnQueryCommRwEnums;
    }
}
