package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 申请设备ID：未申请ID的电脑板，上电后会携带IMEI去申请ID，该ID为6位数。未分配时每10秒申请一次，申请到ID后，不在发送。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaIdApplyMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 净水机ID
        byte[] bytes = ByteBufUtils.readBytes(buf, 4);
        int wpId = BytesUtils.bytesToInt(bytes);
        if (wpId != 0) {
            properties.put(IotConstants.RUNDA_WP_ID, wpId);
        }
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x04);
        Object wpId = properties.get(IotConstants.RUNDA_WP_ID);
        if (ObjectUtil.isNull(wpId)) {
            buf.writeBytes(new byte[]{0x00, 0x00, 0x00, 0x00});
        } else {
            String wpIdHex = StrUtil.fillBefore(HexUtils.toHex(Convert.toInt(wpId)), '0', 8);
            buf.writeBytes(BytesUtils.hexToBytes(wpIdHex));
        }
    }
}
