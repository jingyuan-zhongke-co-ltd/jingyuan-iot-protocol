package com.jingyuan.iot.protocol.tcp.cz.codec;

import com.jingyuan.common.protocol.enums.czdzp.CzDzpFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * @author 李立强
 */

@Slf4j
@Getter
@AllArgsConstructor
public enum CzDzpFeatureCodecEnums implements FeatCodec {
    /**
     * 数据上报
     */
    DATA_REPORT(CzDzpFeatEnums.DATA_REPORT, new CzDzpDataReportMessage()),
    /**
     * 控阀
     */
    VALVE_STATE(CzDzpFeatEnums.VALVE_STATE, new CzDzpValveStateMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
