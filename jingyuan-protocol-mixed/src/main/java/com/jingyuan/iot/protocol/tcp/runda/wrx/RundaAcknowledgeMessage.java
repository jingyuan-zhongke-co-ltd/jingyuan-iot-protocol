package com.jingyuan.iot.protocol.tcp.runda.wrx;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.AcknowledgeDeviceMessage;

import java.util.Map;
import java.util.Optional;

/**
 * 应答消息
 *
 * @author: cc
 * @date: 2024/5/8 14:02
 * @Version: V1.0
 */
public class RundaAcknowledgeMessage implements TcpMessageAdapter<AcknowledgeDeviceMessage> {

    private AcknowledgeDeviceMessage message;

    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
        Optional<Object> header = message.getHeader(IotConstants.RUNDA_WP_ID);
        Map<String, Object> map = header.isPresent() ? MapUtil.of(IotConstants.RUNDA_WP_ID, Convert.toStr(header.get())) : MapUtil.empty();
        FeatCodecs.write(encodeHolder, buf, map);
    }

    @Override
    public void setMessage(AcknowledgeDeviceMessage message) {
        this.message = message;
    }

    @Override
    public AcknowledgeDeviceMessage getMessage() {
        return message;
    }
}
