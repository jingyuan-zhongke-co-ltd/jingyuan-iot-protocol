package com.jingyuan.iot.protocol.http.codec;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.constant.redis.PrefixIotConstants;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.model.north.event.NorthForwardEventBo;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotTopicConstants;
import com.jingyuan.common.utils.collection.MapUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.common.utils.stream.StreamUtils;
import com.jingyuan.iot.protocol.http.holder.HttpDecodeRedisHolder;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.event.EventBus;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.List;
import java.util.Map;

/**
 * 北向数据消息转发
 *
 * @author cc
 * @version V1.0
 * @date 2025/02/17 09:32
 */
@Slf4j
public class NorthForwardUtil {

    /**
     * 数据转发
     *
     * @param payload 待转发数据
     */
    public static Mono<Map<String, Object>> forward(Map<String, Object> payload, HttpDecodeRedisHolder decodeHolder) {
        if (MapUtil.isEmpty(payload)) {
            return Mono.just(payload);
        }

        ReactiveStringRedisTemplate redisTemplate = decodeHolder.getRedisTemplate();
        EventBus eventBus = decodeHolder.getEventBus();

        NorthChannelEnums northChannel = decodeHolder.getNorthChannel();

        NorthForwardEventBo forwardEventBo = NorthForwardEventBo.of();
        forwardEventBo.setRequestParams(JsonUtils.toJsonString(payload));
        forwardEventBo.setNorthPlatform(northChannel.getCode());
        forwardEventBo.setImei(parseImei(payload, northChannel));

        return redisTemplate
            .opsForSet()
            .members(PrefixIotConstants.IOT_NORTH_FORWARD_URLS)
            .publishOn(Schedulers.boundedElastic())
            .flatMap(url -> {
                forwardEventBo.setUrl(url);
                return WebClient
                    .create(url)
                    .post()
                    .contentType(MediaType.APPLICATION_JSON)
                    .bodyValue(JsonUtils.toJsonString(payload))
                    .retrieve()
                    .toEntity(String.class)
                    .flatMap(response -> {
                        String jsonRes = JsonUtils.toJsonString(response);
                        forwardEventBo.setResponseResult(jsonRes);
                        forwardEventBo.setRequestState(true);
                        return eventBus.publish(IotTopicConstants.NORTH_FORWARD_LOGS, forwardEventBo);
                    })
                    .onErrorResume(err -> {
                        String message = err.getMessage();
                        forwardEventBo.setResponseResult(message);
                        forwardEventBo.setRequestState(false);
                        return eventBus.publish(IotTopicConstants.NORTH_FORWARD_LOGS, forwardEventBo);
                    });
            })
            .then(Mono.just(payload));
    }

    /**
     * 从北向消息解析 IMEI
     *
     * @param payload
     * @param northChannel
     * @return java.lang.String
     */
    @SuppressWarnings("all")
    private static String parseImei(Map<String, Object> payload, NorthChannelEnums northChannel) {
        if (NorthChannelEnums.onenet.equals(northChannel)) {
            // ONENET
            Object msg = payload.get("msg");
            if (msg instanceof List) {
                List<Map> maps = JsonUtils.parseArray(JsonUtils.toJsonString(msg), Map.class);
                return CollUtil.isEmpty(maps) ? StrUtil.EMPTY : StreamUtils.join(maps, map -> Convert.toStr(map.get(IotConstants.IMEI_LOWER)));
            } else {
                Map map = JsonUtils.parseObject(JsonUtils.toJsonString(msg), Map.class);
                return MapUtil.isEmpty(map) ? StrUtil.EMPTY : Convert.toStr(map.get(IotConstants.IMEI_LOWER));
            }
        } else if (NorthChannelEnums.ctwing.equals(northChannel)) {
            // CTWING
            return Convert.toStr(MapUtils.getValue(payload, IotConstants.IMEI_UPPER));
        } else {
            return StrUtil.EMPTY;
        }
    }
}
