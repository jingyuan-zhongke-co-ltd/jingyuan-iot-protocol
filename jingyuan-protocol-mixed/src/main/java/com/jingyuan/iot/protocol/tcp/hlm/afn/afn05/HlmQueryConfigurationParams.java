package com.jingyuan.iot.protocol.tcp.hlm.afn.afn05;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询配置下发参数
 *
 * @author: dr
 * @date: 2024-12-03
 * @Version: V1.0
 */
@Slf4j
public class HlmQueryConfigurationParams implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        数据包标识	BIN	0 ~255	1	设备收到下发命令包中的数据包标识
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
//        协议ID	BIN		1	0B表示抄京源大口径热表(NB)，
        String protocolId = BytesUtils.byteToHex(buf.readByte());
        properties.put("protocolId", protocolId);
//        设备地址	BCD		4	设备地址
        byte[] deviceAdd = ByteBufUtils.readBytes(buf, 4);
        properties.put("deviceAdd", BytesUtils.bcdToStr(deviceAdd));
//        上传模式	BIN	0,1	1	0x00：采后传（设备采集完数据立即唤醒上传数据）
        properties.put("uploadMode", BytesUtils.byteToHex(buf.readByte()));
//        开始时间	BCD	HH MM	2	只在时段传时有用，非时段传时，该值不能为00 00。 如6点10分，则此处为06 10。
        String startTime = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 2));
        properties.put("startTime", startTime.substring(0, 2) + ":" + startTime.substring(2, 4));
//        结束时间	BCD	HH MM	2	只在时段传时有用，非时段传时，该值不能为00 00。  如6点10分，则此处为06 10。
        String endTime = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 2));
        properties.put("endTime", endTime.substring(0, 2) + ":" + endTime.substring(2, 4));
//        采集模式	BIN	0,1	1	0x00：周期（设备到了采集周期后，采集存储数据）0x01：定时（设备到了定时时间后，采集存储数据）
        properties.put("collectionMode", BytesUtils.byteToHex(buf.readByte()));
//        采集间隔	BIN	5-1440（分）	2
        properties.put("collectionInterval", BytesUtils.bytesToUnLong(ByteBufUtils.readBytes(buf, 2)));
//        定时1	BCD	HH MM	2	06 10表示6点10分采集，
        String Timing1 = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 2));
//        定时2	BCD	HH MM	2	06 10表示6点10分采集，
        String Timing2 = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 2));
        properties.put("schedule1",  Timing1.substring(0, 2) + ":" + Timing1.substring(2, 4));
        properties.put("schedule2",  Timing2.substring(0, 2) + ":" + Timing2.substring(2, 4));
//        Ip或域名	ASCII	不足补0x00	32	如IP是221.229.214.202。

//        则此处为32 32 31 2E 32 32 39 2E 32 31 34 2E 32 30 32 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
//        端口	BIN	0-65535	2	如端口是5683.则此处为33 16
        String ip = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 32));
        long port = BytesUtils.bytesToUnLong(ByteBufUtils.readBytes(buf, 2));
        properties.put("ip", ip);
        properties.put("port", port);
    }
}
