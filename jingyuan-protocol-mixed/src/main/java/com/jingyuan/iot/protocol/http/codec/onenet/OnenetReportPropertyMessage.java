package com.jingyuan.iot.protocol.http.codec.onenet;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.constant.IotNorthConstants;
import com.jingyuan.common.flowable.enums.NorthFeatureTypes;
import com.jingyuan.common.flowable.enums.NorthLifecycleState;
import com.jingyuan.common.flowable.enums.NorthMessageTypes;
import com.jingyuan.common.flowable.model.north.event.NorthLogEventBo;
import com.jingyuan.common.flowable.model.onenet.OnenetCmdCallbackDto;
import com.jingyuan.common.flowable.model.onenet.OnenetDataPointDto;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotTopicConstants;
import com.jingyuan.common.protocol.enums.IotMessageEvents;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.http.HttpDecodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.message.adapter.HttpMessageAdapter;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.http.codec.NorthForwardUtil;
import com.jingyuan.iot.protocol.http.codec.ctwing.CtwingReportPropertyMessage;
import com.jingyuan.iot.protocol.http.holder.HttpDecodeRedisHolder;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.event.EventMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * ONENET 平台设备数据上报消息
 *
 * @author: cc dr
 * @date: 2024/9/10 17:17
 * @Version: V1.0
 */
@Slf4j
public class OnenetReportPropertyMessage implements HttpMessageAdapter {

    @Override
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public Flux<DeviceMessage> read(HttpDecodeHolder decodeHolder) {
        Map<String, Object> payloadMap = JsonUtils.parseObject(decodeHolder.getPayload(), Map.class);
        if (CollUtil.isEmpty(payloadMap)) {
            return Flux.empty();
        }

        return NorthForwardUtil
            // 北向数据消息转发
            .forward(payloadMap, (HttpDecodeRedisHolder) decodeHolder)
            .switchIfEmpty(Mono.defer(Mono::empty))
            .map(map -> map.get("msg"))
            .flatMapMany(msg -> {
                if (msg instanceof List) {
                    return Flux.fromIterable(JsonUtils.parseArray(JsonUtils.toJsonString(msg), Map.class));
                } else {
                    return Flux.just(JsonUtils.parseObject(JsonUtils.toJsonString(msg), Map.class));
                }
            })
            .flatMap(map -> {
                String imei = Convert.toStr(map.get(IotConstants.IMEI_LOWER));
                NorthFeatureTypes type = NorthFeatureTypes.getByCode(Convert.toStr(map.get("type")));
                if (NorthFeatureTypes.LIFECYCLE_DATA.equals(type)) {
                    OnenetDataPointDto dataPointDto = BeanUtil.toBean(map, OnenetDataPointDto.class);
                    // 生命周期数据
                    return decodeHolder
                        .getDecodeContext()
                        .getDevice(imei)
                        .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("设备：【" + imei + "】不存在或未启用"))))
                        .flatMapMany(operator -> {
                            DeviceMessage deviceMessage = NorthLifecycleState.messageInstance(imei, dataPointDto.getDevId(), dataPointDto.getStatus(), NorthChannelEnums.onenet);

                            return decodeHolder
                                .getEventBus()
                                .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS,
                                         NorthLogEventBo.noNorEvent(NorthChannelEnums.onenet, true, NorthMessageTypes.ONLINE_OFFLINE.getCode(),
                                                                    JsonUtils.toJsonString(payloadMap), JsonUtils.toJsonString(deviceMessage), imei))
                                .thenReturn(deviceMessage);
                        });
                } else if (NorthFeatureTypes.DATANODE_DATA.equals(type)) {
                    OnenetDataPointDto dataPointDto = BeanUtil.toBean(map, OnenetDataPointDto.class);
                    // 数据点数据
                    return getFrameHex(dataPointDto)
                        .filter(uploadFrame -> StrUtil.isAllNotBlank(uploadFrame, imei))
                        .flatMap(uploadFrame -> {
                            // 由于 Onenet 可能同时发送多条消息，是否为相同类型的消息不确定，稳妥起见每条消息之间采用自己的 decodeHolder
                            HttpDecodeHolder holder = HttpDecodeHolder.of(decodeHolder.getPayload(), decodeHolder.getMessageType(), decodeHolder.getNorthChannel(), decodeHolder.getDecodeContext());
                            JoyoIotVendor vendor = JoyoIotVendor.getByRawData(uploadFrame.toLowerCase());
                            holder.setIotVendor(vendor);
                            holder.setEventBus(decodeHolder.getEventBus());
                            return CtwingReportPropertyMessage
                                .doRead(holder, payloadMap, uploadFrame, imei, "", NorthChannelEnums.onenet, true)
                                .map(deviceMessage -> {
                                    MessageHelper.addHeadVendor(deviceMessage, vendor.getCode());
                                    return deviceMessage;
                                });
                        });
                } else if (NorthFeatureTypes.CACHED_COMMANDS_UP.equals(type)) {
                    // 命令回调
                    OnenetCmdCallbackDto callbackDto = BeanUtil.toBean(map, OnenetCmdCallbackDto.class);
                    EventMessage eventMessage = new EventMessage()
                        .event(IotMessageEvents.CMD_CALLBACK.getCode())
                        .messageId(callbackDto.getCmdId())
                        .data(callbackDto)
                        .timestamp(System.currentTimeMillis());
                    eventMessage.thingId(DeviceThingType.device, imei);
                    eventMessage.addHeader(IotConstants.IMEI_LOWER, imei);
                    eventMessage.addHeader(IotNorthConstants.NDE_ID, callbackDto.getDevId());
                    return Flux.just(eventMessage);
                } else {
                    return decodeHolder
                        .getEventBus()
                        .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS,
                                 NorthLogEventBo.noNorEvent(NorthChannelEnums.onenet, false, NorthMessageTypes.UNKNOWN.getCode(),
                                                            JsonUtils.toJsonString(payloadMap), "暂不支持解析的消息类型：" + type, imei))
                        .then(Mono.empty());
                }
            });
    }


    /**
     * 获取请求参数中的数据帧
     *
     * @param dataPointDto 数据点数据
     * @return java.lang.String
     */
    private static Flux<String> getFrameHex(OnenetDataPointDto dataPointDto) {
        Object valueObj = dataPointDto.getValue();
        if (valueObj instanceof String) {
            // 处理十六进制字符串
            return Flux.just((String) valueObj);
        } else if (valueObj instanceof List) {
            // 处理整数数组
            return Flux.just(convertListToHex((List<?>) valueObj));
        } else {
            return Flux.empty();
        }
    }

    private static String convertListToHex(List<?> list) {
        if (list.isEmpty()) {
            return StrUtil.EMPTY;
        }

        byte[] byteArray = new byte[list.size()];
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof Integer) {
                byteArray[i] = ((Integer) list.get(i)).byteValue();
            } else {
                throw new IllegalArgumentException("List elements must be of type Integer");
            }
        }
        return BytesUtils.bytesToHex(byteArray);
    }
}
