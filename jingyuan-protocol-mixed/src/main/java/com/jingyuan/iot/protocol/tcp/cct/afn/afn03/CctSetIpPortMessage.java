package com.jingyuan.iot.protocol.tcp.cct.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn03.CctSetIpPortBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置集采器IP和端口
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctSetIpPortMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctSetIpPortBo setIpPortBo = BeanUtil.toBean(properties, CctSetIpPortBo.class);
        String ip = setIpPortBo.getIp();
        for (String s : StrUtil.split(ip, CharPool.DOT)) {
            buf.writeByte(Integer.parseInt(s));
        }
        byte[] bytes = BytesUtils.intToBytes(setIpPortBo.getPort());
        buf.writeBytes(new byte[]{bytes[3], bytes[2]});
    }
}
