package com.jingyuan.iot.protocol.tcp.rs.codec;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.rs.RsFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author 李立强
 */

@Slf4j
@Getter
@AllArgsConstructor
public enum RsFeatureCodecEnums implements FeatCodec {
    /**
     * 数据上报
     */
    DATA_REPORT(RsFeatEnums.DATA_REPORT, new RsDataReportMessage()),
    /**
     * 阀控应答
     */
    VALVE_STATE(RsFeatEnums.VALVE_STATE, new RsValveMessage()),
    /**
     * 控阀应答
     */
    VALVE_STATE_REPLY(RsFeatEnums.VALVE_STATE_REPLY, new RsValveReplyMessage())
    ;

    private static final Map<String, RsFeatureCodecEnums> VALUES = new HashMap<>();

    private final IotFeat feat;

    private final TcpMessageCodec codec;


    static {
        Arrays.stream(values()).forEach(val -> VALUES.put(val.getFeat().getFeatCode(), val));
    }


    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        RsFeatureCodecEnums featureCodecEnum = VALUES.get(feature.getFeatCode());
        Assert.notNull(featureCodecEnum, "发送消息类型不正确");
        TcpMessageCodec messageCodec = featureCodecEnum.getCodec();
        String deviceId = encodeHolder.getDeviceMessage().getDeviceId();
        buf.writeBytes(BytesUtils.hexToBytes(feature.getFeatCode()));
        buf.writeBytes(BytesUtils.hexToBytes(deviceId));
        messageCodec.write(buf, properties);
    }

}
