package com.jingyuan.iot.protocol.tcp.cct.afn.afn0a;

import cn.hutool.core.text.CharPool;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 设置集采器IP和端口
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctQueryIpPortMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // IP 地址
        byte[] ipBytes = ByteBufUtils.readBytes(buf, 4);
        properties.put("ip", BytesUtils.binary(10, ipBytes[0]) + CharPool.DOT +
            BytesUtils.binary(10, ipBytes[1]) + CharPool.DOT +
            BytesUtils.binary(10, ipBytes[2]) + CharPool.DOT +
            BytesUtils.binary(10, ipBytes[3]));

        // 端口号
        byte[] portBytes = ByteBufUtils.readBytes(buf, 2);
        properties.put("port", BytesUtils.bytesToShort(portBytes, ByteOrder.LITTLE_ENDIAN));
    }
}
