package com.jingyuan.iot.protocol.tcp.nom.tag;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.math.ArithmeticUtil;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 京兆无磁-报警数据
 *
 * @author: cc
 * @date: 2024/9/27 14:22
 * @Version: V1.0
 */
@Slf4j
public class JzNomTagAlarmDataMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        recursionAlarm(buf.readShortLE(), buf, properties);
    }

    /**
     * 递归处理报警数据
     */
    public static void recursionAlarm(short lessLength, ByteBuf buf, Map<String, Object> properties) {
        if (lessLength <= 0) {
            return;
        }
        // 数据ID
        int dataId = buf.readByte();
        switch (dataId) {
            case 0:
                // 低电压报警
                int voltageState = buf.readByte();
                properties.put("voltageState", voltageState);
                lessLength -= 2;
                break;
            case 1:
                // 磁干扰报警
                int magneticState = buf.readByte();
                properties.put("magneticState", magneticState);
                lessLength -= 2;
                break;
            case 2:
                // 过流报警
                int overCurState = buf.readByte();
                properties.put("overCurState", overCurState);
                if (overCurState == 1) {
                    // 过流时间
                    properties.put("overCurTime", JzNomTagRealTimeDataMessage.readDate(buf));
                    // 过流流量
                    properties.put("overCurValue", ArithmeticUtil.div(buf.readUnsignedShort(), 1000, 3));
                } else {
                    buf.readBytes(8);
                }
                lessLength -= 10;
                break;
            case 3:
                // 反流报警
                int reverseState = buf.readByte();
                properties.put("reverseState", reverseState);
                if (reverseState == 1) {
                    // 反流时间
                    properties.put("reverseTime", JzNomTagRealTimeDataMessage.readDate(buf));
                    // 反流流量
                    properties.put("reverseValue", ArithmeticUtil.div(buf.readUnsignedShort(), 1000, 3));
                } else {
                    buf.readBytes(8);
                }
                lessLength -= 10;
                break;
            case 4:
                // 光扰报警
                int lightScrambleState = buf.readByte();
                properties.put("lightScrambleState", lightScrambleState);
                lessLength -= 2;
                break;
            case 5:
                // 脉冲报警
                int pulseState = buf.readByte();
                properties.put("pulseState", pulseState);
                lessLength -= 2;
                break;
            case 6:
                // 机电分离报警
                int eleSeparateState = buf.readByte();
                properties.put("eleSeparateState", eleSeparateState);
                lessLength -= 2;
                break;
            default:
                lessLength -= 1;
                break;
        }
        recursionAlarm(lessLength, buf, properties);
    }
}
