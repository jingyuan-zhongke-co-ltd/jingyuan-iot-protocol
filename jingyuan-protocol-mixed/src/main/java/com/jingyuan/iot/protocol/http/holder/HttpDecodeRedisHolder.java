package com.jingyuan.iot.protocol.http.holder;

import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.holder.http.HttpDecodeHolder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.jetlinks.core.message.codec.MessageDecodeContext;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;

/**
 * @author: cc
 * @date: 2024/12/6 16:33
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class HttpDecodeRedisHolder extends HttpDecodeHolder {

    public static HttpDecodeRedisHolder of() {
        return new HttpDecodeRedisHolder();
    }

    public static HttpDecodeRedisHolder of(byte[] payload, IotMessageType messageType, NorthChannelEnums northChannel) {
        HttpDecodeRedisHolder decodeHolder = HttpDecodeRedisHolder.of();
        decodeHolder.setPayload(payload);
        decodeHolder.setMessageType(messageType);
        decodeHolder.setNorthChannel(northChannel);
        return decodeHolder;
    }

    public static HttpDecodeRedisHolder of(byte[] payload, IotMessageType messageType, NorthChannelEnums northChannel, MessageDecodeContext decodeContext) {
        HttpDecodeRedisHolder decodeHolder = HttpDecodeRedisHolder.of(payload, messageType, northChannel);
        decodeHolder.setDecodeContext(decodeContext);
        return decodeHolder;
    }

    /**
     * ReactiveStringRedisTemplate
     */
    private ReactiveStringRedisTemplate redisTemplate;
}
