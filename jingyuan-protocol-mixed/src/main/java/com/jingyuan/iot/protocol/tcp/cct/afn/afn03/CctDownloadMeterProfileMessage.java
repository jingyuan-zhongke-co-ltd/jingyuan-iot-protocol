package com.jingyuan.iot.protocol.tcp.cct.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn03.CctDownloadMeterProfile;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 向集采器下载表档案
 *
 * @author: dr
 * @date: 2024-12-30 16:36:27
 * @Version: V1.0
 */
@Slf4j
public class CctDownloadMeterProfileMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctDownloadMeterProfile bean = BeanUtil.toBean(properties, CctDownloadMeterProfile.class);
        // 计量表类型
        buf.writeByte(bean.getMeterType());
//        计量表序号
        buf.writeByte(bean.getMeterSerialNum());
//        运行标志
        buf.writeBytes(BytesUtils.hexToBytes(bean.getRunFlag()));
//            边界值
        buf.writeByte(bean.getBoundary());
        // 采集器通信地址
        buf.writeBytes(new byte[]{bean.getCoolAddr().byteValue(), 0x00, 0x00, 0x00, 0x00, 0x00});
    }
}
