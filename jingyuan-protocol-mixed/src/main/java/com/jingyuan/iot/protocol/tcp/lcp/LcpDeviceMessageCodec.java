package com.jingyuan.iot.protocol.tcp.lcp;

import cn.hutool.core.collection.CollUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.lcp.LcpAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.lcp.fn.*;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.wrx.LcpMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.Value;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.property.WritePropertyMessage;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import static com.jingyuan.common.protocol.constant.IotConstants.CACHED_COMMANDS;


/**
 * 京源测控终端通讯协议
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata JOYO_LCP_CONFIG = new DefaultConfigMetadata(
        "京源低功耗终端TCP配置"
        , "京源低功耗终端TCP配置信息");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }


    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        LcpTcpDecodeHolder decodeHolder = LcpMessageTypeEnums.read(payload);
        DeviceMessage message = decodeHolder.getDeviceMessage();
        String vendorCode = MessageHelper.getHeadVendorThrow(message);

        return context.getDevice(message.getDeviceId())
                      .flatMap(deviceOperator -> deviceOperator
                          .setConfig(IotConstants.VENDOR_CODE, vendorCode)
                          .thenReturn(deviceOperator))
                      .flatMapMany(deviceOperator ->
                                       // 判断消息是否需要确认回复
                                       conWrite(context, decodeHolder)
                                           .collectList()
                                           // 缓存的指令写出
                                           .flatMapMany(list -> cachedWrite(context, message)
                                               .collectList()
                                               .flatMapMany(messages -> {
                                                   messages.stream()
                                                           .filter(msg -> msg != message)
                                                           .forEach(list::add);
                                                   return Flux.fromIterable(list);
                                               })));
    }


    /**
     * 确认消息写出
     *
     * @param context      消息上下文
     * @param decodeHolder 消息解析包装类
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> conWrite(MessageDecodeContext context, LcpTcpDecodeHolder decodeHolder) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        CctSeqConEnums seqCon = decodeHolder.getSeqCon();
        if (CctSeqConEnums.no_confirm.equals(seqCon)) {
            return Flux.just(message);
        }

        LcpTcpEncodeHolder messageEncodeHolder = LcpTcpEncodeHolder.of();
        String deviceId = message.getDeviceId();
        messageEncodeHolder.setDeviceId(deviceId);
        messageEncodeHolder.setAfn(LcpAfnFeatureEnums.confirm);
        messageEncodeHolder.setL(LcpAfnConfirmRwEnums.confirm.getLength());
        messageEncodeHolder.setFeature(LcpAfnConfirmRwEnums.confirm);

        WritePropertyMessage writePropertyMessage = IotMessageType.writePmInstance(deviceId);
        // 添加厂商信息
        IotFeat feature = LcpAfnConfirmRwEnums.confirm;
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(writePropertyMessage);

        messageEncodeHolder.setDeviceMessage(writePropertyMessage);
        return doSendAfterDecode(context, messageEncodeHolder, message);
    }


    /**
     * 缓存的消息写出
     *
     * @param context 消息上下文
     * @param message 设备消息
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    @SuppressWarnings("unchecked")
    private Flux<DeviceMessage> cachedWrite(MessageDecodeContext context, DeviceMessage message) {
        return context
            .getDevice(message.getDeviceId())
            .flatMapMany(deviceOperator ->
                             deviceOperator
                                 .getConfig(CACHED_COMMANDS)
                                 .switchIfEmpty(Mono.defer(() -> Mono.just(Value.simple(new HashMap<>()))))
                                 .map(val -> val.as(Map.class))
                                 .flatMapMany(map -> {
                                     if (CollUtil.isEmpty(map)) {
                                         return Flux.just(message);
                                     }
                                     Map<String, Map<String, DeviceMessage>> cachedMap = (Map<String, Map<String, DeviceMessage>>) map;
                                     return Flux.fromIterable(cachedMap.keySet())
                                                .map(cachedMap::get)
                                                .flatMap(cached -> {
                                                    if (CollUtil.isEmpty(cached)) {
                                                        return Flux.just(message);
                                                    }
                                                    return Flux.fromIterable(cached.keySet())
                                                               .flatMap(funcId -> {
                                                                   DeviceMessage deviceMessage = cached.get(funcId);
                                                                   LcpTcpEncodeHolder encodeHolder = getLcpTcpEncodeHolder(deviceMessage);
                                                                   return doSendAfterDecode(context, encodeHolder, message);
                                                               })
                                                               .delayElements(Duration.ofMillis(100))
                                                               .switchIfEmpty(Flux.defer(() -> Flux.just(message)));
                                                });
                                 })
                                 .collectList()
                                 .flatMapMany(messages -> {
                                     if (messages.size() > 1) {
                                         return deviceOperator.setConfig(CACHED_COMMANDS, new HashMap<>())
                                                              .thenMany(Flux.fromIterable(messages));
                                     } else {
                                         return Flux.fromIterable(messages);
                                     }
                                 })
            );
    }

    private static LcpTcpEncodeHolder getLcpTcpEncodeHolder(DeviceMessage deviceMessage) {
        IotLcpFunc feature = (IotLcpFunc) MessageHelper.getHeadFeatThrow(deviceMessage);
        LcpTcpEncodeHolder encodeHolder = LcpTcpEncodeHolder.of();
        encodeHolder.setFeature(feature);
        encodeHolder.setDeviceId(deviceMessage.getDeviceId());
        encodeHolder.setDeviceMessage(deviceMessage);
        encodeHolder.setL(feature.getLength());
        if (feature instanceof LcpAfnSetCommWriteEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.set_comm);
        } else if (feature instanceof LcpAfnContCommWriteEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.cont_comm);
        } else if (feature instanceof LcpAfnLinkDetectionReadEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.link_detect);
        } else if (feature instanceof LcpAfnQueryCommRwEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.query_comm);
        } else if (feature instanceof LcpAfnOneRwEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.realtime_data);
        } else if (feature instanceof LcpAfnTwoRwEnums) {
            encodeHolder.setAfn(LcpAfnFeatureEnums.history_data);
        }
        return encodeHolder;
    }


    /**
     * 解码后消息发送
     *
     * @param context       消息上下文
     * @param encodeHolder  消息编码包装类
     * @param decodeMessage 解码后消息
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> doSendAfterDecode(MessageDecodeContext context, LcpTcpEncodeHolder encodeHolder, DeviceMessage decodeMessage) {
        return doEncode(encodeHolder)
            .flatMap(encodedMessage -> ((FromDeviceMessageContext) context).getSession().send(encodedMessage))
            .flatMapMany(aBoolean -> {
                if (aBoolean) {
                    return Flux.just(decodeMessage, encodeHolder.getDeviceMessage());
                } else {
                    return Flux.just(decodeMessage);
                }
            });
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        LcpTcpEncodeHolder messageEncodeHolder = LcpTcpEncodeHolder.of();
        messageEncodeHolder.setDeviceMessage(deviceMessage);
        return doEncode(messageEncodeHolder);
    }


    /**
     * 执行编码操作
     *
     * @param messageEncodeHolder 消息编码包装类
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(LcpTcpEncodeHolder messageEncodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        LcpMessageTypeEnums.write(messageEncodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
