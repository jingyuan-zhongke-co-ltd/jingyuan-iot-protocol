package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;


/**
 * NB报警帧数据
 *
 * @author: cc
 * @date: 2024/4/15 15:58
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprAlarmMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 报警时间
        String alarmTime = "20" + ByteBufUtils.readBytesToHex(buf, 6);
        properties.put("alarmTime", DateUtil.parse(alarmTime, DatePattern.PURE_DATETIME_PATTERN));

        // 状态位
        byte state = buf.readByte();
        // 阀门状态
        properties.put("valveState", state & 0b00000001);
        // 阀门故障
        properties.put("valveFailure", state & 0b00000100);
        // 温度状态
        properties.put("tempState", state & 0b00100000);
        // 电池状态
        properties.put("voltageState", state & 0b01000000);
        // 攻击状态
        properties.put("attackState", state & 0b10000000);
    }
}
