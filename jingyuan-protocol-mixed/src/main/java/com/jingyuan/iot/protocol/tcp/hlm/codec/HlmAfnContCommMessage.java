package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnControlRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控制命令
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnContCommMessage implements HlmAfnMessage {

    @Override
    public void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = BytesUtils.byteToHex(buf.readByte());
        decodeHolder.setFn(fn);
        decodeHolder.setFeature(HlmAfnControlRwEnums.getByFeatCode(fn));

        FeatCodecs.read(decodeHolder, buf, properties);
    }
    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof HlmAfnControlRwEnums;
        buf.writeByte(0b00000001);
        FeatCodecs.write(feature, buf, properties);
    }
}
