package com.jingyuan.iot.protocol.tcp.wac.wxr;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.DataDirectionEnums;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.wac.WacCommState;
import com.jingyuan.common.protocol.enums.wac.WacGaugeTypes;
import com.jingyuan.common.protocol.enums.wac.WacOperationTypes;
import com.jingyuan.common.protocol.enums.wac.gauge.IotWacFunc;
import com.jingyuan.common.protocol.enums.wac.gauge.WacGaugeFeats;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;

/**
 * 调节阀协议解析入口
 *
 * @author: cc
 * @date: 2024/12/4 15:58
 * @Version: V1.0
 */
@Slf4j
public enum WacMessageTypeEnums {
    /**
     * 消息类型
     */
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), WacReportPropertyMessage::new),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), WacWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), WacWritePropertyMessageReply::new),
    ;

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    WacMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, WacMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (WacMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(WacTcpEncodeHolder encodeHolder, ByteBuf buf) {
        buf.writeBytes(new byte[]{(byte) 0xFE, (byte) 0xFE, (byte) 0xFE});
        buf.writeByte(0x68);
        buf.writeBytes(WacGaugeTypes.getBytesByVendor(encodeHolder.getVendor()));

        DeviceMessage message = encodeHolder.getDeviceMessage();
        // 消息序列号
        if (ObjUtil.isNull(encodeHolder.getSerId())) {
            int serId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
                MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
            encodeHolder.setSerId(serId);
        }

        // 阀门地址
        byte[] bytes = BytesUtils.hexToBytes(message.getDeviceId());
        BytesUtils.bytesReverse(bytes);
        buf.writeBytes(bytes);
        // 厂家代码
        buf.writeBytes(new byte[]{0x00, 0x00, 0x00});
        // 控制码
        IotWacFunc feature = (IotWacFunc) encodeHolder.getFeature();
        WacOperationTypes operationType = feature.getOperationType();
        buf.writeByte(operationType.getContCodeByDir(encodeHolder.getDir()));
        // 数据长度
        buf.writeByte(feature.getLength());
        // 数据域
        TcpMessageAdapter<DeviceMessage> messageAdapter = writeProperty.forTcp.get();
        messageAdapter.setMessage(message);
        messageAdapter.write(buf, encodeHolder);
        // 校验码
        int computeSum = ChecksumUtils.computeSumSingle(buf, 3, buf.writerIndex());
        buf.writeByte(computeSum);
        // 帧结束符
        buf.writeByte(0x16);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static WacTcpDecodeHolder read(ByteBuf buf) {
        String rawDataHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf));
        Assert.isTrue(StrUtil.isNotBlank(rawDataHex), "原始数据帧不能为空");

        WacTcpDecodeHolder decodeHolder = WacTcpDecodeHolder.of();
        decodeHolder.setRawDataHex(rawDataHex);
        // FE FE FE
        buf.readBytes(3);
        // 设备消息解析
        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        IotFeat feat = decodeHolder.getFeature();
        if (ObjUtil.isNotNull(feat)) {
            // 设备消息日志添加功能信息
            feat.setMessageHeaders(deviceMessage);
        }
        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    private static void decodeControlCode(byte controlCode, WacTcpDecodeHolder decodeHolder) {
        // 数据传送方向
        decodeHolder.setDir(DataDirectionEnums.getByControlCode(controlCode));
        // 通讯是否正常标志
        decodeHolder.setCommState(WacCommState.getByControlCode(controlCode));
        // 功能定义区
        decodeHolder.setOperationType(WacOperationTypes.getByControlCode(controlCode));
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf,
                             WacTcpDecodeHolder decodeHolder,
                             BiFunction<DeviceMessage, Integer, T> handler) {
        // 帧起始符
        buf.readByte();
        // 仪表类型
        decodeHolder.setVendor(WacGaugeTypes.getByHexCode(buf.readByte()).getVendor());
        // 地址域
        byte[] meterAddrBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(meterAddrBytes);
        decodeHolder.setDeviceId(BytesUtils.bytesToHex(meterAddrBytes));
        // 厂商代码
        ByteBufUtils.readBytesToHex(buf, 3);
        // 控制码
        decodeControlCode(buf.readByte(), decodeHolder);
        // 数据长度
        int dataLength = buf.readByte();
        decodeHolder.setDataLength(dataLength);

        int replyMsgId = 1;
        if (dataLength > 0) {
            // 功能码
            String featCode = ByteBufUtils.readBytesToHex(buf, 2);
            replyMsgId = buf.readByte();
            IotFeat feat = WacGaugeFeats.lookup(decodeHolder.getVendor(), featCode);
            // 数据帧校验
            // feat.getCheckResult(decodeHolder.getRawDataHex());
            decodeHolder.setFeature(feat);
        }

        TcpMessageAdapter<DeviceMessage> messageAdapter = writePropertyReply.forTcp.get();
        messageAdapter.read(buf, decodeHolder);
        DeviceMessage message = messageAdapter.getMessage();
        // 设置设备 ID
        message.thingId(DeviceThingType.device, decodeHolder.getDeviceId());
        // 设置仪表类型
        message.addHeader(IotConstants.VENDOR_CODE, decodeHolder.getVendor().getCode());
        return handler.apply(message, replyMsgId);
    }
}
