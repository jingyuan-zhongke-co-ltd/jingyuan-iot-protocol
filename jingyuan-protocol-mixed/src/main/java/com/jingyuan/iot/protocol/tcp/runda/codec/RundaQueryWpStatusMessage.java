package com.jingyuan.iot.protocol.tcp.runda.codec;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询设备运行信息：立刻返回心跳信息到服务器
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaQueryWpStatusMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x01);
        buf.writeByte(0x00);
    }
}
