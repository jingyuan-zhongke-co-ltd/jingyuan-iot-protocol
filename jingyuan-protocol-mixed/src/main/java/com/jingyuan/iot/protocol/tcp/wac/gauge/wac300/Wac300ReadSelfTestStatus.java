package com.jingyuan.iot.protocol.tcp.wac.gauge.wac300;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读自检状态(生产 FO/DD)
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac300ReadSelfTestStatus implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 产品型号24字节 ASCII wac300n-t
        byte[] bytes = ByteBufUtils.readBytes(buf, 24);
        properties.put("productModel", BytesUtils.bytesToAscStr(bytes));
        // 以前是否发生过自检，00：没有发生过自检，01：发生过自检.以下值均不记录eep。
        properties.put("isSelfTested", BytesUtils.byteToHex(buf.readByte()));
        // 阀门协议，0x47：nb调节阀，0x48：nb锁闭阀，0x50：wac500nb大阀不带抄表，0x51：wac500nb大阀带抄表,0x48：nb小口径调节阀带抄表
        properties.put("valveProtocols", BytesUtils.byteToHex(buf.readByte()));
        // 运营商版本（指软件运营商版本），01：电信，02：移动
        properties.put("carrierVersion", BytesUtils.byteToHex(buf.readByte()));
        // 软件版本57
        properties.put("softwareVersion", BytesUtils.byteToHex(buf.readByte()));
        // eep是否正常，01：正常，00：不正常，02：不判断
        properties.put("eepStatus", BytesUtils.byteToHex(buf.readByte()));
        // 联网是否正常，01：正常，00：不正常，02：不判断
        properties.put("networkStatus", BytesUtils.byteToHex(buf.readByte()));
        // 阀门最小开度标定是否正常，01：正常，00：不正常，02：不判断
        properties.put("minOpenStatus", BytesUtils.byteToHex(buf.readByte()));
        // 阀门最大开度标定是否正常，01：正常，00：不正常，02：不判断
        properties.put("maxOpenStatus", BytesUtils.byteToHex(buf.readByte()));
        // 回温标定是否正常，01：正常，00：不正常，02：不判断
        properties.put("returnTempStatus", BytesUtils.byteToHex(buf.readByte()));
        // 进温标定是否正常，01：正常，00：不正常，02：不判断
        properties.put("inletTempStatus", BytesUtils.byteToHex(buf.readByte()));
        // 电压是否正常，01：正常，00：不正常，02：不判断
        properties.put("voltage", BytesUtils.byteToHex(buf.readByte()));
    }
}
