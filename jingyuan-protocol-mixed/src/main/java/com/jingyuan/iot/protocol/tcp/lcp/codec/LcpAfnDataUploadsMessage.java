package com.jingyuan.iot.protocol.tcp.lcp.codec;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnQueryCommRwEnums;
import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnDataUploadsREnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Map;

/**
 * 属性上报
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnDataUploadsMessage implements LcpAfnMessage {

    @Override
    public void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> map) {
        String fn = BytesUtils.byteToHex(buf.readByte());
        decodeHolder.setFn(fn);
        LcpAfnDataUploadsREnums byFeatCode = LcpAfnDataUploadsREnums.getByFeatCode(fn);
        decodeHolder.setFeature(byFeatCode);


        String rawDataHex = decodeHolder.getRawDataHex();
        int userLength = (rawDataHex.length() - 10 * 2) / 2;
//        表地址	BCD	6个字节
        String BCD = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 6));
//        map.put("add", BCD);
//                第1个点数据体
        for (int i = userLength, n = 1; i > 32; i = i - 32, n++) {
//        时间（从1970-01-01开始的秒数)	td2类型的 umix 时间	4个字节
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            ZonedDateTime zonedDateTime = Instant.ofEpochMilli(BytesUtils.bytesToUnLong(bytes) * 1000)
                    .atZone(ZoneId.of("UTC")); // 设置为上海时间

            map.put("freezeTime", zonedDateTime.toLocalDateTime());
            // 瞬时流量 Float 4个字节

            byte[] bytes2 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes2);
            float instantaneousFlow = BytesUtils.bytesToShort(bytes2);
            map.put("instantFlowUnit", instantaneousFlow);

            // 正向累计流量整数部分 Long 4个字节
            byte[] bytes1 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes1);
            long forwardAccumulatedFlowInt = BytesUtils.bytesToUnLong(bytes1);
            // 正向累计流量小数部分 Float 4个字节
            byte[] bytes3 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes3);
            float forwardAccumulatedFlowDec = BytesUtils.bytesToFloat(bytes3);

            BigDecimal add = new BigDecimal(forwardAccumulatedFlowInt).add(new BigDecimal(Convert.toStr(forwardAccumulatedFlowDec)));
            map.put("forwardFlow", add);

            // 反向累计流量整数部分 Long 4个字节
            long reverseAccumulatedFlowInt = BytesUtils.bytesToUnLong(ByteBufUtils.readBytes(buf, 4));
            // 反向累计流量小数部分 Float 4个字节
            short reverseAccumulatedFlowDec = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
            BigDecimal bigDecimal = addShortAndLong(reverseAccumulatedFlowInt, reverseAccumulatedFlowDec);
            map.put("reverseFlow", bigDecimal);

            // 第1路模拟量通道采集数据 Int 2个字节
            int analogChannel1 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel1", analogChannel1);

            // 第2路模拟量通道采集数据 Int 2个字节
            int analogChannel2 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel2", analogChannel2);

            // 告警状态 Byte 1个字节
            byte alarmStatus = buf.readByte();
            map.put("alarmStatus", alarmStatus);

            // 电池电压 Int 2个字节
            byte[] bytes4 = ByteBufUtils.readBytes(buf, 2);
            BytesUtils.bytesReverse(bytes4);
            int batteryVoltage = BytesUtils.bytesToShort(bytes4);
            map.put("voltage", String.format("%.2f", batteryVoltage * 0.001));

            // 网络信号 Byte 1个字节
            byte networkSignal = buf.readByte();
            map.put("signal", networkSignal);
        }
//        for (int i = userLength, n = 1; i > 32; i = i - 32, n++) {
//            // 时间（从1970-01-01开始的秒数)	td2类型的 umix 时间	4个字节
//            String freezeTime = BytesUtils.bytesToHex(ByteBufUtils.readBytes(buf, 4));
//            map.put("freezeTime" + n, freezeTime);
//
//            // 瞬时流量 Float 4个字节
//            float instantaneousFlow = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
//            map.put("instantaneousFlow" + n, instantaneousFlow);
//
//            // 正向累计流量整数部分 Long 4个字节
//            long forwardAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
//            map.put("forwardAccumulatedFlowInt" + n, forwardAccumulatedFlowInt);
//
//            // 正向累计流量小数部分 Float 4个字节
//            float forwardAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
//            map.put("forwardAccumulatedFlowDec" + n, forwardAccumulatedFlowDec);
//
//            // 反向累计流量整数部分 Long 4个字节
//            long reverseAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
//            map.put("reverseAccumulatedFlowInt" + n, reverseAccumulatedFlowInt);
//
//            // 反向累计流量小数部分 Float 4个字节
//            float reverseAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
//            map.put("reverseAccumulatedFlowDec" + n, reverseAccumulatedFlowDec);
//
//            // 第1路模拟量通道采集数据 Int 2个字节
//            int analogChannel1 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("analogChannel1" + n, analogChannel1);
//
//            // 第2路模拟量通道采集数据 Int 2个字节
//            int analogChannel2 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("analogChannel2" + n, analogChannel2);
//
//            // 告警状态 Byte 1个字节
//            byte alarmStatus = buf.readByte();
//            map.put("alarmStatus" + n, alarmStatus);
//
//            // 电池电压 Int 2个字节
//            int batteryVoltage = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("batteryVoltage" + n, batteryVoltage);
//
//            // 网络信号 Byte 1个字节
//            byte networkSignal = buf.readByte();
//            map.put("networkSignal" + n, networkSignal);
//        }

    }

    public static BigDecimal addShortAndLong(long shortValue, long longValue) {
        BigDecimal bigDecimalFromShort = BigDecimal.valueOf(shortValue);
        BigDecimal bigDecimalFromLong = BigDecimal.valueOf(longValue);
        return bigDecimalFromShort.add(bigDecimalFromLong);
    }

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof HlmAfnQueryCommRwEnums;
//        FeatCodecs.write(feature, buf, properties);
    }

}
