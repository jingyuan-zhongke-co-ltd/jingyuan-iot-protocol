package com.jingyuan.iot.protocol.tcp.wac.gauge.adjust;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.adjust.WacAdjustRemoteConfBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.wac.gauge.wac710.Wac710WriteValveOpenLimit;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 远程开度模式
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class WacAdjustRemoteCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        byte[] bytes1 = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes1);
        // 目标调节值，表示50开度。
        properties.put("adjustOpening", Convert.toInt(BytesUtils.bytesToHex(bytes1)));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacAdjustRemoteConfBo remoteConfBo = BeanUtil.toBean(properties, WacAdjustRemoteConfBo.class);
        Wac710WriteValveOpenLimit.writeOpening(buf, remoteConfBo.getAdjustOpening());
    }
}
