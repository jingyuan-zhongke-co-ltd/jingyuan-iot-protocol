package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.codec.BCD;
import com.jingyuan.common.protocol.enums.wac.WacTempSensorState;
import com.jingyuan.common.protocol.enums.wac.WacValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读阀门数据
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadValveData implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 预留
        buf.readBytes(4);
        // 进水压力。Float类型。单位Kpa
        properties.put("inletPressure", buf.readFloatLE());
        // 回水压力。Float类型。单位Kpa
        properties.put("returnPressure", buf.readFloatLE());
        // 内部温度。Float类型。单位℃。
        properties.put("temperature", buf.readFloatLE());
        // 内部气压差。Float类型。单位hPa。
        properties.put("pressureDiff", buf.readFloatLE());
        // 当前进水温度（阀）BIN，X0.01后表示实际温度
        properties.put("inletTemp", buf.readShortLE() * 0.01);
        // 当前回水温度（阀）BIN，X0.01后表示实际温度
        properties.put("returnTemp", buf.readShortLE() * 0.01);
        // 累积工作时间（阀）BIN
        properties.put("accrueWorkingTime", buf.readIntLE());
        // 累积阀开时间（阀）BIN
        properties.put("accrueValveOpenTime", buf.readIntLE());
        // 阀门开关次数（阀）BIN
        properties.put("valveContNum", buf.readIntLE());
        // 阀门开度
        properties.put("valveAngular", (int) buf.readByte());

        // 阀门动作电流AD值
        properties.put("valveAd", buf.readShortLE());
        // 设备唤醒到连上服务器的时间
        properties.put("wakesUpSeconds", buf.readShortLE());
        // 实时时间
        byte[] bytes = ByteBufUtils.readBytes(buf, 7);
        BytesUtils.bytesReverse(bytes);
        properties.put("currentTime", DateUtils.parse(BCD.bcdToStr(bytes), DatePatterns.PURE_DATETIME_PATTERN));

        // 读取 ST 状态字
        readSt(buf, properties);
    }

    /**
     * 读取 ST 状态字
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    public static void readSt(ByteBuf buf, Map<String, Object> properties) {
        // ST(状态)
        byte st1 = buf.readByte();
        // 阀门状态
        properties.put("valveState", WacValveStateEnums.getByCode(st1 & 0x03).getCode());
        // 阀门超时故障动作状态
        properties.put("valveTimeoutState", st1 & 0x04);
        // 阀门堵转故障动作状态
        properties.put("valveStallState", st1 & 0x08);
        // 温度传感器状态
        properties.put("tempSenState", WacTempSensorState.getByStCode(st1).getCode());
        // 电池欠压故障状态
        properties.put("batteryVoltageState", st1 & 0x40);
        // 进水压力传感器状态
        properties.put("inletPtState", st1 & 0x80);

        byte st2 = buf.readByte();
        // 阀门控制
        properties.put("valveControl", st2 & 0x03);
        // 蓝牙状态
        properties.put("bluetoothState", st2 & 0x0c);
        // 外供电状态
        properties.put("extSupplyPowerState", st2 & 0x10);
        // 气压传感器状态
        properties.put("atmPtState", st2 & 0x20);
        // 磁性角度传感器
        properties.put("magneticSenState", st2 & 0x40);
        // 回水压力传感器状态
        properties.put("returnPtState", st2 & 0x80);
    }
}
