package com.jingyuan.iot.protocol.tcp.hlm.afn.afn0a;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.hlm.JoyoHlmFlowHUnitEnums;
import com.jingyuan.common.protocol.enums.hlm.JoyoHlmFlowUnitEnums;
import com.jingyuan.common.protocol.enums.hlm.JoyoHlmPowerHUnitEnums;
import com.jingyuan.common.protocol.enums.hlm.JoyoHlmPowerUnitEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

/**
 * 属性上报 热表
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class HlmHeatAfnDataUploadsMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {
        HashMap<String, Object> properties = new HashMap<>();
        // 校验和校验
//        String validData = BytesUtils.bytesToHex(ByteBufUtils.getBytes(buf, 0,59));
//        HlmAAfnDataUploadEnums.HEAT.getInnerCheckResult(validData);

//        头
        buf.readByte();
//            表类型
        properties.put("meterType", BytesUtils.byteToHex(buf.readByte()));

//        地址
        byte[] bytes = ByteBufUtils.readBytes(buf, 7);
        properties.put("innerMeterAddr", BytesUtils.bytesToHex(bytes));

//        控制码
        buf.readByte();
//        数据长度
        int i = BytesUtils.byteToUnInt(buf.readByte());
//        数据标识
        ByteBufUtils.readBytes(buf, 2);
//            序列号
        buf.readByte();

//        冷量 单位
        byte[] coolCapacity = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(coolCapacity);
        properties.put("coolCapacity", JoyoHlmPowerHUnitEnums.addUnitByByte(Convert.toInt(BytesUtils.bytesToHex(coolCapacity)), buf.readByte()));

//        热量 单位
        byte[] heatCapacity = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(heatCapacity);
        properties.put("heatCapacity", JoyoHlmPowerHUnitEnums.addUnitByByte(Convert.toInt(BytesUtils.bytesToHex(heatCapacity)), buf.readByte()));
//        功率 单位
        byte[] power = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(power);
        properties.put("power", JoyoHlmPowerUnitEnums.addUnitByByte(Convert.toInt(BytesUtils.bytesToHex(power)), buf.readByte()));
//        流速 单位
        byte[] velocityFlow = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(velocityFlow);
        properties.put("velocityFlow", JoyoHlmFlowHUnitEnums.addUnitByByte(Convert.toInt(BytesUtils.bytesToHex(velocityFlow)), buf.readByte()));
//        累积流量 单位
        byte[] reading = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(reading);
        properties.put("reading", JoyoHlmFlowUnitEnums.addUnitByByte(Convert.toInt(BytesUtils.bytesToHex(reading)), buf.readByte()));
//        进水温度
        byte[] inletTemp = ByteBufUtils.readBytes(buf, 3);
        BytesUtils.bytesReverse(inletTemp);
        properties.put("inletTemp", Convert.toFloat(BytesUtils.bytesToHex(inletTemp)) / 100);
//            回水温度
        byte[] returnTemp = ByteBufUtils.readBytes(buf, 3);
        BytesUtils.bytesReverse(returnTemp);
        properties.put("returnTemp", Convert.toFloat(BytesUtils.bytesToHex(returnTemp)) / 100);
//        累计工作时间
        byte[] cumulateWorkingTime = ByteBufUtils.readBytes(buf, 3);
        BytesUtils.bytesReverse(cumulateWorkingTime);
        properties.put("cumulateWorkingTime", Convert.toInt(BytesUtils.bytesToHex(cumulateWorkingTime)));
//            当前时间
        properties.put("innerFreezeTime", HlmDecodeUtils.timeByteRead(buf, 7));
//        st1状态
        buf.readByte();
        st1(buf, properties);
        map.put("innerData", properties);
    }

    private static void st1(ByteBuf buf, Map<String, Object> properties) {
        byte state = buf.readByte();
//        当前测量状态 0 冷量 1 热量
        int measurementState = (state & 0b00000001);
//        电池欠压
        int voltageState = (state & 0b00000010) >> 1;
//        流量传感器故障 1 故障
        int flowSensorState = (state & 0b00000100) >> 2;
//        进水温度传感器故障 1 故障
        int inletTempSensorState = (state & 0b00001000) >> 3;
//        回水温度传感器故障 1 故障
        int returnTempSensorState = (state & 0b00010000) >> 4;
//        EEPROM 故障 1 故障
        int eeStatus = (state & 0b00100000) >> 5;
        properties.put("measurementState", measurementState);
        properties.put("voltageState", voltageState);
        properties.put("flowSensorState", flowSensorState);
        properties.put("inletTempSensorState", inletTempSensorState);
        properties.put("returnTempSensorState", returnTempSensorState);
        properties.put("eeStatus", eeStatus);
    }
}
