package com.jingyuan.iot.protocol.tcp.hlm.afn.afn05;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询电池电压
 *
 * @author: dr
 * @date: 2024-12-03
 * @Version: V1.0
 */
@Slf4j
public class HlmQueryBatteryVoltage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        流量差量值	Int 	0~65535	2
        byte[] flowDifference = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(flowDifference);
        properties.put("flowDifference", BytesUtils.bytesToShort(flowDifference));

    }
}
