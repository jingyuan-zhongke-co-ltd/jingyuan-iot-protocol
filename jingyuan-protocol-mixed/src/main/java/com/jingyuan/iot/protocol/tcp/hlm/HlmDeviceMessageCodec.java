package com.jingyuan.iot.protocol.tcp.hlm;

import cn.hutool.core.collection.CollUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnConfirmRwEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnControlRwEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnSetCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.feature.IotFunc;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.utils.lambda.FuncUtils;
import com.jingyuan.iot.protocol.cached.CachedMessageHelper;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.wrx.HlmMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.property.WritePropertyMessage;
import org.jetlinks.core.message.property.WritePropertyMessageReply;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;


/**
 * 京源海龙马超声波 通讯协议
 *
 * @author: dr
 * @date: 2024-10-16
 * @Version: V1.0
 */
@Slf4j
public class HlmDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata HLM_US_CONFIG = new DefaultConfigMetadata(
        "京源海龙马超声波TCP配置"
        , "京源海龙马超声波TCP配置信息");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        HlmTcpDecodeHolder decodeHolder = HlmMessageTypeEnums.read(payload);
        DeviceMessage message = decodeHolder.getDeviceMessage();
        return context.getDevice(message.getDeviceId())
                      .flatMap(deviceOperator -> replyCache(decodeHolder, deviceOperator)
                          .thenReturn(deviceOperator))
                      .flatMapMany(deviceOperator ->
                                       CachedMessageHelper
                                           .getMessage(deviceOperator)
                                           .flatMapMany(messages -> {
                                               //睡眠消息写出
                                               if (CollUtil.isEmpty(messages)) {
                                                   return sleepMessageWrite(context, decodeHolder);
                                               } else {
                                                   return Flux
                                                       // 缓存的指令写出
                                                       .fromIterable(messages)
                                                       .flatMap(toSendMessage -> {
                                                           IotFunc feature = (IotFunc) MessageHelper.getHeadFeatThrow(toSendMessage);
                                                           HlmTcpEncodeHolder encodeHolder = HlmTcpEncodeHolder.of();
                                                           encodeHolder.setFeature(feature);
                                                           encodeHolder.setDeviceId(toSendMessage.getDeviceId());
                                                           encodeHolder.setDeviceMessage(toSendMessage);
                                                           encodeHolder.setProtocolCode(decodeHolder.getProtocol());
                                                           encodeHolder.setL(feature.getLength());
                                                           if (feature instanceof HlmAfnSetCommWriteEnums) {
                                                               encodeHolder.setAfn(HlmAfnFeatureEnums.set_comm);
                                                           } else if (feature instanceof HlmAfnControlRwEnums) {
                                                               encodeHolder.setAfn(HlmAfnFeatureEnums.cont_comm);
                                                           }
                                                           return Flux.just(encodeHolder)
                                                                      .flatMap(holder -> doEncode(holder)
                                                                          .flatMapMany(buf -> ((FromDeviceMessageContext) context)
                                                                              .getSession()
                                                                              .send(buf)
                                                                              .flatMapMany(res -> {
                                                                                  if (res) {
                                                                                      return Flux.just(message, encodeHolder.getDeviceMessage());
                                                                                  } else {
                                                                                      return Flux.just(message);
                                                                                  }
                                                                              })));
                                                       });
                                               }
                                           }))
                      .switchIfEmpty(Flux.defer(() -> Flux.just(message)));
    }

    /**
     * 回复消息缓存
     *
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    private Mono<DeviceMessage> replyCache(HlmTcpDecodeHolder decodeHolder, DeviceOperator deviceOperator) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        return Mono.just(message)
                   .flatMap(deviceMessage -> {
                       if (deviceMessage instanceof WritePropertyMessageReply) {


                           WritePropertyMessageReply messageReply = (WritePropertyMessageReply) deviceMessage;
                           Map<String, Object> properties = messageReply.getProperties();
                           if (CollUtil.isNotEmpty(properties)) {
                               HashMap<String, Object> cacheMap = new HashMap<>(1);

                               String code = decodeHolder.getProtocol().getCode();
                               FuncUtils.isNotBlank(code)
                                        .handle(() -> cacheMap.put(IotConstants.HLM_PROTOCOL_CODE, code));
                               // 设置厂商信息
                               return deviceOperator.setConfig(IotConstants.VENDOR_CODE, JoyoIotVendor.HLM_US.getCode())
                                                    .flatMap(deviceOperator1 -> deviceOperator.setConfigs(cacheMap)
                                                                                              .thenReturn(deviceMessage))
                                                    .thenReturn(deviceMessage);
                           }
                       }
                       return Mono.just(deviceMessage);
                   });
    }


    /**
     * 睡眠消息写出
     *
     * @param context      消息上下文
     * @param decodeHolder 消息解析包装类
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> sleepMessageWrite(MessageDecodeContext context, HlmTcpDecodeHolder decodeHolder) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        CctSeqConEnums seqCon = decodeHolder.getSeqCon();
        // 判断消息是否需要确认回复
        if (CctSeqConEnums.no_confirm.equals(seqCon)) {
            return Flux.just(message);
        }

        HlmTcpEncodeHolder messageEncodeHolder = HlmTcpEncodeHolder.of();
        String deviceId = message.getDeviceId();
        messageEncodeHolder.setDeviceId(deviceId);
        messageEncodeHolder.setAfn(HlmAfnFeatureEnums.cont_comm);
        messageEncodeHolder.setL(HlmAfnControlRwEnums.dormancy.getLength());
        messageEncodeHolder.setFeature(HlmAfnControlRwEnums.dormancy);
        messageEncodeHolder.setAfnCode(HlmAfnControlRwEnums.dormancy.getFeatCode());
        messageEncodeHolder.setProtocolCode(decodeHolder.getProtocol());

        WritePropertyMessage writePropertyMessage = IotMessageType.writePmInstance(deviceId);
        // 添加厂商信息
        final IotFeat feature = HlmAfnConfirmRwEnums.confirm;
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(writePropertyMessage);

        messageEncodeHolder.setDeviceMessage(writePropertyMessage);
        return doSendAfterDecode(context, messageEncodeHolder, message);
    }


    /**
     * 解码后消息发送
     *
     * @param context       消息上下文
     * @param encodeHolder  消息编码包装类
     * @param decodeMessage 解码后消息
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> doSendAfterDecode(MessageDecodeContext context, HlmTcpEncodeHolder encodeHolder, DeviceMessage decodeMessage) {
        return doEncode(encodeHolder)
            .flatMap(encodedMessage -> ((FromDeviceMessageContext) context).getSession().send(encodedMessage))
            .flatMapMany(aBoolean -> {
                if (aBoolean) {
                    return Flux.just(decodeMessage, encodeHolder.getDeviceMessage());
                } else {
                    return Flux.just(decodeMessage);
                }
            });
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        HlmTcpEncodeHolder messageEncodeHolder = HlmTcpEncodeHolder.of();
        messageEncodeHolder.setDeviceMessage(deviceMessage);

        return doEncode(messageEncodeHolder);
    }


    /**
     * 执行编码操作
     *
     * @param messageEncodeHolder 消息编码包装类
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(HlmTcpEncodeHolder messageEncodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        HlmMessageTypeEnums.write(messageEncodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
