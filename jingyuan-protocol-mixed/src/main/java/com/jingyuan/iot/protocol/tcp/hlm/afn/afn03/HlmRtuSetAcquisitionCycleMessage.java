package com.jingyuan.iot.protocol.tcp.hlm.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.hlm.afn03.HlmCollectionCycle;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 设置采集周期
 *
 * @author: dr
 * @date: 2024-11-12
 * @Version: V1.0
 */
@Slf4j
public class HlmRtuSetAcquisitionCycleMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        HlmCollectionCycle bean = BeanUtil.toBean(properties, HlmCollectionCycle.class);
//        01 			// 数据包标识。
        buf.writeByte(1);
//        采集周期（分钟）	Bin		2
        Short collectionCycle = bean.getCollectionCycle();
        byte[] bytes = BytesUtils.shortToBytes(collectionCycle, ByteOrder.LITTLE_ENDIAN);
        BytesUtils.bytesReverse(bytes);
        buf.writeBytes(bytes);
    }

}
