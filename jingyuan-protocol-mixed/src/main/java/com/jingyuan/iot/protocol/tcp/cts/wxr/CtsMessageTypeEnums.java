package com.jingyuan.iot.protocol.tcp.cts.wxr;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.cts.CtsFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 京源电信水表通信协议消息类型枚举
 *
 * @author: cc
 * @date: 2024/8/27 16:28
 * @Version: V1.0
 */
@Slf4j
public enum CtsMessageTypeEnums {

    //0x01
    online(IotMessageType.ONLINE.getCode(), null),

    //0x02
    ack(IotMessageType.ACKNOWLEDGE.getCode(), null),
    //0x03
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), CtsReportPropertyMessage::new),

    //0x04
    readProperty(IotMessageType.READ_PROPERTY.getCode(), null),

    //0x05
    readPropertyReply(IotMessageType.READ_PROPERTY_REPLY.getCode(), null),

    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), CtsWritePropertyMessage::new),

    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), CtsWritePropertyMessageReply::new),

    function(IotMessageType.INVOKE_FUNCTION.getCode(), null),

    functionReply(IotMessageType.INVOKE_FUNCTION_REPLY.getCode(), null),

    event(IotMessageType.EVENT.getCode(), null);

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    CtsMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, CtsMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (CtsMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf) {
        DeviceMessage message = encodeHolder.getDeviceMessage();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        CtsMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);
        // 写出数据到ByteBuf
        buf.writeBytes(BytesUtils.hexToBytes("68100000" + message.getDeviceId().substring(2) + "0404A01700"));
        tcp.write(buf, encodeHolder);
        // 缓存消息ID，供设备回复时匹配，通过 deviceId + featureType
        MsgIdCacheUtil.cacheReplyMsgId(message.getDeviceId(), ByteBufUtils.getBytesToHex(buf, 10, 1), msgId);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static TcpDecodeHolder read(ByteBuf buf) {
        TcpDecodeHolder decodeHolder = TcpDecodeHolder.of();
        String bufWithHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf));
        decodeHolder.setRawDataHex(bufWithHex);
        //功能类型帧
        String nbData = bufWithHex;
        if (bufWithHex.length() == 256) {
            nbData = ByteBufUtils.getBytesToHex(buf, 1, 23);
        }
        //表地址
        decodeHolder.setDeviceId(nbData.substring(2, 4) + nbData.substring(8, 18));
        // 功能码
        String featCode = nbData.substring(18, 26);
        CtsFeatureEnums feature = CtsFeatureEnums.getByFeatCode(featCode);
        Assert.notNull(feature, "功能类型解析异常：【" + featCode + "】，原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        decodeHolder.setFeature(feature);

        // 指令校验
        feature.getCheckResult(bufWithHex);

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);

        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf, TcpDecodeHolder decodeHolder, BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        //区分消息类型
        IotMessageType encodeType = feature.getDecodeType();
        Assert.notNull(encodeType, "暂不支持的消息功能类型：" + feature);
        CtsMessageTypeEnums type = maps.get(encodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature);

        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();

        Integer replyMsgId = MsgIdCacheUtil.getReplyMsgId(message.getDeviceId(), feature.getFeatCode());
        return handler.apply(message, replyMsgId);
    }
}
