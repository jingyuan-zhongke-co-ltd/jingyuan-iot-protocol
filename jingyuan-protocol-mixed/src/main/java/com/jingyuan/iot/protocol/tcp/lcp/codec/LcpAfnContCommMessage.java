package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnContCommWriteEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控制命令
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnContCommMessage implements LcpAfnMessage {

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof LcpAfnContCommWriteEnums;

        FeatCodecs.write(feature, buf, properties);
    }
}
