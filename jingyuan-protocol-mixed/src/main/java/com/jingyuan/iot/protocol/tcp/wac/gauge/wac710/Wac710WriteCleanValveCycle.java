package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteCleanValveCycleBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写清洗阀门周期
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteCleanValveCycle implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteCleanValveCycleBo cleanValveCycleBo = BeanUtil.toBean(properties, Wac710WriteCleanValveCycleBo.class);
        byte[] bytes = BytesUtils.hexToBytes(Convert.toStr(cleanValveCycleBo.getCleanValveCycle()));
        BytesUtils.bytesReverse(bytes);
        buf.writeBytes(bytes);
    }
}
