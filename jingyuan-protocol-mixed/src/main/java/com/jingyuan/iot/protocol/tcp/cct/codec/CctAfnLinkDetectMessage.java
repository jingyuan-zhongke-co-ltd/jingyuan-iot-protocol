package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnLinkDetectReadEnums;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 链路接口检测
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnLinkDetectMessage implements CctAfnMessage {

    @Override
    public void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = String.valueOf(buf.readByte());
        CctAfnLinkDetectReadEnums linkDetectUpEnum = CctAfnLinkDetectReadEnums.getByFeatCode(fn);
        decodeHolder.setFeature(linkDetectUpEnum);
    }
}
