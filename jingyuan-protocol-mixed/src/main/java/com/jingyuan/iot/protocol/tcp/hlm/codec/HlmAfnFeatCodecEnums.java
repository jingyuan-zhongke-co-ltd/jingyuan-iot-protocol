package com.jingyuan.iot.protocol.tcp.hlm.codec;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.hlm.HlmAfnFeatureEnums;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用层功能码指令处理
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
@AllArgsConstructor
public enum HlmAfnFeatCodecEnums {
    /**
     * 确认∕否认
     */
    confirm(HlmAfnFeatureEnums.confirm, new HlmAfnConfirmDenyMessage()),
    set_comm(HlmAfnFeatureEnums.set_comm, new HlmAfnSetCommMessage()),
    report_data_ultrasonic(HlmAfnFeatureEnums.report_data_ultrasonic, new HlmAfnDataUploadsMessage()),
    report_data_rtu(HlmAfnFeatureEnums.report_data_rtu, new HlmAfnRtuDataUploadsMessage()),
    query_comm(HlmAfnFeatureEnums.query_comm, new HlmAfnQueryCommMessage()),
    cont_comm(HlmAfnFeatureEnums.cont_comm, new HlmAfnContCommMessage()),
    file_transfers(HlmAfnFeatureEnums.file_transfers, new HlmAfnFileTransfersMessage()),
    ;
    private final HlmAfnFeatureEnums afnFeat;

    private final HlmAfnMessage codec;

    private static final Map<String, HlmAfnFeatCodecEnums> VALUES = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(val -> VALUES.put(val.afnFeat.getCode(), val));
    }

    public static void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        HlmAfnFeatCodecEnums afn = VALUES.get(decodeHolder.getAfn().getCode());
        Assert.notNull(afn, "不支持的应用层功能码:" + afn + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        afn.codec.read(buf, decodeHolder, properties);
    }

    public static void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        HlmAfnFeatureEnums afnFeatureEnum = encodeHolder.getAfn();
        HlmAfnFeatCodecEnums afnCodecEnum = VALUES.get(afnFeatureEnum.getCode());
        Assert.notNull(afnCodecEnum, "不支持的应用层功能码:" + afnCodecEnum);

        // 应用层功能码AFN
        buf.writeByte(HexUtils.decodeHex(afnFeatureEnum.getCode())[0]);
        // 帧序列域SEQ
        buf.writeByte(0b11111111);

        // 应用层功能码
        buf.writeByte(encodeHolder.getFeature().getFeatCodeByte());

        // 数据单元
        afnCodecEnum.codec.write(buf, encodeHolder, properties);

        // 计算校验和
        int computeSum = ChecksumUtils.computeSumSingle(buf, 4, buf.writerIndex());
        buf.writeByte((byte) computeSum);
        buf.writeByte(0x16);
    }
}
