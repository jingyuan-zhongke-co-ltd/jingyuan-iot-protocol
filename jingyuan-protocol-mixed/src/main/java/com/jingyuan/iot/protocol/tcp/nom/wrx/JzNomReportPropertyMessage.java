package com.jingyuan.iot.protocol.tcp.nom.wrx;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.nom.JzNomMeterTypeEnums;
import com.jingyuan.common.protocol.enums.nom.JzNomVendorEnums;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * 京兆无磁 - 属性上报
 *
 * @author: cc
 * @date: 2024/9/23 10:07
 * @Version: V1.0
 */
public class JzNomReportPropertyMessage implements TcpMessageAdapter<ReportPropertyMessage> {

    private ReportPropertyMessage message;

    /**
     * 设置消息
     *
     * @param message 消息
     */
    @Override
    public void setMessage(ReportPropertyMessage message) {
        this.message = message;
    }

    /**
     * 获取消息
     *
     * @return T 消息
     */
    @Override
    public ReportPropertyMessage getMessage() {
        return message;
    }

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new ReportPropertyMessage();
        message.setDeviceId(decodeHolder.getDeviceId());
        Map<String, Object> map = initReadMap(buf, decodeHolder);

        FeatCodecs.read(decodeHolder, buf, map);
        message.setProperties(map);
    }


    public static Map<String, Object> initReadMap(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JZ_NOM.getCode());
        // 协议版本号
        map.put("protocolVersion", buf.readByte());
        // 厂商代码
        map.put("nbVendor", JzNomVendorEnums.getByCode(buf.readByte()).getDesc());
        // 水表类型代码
        map.put("meterType", JzNomMeterTypeEnums.getByCode(buf.readByte()).getDesc());
        // imei
        buf.readBytes(15);
        map.put("imei", decodeHolder.getDeviceId());
        // RSRP(信号强度)
        map.put("RSRP", buf.readShortLE());
        // SNR(信噪比)
        map.put("SNR", buf.readShortLE());
        // ECL(覆盖等级)
        map.put("ECL", buf.readUnsignedByte());
        // CSQ(信号质量)
        map.put("CSQ", buf.readUnsignedByte());
        // 功能码
        buf.readByte();
        return map;
    }
}
