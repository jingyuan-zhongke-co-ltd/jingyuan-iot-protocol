package com.jingyuan.iot.protocol.tcp.runda.wrx;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.runda.RundaWpFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.Crc16Utils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 润达消息类型枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
public enum RundaMessageTypeEnums {

    online(IotMessageType.ONLINE.getCode(), null),
    ack(IotMessageType.ACKNOWLEDGE.getCode(), RundaAcknowledgeMessage::new),
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), RundaReportPropertyMessage::new),
    readProperty(IotMessageType.READ_PROPERTY.getCode(), null),
    readPropertyReply(IotMessageType.READ_PROPERTY_REPLY.getCode(), null),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), RundaWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), RundaWritePropertyMessageReply::new),
    function(IotMessageType.INVOKE_FUNCTION.getCode(), null),
    functionReply(IotMessageType.INVOKE_FUNCTION_REPLY.getCode(), null),
    event(IotMessageType.EVENT.getCode(), null);

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    RundaMessageTypeEnums(String messageType, Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }


    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, RundaMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (RundaMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }


    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf) {
        DeviceMessage message = encodeHolder.getDeviceMessage();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        RundaMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);

        // 写出设备 IMEI 号数据到 ByteBuf
        buf.writeBytes(BytesUtils.hexToBytes(message.getDeviceId()));
        // 写出功能码
        buf.writeBytes(new byte[]{encodeHolder.getFeature().getFeatCodeByte(), 0x00});

        tcp.write(buf, encodeHolder);

        // 计算校验和
        byte[] bytes = ByteBufUtils.getAll(buf);
        short crc16Modbus = (short) Crc16Utils.crc16Modbus(bytes, 0, bytes.length);
        buf.writeBytes(BytesUtils.shortToBytes(crc16Modbus));

        // 缓存消息ID，供设备回复时匹配
        MsgIdCacheUtil.cacheReplyMsgId(message.getDeviceId(), ByteBufUtils.getBytesToHex(buf, 10, 1), msgId);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static TcpDecodeHolder read(ByteBuf buf) {
        TcpDecodeHolder decodeHolder = TcpDecodeHolder.of();
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        decodeHolder.setDeviceId(ByteBufUtils.readBytesToHex(buf, 10));
        String featCode = ByteBufUtils.readBytesToHex(buf, 1);
        RundaWpFeatureEnums feature = RundaWpFeatureEnums.getByFeatCode(featCode);
        Assert.notNull(feature, "功能类型解析异常：【" + featCode + "】，原始数据帧：【" + decodeHolder.getRawDataHex() + "】");

        decodeHolder.setFeature(feature);

        // 指令校验
        feature.getCheckResult(decodeHolder.getRawDataHex());

        // 类型
        buf.readByte();
        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);

        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf, TcpDecodeHolder decodeHolder, BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        IotMessageType encodeType = feature.getDecodeType();
        Assert.notNull(encodeType, "暂不支持的功能类型：" + feature);
        RundaMessageTypeEnums type = maps.get(encodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature);

        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        message.thingId(DeviceThingType.device, decodeHolder.getDeviceId());

        Integer replyMsgId = MsgIdCacheUtil.getReplyMsgId(message.getDeviceId(), feature.getFeatCode());
        return handler.apply(message, replyMsgId);
    }
}
