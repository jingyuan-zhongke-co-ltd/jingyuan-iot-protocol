package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;

import java.util.Map;

/**
 * 应用层功能码基础消息接口
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
public interface HlmAfnMessage {

    default void read(ByteBuf buf, HlmTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
    }

    default void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        throw new IllegalArgumentException("暂不支持的功能类型");
    }
}
