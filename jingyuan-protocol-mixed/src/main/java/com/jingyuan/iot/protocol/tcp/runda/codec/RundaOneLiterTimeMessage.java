package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpOneLiterTimeBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 制1升水需要的时间，读写
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaOneLiterTimeMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 制1升水需要的时间
        byte[] oneLiterTime = ByteBufUtils.readBytes(buf, 2);
        properties.put("oneLiterTime", BytesUtils.binary(10, oneLiterTime));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpOneLiterTimeBo wpOneLiterTimeBo = BeanUtil.toBean(properties, WpOneLiterTimeBo.class);
        Short oneLiterTime = wpOneLiterTimeBo.getOneLiterTime();
        buf.writeBytes(BytesUtils.shortToBytes(oneLiterTime));
    }
}
