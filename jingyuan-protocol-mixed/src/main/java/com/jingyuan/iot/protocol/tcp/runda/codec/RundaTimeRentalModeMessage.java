package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpTimeRentalBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 时间租赁模式，时长记录应用参数 (读取和修改)
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaTimeRentalModeMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 每运行xx分钟，算运行一天
        byte[] rinseTime = ByteBufUtils.readBytes(buf, 2);
        properties.put("runMinutes1Day", BytesUtils.binary(10, rinseTime));
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpTimeRentalBo wpTimeRentalBo = BeanUtil.toBean(properties, WpTimeRentalBo.class);
        Short runMinutes1Day = wpTimeRentalBo.getRunMinutes1Day();
        buf.writeBytes(BytesUtils.shortToBytes(runMinutes1Day));
    }
}
