package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 读阀门电流AD值上限
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadAdUpperLimit implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 2);
        properties.put("valveAdUpperLimit",BytesUtils.bytesToShort(bytes, ByteOrder.LITTLE_ENDIAN));
    }
}
