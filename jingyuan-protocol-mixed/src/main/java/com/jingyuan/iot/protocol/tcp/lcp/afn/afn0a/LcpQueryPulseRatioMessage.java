package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 *  FC 查询脉冲变比
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpQueryPulseRatioMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        脉冲变比	Int	1、2、3、4	1
        byte[] pulseRatio = ByteBufUtils.readBytes(buf, 1);
        properties.put("pulseRatio", Integer.parseInt( BytesUtils.bytesToHex(pulseRatio)));
    }
}
