package com.jingyuan.iot.protocol.tcp.wac.gauge;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.wac.adjust.WacAdjustFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.wac.gauge.adjust.*;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Map;

/**
 * 调节阀 目标调节 编解码枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum WacAdjustCodecEnums implements FeatCodec {

    /**
     * 面板调节模式
     */
    PANELS_CONT(WacAdjustFeatEnums.PANELS_CONT, new WacAdjustPanelsCont()),
    /**
     * 远程开度模式
     */
    REMOTE_CONT(WacAdjustFeatEnums.REMOTE_CONT, new WacAdjustRemoteCont()),
    /**
     * 回温调节
     */
    RETURN_CONT(WacAdjustFeatEnums.RETURN_CONT, new WacAdjustTempCont()),
    /**
     * 温差调节模式
     */
    TEMP_DIFF_CONT(WacAdjustFeatEnums.TEMP_DIFF_CONT, new WacAdjustTempCont()),
    /**
     * 分时调节
     */
    TIME_SHARING_MODE(WacAdjustFeatEnums.TIME_SHARING_MODE, new WacAdjustTimeShardingMode()),
    /**
     * 刷卡调节
     */
    SWIPE_MODE(WacAdjustFeatEnums.SWIPE_MODE, new WacAdjustPanelsCont()),
    /**
     * 瞬时流量调节
     */
    INSTANT_FLOW(WacAdjustFeatEnums.INSTANT_FLOW, new WacAdjustInstantFlow()),
    /**
     * 瞬时流量PID调节
     */
    INSTANT_FLOW_PID(WacAdjustFeatEnums.INSTANT_FLOW_PID, new WacAdjustPid()),
    /**
     * 热功率PID调节
     */
    THERMAL_POWER_PID(WacAdjustFeatEnums.THERMAL_POWER_PID, new WacAdjustPid()),
    ;

    private final IotFeat feat;
    private final TcpMessageCodec codec;

    public static TcpMessageCodec lookup(WacAdjustFeatEnums wacAdjustMode) {
        return Arrays.stream(values())
                     .filter(e -> e.feat.equals(wacAdjustMode))
                     .findFirst()
                     .map(val -> val.codec)
                     .orElseThrow(() -> new IllegalArgumentException("不支持的调节模式: " + wacAdjustMode));
    }

    public static void read(ByteBuf buf, Map<String, Object> properties) {
        String adjustMode = Convert.toStr(buf.readByte());
        WacAdjustFeatEnums wacAdjustMode = WacAdjustFeatEnums.getByCode(adjustMode);
        TcpMessageCodec messageCodec = lookup(wacAdjustMode);
        // 调节模式
        properties.put("adjustMode", wacAdjustMode.getDesc());
        messageCodec.read(buf, properties);
    }

    public static void write(ByteBuf buf, Map<String, Object> properties) {
        String adjustMode = Convert.toStr(buf.getByte(buf.writerIndex() - 1));
        WacAdjustFeatEnums wacAdjustMode = WacAdjustFeatEnums.getByCode(adjustMode);
        TcpMessageCodec messageCodec = lookup(wacAdjustMode);
        messageCodec.write(buf, properties);
    }
}
