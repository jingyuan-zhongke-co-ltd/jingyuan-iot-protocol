package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnConfirmRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 确认∕否认
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnConfirmDenyMessage implements LcpAfnMessage {

    @Override
    public void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String fn = String.valueOf(buf.readByte());
        LcpAfnConfirmRwEnums confirmEnum = LcpAfnConfirmRwEnums.getByFeatCode(fn);
        decodeHolder.setFeature(confirmEnum);
    }

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof LcpAfnConfirmRwEnums;
    }
}
