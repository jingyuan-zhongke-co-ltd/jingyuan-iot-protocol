package com.jingyuan.iot.protocol.tcp.nom;

import cn.hutool.core.util.ObjectUtil;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.iot.protocol.tcp.nom.wrx.JzNomMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


/**
 * 京兆无磁
 *
 * @author: cc
 * @date: 2024/9/20 14:27
 * @Version: V1.0
 */
@Slf4j
public class JzNomDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata JZ_NOM_CONFIG = new DefaultConfigMetadata(
        "京兆无磁水表TCP配置"
        , "京兆无磁水表TCP配置");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }


    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return doDecode(context);
    }


    /**
     * 执行解码操作
     *
     * @param context 待解码消息上下文
     * @return org.jetlinks.core.message.DeviceMessage
     */
    private Publisher<? extends Message> doDecode(MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        TcpDecodeHolder decodeHolder = JzNomMessageTypeEnums.read(payload);
        if (ObjectUtil.isNull(decodeHolder.getDeviceMessage())) {
            return Mono.empty();
        }
        return Flux.just(decodeHolder.getDeviceMessage());
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        IotFeat feature = MessageHelper.getHeadFeatThrow(deviceMessage);
        return doEncode(TcpEncodeHolder.of(feature, deviceMessage));
    }


    /**
     * 执行编码操作
     *
     * @param encodeHolder 待编码消息
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(TcpEncodeHolder encodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
//        SprMessageTypeEnums.write(encodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
