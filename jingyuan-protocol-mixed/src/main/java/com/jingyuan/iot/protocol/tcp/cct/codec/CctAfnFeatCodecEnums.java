package com.jingyuan.iot.protocol.tcp.cct.codec;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.cct.CctAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.cct.CctPrmEnums;
import com.jingyuan.common.protocol.enums.cct.fn.IotCctFunc;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 应用层功能码指令处理
 *
 * @author: cc
 * @date: 2024/6/19 9:21
 * @Version: V1.0
 */
@Slf4j
@AllArgsConstructor
public enum CctAfnFeatCodecEnums {
    /**
     * 确认∕否认
     */
    confirm(CctAfnFeatureEnums.confirm, new CctAfnConfirmDenyMessage()),
    reset(CctAfnFeatureEnums.reset, new CctAfnResetMessage()),
    link_detect(CctAfnFeatureEnums.link_detect, new CctAfnLinkDetectMessage()),
    set_comm(CctAfnFeatureEnums.set_comm, new CctAfnSetCommMessage()),
    cont_comm(CctAfnFeatureEnums.cont_comm, new CctAfnContCommMessage()),
    spare_5(CctAfnFeatureEnums.spare_5, new CctAfnSpareMessage()),
    spare_6(CctAfnFeatureEnums.spare_6, new CctAfnSpareMessage()),
    spare_7(CctAfnFeatureEnums.spare_7, new CctAfnSpareMessage()),
    spare_8(CctAfnFeatureEnums.spare_8, new CctAfnSpareMessage()),
    spare_9(CctAfnFeatureEnums.spare_9, new CctAfnSpareMessage()),
    query_comm(CctAfnFeatureEnums.query_comm, new CctAfnQueryCommMessage()),
    realtime_data(CctAfnFeatureEnums.realtime_data, new CctAfnRealtimeDataMessage()),
    history_data(CctAfnFeatureEnums.history_data, new CctAfnHistoryDataMessage()),
    data_forward(CctAfnFeatureEnums.data_forward, new CctAfnDataForwardMessage()),
    spare_11(CctAfnFeatureEnums.spare_11, new CctAfnSpareMessage());

    private final CctAfnFeatureEnums afnFeat;

    private final CctAfnMessage codec;

    private static final Map<String, CctAfnFeatCodecEnums> VALUES = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(val -> VALUES.put(val.afnFeat.getCode(), val));
    }

    public static void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        CctAfnFeatCodecEnums afn = VALUES.get(decodeHolder.getAfn().getCode());
        Assert.notNull(afn, "不支持的应用层功能码:" + afn + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        afn.codec.read(buf, decodeHolder, properties);
    }

    public static void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        CctAfnFeatureEnums afnFeatureEnum = encodeHolder.getAfn();
        CctAfnFeatCodecEnums afnCodecEnum = VALUES.get(afnFeatureEnum.getCode());
        Assert.notNull(afnCodecEnum, "不支持的应用层功能码:" + afnCodecEnum);

        // 应用层功能码AFN
        buf.writeByte(HexUtils.decodeHex(afnFeatureEnum.getCode())[0]);
        // 帧序列域SEQ
        IotCctFunc feature = encodeHolder.getFeature();
        if (feature.isNeedCon() && encodeHolder.getPrm().equals(CctPrmEnums.launch_station)) {
            buf.writeByte(0b01110000);
        } else {
            buf.writeByte(0b01100000);
        }

        // 应用层功能码
        buf.writeByte(encodeHolder.getFeature().getFeatCodeByte());

        // 数据单元
        afnCodecEnum.codec.write(buf, encodeHolder, properties);

        // 计算校验和
        int computeSum = ChecksumUtils.computeSumSingle(buf, 6, buf.writerIndex());
        buf.writeByte((byte) computeSum);
        buf.writeByte(0x16);
    }
}
