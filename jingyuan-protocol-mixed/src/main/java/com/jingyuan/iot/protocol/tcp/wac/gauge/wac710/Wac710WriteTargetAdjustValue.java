package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.wac.gauge.WacAdjustCodecEnums;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写目标调节值
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteTargetAdjustValue implements TcpMessageCodec {


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacAdjustCodecEnums.write(buf, properties);
    }
}
