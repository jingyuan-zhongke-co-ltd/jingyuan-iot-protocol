package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteSoftHardModeBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 软硬开度模式设置
 *
 * @author: dr
 * @date: 2024-12-12
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteSoftHardMode implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteSoftHardModeBo bean = BeanUtil.toBean(properties, Wac710WriteSoftHardModeBo.class);
        buf.writeByte(bean.getSoftHardMode());
    }
}
