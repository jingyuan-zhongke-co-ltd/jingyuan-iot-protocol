package com.jingyuan.iot.protocol.tcp.runda;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.constant.redis.PrefixIotConstants;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.runda.RundaWpFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.utils.lambda.FuncUtils;
import com.jingyuan.iot.protocol.tcp.runda.wrx.RundaMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.message.AcknowledgeDeviceMessage;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.property.WritePropertyMessageReply;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.data.redis.core.ReactiveValueOperations;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;


/**
 * 京源NB分体阀控协议
 *
 * @author: cc
 * @date: 2024/3/27 14:15
 * @Version: V1.0
 */
@Slf4j
public class RundaDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata RUNDA_WP_CONFIG = new DefaultConfigMetadata(
        "润达TCP配置"
        , "润达TCP配置");

    private final ReactiveStringRedisTemplate redisTemplate;

    public RundaDeviceMessageCodec(ReactiveStringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }


    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return doDecode(context);
    }


    /**
     * 执行解码操作
     *
     * @param context 待解码消息上下文
     * @return org.jetlinks.core.message.DeviceMessage
     */
    private Publisher<? extends Message> doDecode(MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        TcpDecodeHolder decodeHolder = RundaMessageTypeEnums.read(payload);
        return context.getDevice(decodeHolder.getDeviceMessage().getDeviceId())
                      .flatMap(deviceOperator -> replyCache(decodeHolder.getDeviceMessage(), deviceOperator)
                          .thenReturn(deviceOperator))
                      .flatMapMany(deviceOperator -> ack(decodeHolder, context, deviceOperator));
    }


    /**
     * 回复消息缓存
     *
     * @param message 设备消息
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    private Mono<DeviceMessage> replyCache(DeviceMessage message, DeviceOperator deviceOperator) {
        return Mono.just(message)
                   .flatMap(deviceMessage -> {
                       if (deviceMessage instanceof WritePropertyMessageReply) {
                           WritePropertyMessageReply messageReply = (WritePropertyMessageReply) deviceMessage;
                           Map<String, Object> properties = messageReply.getProperties();
                           if (CollUtil.isNotEmpty(properties)) {
                               HashMap<String, Object> cacheMap = new HashMap<>(4);

                               String wpId = Convert.toStr(properties.get(IotConstants.RUNDA_WP_ID));
                               FuncUtils.isNotBlank(wpId)
                                        .handle(() -> cacheMap.put(IotConstants.RUNDA_WP_ID, wpId));

                               String vendorCode = MessageHelper.getHeadVendor(deviceMessage);
                               FuncUtils.isNotBlank(vendorCode)
                                        .handle(() -> cacheMap.put(IotConstants.VENDOR_CODE, vendorCode));

                               String workMode = Convert.toStr(properties.get("workMode"));
                               FuncUtils.isNotBlank(wpId)
                                        .handle(() -> cacheMap.put("workMode", workMode));

                               return deviceOperator.setConfigs(cacheMap)
                                                    .thenReturn(deviceMessage);
                           }
                       }
                       return Mono.just(deviceMessage);
                   });
    }


    /**
     * 下发确认消息
     *
     * @param context 消息解码上下文
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> ack(TcpDecodeHolder decodeHolder, MessageDecodeContext context, DeviceOperator deviceOperator) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        IotFeat feat = MessageHelper.getHeadFeatThrow(message);
        if (!RundaWpFeatureEnums.ID_APPLY.getFeatCode().equals(feat.getFeatCode())) {
            return Flux.just(message);
        }

        String rawDataHex = decodeHolder.getRawDataHex();
        String id = StrUtil.sub(rawDataHex, 26, 34);
        if (StrUtil.isBlank(id) || !"00000000".equals(id)) {
            return Flux.just(decodeHolder.getDeviceMessage());
        }

        return Mono.just(deviceOperator)
                   .flatMap(operator -> {
                       String deviceId = decodeHolder.getDeviceMessage().getDeviceId();
                       AcknowledgeDeviceMessage ackMessage = IotMessageType.ackDmInstance(deviceId);
                       RundaWpFeatureEnums feature = RundaWpFeatureEnums.ID_APPLY;
                       feature.setMessageHeaders(ackMessage);

                       TcpEncodeHolder encodeHolder = TcpEncodeHolder.of();
                       encodeHolder.setDeviceMessage(ackMessage);
                       encodeHolder.setFeature(feature);

                       return genWpId(operator).map(wpId -> {
                           ackMessage.addHeader(IotConstants.RUNDA_WP_ID, wpId);
                           return encodeHolder;
                       });
                   })
                   .flatMapMany(encodeHolder -> doEncode(encodeHolder)
                       .flatMap(buf -> ((FromDeviceMessageContext) context)
                           .getSession()
                           .send(buf))
                       .flatMapMany(aBoolean -> {
                           if (aBoolean) {
                               return Flux.just(decodeHolder.getDeviceMessage(), encodeHolder.getDeviceMessage());
                           } else {
                               return Flux.just(decodeHolder.getDeviceMessage());
                           }
                       })
                   );
    }


    private Mono<String> genWpId(DeviceOperator deviceOperator) {
        return deviceOperator.getConfig(IotConstants.RUNDA_WP_ID)
                             .switchIfEmpty(Mono.defer(() -> Mono.just(() -> "0")))
                             .map(value -> Convert.toStr(value.get()))
                             .flatMap(cachedId -> {
                                 if (cachedId.length() != 6) {
                                     ReactiveValueOperations<String, String> valueOperations = redisTemplate.opsForValue();
                                     return valueOperations.get(PrefixIotConstants.IOT_RUNDA_WP_ID_INCR)
                                                           .switchIfEmpty(Mono.defer(() -> Mono.just("0")))
                                                           .flatMap(id -> {
                                                               String wpId = (id.length() != 6) ? "100000" : Convert.toStr(Convert.toInt(id) + 1);
                                                               return valueOperations
                                                                   .set(PrefixIotConstants.IOT_RUNDA_WP_ID_INCR, wpId)
                                                                   .flatMap(bool -> deviceOperator.setConfig(IotConstants.RUNDA_WP_ID, wpId))
                                                                   .then(Mono.just(wpId));
                                                           });
                                 } else {
                                     return Mono.just(cachedId);
                                 }
                             });
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());

        IotFeat feature = MessageHelper.getHeadFeatThrow(deviceMessage);
        return doEncode(TcpEncodeHolder.of(feature, deviceMessage));
    }


    /**
     * 执行编码操作
     *
     * @param encodeHolder 待编码消息
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(TcpEncodeHolder encodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        RundaMessageTypeEnums.write(encodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
