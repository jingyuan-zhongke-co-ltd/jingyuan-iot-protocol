package com.jingyuan.iot.protocol.tcp.cct.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0c.CctRtMonthFrozen;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRtMeterMonthFrozenMessage.getRelayDepth;
import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRtMeterMonthFrozenMessage.getRelayPath;
import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRtReadDayFrozenMessage.dataFormat21;
import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRtReadDayFrozenMessage.dataParse21;
import static com.jingyuan.iot.protocol.tcp.hlm.codec.HlmDecodeUtils.timeByteRead;

/**
 * 读集采器内月冻结数据
 *
 * @author: dr
 * @date: 2024-12-24
 * @Version: V1.0
 */
@Slf4j
public class CctRtConcentratorMeterMonthFrozenMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int meterCount = readGroupAndMeterCount(buf, properties);
        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        readMeterData(buf, meterDataVos, count);
        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));

    }

    /**
     * 读取表数据
     *
     * @param buf          写入缓冲区
     * @param meterDataVos 计量表数据
     * @param count        组内表数
     */
    private void readMeterData(ByteBuf buf, List<CctMeterDataVo> meterDataVos, int count) {
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
            // 计量表序号
            int i1 = buf.readByte();
            cctMeterDataVo.setMeterSerialNum(i1);
            // 计量表通信号
            byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
            BytesUtils.bytesReverse(nbAddr);
            cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
            // 采集器号
            byte[] coolAddr = ByteBufUtils.readBytes(buf, 6);
            cctMeterDataVo.setCoolAddr(Integer.valueOf(BytesUtils.binary(10, coolAddr[5])));
            //        表底数
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            String res = BCD.bcdToStr(bytes);
            res = StrUtil.subPre(res, res.length() - 2) + CharPool.DOT + StrUtil.subSuf(res, res.length() - 2);
            cctMeterDataVo.setReading(Double.parseDouble(res));

            // 状态位
            readState(buf, cctMeterDataVo);

            // 中继深度
            String relayDepth = getRelayDepth(buf);
            cctMeterDataVo.setRelayDepth(relayDepth);
            // 中继路径
            String substring = getRelayPath(buf);
            cctMeterDataVo.setRelayPath(substring);

            readTime(buf, cctMeterDataVo);

            meterDataVos.add(cctMeterDataVo);
        }
    }

    private static void readTime(ByteBuf buf, CctMeterDataVo cctMeterDataVo) {
        LocalDateTime localDateTime = null;
        try {
            localDateTime = DateUtils.toLocalDateTime(DateUtils.parseDate(timeByteRead(buf, 6), DatePatterns.NORM_DATETIME_PATTERN));
        } catch (ParseException e) {
            log.error("时间解析异常", e);
            throw new RuntimeException(e);
        }
        cctMeterDataVo.setCurrentTime(localDateTime);
    }

    /**
     * 状态位解析
     * @param buf
     * @param cctMeterDataVo
     */
    private static void readState(ByteBuf buf, CctMeterDataVo cctMeterDataVo) {
        byte state = buf.readByte();
        // 阀门状态
        int valveStatus = state & 0b00000011;
        if (valveStatus == 0) {
            cctMeterDataVo.setValveState(IotValveStateEnums.OPEN.getCode());
        } else if (valveStatus == 3) {
            cctMeterDataVo.setValveState(IotValveStateEnums.CLOSE.getCode());
        } else {
            cctMeterDataVo.setValveState(IotValveStateEnums.ERROR.getCode());
        }
        // 通讯状态
        cctMeterDataVo.setCommState(String.valueOf(state & 0b00001000));
        // 剩余量
        cctMeterDataVo.setRemainingState(String.valueOf(state & 0b00010000));
        // 攻击状态
        cctMeterDataVo.setAttackState(String.valueOf(state & 0b10000000));
        // 电池电压
        String voltageHex = ByteBufUtils.readBytesToHex(buf, 1);
        double voltage = BigDecimal
            .valueOf(HexUtils.hexToInt(voltageHex))
            .setScale(2, RoundingMode.HALF_UP)
            .add(BigDecimal.valueOf(200))
            .movePointLeft(2)
            .doubleValue();
        cctMeterDataVo.setVoltage(voltage);
    }


    /**
     * 读取组信息
     *
     * @param buf        写入缓冲区
     * @param properties 读取结果
     * @return int
     */
    public static int readGroupAndMeterCount(ByteBuf buf, Map<String, Object> properties) {
        // 总组数
        byte[] groupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(groupBytes);
        properties.put("groupCount", Integer.parseInt(BCD.bcdToStr(groupBytes)));
        // 当前组
        byte[] curGroupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(curGroupBytes);
        properties.put("curGroup", Integer.parseInt(BCD.bcdToStr(curGroupBytes)));
        // 组内表数
        int meterCount = buf.readByte();
        properties.put("meterCount", meterCount);
        dataFormat21(buf, properties);

        // 计量表类型
        int meterType = buf.readByte();
        properties.put("meterType", meterType);
        return meterCount;
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctRtMonthFrozen bean = BeanUtil.toBean(properties, CctRtMonthFrozen.class);
        //月年
        Date dateParam = bean.getDateParam();
        dataParse21(buf, dateParam);
        // 设备类型
        buf.writeByte(bean.getMeterType());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(bean.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
    }
}
