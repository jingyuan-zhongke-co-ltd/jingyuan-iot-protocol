package com.jingyuan.iot.protocol.http;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.enums.NorthMessageTypes;
import com.jingyuan.common.flowable.model.north.event.NorthLogEventBo;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotTopicConstants;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.http.codec.HttpMessageCodecEnums;
import com.jingyuan.iot.protocol.http.topic.HttpResponseHelper;
import io.netty.buffer.ByteBufUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.defaults.Authenticator;
import org.jetlinks.core.device.*;
import org.jetlinks.core.event.EventBus;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.MessageType;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.codec.http.HttpExchangeMessage;
import org.jetlinks.core.message.codec.http.SimpleHttpResponseMessage;
import org.jetlinks.core.message.codec.http.websocket.WebSocketSessionMessage;
import org.jetlinks.core.message.property.WritePropertyMessage;
import org.jetlinks.core.trace.DeviceTracer;
import org.jetlinks.core.trace.FluxTracer;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.http.MediaType;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.Objects;

/**
 * Http 的消息编解码器
 *
 * @author zhouhao
 * @since 3.0.0
 */
@Slf4j
public class HttpDeviceMessageCodec implements DeviceMessageCodec, Authenticator {

    static AuthenticationResponse deviceNotFound = AuthenticationResponse.error(404, "设备不存在");
    private final Transport transport;
    private final ReactiveStringRedisTemplate redisTemplate;
    private final EventBus eventBus;

    public HttpDeviceMessageCodec(Transport transport, ReactiveStringRedisTemplate redisTemplate, EventBus eventBus) {
        this.transport = transport;
        this.redisTemplate = redisTemplate;
        this.eventBus = eventBus;
    }

    public HttpDeviceMessageCodec(ReactiveStringRedisTemplate redisTemplate, EventBus eventBus) {
        this(DefaultTransport.HTTP, redisTemplate, eventBus);
    }

    @Override
    public Transport getSupportTransport() {
        return transport;
    }

    @Nonnull
    @Override
    public Mono<EncodedMessage> encode(@Nonnull MessageEncodeContext context) {
        DeviceMessage message = (DeviceMessage) context.getMessage();

        // 设备端编号以 IMEI 为唯一标识，实际指令下发时，需要获取表号
        String meterAddr = MessageHelper.getHeadMeter(message);
        if (StrUtil.isNotBlank(meterAddr)) {
            return getEncodedMessage(message, meterAddr);
        }
        return Mono.justOrEmpty(context.getDevice())
                   .flatMap(operator -> operator
                       .getConfigs(IotConstants.METER_ADDR)
                       .map(values -> MessageHelper.getValueStr(values, IotConstants.METER_ADDR, "设备未上过线，设备信息不完善"))
                       .flatMap(meter -> getEncodedMessage(message, meter)));
    }

    private static Mono<EncodedMessage> getEncodedMessage(DeviceMessage message, String meterAddr) {
        if (message instanceof WritePropertyMessage) {
            WritePropertyMessage writePropertyMessage = (WritePropertyMessage) message;
            writePropertyMessage.setDeviceId(meterAddr);
        }
        HttpMessageCodecEnums.encode(message);
        return Mono.just(SimpleHttpResponseMessage
                             .builder()
                             .contentType(MediaType.APPLICATION_JSON)
                             .status(200)
                             .body(JsonUtils.toJsonByte(message))
                             .build());
    }

    @Nonnull
    @Override
    public Flux<DeviceMessage> decode(@Nonnull MessageDecodeContext context) {
        if (context.getMessage() instanceof HttpExchangeMessage) {
            return decodeHttp(context);
        }
        if (context.getMessage() instanceof WebSocketSessionMessage) {
            return decodeWebsocket(context);
        }
        return Flux.empty();
    }

    private Flux<DeviceMessage> decodeWebsocket(MessageDecodeContext context) {
        WebSocketSessionMessage msg = ((WebSocketSessionMessage) context.getMessage());
        return Mono
            .justOrEmpty(MessageType.convertMessage(msg.payloadAsJson()))
            .cast(DeviceMessage.class)
            .flux();
    }

    private Flux<DeviceMessage> decodeHttp(MessageDecodeContext context) {
        HttpExchangeMessage exchangeMessage = (HttpExchangeMessage) context.getMessage();

        String[] topics = HttpMessageCodecEnums.splitTopic(exchangeMessage.getPath());
        if (ArrayUtil.isEmpty(topics) || ObjectUtil.isNull(NorthChannelEnums.getByPathVariable(topics[0]))) {
            return exchangeMessage
                .response(HttpResponseHelper.notFound("request url not exist"))
                .thenMany(Flux.empty());
        }

        NorthChannelEnums northChannel = NorthChannelEnums.getByPathVariable(topics[0]);
        return validateClient(exchangeMessage, topics, northChannel)
            .flatMapMany(topic -> exchangeMessage
                .payload()
                .flatMapMany(buf -> {
                    byte[] body = ByteBufUtil.getBytes(buf);
                    return HttpMessageCodecEnums.decode(context, redisTemplate, eventBus, topic, body);
                }))
            .switchOnFirst((s, flux) -> {
                //有结果则认为成功
                if (s.hasValue()) {
                    return exchangeMessage.response(HttpResponseHelper.ok())
                                          .thenMany(flux);
                } else {
                    return exchangeMessage
                        .payload()
                        .flatMapMany(buf -> {
                            byte[] body = ByteBufUtil.getBytes(buf);
                            if (ObjectUtil.isNull(s.getThrowable())) {
                                String msg = exchangeMessage.getQueryParameter("msg").orElse("");
                                SimpleHttpResponseMessage responseMessage;
                                NorthLogEventBo logVo;
                                Object requestParam = ArrayUtil.isEmpty(body) ? exchangeMessage.getQueryParameters() : JsonUtils.parseObject(body, Map.class);
                                if (StrUtil.isBlank(msg)) {
                                    responseMessage = HttpResponseHelper.badRequest("原始数据帧或 IMEI 解析为空");
                                    logVo = NorthLogEventBo.noParseEvent(northChannel, false, NorthMessageTypes.DATA_PARSE_ERR.getCode(), JsonUtils.toJsonString(requestParam), "原始数据帧或 IMEI 解析为空");
                                } else {
                                    // ONENET 平台数据推送校验适配
                                    logVo = NorthLogEventBo.noParseEvent(northChannel, true, NorthMessageTypes.INTERFACE_CHECK.getCode(), JsonUtils.toJsonString(requestParam), StrUtil.EMPTY);
                                    responseMessage = HttpResponseHelper.instance(msg);
                                }
                                return eventBus
                                    .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS, logVo)
                                    .then(exchangeMessage.response(responseMessage).then(Mono.empty()));
                            } else {
                                String errMessage = s.getThrowable().getMessage();
                                String deviceId = StrUtil.EMPTY;
                                if (StrUtil.isNotBlank(errMessage) && errMessage.contains("：【") && errMessage.contains("】")) {
                                    deviceId = StrUtil.subBetween(errMessage, "：【", "】");
                                }
                                return eventBus
                                    .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS,
                                             NorthLogEventBo.noParseEvent(northChannel, false, NorthMessageTypes.DATA_PARSE_ERR.getCode(),
                                                                          JsonUtils.toJsonString(JsonUtils.parseObject(body, Map.class)), deviceId, errMessage))
                                    .then(exchangeMessage
                                              .response(HttpResponseHelper.badRequest(errMessage))
                                              .then(Mono.empty()));
                            }
                        });
                }
            })
            .onErrorResume(err -> exchangeMessage
                .response(HttpResponseHelper.badRequest(err.getMessage()))
                .then(Mono.error(err)))
            // 跟踪信息
            .as(FluxTracer.create(DeviceTracer.SpanName.decode("deviceId"),
                                  builder -> builder.setAttribute(DeviceTracer.SpanKey.message, exchangeMessage.print())));
    }

    private Mono<String[]> validateClient(HttpExchangeMessage message, String[] topic, NorthChannelEnums northChannel) {
        String signature = message.getQueryParameter("_signature_").orElse("");
        String msg = message.getQueryParameter("msg").orElse("");
        String nonce = message.getQueryParameter("nonce").orElse("");
        if (StrUtil.isAllNotBlank(signature, msg, nonce)) {
            return message
                .response(HttpResponseHelper.instance(msg))
                .then(Mono.empty());
        }

        String param = message
            .getQueryParameter(IotConstants.CLIENT_ID)
            .orElseGet(() -> ArrayUtil.length(topic) == 4 ? topic[3] : StrUtil.EMPTY);
        if (ObjectUtil.isNull(northChannel) || !northChannel.getClientId().equals(param)) {
            return message
                .response(HttpResponseHelper.unauthorized())
                .then(Mono.empty());
        }
        return Mono.just(topic);
    }

    @Override
    public Mono<AuthenticationResponse> authenticate(@Nonnull AuthenticationRequest request, @Nonnull DeviceOperator device) {
        if (!(request instanceof WebsocketAuthenticationRequest)) {
            return Mono.just(AuthenticationResponse.error(400, "不支持的认证方式"));
        }
        WebsocketAuthenticationRequest req = ((WebsocketAuthenticationRequest) request);
        String token = req
            .getSocketSession()
            .getQueryParameters()
            .get("token");

        if (StrUtil.isBlank(token)) {
            return Mono.just(AuthenticationResponse.error(401, "认证参数错误"));
        }

        return device
            .getConfig("bearer_token")
            //校验token
            .filter(value -> Objects.equals(value.asString(), token))
            .map(ignore -> AuthenticationResponse.success(device.getDeviceId()))
            //未配置或者配置不对
            .switchIfEmpty(Mono.fromSupplier(() -> AuthenticationResponse.error(401, "token错误")));
    }


    @Override
    public Mono<AuthenticationResponse> authenticate(@Nonnull AuthenticationRequest request, @Nonnull DeviceRegistry registry) {
        if (!(request instanceof WebsocketAuthenticationRequest)) {
            return Mono.just(AuthenticationResponse.error(400, "不支持的认证方式"));
        }
        WebsocketAuthenticationRequest req = ((WebsocketAuthenticationRequest) request);
        String[] paths = HttpMessageCodecEnums.splitTopic(req.getSocketSession().getPath());
        if (paths.length < 1) {
            return Mono.just(AuthenticationResponse.error(400, "URL格式错误"));
        }

        return registry
            .getDevice(paths[1])
            .flatMap(device -> authenticate(request, device))
            .defaultIfEmpty(deviceNotFound);
    }
}
