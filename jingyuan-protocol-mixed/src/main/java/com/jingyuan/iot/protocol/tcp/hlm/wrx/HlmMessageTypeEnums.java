package com.jingyuan.iot.protocol.tcp.hlm.wrx;

import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.cct.CctSeqFfEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmDirEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmPrmEnums;
import com.jingyuan.common.protocol.enums.hlm.HlmProtocolFeatureEnums;
import com.jingyuan.common.protocol.enums.hlm.fn.HlmAfnConfirmRwEnums;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 海龙马消息类型枚举
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public enum HlmMessageTypeEnums {
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), HlmWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), HlmWritePropertyMessageReply::new),
    ;

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    HlmMessageTypeEnums(String messageType,
                        Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    private static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, HlmMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (HlmMessageTypeEnums value : values()) maps.put(value.messageType, value);
    }

    /**
     * 数据写出
     *
     * @param messageEncodeHolder 待写出消息
     * @param buf                 待写入缓冲区
     */
    public static void write(HlmTcpEncodeHolder messageEncodeHolder, ByteBuf buf) {
        DeviceMessage message = messageEncodeHolder.getDeviceMessage();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
                MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        HlmMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);

        // 链路层
        buf.writeByte(0x68);
        byte[] lBytes = BytesUtils.shortToBytes((short) messageEncodeHolder.getL(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(lBytes);
        buf.writeByte(0x68);

        // 控制码
        buf.writeBytes(BytesUtils.hexToBytes("40"));
        //协议id
        buf.writeBytes(BytesUtils.hexToBytes(messageEncodeHolder.getProtocolCode().getCode()));
        // 地址域
        buf.writeBytes(BytesUtils.hexToBytes(messageEncodeHolder.getDeviceId()));

        // 应用层
        tcp.write(buf, messageEncodeHolder);
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 待写出缓冲区
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static HlmTcpDecodeHolder read(ByteBuf buf) {
        HlmTcpDecodeHolder decodeHolder = HlmTcpDecodeHolder.of();
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        // 校验和校验
        HlmAfnConfirmRwEnums.confirm.getCheckResult(decodeHolder.getRawDataHex());

        ByteBufUtils.readBytesToHex(buf, 4);
        // 解析控制码 协议ID
        readControlCode(buf.readByte(), ByteBufUtils.readBytesToHex(buf, 1), decodeHolder);

        // 解析地址域
        readAddress(buf, decodeHolder);

        // 链路用户数据区
        // 应用层功能码
        decodeHolder.setAfn(HlmAfnFeatureEnums.getByCode(BytesUtils.byteToHex(buf.readByte())));
        // seq
        readSeq(buf.readByte(), decodeHolder);

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);

        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    private static void readSeq(byte seq, HlmTcpDecodeHolder decodeHolder) {
        CctSeqFfEnums seqFfEnum = CctSeqFfEnums.getBySeq(seq);
        decodeHolder.setSeqFf(seqFfEnum);
        CctSeqConEnums seqConEnum = CctSeqConEnums.getBySeq(seq);
        decodeHolder.setSeqCon(seqConEnum);
    }

    private static void readAddress(ByteBuf buf, HlmTcpDecodeHolder decodeHolder) {
        HlmProtocolFeatureEnums protocol = (HlmProtocolFeatureEnums) decodeHolder.getProtocol();
        decodeHolder.setDeviceId(ByteBufUtils.readBytesToHex(buf, protocol.getAddressL()));
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf                 待写出缓冲区
     * @param messageDecodeHolder 消息解码数据包装类
     * @param handler             消息ID处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf, HlmTcpDecodeHolder messageDecodeHolder, BiFunction<DeviceMessage, Integer, T> handler) {
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = HlmMessageTypeEnums.writePropertyReply.forTcp.get();

        // 从ByteBuf读取
        nbMessage.read(buf, messageDecodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        message.thingId(DeviceThingType.device, messageDecodeHolder.getDeviceId());

        int msa =messageDecodeHolder.getPacketId();
        return handler.apply(message, msa == 1 ? Integer.parseInt(messageDecodeHolder.getDeviceId()) : msa);
    }

    private static void readControlCode(byte controlCode, String protocolCode, HlmTcpDecodeHolder messageDecodeHolder) {
        messageDecodeHolder.setDir(HlmDirEnums.getByControlCode(controlCode));
        messageDecodeHolder.setPrm(HlmPrmEnums.getByControlCode(controlCode));
        messageDecodeHolder.setProtocol(HlmProtocolFeatureEnums.getByCode(protocolCode));
    }

}
