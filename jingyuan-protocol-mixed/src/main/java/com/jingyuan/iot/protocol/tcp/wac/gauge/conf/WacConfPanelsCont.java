package com.jingyuan.iot.protocol.tcp.wac.gauge.conf;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfAllParamBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.BcdAndPad;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.revertBCD;

/**
 * 面板调节模式
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
public class WacConfPanelsCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值
        byte[] bytes = ByteBufUtils.readBytes(buf, 2);
        genRead(buf, properties);

    }

    public static void genRead(ByteBuf buf, Map<String, Object> properties) {
        //  调节周期（BCD码）30分钟,单位分钟
        properties.put("adjustCycle", Convert.toInt(revertBCD(buf, 2)));
        //  调节死区（BCD码）0.50℃
        properties.put("adjustDeadZone", Convert.toInt(revertBCD(buf, 2)));

        //  P参数  0.01（BCD码）
        properties.put("p", Convert.toFloat(revertBCD(buf, 2)) / 100);

        //  I参数  0.01（BCD码）
        properties.put("i", Convert.toFloat(revertBCD(buf, 2)) / 100);

        //  D参数  0.01（BCD码）
        properties.put("d", Convert.toFloat(revertBCD(buf, 2)) / 100);
        //  定比例调节，每次调节按照5%调节（BCD码）
        properties.put("proportional", Convert.toInt(revertBCD(buf, 2)));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacConfAllParamBo bean = BeanUtil.toBean(properties, WacConfAllParamBo.class);
        // 目标调节值
        buf.writeBytes(BcdAndPad(bean.getAdjustValue(), 2));
        writeGeneralParma(buf, bean);
    }

    static void writeGeneralParma(ByteBuf buf, WacConfAllParamBo bean) {
        //  调节周期（BCD码）30分钟,单位分钟
        buf.writeBytes(BcdAndPad(bean.getAdjustCycle(), 2));
        //  调节死区（BCD码）0.50℃
        short adjustDeadZone = (short) new BigDecimal(Convert.toStr(bean.getAdjustDeadZone()))
            .movePointRight(2)
            .intValue();
        buf.writeBytes(BcdAndPad(adjustDeadZone, 2));
        //  P参数  0.01（BCD码）
        short p = (short) new BigDecimal(Convert.toStr(bean.getP()))
            .movePointRight(2)
            .intValue();
        buf.writeBytes(BcdAndPad(p, 2));
        //  I参数  0.01（BCD码）
        short i = (short) new BigDecimal(Convert.toStr(bean.getI()))
            .movePointRight(2)
            .intValue();
        buf.writeBytes(BcdAndPad(i, 2));
        //  D参数  0.01（BCD码）
        short d = (short) new BigDecimal(Convert.toStr(bean.getD()))
            .movePointRight(2)
            .intValue();
        buf.writeBytes(BcdAndPad(d, 2));
        //  定比例调节，每次调节按照5%调节（BCD码）
        short proportional = bean.getProportional();
        buf.writeBytes(BcdAndPad(proportional, 2));
    }
}
