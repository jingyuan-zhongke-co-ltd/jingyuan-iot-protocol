package com.jingyuan.iot.protocol.tcp.wac.gauge.adjust;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.adjust.WacAdjustInstantFlowBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 瞬时流量调节
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class WacAdjustInstantFlow implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值，float，单位m3/h
        properties.put("adjustInstantFlow", buf.readFloatLE());
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacAdjustInstantFlowBo instantFlowBo = BeanUtil.toBean(properties, WacAdjustInstantFlowBo.class);
        buf.writeFloatLE(instantFlowBo.getAdjustInstantFlow());
    }
}
