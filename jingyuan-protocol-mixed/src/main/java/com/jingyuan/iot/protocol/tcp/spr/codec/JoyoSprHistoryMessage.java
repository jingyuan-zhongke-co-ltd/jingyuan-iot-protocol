package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.spr.SprHistoryBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 历史数据
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprHistoryMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(JoyoSprFeatureEnums.HISTORY.getFeatCode()));
        SprHistoryBo sprHistoryBo = BeanUtil.toBean(properties, SprHistoryBo.class);
        String format = DateUtil.format(sprHistoryBo.getSearchDate(), DatePattern.PURE_DATE_PATTERN);
        buf.writeBytes(BytesUtils.hexToBytes(StrUtil.sub(format, 2, format.length() + 1)));
    }
}
