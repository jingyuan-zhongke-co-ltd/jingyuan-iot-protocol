package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.spr.SprReportCycleBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 上报周期设置与查询帧
 * 帧长度：15字节
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprReportCycleMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(JoyoSprFeatureEnums.REPORT_CYCLE.getFeatCode()));
        SprReportCycleBo reportCycleBo = BeanUtil.toBean(properties, SprReportCycleBo.class);
        buf.writeByte(reportCycleBo.getReportCycle().byteValue());
    }
}
