package com.jingyuan.iot.protocol.tcp.cct.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0c.CctRtNodeDayFrozenMessageBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRt0DayFrozenMessage.dataFormat20;
import static com.jingyuan.iot.protocol.tcp.cct.afn.afn0c.CctRt0DayFrozenMessage.dataParse20;

/**
 * 请求节点设备日冻结数据
 *
 * @author: dr
 * @date: 2024-12-24
 * @Version: V1.0
 */
@Slf4j
public class CctRtNodeDayFrozenMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int meterCount = readGroupAndMeterCount(buf, properties);
        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
            //            设备ID_1
            byte[] deviceIdBytes = ByteBufUtils.readBytes(buf, 2);
            cctMeterDataVo.setDevId(Convert.toStr(BytesUtils.bytesToShort(deviceIdBytes)));

            //        累计用量
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            String res = BCD.bcdToStr(bytes);
            res = StrUtil.subPre(res, res.length() - 2) + CharPool.DOT + StrUtil.subSuf(res, res.length() - 2);
            cctMeterDataVo.setReading(Double.parseDouble(res));
            meterDataVos.add(cctMeterDataVo);
        }

        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));

    }

    /**
     * 读取组信息
     *
     * @param buf        写入缓冲区
     * @param properties 读取结果
     * @return int
     */
    public static int readGroupAndMeterCount(ByteBuf buf, Map<String, Object> properties) {
        // 总组数
        byte[] groupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(groupBytes);
        properties.put("groupCount", Integer.parseInt(BCD.bcdToStr(groupBytes)));
        // 当前组
        byte[] curGroupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(curGroupBytes);
        properties.put("curGroup", Integer.parseInt(BCD.bcdToStr(curGroupBytes)));
        // 组内表数
        int meterCount = buf.readByte();
        properties.put("meterCount", meterCount);
        dataFormat20(buf, properties);
        // 计量表类型
        int meterType = buf.readByte();
        properties.put("meterType", meterType);
        return meterCount;
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctRtNodeDayFrozenMessageBo bean = BeanUtil.toBean(properties, CctRtNodeDayFrozenMessageBo.class);
        //日月年
        Date dateParam = bean.getDateParam();
        dataParse20(buf, dateParam);
//        设备ID
        buf.writeBytes(BytesUtils.shortToBytes(bean.getDevId(), ByteOrder.LITTLE_ENDIAN));
        // 设备类型
        buf.writeByte(bean.getMeterType());
    }
}
