package com.jingyuan.iot.protocol.tcp.hlm.afn.afn0a;

import com.jingyuan.common.protocol.enums.hlm.fn.HlmAAfnDataUploadEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

/**
 * 属性上报 超声波水表
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class HlmUltAfnDataUploadsMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {
        HashMap<String, Object> properties = new HashMap<>();
        byte[] bytes = ByteBufUtils.readBytes(buf, 41);
        String validData = BytesUtils.bytesToHex(bytes);
        // 校验和校验
        HlmAAfnDataUploadEnums.ULTRASONIC.getCheckResult(validData);

        //        int lowUsage = bytes[51] & 0xFF; // 用水过低用量
//        int status = bytes[52]; // 表状态
//        int checksum = bytes[53]; // 校验码
//        int endFlag = bytes[54]; // 结束符
//        读取仪表数据表端回复帧起始符

        int index = 0;
        //读取仪表数据表端回复帧起始符
        String x68 = validData.substring(index, index += 2);
        //表类型
        String meterType = validData.substring(index, index += 2);
        properties.put("meterType", meterType);
        //内层表地址
        String meterAddress = validData.substring(index, index += 14);
        properties.put("innerMeterAddr", meterAddress);
        //控制码
        String controlCode = validData.substring(index, index += 2);
        //数据长度
        String dataLength = validData.substring(index, index += 1);
        //数据标识
        String dataIdentifier = validData.substring(index, index += 4);
        //ser
        String ser = validData.substring(index, index += 2);
        properties.put("ser", ser);

        // 净流量
        String netFlow = unitNumber(new BigDecimal(reversePairsAndRemoveLeadingZeros(validData.substring(30, 38))), validData.substring(28, 30));
        properties.put("reading", netFlow);

        // 瞬时流量
        String instantFlowUnit = unitNumber(new BigDecimal(reversePairsAndRemoveLeadingZeros(validData.substring(40, 48))), validData.substring(38, 40));
        properties.put("instantFlowUnit", instantFlowUnit);

        // 正向流量
        String forwardFlowUnit = unitNumber(new BigDecimal(reversePairsAndRemoveLeadingZeros(validData.substring(50, 58))), validData.substring(48, 50));
        properties.put("forwardFlow", forwardFlowUnit);

        // 反向流量
        String reverseFlow = unitNumber(new BigDecimal(StringUtils.reverse(validData.substring(60, 68))), validData.substring(58, 60));
        properties.put("reverseFlow", reverseFlow);

        // 供水温度
        String waterTemperature = new BigDecimal(validData.substring(72, 74) + validData.substring(70, 72) + validData.substring(68, 70))
            .divide(new BigDecimal("100"))
            .setScale(2)
            .toString();
        properties.put("waterTemperature", waterTemperature);

        //        用水过低用量（单位L)  转成十进制后为50，x0.1表示5.0L
        String lowUsageS = validData.substring(76, 78) + validData.substring(74, 76);
        int lowUsage = BytesUtils.bytesToShort(BytesUtils.hexToBytes(lowUsageS));
        properties.put("lowUsage", lowUsage * 0.1);
        String substring = validData.substring(78, validData.length());
//        表状态ST1 ST2
        String st1 = validData.substring(validData.length() - 4, validData.length() - 2);
        String st2 = validData.substring(validData.length() - 2, validData.length());
        st1(st1, properties);
        st2(st2, properties);
//       todo 校验码

        map.put("innerData", properties);
    }

    private void st1(String st1, Map<String, Object> properties) {
        byte[] moduleStatus = BytesUtils.hexToBytes(st1);
        int statusByte = moduleStatus[0];
        // 解析 BIT0 到 BIT7

        // 阀门状态，由BIT0和BIT1共同决定
        int valveStatus = (statusByte & 0b00000011);
        // 滴漏 报警状态，由BIT2决定
        int alarmStatus = (statusByte & 0b00000100) >> 2;
        //  爆管 保留位，由BIT3决定
        int reservedBits = (statusByte & 0b00001000) >> 3;
        // 低水位报警状态，由BIT6和BIT7共同决定
        int lowWaterAlarm = (statusByte & 0b11000000) >> 6;
        properties.put("valveState", Integer.toBinaryString(valveStatus));
        properties.put("alarmStatus", alarmStatus);
        properties.put("burstPipeStatus", reservedBits);
        properties.put("lowWaterAlarm", Integer.toBinaryString(lowWaterAlarm));
    }


    private void st2(String st2, Map<String, Object> properties) {
        byte[] moduleStatus = BytesUtils.hexToBytes(st2);
        int statusByte = moduleStatus[0];

        // 解析 BIT0 到 BIT7

        // 电池状态，由BIT0决定
        int batteryStatus = (statusByte & 0b00000001);
        // 空管状态，由BIT1决定
        int emptyTubeStatus = (statusByte & 0b00000010) >> 1;
        // 反流状态，由BIT2决定
        int reverseFlowStatus = (statusByte & 0b00000100) >> 2;
        // 超范围状态，由BIT3决定
        int overRangeStatus = (statusByte & 0b00001000) >> 3;
        // 水温状态，由BIT4决定
        int waterTempStatus = (statusByte & 0b00010000) >> 4;
        // EEPROM状态，由BIT5决定
        int eeStatus = (statusByte & 0b00100000) >> 5;
        // 进水端换能器
        int iTransducer = (statusByte & 0b01000000) >> 6;
        // 出水端换能器
        int oTransducer = (statusByte & 0b10000000) >> 6;

        // 状态字 ST2 的解析
        properties.put("voltageState", batteryStatus);
        properties.put("emptyTubeStatus", emptyTubeStatus);
        properties.put("reverseFlowStatus", reverseFlowStatus);
        properties.put("overRangeStatus", overRangeStatus);
        properties.put("waterTempStatus", waterTempStatus);
        properties.put("eeStatus", eeStatus);
        properties.put("inletValveStatus", iTransducer);
        properties.put("outletValveStatus", oTransducer);

    }

    public static String reversePairsAndRemoveLeadingZeros(String input) {
        StringBuilder result = new StringBuilder();

        // 逐步处理字符串并反转每对字符
        for (int i = 0; i < input.length(); i += 2) {
            // 反转并添加一对字符
            result.append(input.charAt(i + 1)).append(input.charAt(i));
        }

        // 转为字符串
        String finalResult = result.reverse().toString();

        // 去掉前面的零
        String re = finalResult.replaceFirst("^0+", "");
        return StringUtils.equals(re, "") ? "0" : re;
    }

    public static String unitNumber(BigDecimal number, String unit) {

        BigDecimal readNUmber = new BigDecimal("0");
        switch (unit) {
            case "2a":
                //0.0001
                readNUmber = number.multiply(new BigDecimal("0.0001")).setScale(4);
                break;
            case "2b":
                //0.001
                readNUmber = number.multiply(new BigDecimal("0.001")).setScale(4);
                break;
            case "2c":
                //0.01
                readNUmber = number.multiply(new BigDecimal("0.01")).setScale(2);
                break;
            case "2d":
                //0.01
                readNUmber = number.multiply(new BigDecimal("0.1")).setScale(1);
                break;
            case "2e":
                //1;
                readNUmber = number;
                break;
            case "35":
                //0.0001
                readNUmber = number.multiply(new BigDecimal("0.0001")).setScale(4);
                break;
        }
        return readNUmber.toString();
    }
}
