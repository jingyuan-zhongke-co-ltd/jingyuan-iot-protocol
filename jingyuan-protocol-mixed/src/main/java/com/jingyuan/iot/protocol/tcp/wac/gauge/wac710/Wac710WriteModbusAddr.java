package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteModbusAddrBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 修改modbus本机地址
 *
 * @author: dr
 * @date: 2024-12-12
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteModbusAddr implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteModbusAddrBo bean = BeanUtil.toBean(properties, Wac710WriteModbusAddrBo.class);
        buf.writeByte(bean.getModbusAddr());
    }
}
