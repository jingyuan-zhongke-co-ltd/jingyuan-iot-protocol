package com.jingyuan.iot.protocol.tcp.lcp.afn.afn04;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn04.LcpValveStateBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 遥控计量表关阀
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpContValveMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpValveStateBo bean = BeanUtil.toBean(properties, LcpValveStateBo.class);
        String valveState = bean.getValveState();
        buf.writeBytes(BytesUtils.hexToBytes(valveState));

    }


}
