package com.jingyuan.iot.protocol.tcp.cct.wrx;

import com.jingyuan.common.protocol.constant.IotCctConstants;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cct.codec.CctAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.ChildDeviceMessageReply;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class CctChildDeviceMessageReply implements TcpMessageAdapter<ChildDeviceMessageReply> {

    private ChildDeviceMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder tcpDecodeHolder) {
        message = new ChildDeviceMessageReply();
        CctTcpDecodeHolder decodeHolder = (CctTcpDecodeHolder) tcpDecodeHolder;
        Map<String, Object> map = CctWritePropertyMessageReply.initReadMap(decodeHolder);
        CctAfnFeatCodecEnums.read(buf, decodeHolder, map);
        map.put(IotCctConstants.ADCODE, decodeHolder.getAdcode());

        message.setChildDeviceId("");
        message.setChildDeviceMessage(new WritePropertyMessageReply());
    }


    @Override
    public void setMessage(ChildDeviceMessageReply message) {
        this.message = message;
    }


    @Override
    public ChildDeviceMessageReply getMessage() {
        return message;
    }
}
