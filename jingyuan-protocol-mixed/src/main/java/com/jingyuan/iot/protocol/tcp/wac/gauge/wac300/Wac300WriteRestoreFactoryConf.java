package com.jingyuan.iot.protocol.tcp.wac.gauge.wac300;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 恢复出厂配置(生产 FF/00)
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac300WriteRestoreFactoryConf implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {

    }

}
