package com.jingyuan.iot.protocol.tcp.wac.gauge.conf;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfInstantFlowBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.genRead;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.writeGeneralParma;

/**
 * 瞬时流量调节
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
public class WacConfInstantFlow implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值，float，单位m3/h
        properties.put("adjustInstantFlow", buf.readFloatLE());
        genRead(buf, properties);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacConfInstantFlowBo bean = BeanUtil.toBean(properties, WacConfInstantFlowBo.class);
        // 目标调节值
        buf.writeBytes(BytesUtils.intToBytes(Convert.toShort(bean.getAdjustInstantFlow()), ByteOrder.LITTLE_ENDIAN));

        writeGeneralParma(buf, bean);
    }
}
