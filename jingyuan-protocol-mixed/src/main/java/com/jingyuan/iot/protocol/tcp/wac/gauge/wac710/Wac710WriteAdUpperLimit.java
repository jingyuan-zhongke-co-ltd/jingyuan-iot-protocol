package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteAdUpperLimitBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 设置阀门电流AD值上限
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteAdUpperLimit implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteAdUpperLimitBo bean = BeanUtil.toBean(properties, Wac710WriteAdUpperLimitBo.class);
        buf.writeBytes(BytesUtils.shortToBytes(bean.getValveAdUpperLimit(), ByteOrder.LITTLE_ENDIAN));
    }
}
