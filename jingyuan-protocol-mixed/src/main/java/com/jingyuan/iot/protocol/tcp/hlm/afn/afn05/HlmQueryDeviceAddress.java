package com.jingyuan.iot.protocol.tcp.hlm.afn.afn05;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询设备地址
 *
 * @author: dr
 * @date: 2024-12-03
 * @Version: V1.0
 */
@Slf4j
public class HlmQueryDeviceAddress implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        数据包标识	BIN	0 ~ 255	1	设备收到下发命令包中的数据包标识
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
//        协议ID	BIN	0x4A	1	0B表示抄京源大口径热表(NB)，
        String protocolId = BytesUtils.byteToHex(buf.readByte());
        properties.put("protocolId", protocolId);
//        设备地址	BCD		4	如，设备地址为12345678。则此处为16进制数据12 34 56 78
        byte[] deviceAdd = ByteBufUtils.readBytes(buf, 4);
        properties.put("deviceAdd", BytesUtils.bcdToStr(deviceAdd));
    }
}
