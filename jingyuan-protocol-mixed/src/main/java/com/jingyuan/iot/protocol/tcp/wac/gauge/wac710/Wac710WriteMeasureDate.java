package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteMeasureDateBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写开启/结束计量日期
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteMeasureDate implements TcpMessageCodec {


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteMeasureDateBo measureDateBo = BeanUtil.toBean(properties, Wac710WriteMeasureDateBo.class);
        byte[] beginBytes = BytesUtils.hexToBytes(DateUtils.format(measureDateBo.getBeginMeasureDate(), DatePatterns.MONTH_DAY_FORMAT));
        BytesUtils.bytesReverse(beginBytes);
        byte[] endBytes = BytesUtils.hexToBytes(DateUtils.format(measureDateBo.getEndMeasureDate(), DatePatterns.MONTH_DAY_FORMAT));
        BytesUtils.bytesReverse(endBytes);
        buf.writeBytes(beginBytes);
        buf.writeBytes(endBytes);
    }
}
