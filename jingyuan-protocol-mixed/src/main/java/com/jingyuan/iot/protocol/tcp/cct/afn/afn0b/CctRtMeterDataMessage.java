package com.jingyuan.iot.protocol.tcp.cct.afn.afn0b;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0b.CctRtMeterDataBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 请求计量表当前数据
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctRtMeterDataMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int meterCount = readGroupAndMeterCount(buf, properties);

        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        readMeterData(buf, meterDataVos, count);
        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));
    }


    /**
     * 读取组信息
     *
     * @param buf        写入缓冲区
     * @param properties 读取结果
     * @return int
     */
    public static int readGroupAndMeterCount(ByteBuf buf, Map<String, Object> properties) {
        // 总组数
        byte[] groupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(groupBytes);
        properties.put("groupCount", Integer.parseInt(BCD.bcdToStr(groupBytes)));
        // 当前组
        byte[] curGroupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(curGroupBytes);
        properties.put("curGroup", Integer.parseInt(BCD.bcdToStr(curGroupBytes)));
        // 组内表数
        int meterCount = buf.readByte();
        properties.put("meterCount", meterCount);
        // 计量表类型
        int meterType = buf.readByte();
        properties.put("meterType", meterType);
        return meterCount;
    }

    /**
     * 读取表数据
     *
     * @param buf          写入缓冲区
     * @param meterDataVos 计量表数据
     * @param count        组内表数
     */
    private void readMeterData(ByteBuf buf, List<CctMeterDataVo> meterDataVos, int count) {
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
            // 计量表通信号
            byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
            BytesUtils.bytesReverse(nbAddr);
            cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
            // 数据项
            int dataItem = buf.readByte();
            cctMeterDataVo.setDataItem(dataItem);

            // 计量表数据体
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            String res = BCD.bcdToStr(bytes);
            res = StrUtil.subPre(res, res.length() - 2) + CharPool.DOT + StrUtil.subSuf(res, res.length() - 2);
            cctMeterDataVo.setReading(Double.parseDouble(res));

            // 状态位
            byte state = buf.readByte();
            // 阀门状态
            int valveStatus = state & 0b00000011;
            if (valveStatus == 0) {
                cctMeterDataVo.setValveState(IotValveStateEnums.OPEN.getCode());
            } else if (valveStatus == 3) {
                cctMeterDataVo.setValveState(IotValveStateEnums.CLOSE.getCode());
            } else {
                cctMeterDataVo.setValveState(IotValveStateEnums.ERROR.getCode());
            }
            // 通讯状态
            cctMeterDataVo.setCommState(String.valueOf(state & 0b00001000));
            // 剩余量
            cctMeterDataVo.setRemainingState(String.valueOf(state & 0b00010000));
            // 攻击状态
            cctMeterDataVo.setAttackState(String.valueOf(state & 0b10000000));
            // 电池电压
            String voltageHex = ByteBufUtils.readBytesToHex(buf, 1);
            double voltage = BigDecimal
                .valueOf(HexUtils.hexToInt(voltageHex))
                .setScale(2, RoundingMode.HALF_UP)
                .add(BigDecimal.valueOf(200))
                .movePointLeft(2)
                .doubleValue();
            cctMeterDataVo.setVoltage(voltage);
            cctMeterDataVo.setCurrentTime(LocalDateTime.now());

            meterDataVos.add(cctMeterDataVo);
        }
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctRtMeterDataBo rtNbDataBo = BeanUtil.toBean(properties, CctRtMeterDataBo.class);

        // 设置计量表地址(组)和计量表类型
        writeMeterOrGroup(buf, rtNbDataBo.getMeterType(), rtNbDataBo.getMeterAddr());
        // 数据项
        buf.writeByte(rtNbDataBo.getDataItem());
    }


    /**
     * 设置计量表地址和计量表类型
     *
     * @param buf              写出缓冲区
     * @param meterType        计量表类型
     * @param meterAddrOrGroup 计量表地址(组)
     */
    public static void writeMeterOrGroup(ByteBuf buf, Integer meterType, String meterAddrOrGroup) {
        // 计量表类型
        buf.writeByte(meterType);
        // 计量表通信号(组号)
        byte[] bytes = BCD.strToBcd(meterAddrOrGroup);
        Assert.isTrue(ArrayUtil.isNotEmpty(bytes), "计量表地址错误");
        if (Long.parseLong(meterAddrOrGroup) < 10000) {
            // 组抄
            buf.writeBytes(new byte[]{(byte) 0xff, (byte) 0xff});
            if (bytes.length == 1) {
                buf.writeByte(bytes[0]);
                buf.writeByte(0x00);
            } else {
                BytesUtils.bytesReverse(bytes);
                buf.writeBytes(bytes);
            }
            buf.writeBytes(new byte[]{(byte) 0xff, (byte) 0xff});
        } else {
            // 单抄
            Assert.isTrue(bytes.length == 6, "计量表地址错误");
            BytesUtils.bytesReverse(bytes);
            buf.writeBytes(bytes);
        }
    }
}
