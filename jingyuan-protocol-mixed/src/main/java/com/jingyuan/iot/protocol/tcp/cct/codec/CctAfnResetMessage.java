package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 复位
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnResetMessage implements CctAfnMessage {

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        CctAfnMessage.super.write(buf, encodeHolder, properties);
    }
}
