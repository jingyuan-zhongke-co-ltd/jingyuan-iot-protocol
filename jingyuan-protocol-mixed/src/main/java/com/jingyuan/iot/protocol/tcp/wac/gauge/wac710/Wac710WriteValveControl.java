package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteValveControlBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写阀门控制
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteValveControl implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 读取 ST 状态字
        Wac710ReadValveData.readSt(buf, properties);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteValveControlBo valveControlBo = BeanUtil.toBean(properties, Wac710WriteValveControlBo.class);
        buf.writeBytes(BytesUtils.hexToBytes(valveControlBo.getValveOpcode()));
    }
}
