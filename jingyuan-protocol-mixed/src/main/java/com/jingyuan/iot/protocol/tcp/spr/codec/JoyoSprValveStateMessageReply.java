package com.jingyuan.iot.protocol.tcp.spr.codec;

import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控阀应答
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprValveStateMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 阀门状态: 0x55开阀门,0xAA关阀门,0xXX关阀并进入待开阀状态
        IotValveStateEnums valveState = IotValveStateEnums.getByByte(buf.readByte());
        properties.put("valveState", valveState.getCode());
    }
}