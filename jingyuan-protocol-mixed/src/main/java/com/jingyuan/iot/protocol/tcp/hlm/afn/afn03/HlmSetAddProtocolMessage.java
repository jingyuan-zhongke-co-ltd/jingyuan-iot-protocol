package com.jingyuan.iot.protocol.tcp.hlm.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.hlm.afn03.HlmSetAddProtocolBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置服务器1信息
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmSetAddProtocolMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        HlmSetAddProtocolBo bean = BeanUtil.toBean(properties, HlmSetAddProtocolBo.class);
//        01 			// 数据包标识。
        buf.writeByte(1);
//        协议ID	BIN	0x4A	1	0B表示抄京源大口径热表(NB)，
        byte[] bytes1 = BytesUtils.hexToBytes(bean.getProtocolId());
        buf.writeBytes(bytes1);
//        设备地址	BCD		4	如，设备地址为12345678。则此处为16进制数据12 34 56 78
        buf.writeBytes(BytesUtils.hexToBytes(bean.getDeviceAdd()));
    }

}
