package com.jingyuan.iot.protocol.http.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.constant.redis.PrefixIotConstants;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.constant.IotNorthConstants;
import com.jingyuan.common.flowable.model.ctwing.CtwingNorthParamDto;
import com.jingyuan.common.flowable.model.north.NorthConfigBo;
import com.jingyuan.common.flowable.model.onenet.OnenetNorthParamDto;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.http.HttpEncodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.message.adapter.HttpMessageAdapter;
import com.jingyuan.common.protocol.model.FirmwareFrameBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.http.codec.ctwing.CtwingOnlineOfflineMessage;
import com.jingyuan.iot.protocol.http.codec.ctwing.CtwingReportPropertyMessage;
import com.jingyuan.iot.protocol.http.codec.ctwing.NorthWritePropertyMessage;
import com.jingyuan.iot.protocol.http.codec.onenet.OnenetReportPropertyMessage;
import com.jingyuan.iot.protocol.http.holder.HttpDecodeRedisHolder;
import com.jingyuan.iot.protocol.http.topic.HttpTopicEnums;
import com.jingyuan.iot.protocol.tcp.spr.SprChildVendorEnums;
import com.jingyuan.iot.protocol.tcp.spr.SprDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.spr.codec.JoyoSprUpgradeIssuedMessage;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.event.EventBus;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.DeviceOfflineMessage;
import org.jetlinks.core.message.DeviceOnlineMessage;
import org.jetlinks.core.message.codec.MessageDecodeContext;
import org.jetlinks.core.message.event.EventMessage;
import org.jetlinks.core.utils.TopicUtils;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuples;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

/**
 * HTTP 消息编解码处理
 *
 * @author: cc
 * @date: 2024/9/9 12:23
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum HttpMessageCodecEnums {

    /**
     * CTWING 平台消息处理
     */
    ctwing_report_property(NorthChannelEnums.ctwing, HttpTopicEnums.reportProperty, new CtwingReportPropertyMessage()),
    ctwing_online_offline(NorthChannelEnums.ctwing, HttpTopicEnums.online_offline, new CtwingOnlineOfflineMessage()),
    /**
     * ONENET 平台消息处理
     */
    onenet_report_property(NorthChannelEnums.onenet, HttpTopicEnums.reportProperty, new OnenetReportPropertyMessage()),
    onenet_report_property_v2(NorthChannelEnums.onenet, HttpTopicEnums.reportPropertyV2, new OnenetReportPropertyMessage()),

    ctwing_onenet_write(NorthChannelEnums.ctwing, HttpTopicEnums.writeProperty, new NorthWritePropertyMessage()),
    ;

    private final NorthChannelEnums northChannel;
    private final HttpTopicEnums httpTopic;
    private final HttpMessageAdapter codec;

    /**
     * 设备消息解码
     *
     * @param context       解码上下文
     * @param redisTemplate redis 缓存
     * @param topics        请求路径
     * @param payload       请求负载
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    public static Flux<DeviceMessage> decode(MessageDecodeContext context, ReactiveStringRedisTemplate redisTemplate, EventBus eventBus, String[] topics, byte[] payload) {
        // 根据请求路径获取发送过来的北向渠道
        NorthChannelEnums northChannel = NorthChannelEnums.getByPathVariable(topics[0]);
        return Mono
            .justOrEmpty(lookupByTopic(topics, northChannel))
            .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("未知的请求地址: " + Arrays.toString(topics)))))
            .flatMapMany(codec -> {
                HttpDecodeRedisHolder holder = HttpDecodeRedisHolder
                    .of(payload, codec.getHttpTopic().getMessageType(), northChannel, context);
                holder.setRedisTemplate(redisTemplate);
                holder.setEventBus(eventBus);
                return codec
                    .getCodec()
                    .read(holder)
                    .flatMap(message -> {
                                 JoyoIotVendor vendor = JoyoIotVendor.getByCodeNullable(MessageHelper.getHeadVendor(message));
                                 return context
                                     .getDevice(message.getDeviceId())
                                     .switchIfEmpty(Mono.error(new IllegalArgumentException("设备：【" + message.getDeviceId() + "】不存在或未启用")))
                                     // 设置厂商信息等
                                     .flatMap(deviceOperator -> setConfigs(message, deviceOperator, northChannel, vendor)
                                         .thenReturn(deviceOperator)
                                     )
                                     .flatMapMany(deviceOperator -> message
                                         .getHeader(IotSprConstants.UPGRADE_N)
                                         .map(Convert::toShort)
                                         // 升级指令下发
                                         .map(aShort -> upgradeIssued(deviceOperator, message, vendor, aShort, redisTemplate, northChannel))
                                         // 缓存指令下发
                                         .orElseGet(() -> Flux.just(message)));
                             }
                    );
            });
    }


    /**
     * 升级指令应答
     *
     * @param deviceOperator 设备操作对象
     * @param message        解密消息
     * @param iotVendor      厂商类型
     * @param upgradeN       第N帧升级指令
     * @param redisTemplate  redis 缓存
     * @param northChannel   北向渠道
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    public static Flux<DeviceMessage> upgradeIssued(DeviceOperator deviceOperator, DeviceMessage message,
                                                    JoyoIotVendor iotVendor, Short upgradeN,
                                                    ReactiveStringRedisTemplate redisTemplate, NorthChannelEnums northChannel) {
        // 由于目前接入固件升级的厂商只有分体阀控协议, 所以此处暂时只处理分体阀控协议
        SprChildVendorEnums childVendor = JoyoIotVendor.JOYO_SPR.isSame(iotVendor)
            ? SprChildVendorEnums.JOYO_SPR : SprChildVendorEnums.JOYO_SPR_THIRD;
        return deviceOperator
            .getConfigs(IotConstants.FIRMWARE_ID, IotConstants.PRODUCT_ID, IotNorthConstants.NSE_ID)
            .map(values -> {
                String firmwareId = MessageHelper.getValueStr(values, IotConstants.FIRMWARE_ID, "设备升级固件信息待完善");
                String productId = MessageHelper.getValueStr(values, IotConstants.PRODUCT_ID, "产品信息获取失败");
                String nseId = MessageHelper.getValueStr(values, IotNorthConstants.NSE_ID, "设备未上过线，设备信息不完善");
                return Tuples.of(firmwareId, productId, nseId);
            })
            .flatMapMany(tuple3 -> redisTemplate
                .opsForValue()
                // 查询固件信息
                .get(PrefixIotConstants.FIRMWARE_FRAME + tuple3.getT1())
                .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("设备升级固件信息不存在"))))
                .map(firmware -> JsonUtils.parseObject(firmware, FirmwareFrameBo.class))
                .flatMapMany(frameBo -> {
                    ByteBuf buf = Unpooled.buffer();
                    // 组装固件升级下发指令
                    return JoyoSprUpgradeIssuedMessage
                        .write(buf, childVendor, upgradeN, frameBo)
                        .flatMapMany(upgradeProperties -> redisTemplate
                            .opsForValue()
                            // 查询北向配置信息
                            .get(PrefixIotConstants.IOT_NORTH_PRODUCT_CONFIG + tuple3.getT2())
                            .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("北向通信配置信息待完善"))))
                            .map(val -> JsonUtils.parseObject(val, NorthConfigBo.class))
                            .flatMapMany(bo -> {
                                String imei = message.getHeader(IotConstants.IMEI_LOWER)
                                                     .map(Convert::toStr)
                                                     .orElseThrow(() -> new IllegalArgumentException("设备未上过线，设备信息不完善"));
                                if (northChannel == NorthChannelEnums.ctwing) {
                                    // AEP 平台需要的参数组装并下发
                                    CtwingNorthParamDto paramDto = CtwingNorthParamDto.of();
                                    String ndeId = message.getHeader(IotNorthConstants.NDE_ID)
                                                          .map(Convert::toStr)
                                                          .orElseThrow(() -> new IllegalArgumentException("设备未上过线，设备信息不完善"));

                                    paramDto.setImei(imei);
                                    paramDto.setNdeId(ndeId);
                                    paramDto.setNprId(bo.getNprId());
                                    paramDto.setMasterKey(bo.getMasterKey());
                                    paramDto.setAppKey(bo.getAppKey());
                                    paramDto.setAppSecret(bo.getAppSecret());
                                    paramDto.setServiceId(tuple3.getT3());

                                    String encodeHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)).toUpperCase();
                                    HttpCommandHelper.createCommand2Ctwing(encodeHex, paramDto);
                                } else if (northChannel == NorthChannelEnums.onenet) {
                                    // ONENET 平台需要的参数组装并下发
                                    OnenetNorthParamDto paramDto = OnenetNorthParamDto.of();
                                    paramDto.setImei(imei);
                                    paramDto.setApiKey(bo.getApiKey());
                                    paramDto.setNorthUrl(bo.getInterfaceUrl());

                                    String encodeHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf));
                                    HttpCommandHelper.createCommand2Onenet(encodeHex, paramDto);
                                } else {
                                    return Mono.error(new IllegalArgumentException("不支持的北向渠道【" + northChannel + "】"));
                                }
                                return SprDeviceMessageCodec.mergeUpgradeProperties(message, upgradeProperties);
                            }));
                })
            );
    }

    private static Mono<DeviceMessage> setConfigs(DeviceMessage message, DeviceOperator deviceOperator, NorthChannelEnums northChannel, JoyoIotVendor vendor) {
        if (message instanceof DeviceOfflineMessage || message instanceof DeviceOnlineMessage || message instanceof EventMessage) {
            return Mono.just(message);
        }
        // 实际厂商根据解析的指令获取
        Map<String, Object> map = new HashMap<>();
        map.put(IotConstants.VENDOR_CODE, vendor.getCode());
        map.put(IotConstants.NORTH_CHANNEL, northChannel.getCode());

        if (NorthChannelEnums.ctwing.equals(northChannel) || NorthChannelEnums.onenet.equals(northChannel)) {
            message.getHeader(IotConstants.IMEI_LOWER)
                   .ifPresent(imei -> map.put(IotConstants.IMEI_LOWER, imei));
            message.getHeader(IotConstants.METER_ADDR)
                   .ifPresent(meterAddr -> map.put(IotConstants.METER_ADDR, meterAddr));

            message.getHeader(IotNorthConstants.NSE_ID)
                   .ifPresent(nseId -> map.put(IotNorthConstants.NSE_ID, nseId));
            message.getHeader(IotNorthConstants.NDE_ID)
                   .ifPresent(deviceId -> map.put(IotNorthConstants.NDE_ID, deviceId));
            message.getHeader(IotNorthConstants.NPR_ID)
                   .ifPresent(deviceId -> map.put(IotNorthConstants.NPR_ID, deviceId));
        }
        return deviceOperator.setConfigs(map)
                             .flatMap(r -> {
                                 if (!r) {
                                     return deviceOperator.setConfigs(map)
                                                          .thenReturn(message);
                                 }
                                 return Mono.just(message);
                             });
    }

    static Optional<HttpMessageCodecEnums> lookupByTopic(String[] topic, NorthChannelEnums northChannel) {
        for (HttpMessageCodecEnums value : values()) {
            if (value.northChannel.equals(northChannel) && TopicUtils.match(value.httpTopic.getPattern(), topic)) {
                return Optional.of(value);
            }
        }
        return Optional.empty();
    }

    public static void encode(DeviceMessage message) {
        HttpMessageCodecEnums.ctwing_onenet_write.getCodec().write(HttpEncodeHolder.of(message));
    }

    /**
     * 获取 topic 中的厂商编号
     *
     * @param path topic
     * @return 移除后的topic
     */
    public static String[] splitTopic(String path) {
        if (StrUtil.isBlank(path)) {
            return new String[]{};
        }
        if (path.contains("?")) {
            path = StrUtil.sub(path, 0, path.indexOf("?"));
        }
        if (path.startsWith("/")) {
            path = StrUtil.sub(path, 1, path.length());
        }
        return path.split("/");
    }
}
