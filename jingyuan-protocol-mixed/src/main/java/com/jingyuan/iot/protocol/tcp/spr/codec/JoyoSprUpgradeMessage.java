package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.spr.SprUpgradeBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * NB升级程序帧（适用于TCP直连模式）
 *
 * @author: cc
 * @date: 2024/4/15 15:57
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprUpgradeMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(JoyoSprFeatureEnums.UPGRADE.getFeatCode()));
        SprUpgradeBo sprUpgradeBo = BeanUtil.toBean(properties, SprUpgradeBo.class);

        buf.writeBytes(BytesUtils.hexToBytes(sprUpgradeBo.getPassword()));
        buf.writeBytes(BytesUtils.shortToBytes(sprUpgradeBo.getFrameNum()));
        buf.writeBytes(BytesUtils.shortToBytes(sprUpgradeBo.getFrameLen()));
    }
}