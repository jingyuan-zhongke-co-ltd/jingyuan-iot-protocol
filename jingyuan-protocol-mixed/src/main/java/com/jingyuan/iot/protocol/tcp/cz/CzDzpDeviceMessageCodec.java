package com.jingyuan.iot.protocol.tcp.cz;

import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.cz.wrx.CzDzpMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

/**
 * 沧州定制协议
 *
 * @author: 李立强
 * @date: 2024/10/08 15:27
 * @Version: V1.0
 */
public class CzDzpDeviceMessageCodec implements DeviceMessageCodec {
    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return doDecode(context);
    }

    /**
     * 执行解码操作
     *
     * @param context 待解码消息上下文
     * @return org.jetlinks.core.message.DeviceMessage
     */
    private Publisher<? extends Message> doDecode(MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        TcpDecodeHolder decodeHolder = CzDzpMessageTypeEnums.read(payload);
        return Flux.just(decodeHolder.getDeviceMessage());
    }

    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @Nonnull
    @Override
    public Publisher<? extends EncodedMessage> encode(@Nonnull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        TcpEncodeHolder encodeHolder = TcpEncodeHolder.of();
        encodeHolder.setDeviceMessage(deviceMessage);
        return doEncode(encodeHolder);
    }

    /**
     * 执行编码操作
     *
     * @param encodeHolder 消息编码包装类
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(TcpEncodeHolder encodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        CzDzpMessageTypeEnums.write(encodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
