package com.jingyuan.iot.protocol.tcp.cct.holder;

import com.jingyuan.common.enums.JingyuanBaseEnum;
import com.jingyuan.common.protocol.enums.DataDirectionEnums;
import com.jingyuan.common.protocol.enums.cct.CctAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.cct.CctPrmEnums;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.cct.CctSeqFfEnums;
import com.jingyuan.common.protocol.enums.cct.fn.IotCctFunc;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息解码包装类
 *
 * @author: cc
 * @date: 2024/4/22 11:28
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class CctTcpDecodeHolder extends TcpDecodeHolder {
    /**
     * 传输方向位
     */
    private DataDirectionEnums dir;
    /**
     * 启动表示位
     */
    private CctPrmEnums prm;
    private JingyuanBaseEnum<Integer> prmFeature;

    /**
     * 行政区划码
     */
    private String adcode;
    /**
     * 主站地址和组地址标志
     */
    private int msa;
    /**
     * 置“0”表示终端地址A2为单地址；置“1”表示终端地址A2为组地址
     */
    private int broadcast;

    /**
     * 应用层功能码（AFN）
     */
    private CctAfnFeatureEnums afn;
    /**
     * 帧序列域 FIR、FIN
     */
    private CctSeqFfEnums seqFf;
    /**
     * 帧序列域 CON
     */
    private CctSeqConEnums seqCon;

    /**
     * 应用层数据单元功能
     */
    private IotCctFunc feature;
}
