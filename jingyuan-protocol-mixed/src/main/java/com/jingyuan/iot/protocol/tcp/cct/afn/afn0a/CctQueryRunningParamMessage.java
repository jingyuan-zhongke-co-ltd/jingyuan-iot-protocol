package com.jingyuan.iot.protocol.tcp.cct.afn.afn0a;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0a.CctQueryRunningParamBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.tcp.cct.afn.afn0b.CctRtMeterDataMessage;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 设置集采器IP和端口
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctQueryRunningParamMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int meterCount = CctRtMeterDataMessage.readGroupAndMeterCount(buf, properties);
        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        readMeterData(buf, meterDataVos, count);
        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));
    }

    /**
     * 读取表数据
     *
     * @param buf          写入缓冲区
     * @param meterDataVos 计量表数据
     * @param count        组内表数
     */
    private void readMeterData(ByteBuf buf, List<CctMeterDataVo> meterDataVos, int count) {
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();

            // 计量表序号
            byte[] meterSerialNum = ByteBufUtils.readBytes(buf, 2);
            cctMeterDataVo.setMeterSerialNum((int) BytesUtils.bytesToShort(meterSerialNum, ByteOrder.LITTLE_ENDIAN));
            // 计量表通信号
            byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
            BytesUtils.bytesReverse(nbAddr);
            cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
            // 采集器号
            byte[] coolAddr = ByteBufUtils.readBytes(buf, 6);
            cctMeterDataVo.setCoolAddr(Integer.valueOf(BytesUtils.binary(10, coolAddr[5])));
            // 运行标志
            int runFlag = buf.readByte();
            cctMeterDataVo.setRunFlag(runFlag);
            // 数据项
            int dataItem = buf.readByte();
            cctMeterDataVo.setDataItem(dataItem);

            meterDataVos.add(cctMeterDataVo);
        }
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctQueryRunningParamBo queryRunningParamBo = BeanUtil.toBean(properties, CctQueryRunningParamBo.class);

        // 设置计量表地址(组)和计量表类型
        CctRtMeterDataMessage.writeMeterOrGroup(buf, queryRunningParamBo.getMeterType(), queryRunningParamBo.getMeterAddr());
    }
}
