package com.jingyuan.iot.protocol.tcp.cct.codec;

import lombok.extern.slf4j.Slf4j;

/**
 * 数据转发（如通过管理中心对表进行直接通讯）
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnDataForwardMessage implements CctAfnMessage {

}
