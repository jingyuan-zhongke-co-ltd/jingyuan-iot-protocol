package com.jingyuan.iot.protocol.tcp.nom.wrx;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.nom.JzNomFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;

import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 京兆无磁消息类型枚举
 *
 * @author: cc
 * @date: 2024/9/20 14:34
 * @Version: V1.0
 */
@Slf4j
@AllArgsConstructor
public enum JzNomMessageTypeEnums {

    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), JzNomReportPropertyMessage::new),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), JzNomWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), JzNomWritePropertyMessageReply::new),
    ;

    private final String messageType;
    private final Supplier<TcpMessageAdapter<? extends DeviceMessage>> forTcp;

    private static final Map<String, JzNomMessageTypeEnums> MAPS = new HashMap<>(16);

    static {
        Arrays.stream(values()).forEach(item -> MAPS.put(item.messageType, item));
    }

    /**
     * 数据写出
     *
     * @param encodeHolder 待写出消息
     * @param buf          写出数据
     */
    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf) {
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static TcpDecodeHolder read(ByteBuf buf) {
        TcpDecodeHolder decodeHolder = TcpDecodeHolder.of();
        // 原始数据帧
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        // 设备ID
        decodeHolder.setDeviceId(BytesUtils.hexToAscStr(ByteBufUtils.getBytesToHex(buf, 9, 15)));
        // 功能码
        JzNomFeatEnums feature = JzNomFeatEnums.getByFeatCode(ByteBufUtils.getBytesToHex(buf, 30, 1));
        decodeHolder.setFeature(feature);
        // 指令校验
        feature.getCheckResult(decodeHolder.getRawDataHex());

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);
        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }


    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf     写入数据
     * @param handler 处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf,
                             TcpDecodeHolder decodeHolder,
                             BiFunction<DeviceMessage, Integer, T> handler) {
        IotFeat feature = decodeHolder.getFeature();
        IotMessageType decodeType = feature.getDecodeType();
        Assert.notNull(decodeType, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        JzNomMessageTypeEnums type = MAPS.get(decodeType.getCode());
        Assert.isFalse(type == null || type.forTcp == null, "暂不支持的功能类型：" + feature + ", 原始数据帧：【" + decodeHolder.getRawDataHex() + "】");

        // a568
        buf.readBytes(2);
        // 帧长度
        short length = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2), ByteOrder.LITTLE_ENDIAN);
        Assert.isTrue(length * 2 == decodeHolder.getRawDataHex().length(), "数据帧长度校验失败，原始数据帧：【" + decodeHolder.getRawDataHex() + "】");
        // msgId
        short msgId = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2), ByteOrder.LITTLE_ENDIAN);

        // 创建消息对象
        TcpMessageAdapter<? extends DeviceMessage> nbMessage = type.forTcp.get();
        // 从ByteBuf读取
        nbMessage.read(buf, decodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        return handler.apply(message, (int) msgId);
    }
}
