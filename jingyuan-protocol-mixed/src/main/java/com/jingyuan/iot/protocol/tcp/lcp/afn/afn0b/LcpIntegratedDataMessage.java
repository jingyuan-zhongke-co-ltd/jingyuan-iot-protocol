package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.joda.time.LocalDateTime;

import java.math.BigDecimal;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.lcp.codec.LcpAfnDataUploadsMessage.addShortAndLong;

/**
 * 一体数据
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpIntegratedDataMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {
//        表地址	BCD	6个字节
        String BCD = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 6));
//        时间（从1970-01-01开始的秒数)	td2类型的 umix 时间	4个字节
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            map.put("freezeTime", new LocalDateTime(BytesUtils.bytesToUnLong(bytes) * 1000));
            // 瞬时流量 Float 4个字节
            byte[] bytes2 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes2);
            float instantaneousFlow = BytesUtils.bytesToShort(bytes2);
            map.put("instantFlowUnit", instantaneousFlow);

            // 正向累计流量整数部分 Long 4个字节
            byte[] bytes1 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes1);
            long forwardAccumulatedFlowInt = BytesUtils.bytesToUnLong(bytes1);
            // 正向累计流量小数部分 Float 4个字节
            byte[] bytes3 = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes3);
            float forwardAccumulatedFlowDec = BytesUtils.bytesToFloat(bytes3);

            BigDecimal add = new BigDecimal(forwardAccumulatedFlowInt).add(new BigDecimal(Convert.toStr(forwardAccumulatedFlowDec)));
            map.put("forwardFlow", add);

            // 反向累计流量整数部分 Long 4个字节
            long reverseAccumulatedFlowInt = BytesUtils.bytesToUnLong(ByteBufUtils.readBytes(buf, 4));
            // 反向累计流量小数部分 Float 4个字节
            short reverseAccumulatedFlowDec = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
            BigDecimal bigDecimal = addShortAndLong(reverseAccumulatedFlowInt, reverseAccumulatedFlowDec);
            map.put("reverseFlow", bigDecimal);

            // 第1路模拟量通道采集数据 Int 2个字节
            int analogChannel1 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel1", analogChannel1);

            // 第2路模拟量通道采集数据 Int 2个字节
            int analogChannel2 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel2", analogChannel2);

            // 告警状态 Byte 1个字节
            byte alarmStatus = buf.readByte();
            map.put("alarmStatus", alarmStatus);

            // 电池电压 Int 2个字节
            byte[] bytes4 = ByteBufUtils.readBytes(buf, 2);
            BytesUtils.bytesReverse(bytes4);
            int batteryVoltage = BytesUtils.bytesToShort(bytes4);
            map.put("voltage", String.format("%.2f", batteryVoltage * 0.001));

            // 网络信号 Byte 1个字节
            byte networkSignal = buf.readByte();
            map.put("signal", networkSignal);
    }


}
