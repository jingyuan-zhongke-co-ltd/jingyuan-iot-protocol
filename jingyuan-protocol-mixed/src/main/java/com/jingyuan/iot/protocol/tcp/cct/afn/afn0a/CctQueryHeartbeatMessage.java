package com.jingyuan.iot.protocol.tcp.cct.afn.afn0a;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置集采器心跳周期
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctQueryHeartbeatMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 心跳周期
        int heartbeat = buf.readByte();
        properties.put("heartbeat", heartbeat);
    }
}
