package com.jingyuan.iot.protocol.cached;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.ObjectUtil;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.message.DeviceMessage;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static com.jingyuan.common.protocol.constant.IotConstants.CACHED_COMMANDS;

/**
 * 缓存消息处理辅助类
 *
 * @author: cc
 * @date: 2024/9/14 16:49
 * @Version: V1.0
 */
@Slf4j
public class CachedMessageHelper {

    /**
     * 获取一条缓存消息；TCP 直连的水表一次请求后只能发送一条，响应后可以在发送下一条
     *
     * @param deviceOperator 设备操作对象
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    public static Mono<List<DeviceMessage>> getMessage(DeviceOperator deviceOperator) {
        return getMessages(deviceOperator, false)
            .collectList()
            .switchIfEmpty(Mono.defer(() -> Mono.just(new ArrayList<>())));
    }

    /**
     * 获取设备下缓存的全部消息；AEP、Onenet 平台
     *
     * @param deviceOperator 设备操作对象
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    public static Mono<List<DeviceMessage>> getMessages(DeviceOperator deviceOperator) {
        return getMessages(deviceOperator, true)
            .collectList()
            .switchIfEmpty(Mono.defer(() -> Mono.just(new ArrayList<>())));
    }

    @SuppressWarnings("unchecked")
    private static Flux<DeviceMessage> getMessages(DeviceOperator deviceOperator, Boolean batch) {
        String deviceId = deviceOperator.getDeviceId();
        return deviceOperator
            .getConfig(CACHED_COMMANDS)
            .switchIfEmpty(Mono.defer(Mono::empty))
            .map(val -> val.as(Map.class))
            .flatMap(map -> {
                if (CollUtil.isEmpty(map)) {
                    return Mono.empty();
                }
                Object cachedMap = map.get(deviceId);
                if (ObjectUtil.isNull(cachedMap)) {
                    return Mono.empty();
                }
                return Mono.just((Map<String, DeviceMessage>) cachedMap);
            })
            .flatMapMany(cachedMap -> {
                if (CollUtil.isEmpty(cachedMap)) {
                    return Flux.empty();
                }
                if (batch) {
                    Collection<DeviceMessage> values = cachedMap.values();
                    return deviceOperator
                        .setConfig(CACHED_COMMANDS, MapUtil.of(deviceId, MapUtil.empty()))
                        .flatMapMany(aBoolean -> {
                            if (!aBoolean) {
                                // 缓存失败时，重新放入缓存
                                return deviceOperator
                                    .setConfig(CACHED_COMMANDS, MapUtil.of(deviceId, cachedMap))
                                    .thenMany(Flux.empty());
                            } else {
                                return Flux.fromIterable(values);
                            }
                        });
                } else {
                    String key = Convert.toStr(cachedMap.keySet().iterator().next());
                    DeviceMessage toSendMessage = cachedMap.get(key);
                    cachedMap.remove(key);
                    return deviceOperator
                        .setConfig(CACHED_COMMANDS, MapUtil.of(deviceId, cachedMap))
                        .flatMapMany(aBoolean -> {
                            if (!aBoolean) {
                                // 缓存失败时，重新放入缓存
                                cachedMap.put(key, toSendMessage);
                                return deviceOperator
                                    .setConfig(CACHED_COMMANDS, MapUtil.of(deviceId, cachedMap))
                                    .thenMany(Flux.empty());
                            } else {
                                return Mono.just(toSendMessage);
                            }
                        });
                }
            });
    }
}
