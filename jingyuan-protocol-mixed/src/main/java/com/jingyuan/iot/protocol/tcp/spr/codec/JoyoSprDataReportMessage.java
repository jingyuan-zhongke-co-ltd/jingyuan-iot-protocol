package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.codec.BCD;
import cn.hutool.core.date.DatePattern;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.enums.spr.JoyoSprUnitEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprDataReportMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 当前时间, 6字节; 当前表数据时间，格式为年月日时分秒，BCD码格式。如当前时间为2020年6月22日10点48分02秒，则Z2为0x20,0x06,0x22,0x10,0x48,0x02
        String currentTime = "20" + ByteBufUtils.readBytesToHex(buf, 6);
        properties.put("currentTime", DateUtil.parse(currentTime, DatePattern.PURE_DATETIME_PATTERN));
        // 当前表数据
        String reading = ByteBufUtils.readBytesToHex(buf, 4);
        // 计量单位
        JoyoSprUnitEnums unitEnum = JoyoSprUnitEnums.getByCode(buf.readByte());
        properties.put("reading", unitEnum.calculateToTon(reading));

        // 冻结数据读取
        nbFreezeDataRead(buf, properties, unitEnum);
        // IMEI/CIMI 等数据读取
        reportExtendRead(buf, properties);

        // 20字节ICCID,ASCII码格式
        String iccid = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 20));
        properties.put("iccid", iccid);
        // 16进制CSQ值，1字节
        String csq = BytesUtils.binary(10, buf.readByte());
        properties.put("csq", csq);
        // RSRP，2字节，int16
        properties.put("rsrp", buf.readShort());
        // SNR，2字节，int16
        properties.put("snr", buf.readShort());
    }


    /**
     * 冻结数据读取
     *
     * @param buf 数据
     * @param map 结果Map
     */
    public static void nbFreezeDataRead(ByteBuf buf, Map<String, Object> map, JoyoSprUnitEnums unitEnum) {
        // 冻结数据起始时间
        String freezeStartTime = "20" + ByteBufUtils.readBytesToHex(buf, 6);
        map.put("freezeStartTime", DateUtil.parse(freezeStartTime, DatePattern.PURE_DATETIME_PATTERN));
        // 冻结数据采集间隔
        map.put("freezeInterval", (int) buf.readByte());
        // 冻结数据条数
        int freezeNum = buf.readByte();
        map.put("freezeNum", freezeNum);
        // 冻结点的冻结数据
        byte[] freezeDataBytes = ByteBufUtils.readBytes(buf, 4 * freezeNum);
        List<Double> freezeDataList = new ArrayList<>();
        for (byte[] bytes : ArrayUtil.split(freezeDataBytes, 4)) {
            freezeDataList.add(unitEnum.calculateToTon(StringUtil.toHexString(bytes)));
        }
        map.put("freezeData", freezeDataList);
    }


    public static void reportExtendRead(ByteBuf buf, Map<String, Object> properties) {
        // 9字节模组版本号
        String modVersion = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 9));
        properties.put("modVersion", modVersion);
        // 9字节程序版本号
        String programVersion = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 9));
        properties.put("programVersion", programVersion);

        // 温度，2字节,BCD码，例如：温度为12.58℃为0x12 0x58;第一个字节的最高位为符号位，“0”代表正，“1”代表负，如0x82 0x24代表-2.24℃
        if (buf.capacity() == 208) {
            // 温度
            byte firstByte = buf.readByte();
            BigDecimal bigDecimal = new BigDecimal(BCD.bcdToStr(new byte[]{(byte) (firstByte & 0b01111111), buf.readByte()})).movePointLeft(2);
            properties.put("temperature", (firstByte & 0b10000000) == 128 ?
                bigDecimal.negate().doubleValue() : bigDecimal.doubleValue());
        }

        // 模块状态，2字节
        byte highByte = buf.readByte();
        // 阀门状态
        properties.put("valveState", highByte & 0b00000001);
        // 阀门故障
        properties.put("valveFailure", highByte & 0b00000100);
        // 霍尔故障
        properties.put("hallFailure", highByte & 0b00100000);
        // 电池状态
        properties.put("voltageState", highByte & 0b01000000);
        // 攻击状态
        properties.put("attackState", highByte & 0b10000000);
        // 上传平台
        properties.put("uploadPlatform", buf.readByte() & 0b00000010);

        // 2字节电池电压
        double voltage = BigDecimal
            .valueOf((float) buf.readShort() / 100)
            .setScale(2, RoundingMode.HALF_UP)
            .doubleValue();
        properties.put("voltage", voltage);

        // 15字节的CIMI值（ASIC码格式）
        String cimi = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 15));
        properties.put("cimi", cimi);
        // 15字节的IMEI值（ASIC码格式）
        String imei = BytesUtils.bytesToAscStr(ByteBufUtils.readBytes(buf, 15));
        properties.put("imei", imei);
    }
}
