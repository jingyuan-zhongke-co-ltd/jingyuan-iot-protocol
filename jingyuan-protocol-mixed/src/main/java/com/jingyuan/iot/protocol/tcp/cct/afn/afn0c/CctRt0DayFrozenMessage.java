package com.jingyuan.iot.protocol.tcp.cct.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0c.CctRt0DayFrozenMessageBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.bytes.HexUtils;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.*;

/**
 * F1日零点冻结数据
 *
 * @author: dr
 * @date: 2024-12-24
 * @Version: V1.0
 */
@Slf4j
public class CctRt0DayFrozenMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int meterCount = readGroupAndMeterCount(buf, properties);

        // 计量表数据
        int count = (meterCount == 0 ? 1 : meterCount);
        List<CctMeterDataVo> meterDataVos = new ArrayList<>(count);
        readMeterData(buf, meterDataVos, count);
        properties.put("meterData", JsonUtils.toJsonString(meterDataVos));
    }

    /**
     * 读取表数据
     *
     * @param buf          写入缓冲区
     * @param meterDataVos 计量表数据
     * @param count        组内表数
     */
    public void readMeterData(ByteBuf buf, List<CctMeterDataVo> meterDataVos, int count) {
        for (int i = 0; i < count; i++) {
            CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
            // 计量表通信号
            byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
            BytesUtils.bytesReverse(nbAddr);
            cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
            // 数据项
            int dataItem = buf.readByte();
            cctMeterDataVo.setDataItem(dataItem);

            // 计量表数据体
            byte[] bytes = ByteBufUtils.readBytes(buf, 4);
            BytesUtils.bytesReverse(bytes);
            String res = BCD.bcdToStr(bytes);
            res = StrUtil.subPre(res, res.length() - 2) + CharPool.DOT + StrUtil.subSuf(res, res.length() - 2);
            cctMeterDataVo.setReading(Double.parseDouble(res));

            // 状态位
            byte state = buf.readByte();
            // 阀门状态
            int valveStatus = state & 0b00000011;
            if (valveStatus == 0) {
                cctMeterDataVo.setValveState(IotValveStateEnums.OPEN.getCode());
            } else if (valveStatus == 3) {
                cctMeterDataVo.setValveState(IotValveStateEnums.CLOSE.getCode());
            } else {
                cctMeterDataVo.setValveState(IotValveStateEnums.ERROR.getCode());
            }
            // 通讯状态
            cctMeterDataVo.setCommState(String.valueOf(state & 0b00001000));
            // 剩余量
            cctMeterDataVo.setRemainingState(String.valueOf(state & 0b00010000));
            // 攻击状态
            cctMeterDataVo.setAttackState(String.valueOf(state & 0b10000000));
            // 电池电压
            String voltageHex = ByteBufUtils.readBytesToHex(buf, 1);
            double voltage = BigDecimal
                .valueOf(HexUtils.hexToInt(voltageHex))
                .setScale(2, RoundingMode.HALF_UP)
                .add(BigDecimal.valueOf(200))
                .movePointLeft(2)
                .doubleValue();
            cctMeterDataVo.setVoltage(voltage);
            cctMeterDataVo.setCurrentTime(LocalDateTime.now());

            meterDataVos.add(cctMeterDataVo);
        }
    }

    /**
     * 读取组信息
     *
     * @param buf        写入缓冲区
     * @param properties 读取结果
     * @return int
     */
    public static int readGroupAndMeterCount(ByteBuf buf, Map<String, Object> properties) {
        // 总组数
        byte[] groupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(groupBytes);
        properties.put("groupCount", Integer.parseInt(BCD.bcdToStr(groupBytes)));
        // 当前组
        byte[] curGroupBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(curGroupBytes);
        properties.put("curGroup", Integer.parseInt(BCD.bcdToStr(curGroupBytes)));
        // 组内表数
        int meterCount = buf.readByte();
        properties.put("meterCount", meterCount);
//
        dataFormat20(buf, properties);
        // 计量表类型
        int meterType = buf.readByte();
        properties.put("meterType", meterType);
        return meterCount;
    }


    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctRt0DayFrozenMessageBo bean = BeanUtil.toBean(properties, CctRt0DayFrozenMessageBo.class);
        //日月年
        Date dateParam = bean.getDateParam();
        dataParse20(buf, dateParam);

        // 计量表类型
        buf.writeByte(bean.getMeterType());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(bean.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
        // 数据项
        buf.writeByte(bean.getDataItem());
    }

    static void dataParse20(ByteBuf buf, Date dateParam) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateParam);

        int day = calendar.get(Calendar.DAY_OF_MONTH);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR) % 100;

        // 将日、月、年转换为BCD码
        byte dayBcd = (byte) ((day / 10 << 4) | (day % 10));
        byte monthBcd = (byte) ((month / 10 << 4) | (month % 10));
        byte yearBcd = (byte) ((year / 10 << 4) | (year % 10));

        // 写入年月日
        buf.writeByte(dayBcd);
        buf.writeByte(monthBcd);
        buf.writeByte(yearBcd);
    }

    /**
     * 日月年
     * @param buf
     * @param properties
     */
    static void dataFormat20(ByteBuf buf, Map<String, Object> properties) {
        // 从 ByteBuf 中读取 BCD 编码的日、月、年
        byte dayBcd = buf.readByte();
        byte monthBcd = buf.readByte();
        byte yearBcd = buf.readByte();

        // 将 BCD 编码转换为十进制整数
        int day = ((dayBcd & 0xF0) >> 4) * 10 + (dayBcd & 0x0F);
        int month = ((monthBcd & 0xF0) >> 4) * 10 + (monthBcd & 0x0F);
        int year = ((yearBcd & 0xF0) >> 4) * 10 + (yearBcd & 0x0F);

        // 创建 Calendar 对象
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2000 + year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);

        // 将 Calendar 对象转换为 Date 对象
        LocalDateTime localDateTime = DateUtils.toLocalDateTime(calendar.toInstant());
        properties.put("meterType", localDateTime);

    }
}
