package com.jingyuan.iot.protocol.http.codec.ctwing;

import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.holder.http.HttpEncodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.message.adapter.HttpMessageAdapter;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.mixed.MixedTransportEnums;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.DeviceMessage;

/**
 * AEP 平台写出消息
 *
 * @author: cc
 * @date: 2024/9/11 9:08
 * @Version: V1.0
 */
@Slf4j
public class NorthWritePropertyMessage implements HttpMessageAdapter {

    @Override
    public void write(HttpEncodeHolder encodeHolder) {
        DeviceMessage deviceMessage = encodeHolder.getDeviceMessage();
        encodeHolder.setFeature(MessageHelper.getHeadFeatThrow(deviceMessage));
        // 将参数组组装为二进制数据帧
        ByteBuf byteBuf = MixedTransportEnums.encodeByMessage(deviceMessage);
        String encodeHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(byteBuf));
        if (StrUtil.isNotBlank(encodeHex)) {
            encodeHex = encodeHex.toUpperCase();
        }
        MessageHelper.addHeadRawHex(deviceMessage, encodeHex);
    }
}
