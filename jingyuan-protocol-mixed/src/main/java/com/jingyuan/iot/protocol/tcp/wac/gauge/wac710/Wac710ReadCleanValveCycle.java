package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读清洗阀门周期
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadCleanValveCycle implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes);
        // 清洗阀门周期
        properties.put("cleanValveCycle", Convert.toInt(BytesUtils.bytesToHex(bytes)));
    }
}
