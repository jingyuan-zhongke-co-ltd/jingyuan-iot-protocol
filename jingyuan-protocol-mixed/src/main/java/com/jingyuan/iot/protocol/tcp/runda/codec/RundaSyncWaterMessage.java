package com.jingyuan.iot.protocol.tcp.runda.codec;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 用水同步：每次水龙头出水10秒后上传本次用水量，单位/10ml。
 * 电脑板主动上传无需应答
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaSyncWaterMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 本次用水量
        byte[] curUseAmount = ByteBufUtils.readBytes(buf, 2);
        properties.put("curUseAmount", BytesUtils.binary(10, curUseAmount));
    }
}
