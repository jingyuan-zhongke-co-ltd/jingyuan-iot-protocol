package com.jingyuan.iot.protocol.tcp.cts.wxr;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cts.codec.CtsFeatureCodecEnums;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.HashMap;
import java.util.Map;
/**
 * 电信NB协议 - 属性上报
 *
 * @author: dr
 * @date: 2024-09-02
 * @Version: V1.0
 */
public class CtsReportPropertyMessage implements TcpMessageAdapter<ReportPropertyMessage> {

    private ReportPropertyMessage message;

    /**
     * 设置消息
     *
     * @param message 消息
     */
    @Override
    public void setMessage(ReportPropertyMessage message) {
        this.message = message;
    }

    /**
     * 获取消息
     *
     * @return T 消息
     */
    @Override
    public ReportPropertyMessage getMessage() {
        return message;
    }

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new ReportPropertyMessage();
        message.setDeviceId(decodeHolder.getDeviceId());
        Map<String, Object> map = initReadMap(decodeHolder);
        CtsFeatureCodecEnums.read(decodeHolder, buf, map);
        map.put("meterAddr",decodeHolder.getDeviceId());
        message.setProperties(map);
    }
    public static Map<String, Object> initReadMap(TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JOYO_CTS.getCode());
        return map;
    }
}
