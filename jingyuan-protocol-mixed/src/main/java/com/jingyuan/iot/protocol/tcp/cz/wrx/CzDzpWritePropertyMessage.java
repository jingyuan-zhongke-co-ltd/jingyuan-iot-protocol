package com.jingyuan.iot.protocol.tcp.cz.wrx;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.WritePropertyMessage;

/**
 * 沧州定制协议 - 属性上报
 *
 * @author: 李立强
 * @date: 2024-10-14
 * @Version: V1.0
 */
public class CzDzpWritePropertyMessage implements TcpMessageAdapter<WritePropertyMessage> {


    private WritePropertyMessage message;

    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
        FeatCodecs.write(encodeHolder, buf, message.getProperties());
    }

    @Override
    public void setMessage(WritePropertyMessage message) {
        this.message = message;
    }

    @Override
    public WritePropertyMessage getMessage() {
        return message;
    }

}
