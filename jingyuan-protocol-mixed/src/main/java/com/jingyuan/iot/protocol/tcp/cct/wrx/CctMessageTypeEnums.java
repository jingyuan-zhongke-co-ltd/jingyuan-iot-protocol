package com.jingyuan.iot.protocol.tcp.cct.wrx;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.collection.ListUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotCctConstants;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.DataDirectionEnums;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.enums.cct.*;
import com.jingyuan.common.protocol.enums.cct.fn.CctAfnConfirmRwEnums;
import com.jingyuan.common.protocol.enums.cct.fn.IotCctFunc;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.ChildDeviceMessageReply;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * 润达消息类型枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
public enum CctMessageTypeEnums {

    online(IotMessageType.ONLINE.getCode(), null),
    ack(IotMessageType.ACKNOWLEDGE.getCode(), null),
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), null),
    readProperty(IotMessageType.READ_PROPERTY.getCode(), null),
    readPropertyReply(IotMessageType.READ_PROPERTY_REPLY.getCode(), null),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), CctWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), CctWritePropertyMessageReply::new),
    childDevice(IotMessageType.CHILD.getCode(), CctChildDeviceMessage::new),
    childDeviceReply(IotMessageType.CHILD_REPLY.getCode(), null),
    function(IotMessageType.INVOKE_FUNCTION.getCode(), null),
    functionReply(IotMessageType.INVOKE_FUNCTION_REPLY.getCode(), null),
    event(IotMessageType.EVENT.getCode(), null);

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    CctMessageTypeEnums(String messageType,
                        Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, CctMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (CctMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param messageEncodeHolder 待写出消息
     * @param buf                 待写入缓冲区
     */
    public static void write(CctTcpEncodeHolder messageEncodeHolder, ByteBuf buf) {
        DeviceMessage message = messageEncodeHolder.getDeviceMessage();
        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
            MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        CctMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);

        // 链路层
        buf.writeByte(0x68);
        byte[] lBytes = BytesUtils.shortToBytes(Convert.toShort((messageEncodeHolder.getL() << 2) ^ 0x03), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(lBytes);
        buf.writeBytes(lBytes);
        buf.writeByte(0x68);

        // 控制码
        buf.writeByte(messageEncodeHolder.getPrm().getPrmByte() ^ messageEncodeHolder.getPrmFeature().getCode());
        // 地址域
        writeAddress(buf, messageEncodeHolder, msgId);
        // 应用层
        tcp.write(buf, messageEncodeHolder);
    }

    /**
     * DeviceMessage 数据转换到 CctMessageEncodeHolder
     *
     * @param message             设备消息
     * @param messageEncodeHolder 消息编码包装类
     */
    @SuppressWarnings("all")
    public static void convertHeaderToHolder(DeviceMessage message, CctTcpEncodeHolder messageEncodeHolder) {
        // 控制域、地址域、链路用户数据（应用层）的字节总数
        int l = Convert.toInt(message.getHeader(IotCctConstants.L)
                                     .orElseThrow(() -> new IllegalArgumentException("指令长度不能为空")));
        messageEncodeHolder.setL(l);

        // 启动标识位
        CctPrmEnums prm = (CctPrmEnums) message
            .getHeader(IotCctConstants.PRM)
            .orElseThrow(() -> new IllegalArgumentException("启动标志位不能为空"));
        messageEncodeHolder.setPrm(prm);
        // 链路层功能码
        Object prmFeature = message
            .getHeader(IotCctConstants.PRM_FEATURE)
            .orElseThrow(() -> new IllegalArgumentException("链路层功能码不能为空"));
        if (prm.equals(CctPrmEnums.launch_station)) {
            Assert.isTrue(prmFeature instanceof CctLaunchFeatureEnums, "链路层功能码与启动标志位不匹配");
            messageEncodeHolder.setPrmFeature((CctLaunchFeatureEnums) prmFeature);
        } else {
            Assert.isTrue(prmFeature instanceof CctSlaveFeatureEnums, "链路层功能码与启动标志位不匹配");
            messageEncodeHolder.setPrmFeature((CctSlaveFeatureEnums) prmFeature);
        }

        // 行政区划码
        Object adcode = message
            .getHeader(IotCctConstants.ADCODE)
            .orElseThrow(() -> new IllegalArgumentException("启动标志位不能为空"));
        messageEncodeHolder.setAdcode(Convert.toStr(adcode));

        // 集采器ID
        String deviceId = message.getDeviceId();
        Assert.notBlank(deviceId, "集采器ID不能为空");
        messageEncodeHolder.setDeviceId(deviceId);

        // 应用层功能码
        CctAfnFeatureEnums afn = (CctAfnFeatureEnums) message
            .getHeader(IotCctConstants.AFN)
            .orElseThrow(() -> new IllegalArgumentException("应用层功能码不能为空"));
        messageEncodeHolder.setAfn(afn);
        // 实际下发指令
        IotCctFunc feature = (IotCctFunc) message
            .getHeader(IotConstants.FEATURE)
            .orElseThrow(() -> new IllegalArgumentException("链路层功能码不能为空"));
        afn.funcMatch(feature);
        messageEncodeHolder.setFeature(feature);
    }

    /**
     * 写入地址域信息
     *
     * @param buf                 待写入缓冲区
     * @param messageEncodeHolder 消息编码包装类
     * @param msa                 消息ID
     */
    private static void writeAddress(ByteBuf buf, CctTcpEncodeHolder messageEncodeHolder, Integer msa) {
        // 行政区划码A1
        byte[] a1 = BCD.strToBcd(messageEncodeHolder.getAdcode());
        buf.writeBytes(new byte[]{a1[1], a1[0]});

        // 终端地址A2
        int deviceId = Integer.parseInt(messageEncodeHolder.getDeviceId());
        Assert.isTrue(deviceId > 0, "deviceId must be greater than 0");

        byte d0 = 0x00;
        if (deviceId >= 65535) {
            // 终端地址A2=FFFFH且A3的D0位为“1”时表示系统广播地址。
            d0 = 0x01;
            buf.writeBytes(new byte[]{(byte) 0xff, (byte) 0xff});
        } else {
            byte[] a2 = BytesUtils.intToBytes(deviceId);
            buf.writeBytes(new byte[]{a2[3], a2[2]});
        }

        // 主站地址和组地址标志A3
        CctPrmEnums prm = messageEncodeHolder.getPrm();
        if (prm.equals(CctPrmEnums.launch_station)) {
            // 消息来自启动站
            buf.writeByte((msa.byteValue() << 1) ^ d0);
        } else {
            // 消息来自从动站
            buf.writeByte(d0);
        }
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 待写出缓冲区
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static CctTcpDecodeHolder read(ByteBuf buf) {
        CctTcpDecodeHolder decodeHolder = CctTcpDecodeHolder.of();
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        // 校验和校验
        CctAfnConfirmRwEnums.confirm.getCheckResult(decodeHolder.getRawDataHex());

        ByteBufUtils.readBytesToHex(buf, 6);
        // 解析控制码
        readControlCode(buf.readByte(), decodeHolder);
        // 解析地址域
        readAddress(ByteBufUtils.readBytes(buf, 5), decodeHolder);
        // 应用层功能码
        CctAfnFeatureEnums afnFeatureEnum = CctAfnFeatureEnums.getByCode(BytesUtils.byteToHex(buf.readByte()));
        decodeHolder.setAfn(afnFeatureEnum);
        // 帧序列域
        byte seq = buf.readByte();
        CctSeqFfEnums seqFfEnum = CctSeqFfEnums.getBySeq(seq);
        decodeHolder.setSeqFf(seqFfEnum);
        CctSeqConEnums seqConEnum = CctSeqConEnums.getBySeq(seq);
        decodeHolder.setSeqCon(seqConEnum);

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);

        IotCctFunc feature = decodeHolder.getFeature();
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);
        if (IotMessageType.CHILD_REPLY.codeEquals(feature.getDecodeType())) {
            List<DeviceMessage> childMessages = readChildMessage(deviceMessage);
            decodeHolder.addChildMessage(childMessages);
        }
        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    private static void readControlCode(byte controlCode, CctTcpDecodeHolder messageDecodeHolder) {
        messageDecodeHolder.setDir(DataDirectionEnums.getByControlCode(controlCode));
        CctPrmEnums prmEnum = CctPrmEnums.getByControlCode(controlCode);
        messageDecodeHolder.setPrm(prmEnum);
        if (prmEnum == CctPrmEnums.launch_station) {
            CctLaunchFeatureEnums featureEnum = CctLaunchFeatureEnums.getByControlCode(controlCode);
            messageDecodeHolder.setPrmFeature(featureEnum);
        } else {
            CctSlaveFeatureEnums featureEnum = CctSlaveFeatureEnums.getByControlCode(controlCode);
            messageDecodeHolder.setPrmFeature(featureEnum);
        }
    }

    private static void readAddress(byte[] address, CctTcpDecodeHolder messageDecodeHolder) {
        messageDecodeHolder.setAdcode(BCD.bcdToStr(new byte[]{address[1], address[0]}));
        messageDecodeHolder.setDeviceId(String.valueOf(BytesUtils.bytesToShort(new byte[]{address[3], address[2]})));
        // 主站地址和组地址标志A3
        byte a3 = address[4];
        messageDecodeHolder.setBroadcast(a3 & 0x01);
        messageDecodeHolder.setMsa(a3 >> 1);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf                 待写出缓冲区
     * @param messageDecodeHolder 消息解码数据包装类
     * @param handler             消息ID处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf, CctTcpDecodeHolder messageDecodeHolder, BiFunction<DeviceMessage, Integer, T> handler) {
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = CctMessageTypeEnums.writePropertyReply.forTcp.get();

        // 从ByteBuf读取
        nbMessage.read(buf, messageDecodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        message.thingId(DeviceThingType.device, messageDecodeHolder.getDeviceId());

        // 从消息体中的地址域A解析 MSA -> msgId
        int msa = messageDecodeHolder.getMsa();
        return handler.apply(message, msa == 0 ? Integer.parseInt(messageDecodeHolder.getDeviceId()) : msa);
    }

    private static List<DeviceMessage> readChildMessage(DeviceMessage deviceMessage) {
        if (!(deviceMessage instanceof WritePropertyMessageReply)) {
            return ListUtil.empty();
        }
        WritePropertyMessageReply writeMessageReply = (WritePropertyMessageReply) deviceMessage;
        Map<String, Object> properties = writeMessageReply.getProperties();
        if (CollUtil.isEmpty(properties)) {
            return ListUtil.empty();
        }
        String meterData = Convert.toStr(properties.get("meterData"));
        List<CctMeterDataVo> cctMeterDataVos = JsonUtils.parseArray(meterData, CctMeterDataVo.class);
        if (CollUtil.isEmpty(cctMeterDataVos)) {
            return ListUtil.empty();
        }

        return cctMeterDataVos
            .stream()
            .filter(meterDataVo -> StrUtil.isNotBlank(meterDataVo.getMeterAddr()))
            .map(meterDataVo -> {
                Map<String, Object> headers = deviceMessage.getHeaders();
                String messageId = deviceMessage.getMessageId();

                WritePropertyMessageReply childWriteReply = (WritePropertyMessageReply) IotMessageType.WRITE_PROPERTY_REPLY.forDeviceInstance();
                childWriteReply.setDeviceId(meterDataVo.getMeterAddr());

                Map<String, Object> map = BeanUtil.beanToMap(meterDataVo);
                // 设置厂商(协议)信息
                map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.JOYO_CCT.getCode());
                childWriteReply.setProperties(map);
                childWriteReply.setHeaders(headers);
                childWriteReply.setMessageId(messageId);

                ChildDeviceMessageReply childReply = (ChildDeviceMessageReply) IotMessageType.CHILD_REPLY.forDeviceInstance();
                childReply.setDeviceId(deviceMessage.getDeviceId());
                childReply.setMessageId(messageId);
                childReply.setChildDeviceMessage(childWriteReply);
                childReply.setChildDeviceId(meterDataVo.getMeterAddr());
                childReply.setHeaders(headers);

                return childReply;
            }).collect(Collectors.toList());
    }
}
