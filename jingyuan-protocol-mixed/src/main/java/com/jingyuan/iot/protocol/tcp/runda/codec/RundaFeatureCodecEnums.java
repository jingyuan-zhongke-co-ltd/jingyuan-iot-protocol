package com.jingyuan.iot.protocol.tcp.runda.codec;

import com.jingyuan.common.protocol.enums.runda.RundaWpFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 润达消息功能类型枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum RundaFeatureCodecEnums implements FeatCodec {
    /**
     * 润达净水机消息类型枚举值，具体功能参考实现类
     */
    ID_APPLY(RundaWpFeatureEnums.ID_APPLY, new RundaIdApplyMessage()),
    KEEPALIVE(RundaWpFeatureEnums.KEEPALIVE, new RundaKeepaliveMessage()),
    BIND_PACKAGE(RundaWpFeatureEnums.BIND_PACKAGE, new RundaBindPackageMessage()),
    CLOSE_SCREEN(RundaWpFeatureEnums.CLOSE_SCREEN, new RundaCloseScreenMessage()),
    OPEN_SCREEN(RundaWpFeatureEnums.OPEN_SCREEN, new RundaOpenScreenMessage()),
    SHUTDOWN(RundaWpFeatureEnums.SHUTDOWN, new RundaShutdownMessage()),
    START_UP(RundaWpFeatureEnums.START_UP, new RundaStartUpMessage()),
    FORCE_RINSE(RundaWpFeatureEnums.FORCE_RINSE, new RundaForceRinseMessage()),
    RECHARGE_UP(RundaWpFeatureEnums.RECHARGE_UP, new RundaRechargeUpMessage()),
    RECHARGE_DOWN(RundaWpFeatureEnums.RECHARGE_DOWN, new RundaRechargeDownMessage()),
    SYNC_WATER(RundaWpFeatureEnums.SYNC_WATER, new RundaSyncWaterMessage()),
    SYNC_TIME(RundaWpFeatureEnums.SYNC_TIME, new RundaSyncTimeMessage()),
    SYNC_WP_STATUS(RundaWpFeatureEnums.SYNC_WP_STATUS, new RundaSyncWpStatusMessage()),
    QUERY_WP_STATUS(RundaWpFeatureEnums.QUERY_WP_STATUS, new RundaQueryWpStatusMessage()),
    FILTER_RESET(RundaWpFeatureEnums.FILTER_RESET, new RundaFilterResetMessage()),
    MODE_SWITCH(RundaWpFeatureEnums.MODE_SWITCH, new RundaModeSwitchMessage()),

    FACTORY_RESET(RundaWpFeatureEnums.FACTORY_RESET, new RundaFactoryResetMessage()),
    IP_PORT(RundaWpFeatureEnums.IP_PORT, new RundaIpPortMessage()),
    HEARTBEAT_MODIFY(RundaWpFeatureEnums.HEARTBEAT_MODIFY, new RundaHeartbeatModifyMessage()),
    TIME_RENTAL_MODE(RundaWpFeatureEnums.TIME_RENTAL_MODE, new RundaTimeRentalModeMessage()),
    RINSE_TIME_MODIFY(RundaWpFeatureEnums.RINSE_TIME_MODIFY, new RundaRinseTimeModifyMessage()),
    TIMED_RINSE_MODIFY(RundaWpFeatureEnums.TIMED_RINSE_MODIFY, new RundaTimedRinseModifyMessage()),
    OVERHAUL_TIME_MODIFY(RundaWpFeatureEnums.OVERHAUL_TIME_MODIFY, new RundaOverhaulTimeModifyMessage()),
    AGAIN_RINSE_TIME(RundaWpFeatureEnums.AGAIN_RINSE_TIME, new RundaAgainRinseTimeMessage()),
    QUERY_ICCID(RundaWpFeatureEnums.QUERY_ICCID, new RundaQueryIccidMessage()),
    ONE_LITER_TIME(RundaWpFeatureEnums.ONE_LITER_TIME, new RundaOneLiterTimeMessage());

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
