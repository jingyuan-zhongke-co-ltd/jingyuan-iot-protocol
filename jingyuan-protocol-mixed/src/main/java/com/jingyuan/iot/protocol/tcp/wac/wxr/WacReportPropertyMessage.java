package com.jingyuan.iot.protocol.tcp.wac.wxr;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.wac.gauge.WacFeatCodecs;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 属性上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class WacReportPropertyMessage implements TcpMessageAdapter<ReportPropertyMessage> {

    private ReportPropertyMessage message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new ReportPropertyMessage();
        Map<String, Object> map = doRead(buf, decodeHolder);
        message.setProperties(map);
    }

    @Override
    public void setMessage(ReportPropertyMessage message) {
        this.message = message;
    }

    @Override
    public ReportPropertyMessage getMessage() {
        return message;
    }

    /**
     * 回复消息读取
     *
     * @param buf          ByteBuf
     * @param decodeHolder 厂商及原始数据信息
     * @return java.util.Map<java.lang.String, java.lang.Object>
     */
    public static Map<String, Object> doRead(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        WacTcpDecodeHolder holder = (WacTcpDecodeHolder) decodeHolder;

        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, ((WacTcpDecodeHolder) decodeHolder).getVendor().getCode());
        map.put(IotConstants.METER_ADDR, decodeHolder.getDeviceId());

        if (holder.getDataLength() > 0) {
            WacFeatCodecs.read(holder, buf, map);
        }
        return map;
    }
}
