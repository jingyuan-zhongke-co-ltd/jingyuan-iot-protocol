package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读软开度参数
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadSoftMode implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        properties.put("actionFirst",buf.readByte());
        byte[] bytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(bytes);
        properties.put("totalTravelTime", BytesUtils.bytesToUnLong(bytes));

    }
}
