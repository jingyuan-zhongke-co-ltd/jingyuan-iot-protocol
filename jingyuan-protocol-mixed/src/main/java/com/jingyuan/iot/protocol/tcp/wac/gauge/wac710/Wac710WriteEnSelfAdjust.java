package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteEnSelfAdjustBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 写是否启用自调节
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteEnSelfAdjust implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteEnSelfAdjustBo enSelfAdjustBo = BeanUtil.toBean(properties, Wac710WriteEnSelfAdjustBo.class);
        if (enSelfAdjustBo.getEnSelfAdjust()) {
            buf.writeByte(0);
        } else {
            buf.writeByte(1);
        }
    }
}
