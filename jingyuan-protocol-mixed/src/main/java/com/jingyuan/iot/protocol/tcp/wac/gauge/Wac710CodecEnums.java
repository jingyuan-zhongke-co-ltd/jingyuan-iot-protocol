package com.jingyuan.iot.protocol.tcp.wac.gauge;

import com.jingyuan.common.protocol.enums.wac.gauge.Wac710FeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.wac.gauge.wac710.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 调节阀 710 编解码枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum Wac710CodecEnums implements FeatCodec {

    /**
     * 读阀门数据
     */
    R_VALVE_DATA(Wac710FeatEnums.R_VALVE_DATA, new Wac710ReadValveData()),
    /**
     * 写阀门控制
     */
    W_VALVE_CONTROL(Wac710FeatEnums.W_VALVE_CONTROL, new Wac710WriteValveControl()),

//    开启/结束计量日期
    W_MEASURE_DATE(Wac710FeatEnums.W_MEASURE_DATE, new Wac710WriteMeasureDate()),
    R_MEASURE_DATE(Wac710FeatEnums.R_MEASURE_DATE, new Wac710ReadMeasureDate()),

//    写清洗阀门周期
    W_CLEAN_VALVE_CYCLE(Wac710FeatEnums.W_CLEAN_VALVE_CYCLE, new Wac710WriteCleanValveCycle()),
    R_CLEAN_VALVE_CYCLE(Wac710FeatEnums.R_CLEAN_VALVE_CYCLE, new Wac710ReadCleanValveCycle()),

//    写阀门开度上下限
    W_VALVE_OPEN_LIMIT(Wac710FeatEnums.W_VALVE_OPEN_LIMIT, new Wac710WriteValveOpenLimit()),
    R_VALVE_OPEN_LIMIT(Wac710FeatEnums.R_VALVE_OPEN_LIMIT, new Wac710ReadValveOpenLimit()),

//    写目标调节值
    W_TARGET_ADJUST_VALUE(Wac710FeatEnums.W_TARGET_ADJUST_VALUE, new Wac710WriteTargetAdjustValue()),
    R_TARGET_ADJUST_VALUE(Wac710FeatEnums.R_TARGET_ADJUST_VALUE, new Wac710ReadTargetAdjustValue()),

//    写是否启用自调节
    W_EN_SELF_ADJUST(Wac710FeatEnums.W_EN_SELF_ADJUST, new Wac710WriteEnSelfAdjust()),

//    写调节阀所有配置参数
    W_WAC_ALL_CONFIG(Wac710FeatEnums.W_WAC_ALL_CONFIG, new Wac710WriteAllConfParams()),
    R_WAC_ALL_CONFIG(Wac710FeatEnums.R_WAC_ALL_CONFIG, new Wac710ReadAllConfParams()),

//    写预留编码
    W_RESERVED_CODE(Wac710FeatEnums.W_RESERVED_CODE, new Wac710WriteReservedCode()),
    R_RESERVED_CODE(Wac710FeatEnums.R_RESERVED_CODE, new Wac710ReadReservedCode()),

//    设置阀门电流AD值上限
    W_AD_UPPER_LIMIT(Wac710FeatEnums.W_AD_UPPER_LIMIT, new Wac710WriteAdUpperLimit()),
    R_AD_UPPER_LIMIT(Wac710FeatEnums.R_AD_UPPER_LIMIT, new Wac710ReadAdUpperLimit()),

//    写累计开阀时间
    W_VALVE_OPEN_TIME(Wac710FeatEnums.W_VALVE_OPEN_TIME, new Wac710WriteValveOpenTime()),

//    写累计工作时间
    W_WORKING_HOURS(Wac710FeatEnums.W_WORKING_HOURS, new Wac710WriteWorkingHours()),

//    阀门开关次数
    W_VALVE_CONT_NUM(Wac710FeatEnums.W_VALVE_CONT_NUM, new Wac710WriteValveContNum()),

//    软重启
    W_SOFT_REBOOT(Wac710FeatEnums.W_SOFT_REBOOT, new Wac710WriteSoftReboot()),

//    以当前角度增减开度
    W_ADJUST_ANGULAR(Wac710FeatEnums.W_ADJUST_ANGULAR, new Wac710WriteAdjustAngular()),

//    修改modbus本机地址
    W_MODBUS_ADDR(Wac710FeatEnums.W_MODBUS_ADDR, new Wac710WriteModbusAddr()),
    R_MODBUS_ADDR(Wac710FeatEnums.R_MODBUS_ADDR, new Wac710ReadModbusAddr()),

//    修改modbus本机串口属性
    W_MODBUS_SERIAL_PORT(Wac710FeatEnums.W_MODBUS_SERIAL_PORT, new Wac710WriteModbusSerialPort()),
    R_MODBUS_SERIAL_PORT(Wac710FeatEnums.R_MODBUS_SERIAL_PORT, new Wac710ReadModbusSerialPort()),

//    软硬开度模式设置
    W_SOFT_HARD_MODE(Wac710FeatEnums.W_SOFT_HARD_MODE, new Wac710WriteSoftHardMode()),
    R_SOFT_HARD_MODE(Wac710FeatEnums.R_SOFT_HARD_MODE, new Wac710ReadSoftHardMode()),

//    软开度模式参数设置
    W_SOFT_MODE(Wac710FeatEnums.W_SOFT_MODE, new Wac710WriteSoftMode()),
    R_SOFT_MODE(Wac710FeatEnums.R_SOFT_MODE, new Wac710ReadSoftMode()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
