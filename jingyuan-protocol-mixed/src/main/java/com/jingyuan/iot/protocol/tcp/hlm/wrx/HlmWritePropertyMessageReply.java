package com.jingyuan.iot.protocol.tcp.hlm.wrx;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.hlm.codec.HlmAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpDecodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessageReply;

import java.util.HashMap;
import java.util.Map;

/**
 * 海龙马超声波协议 - 数据上报
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class HlmWritePropertyMessageReply implements TcpMessageAdapter<WritePropertyMessageReply> {

    private WritePropertyMessageReply message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder tcpDecodeHolder) {
        message = new WritePropertyMessageReply();
        HlmTcpDecodeHolder decodeHolder = (HlmTcpDecodeHolder) tcpDecodeHolder;
        Map<String, Object> map = initReadMap(decodeHolder);
        HlmAfnFeatCodecEnums.read(buf, decodeHolder, map);
        message.setProperties(map);
    }

    @Override
    public void setMessage(WritePropertyMessageReply message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessageReply getMessage() {
        return message;
    }


    public static Map<String, Object> initReadMap(HlmTcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(16);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.METER_ADDR, decodeHolder.getDeviceId());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.HLM_US.getCode());
        return map;
    }
}
