package com.jingyuan.iot.protocol.tcp.wac.gauge.conf;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfTempDiffConfBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.BcdAndPad;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.WacConfCodecEnums.revertBCD;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.genRead;
import static com.jingyuan.iot.protocol.tcp.wac.gauge.conf.WacConfPanelsCont.writeGeneralParma;

/**
 * 0x02:回温调节
 * 0x03:温差调节模式
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
public class WacConfTempCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值，目标调节值，表示0.5摄氏度温度。
        properties.put("adjustTemp", BigDecimal.valueOf(Convert.toInt(revertBCD(buf, 2))).movePointLeft(2));

        genRead(buf, properties);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacConfTempDiffConfBo bean = BeanUtil.toBean(properties, WacConfTempDiffConfBo.class);
        // 目标调节值
        buf.writeBytes(BcdAndPad(Convert.toShort(bean.getAdjustTemp()* 100), 2));

        writeGeneralParma(buf, bean);
    }
}
