package com.jingyuan.iot.protocol.tcp.spr.holder;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.spr.SprChildVendorEnums;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息编码包装类
 *
 * @author: cc
 * @date: 2024/4/22 11:28
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class SprTcpEncodeHolder extends TcpEncodeHolder {
    /**
     * 厂商类型
     */
    private SprChildVendorEnums childVendorEnum;
}
