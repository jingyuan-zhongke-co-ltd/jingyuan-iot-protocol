package com.jingyuan.iot.protocol.tcp.nom.tag;

import com.jingyuan.common.protocol.enums.nom.JzNomResultEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 京兆无磁-结果码
 *
 * @author: cc
 * @date: 2024/9/27 14:22
 * @Version: V1.0
 */
@Slf4j
public class JzNomTagResultCodeMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int length = buf.readShortLE();

        // 结果码
        properties.put("resultCode", JzNomResultEnums.getByCode(buf.readByte()).getDesc());
    }
}
