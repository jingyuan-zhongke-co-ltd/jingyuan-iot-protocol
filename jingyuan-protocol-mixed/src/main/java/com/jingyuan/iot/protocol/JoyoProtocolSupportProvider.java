package com.jingyuan.iot.protocol;

import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.iot.protocol.http.HttpDeviceMessageCodec;
import com.jingyuan.iot.protocol.http.codec.HttpMessageCodecEnums;
import com.jingyuan.iot.protocol.http.topic.HttpTopicEnums;
import com.jingyuan.iot.protocol.tcp.cct.CctDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.mixed.MixedDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.wac.WacDeviceMessageCodec;
import org.jetlinks.core.defaults.CompositeProtocolSupport;
import org.jetlinks.core.event.EventBus;
import org.jetlinks.core.message.codec.DefaultTransport;
import org.jetlinks.core.route.HttpRoute;
import org.jetlinks.core.route.WebsocketRoute;
import org.jetlinks.core.spi.ProtocolSupportProvider;
import org.jetlinks.core.spi.ServiceContext;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import reactor.core.publisher.Mono;

import java.util.Collections;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * 京源多协议支持
 *
 * @author: cc
 * @date: 2024/7/29 16:04
 * @Version: V1.0
 */
public class JoyoProtocolSupportProvider implements ProtocolSupportProvider {
    @Override
    public Mono<CompositeProtocolSupport> create(ServiceContext context) {
        return Mono.defer(() -> {

            ReactiveStringRedisTemplate redisTemplate = context
                .getService(ReactiveStringRedisTemplate.class)
                .orElseThrow(() -> new IllegalStateException("ReactiveStringRedisTemplate not found"));

            EventBus eventBus = context
                .getService(EventBus.class)
                .orElseThrow(() -> new IllegalStateException("EventBus not found"));

            CompositeProtocolSupport support = new CompositeProtocolSupport();

            support.setId("mixed.v1.0.3");
            support.setName("mixed V1.0.3");
            support.setDescription("Mixed Protocol Version 1.0.3");

            // TCP mixed
            support.addConfigMetadata(JoyoIotTransport.JOYO_MIXED_TCP, MixedDeviceMessageCodec.MIXED_CONFIG);
            support.addMessageCodecSupport(new MixedDeviceMessageCodec(redisTemplate));

            // TCP Joyo 集采器
            support.addConfigMetadata(JoyoIotTransport.JOYO_CCT_TCP, CctDeviceMessageCodec.JOYO_CCT_CONFIG);
            support.addMessageCodecSupport(new CctDeviceMessageCodec());

            // TCP Joyo 调节阀
            support.addConfigMetadata(JoyoIotTransport.JOYO_WAC_TCP, WacDeviceMessageCodec.JOYO_WAC_CONFIG);
            support.addMessageCodecSupport(new WacDeviceMessageCodec(redisTemplate));

            //HTTP
            support.setDocument(DefaultTransport.HTTP,
                                "document-http.md",
                                JoyoProtocolSupportProvider.class.getClassLoader());

            support.addMessageCodecSupport(new HttpDeviceMessageCodec(redisTemplate, eventBus));

            support.addRoutes(DefaultTransport.HTTP, Stream
                .of(HttpMessageCodecEnums.values())
                .map(HttpMessageCodecEnums::getHttpTopic)
                .map(HttpTopicEnums::getRoute)
                .distinct()
                .filter(route -> route != null && route.isUpstream())
                .map(route -> HttpRoute
                    .builder()
                    .address(route.getTopic())
                    .group(route.getGroup())
                    .contentType(MediaType.APPLICATION_JSON)
                    .method(HttpMethod.POST)
                    .description(route.getDescription())
                    .example(route.getExample())
                    .build())
                .collect(Collectors.toList())
            );

            support.addRoutes(DefaultTransport.HTTP,
                              Stream.of(HttpTopicEnums.reportProperty, HttpTopicEnums.reportPropertyV2)
                                    .map(HttpTopicEnums::getRoute)
                                    .map(route -> HttpRoute
                                        .builder()
                                        .address(route.getTopic())
                                        .group(route.getGroup())
                                        .contentType(MediaType.APPLICATION_JSON)
                                        .method(HttpMethod.GET)
                                        .description(route.getDescription())
                                        .example(route.getExample())
                                        .build())
                                    .collect(Collectors.toList()));

            //Websocket
            HttpDeviceMessageCodec codec = new HttpDeviceMessageCodec(DefaultTransport.WebSocket, redisTemplate, eventBus);
            support.addMessageCodecSupport(codec);
            support.addAuthenticator(DefaultTransport.WebSocket, codec);

            support.addRoutes(
                DefaultTransport.WebSocket,
                Collections.singleton(
                    WebsocketRoute
                        .builder()
                        .path("/{productId:产品ID}/{productId:设备ID}/socket")
                        .description("通过Websocket接入平台")
                        .build()
                ));

            return Mono.just(support);
        });
    }
}
