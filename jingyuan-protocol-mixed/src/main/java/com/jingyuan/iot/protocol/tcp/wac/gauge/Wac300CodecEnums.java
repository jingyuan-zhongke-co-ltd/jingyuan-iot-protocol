package com.jingyuan.iot.protocol.tcp.wac.gauge;

import com.jingyuan.common.protocol.enums.wac.gauge.Wac300FeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.wac.gauge.wac300.*;
import com.jingyuan.iot.protocol.tcp.wac.gauge.wac710.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 调节阀 710 编解码枚举
 *
 * @author: cc
 * @date: 2024/3/27 14:39
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum Wac300CodecEnums implements FeatCodec {

    /**
     * 读阀门数据
     */
    R_VALVE_DATA(Wac300FeatEnums.R_VALVE_DATA, new Wac300ReadValveData()),
    //    写阀门控制
    W_VALVE_CONTROL(Wac300FeatEnums.W_VALVE_CONTROL, new Wac710WriteValveControl()),

    //    写清洗阀门周期
    W_CLEAN_VALVE_CYCLE(Wac300FeatEnums.W_CLEAN_VALVE_CYCLE, new Wac710WriteCleanValveCycle()),
    R_CLEAN_VALVE_CYCLE(Wac300FeatEnums.R_CLEAN_VALVE_CYCLE, new Wac710ReadCleanValveCycle()),

    //    写目标调节值
    W_TARGET_ADJUST_VALUE(Wac300FeatEnums.W_TARGET_ADJUST_VALUE, new Wac710WriteTargetAdjustValue()),
    R_TARGET_ADJUST_VALUE(Wac300FeatEnums.R_TARGET_ADJUST_VALUE, new Wac710ReadTargetAdjustValue()),

    //    写是否启用温差、回温调节(AC/03) (写是否启用自调节)
    W_EN_SELF_ADJUST(Wac300FeatEnums.W_EN_SELF_ADJUST, new Wac710WriteEnSelfAdjust()),

    //    写调节阀所有配置参数(客户 AC/01)
    //    读调节阀所有配置参数(客户 AB/01)
    W_WAC_ALL_CONFIG(Wac300FeatEnums.W_WAC_ALL_CONFIG, new Wac710WriteAllConfParams()),
    R_WAC_ALL_CONFIG(Wac300FeatEnums.R_WAC_ALL_CONFIG, new Wac710ReadAllConfParams()),

    //    写预留编码(AC/08)
    //    读预留编码(AB/08)
    W_RESERVED_CODE(Wac300FeatEnums.W_RESERVED_CODE, new Wac710WriteReservedCode()),
    R_RESERVED_CODE(Wac300FeatEnums.R_RESERVED_CODE, new Wac710ReadReservedCode()),

    //    写探头与屏幕配置参数(生产 AC/09)
    //    读探头与屏幕配置参数(生产 AB/09)
    W_PROBE_SCREEN_CONF(Wac300FeatEnums.W_PROBE_SCREEN_CONF, new Wac300WriteProbeScreenConfParam()),
    R_PROBE_SCREEN_CONF(Wac300FeatEnums.R_PROBE_SCREEN_CONF, new Wac300ReadProbeScreenConfParam()),

    //    退出自检(生产 DD/AA)
    W_EXIT_SELF_TEST(Wac300FeatEnums.W_EXIT_SELF_TEST, new Wac300WriteExitSelfTest()),

    //  todo  自检(可选自检类型)(生产 DD/F0,DD/F2)
    W_SELF_TEST_TYPE(Wac300FeatEnums.W_SELF_TEST_TYPE, new Wac300WriteSelfTestType()),

    //    读自检状态(生产 FO/DD)
    R_SELF_TEST_STATUS(Wac300FeatEnums.R_SELF_TEST_STATUS, new Wac300ReadSelfTestStatus()),

    //    恢复出厂配置(生产 FF/00)
    W_RESTORE_FACTORY_CONF(Wac300FeatEnums.W_RESTORE_FACTORY_CONF, new Wac300WriteRestoreFactoryConf()),

    //    设置阀门电流 AD 值上限(AC/0A)
    //    读阀门电流 AD 值上限(AB/0A)
    W_AD_UPPER_LIMIT(Wac300FeatEnums.W_AD_UPPER_LIMIT, new Wac710WriteAdUpperLimit()),
    R_AD_UPPER_LIMIT(Wac300FeatEnums.R_AD_UPPER_LIMIT, new Wac710ReadAdUpperLimit()),

    //    写累计开阀时间(AC/0C)
    W_VALVE_OPEN_TIME(Wac300FeatEnums.W_VALVE_OPEN_TIME, new Wac710WriteValveOpenTime()),

    //    写累计工作时间(AC/0D)
    W_WORKING_HOURS(Wac300FeatEnums.W_WORKING_HOURS, new Wac710WriteWorkingHours()),

    //    写阀门开关次数(AC/0E)
    W_VALVE_CONT_NUM(Wac300FeatEnums.W_VALVE_CONT_NUM, new Wac710WriteValveContNum()),

    //    软重启(FF/55)
    W_SOFT_REBOOT(Wac300FeatEnums.W_SOFT_REBOOT, new Wac710WriteSoftReboot()),

    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
