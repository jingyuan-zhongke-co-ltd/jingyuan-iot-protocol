package com.jingyuan.iot.protocol.tcp.wac.gauge.adjust;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.adjust.WacAdjustTimeShardingBo;
import com.jingyuan.common.protocol.model.wac.adjust.WacTimeShardingBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DatePatterns;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.common.utils.validator.ValidatorUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分时调节
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class WacAdjustTimeShardingMode implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        List<Map<String, Object>> maps = new ArrayList<>(6);
        int count = buf.readByte();
        for (int i = 1; i <= count; i++) {
            Map<String, Object> map = new HashMap<>(2);
            // 时间
            String time = BCD.bcdToStr(ByteBufUtils.readBytes(buf, 2));
            map.put("time" + i, time);
            // 开度
            int opening = buf.readByte();
            map.put("opening" + i, opening);

            maps.add(map);
        }
        properties.put("adjustTimeSharding", maps);
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacAdjustTimeShardingBo timeShardingBo = BeanUtil.toBean(properties, WacAdjustTimeShardingBo.class);
        List<WacTimeShardingBo> timeSharding = timeShardingBo.getTimeSharding();
        // 由于长度由实际参数数量决定，所以此处需根据实际值计算
        buf.setByte(13, 5 + timeSharding.size() * 3);
        // 调节个数
        buf.writeByte(timeSharding.size());
        timeSharding.forEach(item -> {
            ValidatorUtils.validateEntity(item);
            // 时间
            String format = DateUtils.format(item.getTime(), DatePatterns.HOUR_MINUTE_PATTERN);
            buf.writeBytes(BytesUtils.hexToBytes(format));
            // 阀门开度
            buf.writeByte(item.getOpening());
        });
    }
}
