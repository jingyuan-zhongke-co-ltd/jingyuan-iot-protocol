package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnOneRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 请求1类数据（实时数据）
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnRealtimeDataMessage implements LcpAfnMessage {

    @Override
    public void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String afn = BytesUtils.byteToHex(buf.readByte());
        LcpAfnOneRwEnums realtimeRwEnum = LcpAfnOneRwEnums.getByFeatCode(afn);
        decodeHolder.setFeature(realtimeRwEnum);

        FeatCodecs.read(realtimeRwEnum, buf, properties);
    }

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof LcpAfnOneRwEnums;
        LcpAfnOneRwEnums featureEnum = (LcpAfnOneRwEnums) feature;

        FeatCodecs.write(featureEnum, buf, properties);
    }
}
