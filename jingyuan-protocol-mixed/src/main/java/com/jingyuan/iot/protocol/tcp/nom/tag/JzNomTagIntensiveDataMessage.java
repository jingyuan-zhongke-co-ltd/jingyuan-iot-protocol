package com.jingyuan.iot.protocol.tcp.nom.tag;

import cn.hutool.core.util.ArrayUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.math.ArithmeticUtil;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 京兆无磁-水表密集数据
 *
 * @author: cc
 * @date: 2024/9/27 14:22
 * @Version: V1.0
 */
@Slf4j
public class JzNomTagIntensiveDataMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        int length = buf.readShortLE();

        // 起始时间
        properties.put("inseStartTime", JzNomTagRealTimeDataMessage.readDate(buf));
        // 数据记录间隔
        properties.put("inseInterval", (int) buf.readByte());
        // 记录个数
        int insRecordNum = buf.readByte();
        properties.put("inseRecordNum", insRecordNum);

        // 记录值
        byte[] inseDataBytes = ByteBufUtils.readBytes(buf, 4 * insRecordNum);
        // 正累计流量列表
        List<Double> forwardQts = new ArrayList<>();
        // 逆累计流量列表
        List<Double> reverseQts = new ArrayList<>();
        for (byte[] bytes : ArrayUtil.split(inseDataBytes, 4)) {
            forwardQts.add(ArithmeticUtil.div(BytesUtils.bytesToShort(new byte[]{bytes[3], bytes[2]}, ByteOrder.LITTLE_ENDIAN), 1000, 3));
            reverseQts.add(ArithmeticUtil.div(BytesUtils.bytesToShort(new byte[]{bytes[1], bytes[0]}, ByteOrder.LITTLE_ENDIAN), 1000, 3));
        }
        properties.put("forwardQts", forwardQts);
        properties.put("reverseQts", reverseQts);
    }
}
