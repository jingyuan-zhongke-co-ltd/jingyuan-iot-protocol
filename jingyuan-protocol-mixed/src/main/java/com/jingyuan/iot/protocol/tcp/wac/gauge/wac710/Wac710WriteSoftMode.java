package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteSoftModeBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 软开度模式参数设置
 *
 * @author: dr
 * @date: 2024-12-12
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteSoftMode implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteSoftModeBo bean = BeanUtil.toBean(properties, Wac710WriteSoftModeBo.class);
        buf.writeByte(bean.getActionFirst());
        buf.writeBytes(BytesUtils.intToBytes(bean.getTotalTravelTime(), ByteOrder.LITTLE_ENDIAN));
    }
}
