package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpOverhaulTimeBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 检修时间读写(超时未水满)
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaOverhaulTimeModifyMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 检修时间
        byte[] overhaulTime = ByteBufUtils.readBytes(buf, 2);
        properties.put("overhaulTime", BytesUtils.binary(10, overhaulTime));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpOverhaulTimeBo wpOverhaulTimeBo = BeanUtil.toBean(properties, WpOverhaulTimeBo.class);
        Short overhaulTime = wpOverhaulTimeBo.getOverhaulTime();
        buf.writeBytes(BytesUtils.shortToBytes(overhaulTime));
    }
}
