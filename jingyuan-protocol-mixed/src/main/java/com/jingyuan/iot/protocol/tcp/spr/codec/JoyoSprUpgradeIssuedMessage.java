package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotSprConstants;
import com.jingyuan.common.protocol.model.FirmwareFrameBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.Crc16Utils;
import com.jingyuan.iot.protocol.tcp.spr.SprChildVendorEnums;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * NB升级包下发帧
 *
 * @author: cc
 * @date: 2024/4/15 15:57
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprUpgradeIssuedMessage {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf 数据
     */
    public static Mono<Map<String, Object>> write(ByteBuf buf, SprChildVendorEnums childVendorEnum, Short upgradeN, FirmwareFrameBo frameBo) {
        String prefix = SprChildVendorEnums.JOYO_SPR.equals(childVendorEnum) ? "a168" : "a268";
        buf.writeBytes(BytesUtils.hexToBytes(prefix));

        // 第N帧
        buf.writeBytes(BytesUtils.shortToBytes(Short.parseShort(String.valueOf(upgradeN))));
        // 本帧长度M, 默认 512
        buf.writeBytes(BytesUtils.shortToBytes(frameBo.getFrameLen()));
        String issuedFrame;
        if (frameBo.getFrameNum() > upgradeN) {
            int fromIndex = (upgradeN - 1) * frameBo.getFrameLen() * 2;
            issuedFrame = StrUtil.sub(frameBo.getContent(), fromIndex, fromIndex + frameBo.getFrameLen() * 2);
        } else if (frameBo.getFrameNum().longValue() == upgradeN) {
            issuedFrame = StrUtil.sub(frameBo.getContent(),
                                      (upgradeN - 1) * frameBo.getFrameLen() * 2, frameBo.getContent().length());
        } else {
            throw new IllegalArgumentException("请求的数据帧长度大于升级包的总长度");
        }
        buf.writeBytes(BytesUtils.hexToBytes(issuedFrame));

        // CRC校验CRC-16/CCITT x16+x12+x5+1，校验从A1到CRC前一位。
        byte[] toCheckBytes = ByteBufUtils.getAll(buf);
        short computeSum = Convert.toShort(Crc16Utils.crc16Ccitt(toCheckBytes, 0, toCheckBytes.length));
        buf.writeBytes(BytesUtils.shortToBytes(computeSum));
        // 终止符，固定为0x16
        buf.writeByte(0x16);

        // 将下发的升级数据帧转为 DeviceMessage 以记录
        Map<String, Object> map = new HashMap<>(5);
        map.put(IotSprConstants.UPGRADE_HEX, ByteBufUtils.getAllToHex(buf));
        map.put(IotSprConstants.UPGRADE_N, upgradeN);
        map.put(IotSprConstants.FRAME_NUM, frameBo.getFrameNum());
        map.put(IotSprConstants.FRAME_LEN, frameBo.getFrameLen());
        map.put(IotConstants.FIRMWARE_ID, frameBo.getFirmwareId());
        return Mono.just(map);
    }
}