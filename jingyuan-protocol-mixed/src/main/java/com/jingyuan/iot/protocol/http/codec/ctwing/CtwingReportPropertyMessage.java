package com.jingyuan.iot.protocol.http.codec.ctwing;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ObjUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.enums.impl.iot.NorthChannelEnums;
import com.jingyuan.common.flowable.constant.IotNorthConstants;
import com.jingyuan.common.flowable.enums.NorthMessageTypes;
import com.jingyuan.common.flowable.enums.CtwingServiceEnums;
import com.jingyuan.common.flowable.model.north.event.NorthLogEventBo;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.constant.IotTopicConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.http.HttpDecodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.protocol.message.adapter.HttpMessageAdapter;
import com.jingyuan.common.utils.collection.MapUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import com.jingyuan.iot.protocol.http.holder.HttpDecodeRedisHolder;
import com.jingyuan.iot.protocol.tcp.mixed.MixedTransportEnums;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.Values;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Headers;
import org.jetlinks.core.message.property.ReportPropertyMessage;
import org.jetlinks.core.message.property.WritePropertyMessageReply;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * AEP 平台设备数据上报消息
 *
 * @author: cc
 * @date: 2024/9/10 17:17
 * @Version: V1.0
 */
@Slf4j
public class CtwingReportPropertyMessage implements HttpMessageAdapter {

    private static final String NORTH_DEVICES_KEY = "north_devices:%s:%s";

    @Override
    @SneakyThrows
    @SuppressWarnings("unchecked")
    public Flux<DeviceMessage> read(HttpDecodeHolder decodeHolder) {
        Map<String, Object> payloadMap = JsonUtils.parseObject(decodeHolder.getPayload(), Map.class);
        if (CollUtil.isEmpty(payloadMap)) {
            return Flux.empty();
        }
        return Flux.just(payloadMap)
                   .switchIfEmpty(Mono.defer(Mono::empty))
                   .flatMap(map -> {
                       String uploadFrame = getFrameHex(map);
                       String imei = Convert.toStr(MapUtils.getValue(map, IotConstants.IMEI_UPPER));
                       String productId = Convert.toStr(map.get(IotConstants.PRODUCT_ID));
                       String deviceId = Convert.toStr(map.get(IotConstants.DEVICE_ID));
                       String nseId = Convert.toStr(map.get(IotNorthConstants.SERVICE_ID));
                       CtwingServiceEnums ctwingService = CtwingServiceEnums.getByServiceId(nseId);

                       if (StrUtil.isBlank(uploadFrame) || StrUtil.isBlank(imei)) {
                           return Flux.empty();
                       }
                       JoyoIotVendor vendor = JoyoIotVendor.getByRawData(uploadFrame.toLowerCase());
                       decodeHolder.setIotVendor(vendor);

                       return cacheNorthDevice(decodeHolder, productId, imei, uploadFrame)
                           .thenMany(doRead(decodeHolder, map, uploadFrame, imei, productId, NorthChannelEnums.ctwing, true)
                                         .map(deviceMessage -> {
                                             MessageHelper.addHeadVendor(deviceMessage, vendor.getCode());
                                             deviceMessage.addHeader(IotNorthConstants.NDE_ID, deviceId);
                                             deviceMessage.addHeader(IotNorthConstants.NPR_ID, productId);
                                             deviceMessage.addHeader(IotNorthConstants.NSE_ID, ctwingService.getServiceId());
                                             return deviceMessage;
                                         }));
                   });
    }


    /**
     * 缓存北向设备信息
     *
     * @param decodeHolder 解析数据
     * @param productId    北向产品ID
     * @param imei         imei
     * @param uploadFrame  数据帧
     * @return reactor.core.publisher.Mono<java.lang.Long>
     */
    private Mono<Long> cacheNorthDevice(HttpDecodeHolder decodeHolder, String productId, String imei, String uploadFrame) {
        if (StrUtil.isBlank(uploadFrame) || MessageHelper.isUpgradeFrame(uploadFrame)) {
            return Mono.just(0L);
        }

        HttpDecodeRedisHolder holder = (HttpDecodeRedisHolder) decodeHolder;
        JoyoIotVendor vendor = decodeHolder.getIotVendor();
        ReactiveStringRedisTemplate redisTemplate = holder.getRedisTemplate();
        return redisTemplate
            .opsForSet()
            .add(String.format(NORTH_DEVICES_KEY, productId, vendor.getCode()), imei);
    }


    public static Flux<DeviceMessage> doRead(HttpDecodeHolder decodeHolder, Map<String, Object> map, String uploadFrame,
                                             String imei, String norProductId, NorthChannelEnums northChannel, boolean state) {
        return decodeHolder
            .getDecodeContext()
            .getDevice(imei)
            .switchIfEmpty(Mono.defer(() -> Mono.error(new IllegalArgumentException("设备：【" + imei + "】不存在或未启用"))))
            .flatMap(operator -> operator.getConfigs(IotConstants.VENDOR_CODE))
            .switchIfEmpty(Mono.defer(() -> Mono.just(Values.of(MapUtils.empty()))))
            .map(configs -> MessageHelper.getValueStr(configs, IotConstants.VENDOR_CODE))
            .flatMapMany(vendorCode -> {
                JoyoIotVendor iotVendor = decodeHolder.getIotVendor();
                if (StrUtil.isBlank(vendorCode)) {
                    return MixedTransportEnums.decodeByHexStr(uploadFrame, null);
                }
                if (ObjUtil.isNotNull(iotVendor) && iotVendor.isSame(vendorCode)) {
                    return MixedTransportEnums.decodeByHexStr(uploadFrame, vendorCode);
                } else {
                    return MixedTransportEnums.decodeByHexStr(uploadFrame, null);
                }
            })
            .flatMap(deviceMessage -> {
                // 重置设备编号
                resetDeviceId(deviceMessage, imei);
                deviceMessage.addHeader(IotConstants.IMEI_LOWER, imei);
                // 设置超时时间（可选,默认10分钟），如果超过这个时间没有收到任何消息则认为离线。
                deviceMessage.addHeader(Headers.keepOnlineTimeoutSeconds, 10);
                decodeHolder.setDeviceMessage(deviceMessage);

                return decodeHolder
                    .getEventBus()
                    .publish(IotTopicConstants.NORTH_MSG_PROCESS_LOGS,
                             NorthLogEventBo.nleEvent(northChannel, state, NorthMessageTypes.DATA_REPORT.getCode(),
                                                      JsonUtils.toJsonString(map), JsonUtils.toJsonString(deviceMessage), imei, norProductId))
                    .thenReturn(deviceMessage);
            });
    }


    /**
     * 重置设备编号：
     * CTWing 平台、Onenet 平台设备以 imei 号作为设备编号
     *
     * @param deviceMessage 设备消息
     * @param imei          imei
     */
    public static void resetDeviceId(DeviceMessage deviceMessage, String imei) {
        // CTWing 平台、Onenet 平台设备以 imei 号作为设备编号
        String deviceId = deviceMessage.getDeviceId();
        deviceMessage.addHeader(IotConstants.METER_ADDR, deviceId);
        Map<String, Object> properties;
        if (deviceMessage instanceof ReportPropertyMessage) {
            ReportPropertyMessage reportPropertyMessage = (ReportPropertyMessage) deviceMessage;
            reportPropertyMessage.setDeviceId(imei);

            properties = reportPropertyMessage.getProperties();
            if (CollUtil.isNotEmpty(properties)) {
                properties.put(IotConstants.METER_ADDR, deviceId);
            }
            reportPropertyMessage.setProperties(properties);
        } else if (deviceMessage instanceof WritePropertyMessageReply) {
            WritePropertyMessageReply writePropertyMessageReply = (WritePropertyMessageReply) deviceMessage;
            ((WritePropertyMessageReply) deviceMessage).setDeviceId(imei);

            properties = writePropertyMessageReply.getProperties();
            if (CollUtil.isNotEmpty(properties)) {
                properties.put(IotConstants.METER_ADDR, deviceId);
            }
            writePropertyMessageReply.setProperties(properties);
        }
    }


    /**
     * 获取请求参数中的数据帧
     *
     * @param map 请求参数
     * @return java.lang.String
     */
    private static String getFrameHex(Map<String, Object> map) {
        // AEP 分体阀控水表数据帧
        String uploadFrame = Convert.toStr(MapUtils.getValue(map, "payload.serviceData.UploadFrame"));
        if (StrUtil.isNotBlank(uploadFrame)) {
            return uploadFrame;
        }
        // AEP 电信磁阻水表数据帧
        uploadFrame = Convert.toStr(MapUtils.getValue(map, "payload.APPdata"));
        if (StrUtil.isNotBlank(uploadFrame)) {
            // 解密
            return AppDataDecryptUtils.decrypt(uploadFrame).orElse(StrUtil.EMPTY);
        }
        // AEP xxx 水表数据帧
        uploadFrame = Convert.toStr(MapUtils.getValue(map, "payload.serviceData.WaterCurrentData03"));
        if (StrUtil.isNotBlank(uploadFrame)) {
            // 解密
            return AppDataDecryptUtils.decrypt(uploadFrame).orElse(StrUtil.EMPTY);
        }
        // AEP 自研、低功耗测控终端协议
        uploadFrame = Convert.toStr(MapUtils.getValue(map, "payload.serviceData.extraData"));
        return StrUtil.isNotBlank(uploadFrame) ? uploadFrame : StrUtil.EMPTY;
    }
}
