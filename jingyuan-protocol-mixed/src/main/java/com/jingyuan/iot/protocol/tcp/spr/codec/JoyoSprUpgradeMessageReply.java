package com.jingyuan.iot.protocol.tcp.spr.codec;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;


/**
 * NB升级程序应答帧（适用于TCP直连模式）
 *
 * @author: cc
 * @date: 2024/4/15 15:58
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprUpgradeMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // B12为密码正确与否的标志，其中0xAA代表正确，0xBB代表密码错误
        properties.put("upgradeCheck", "aa".equalsIgnoreCase(ByteBufUtils.readBytesToHex(buf, 1)));
    }
}
