package com.jingyuan.iot.protocol.tcp.hlm.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.model.hlm.afnff.HlmRtuFileBo;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Map;

/**
 * 低功耗 文件传输
 *
 * @author: dr
 * @date: 2024-11-12  15:06
 * @Version: V1.0
 */
@Slf4j
public class HlmAfnFileTransfersMessage implements HlmAfnMessage {


    @Override
    public void write(ByteBuf buf, HlmTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        //todo 升级暂不对接
        HlmRtuFileBo bean = BeanUtil.toBean(properties, HlmRtuFileBo.class);
        File file = bean.getFile();
        IotFeat feature = encodeHolder.getFeature();
        //        01 			// 数据包标识。
        buf.writeByte(0b00000001);
    }
}
