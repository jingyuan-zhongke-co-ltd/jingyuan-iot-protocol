package com.jingyuan.iot.protocol.tcp.mixed;

import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;


/**
 * 京源TCP多协议支持
 *
 * @author: cc
 * @date: 2024/3/27 14:15
 * @Version: V1.0
 */
@Slf4j
public class MixedDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata MIXED_CONFIG = new DefaultConfigMetadata(
        "多协议TCP配置"
        , "多协议TCP配置");

    private final ReactiveStringRedisTemplate redisTemplate;

    public MixedDeviceMessageCodec(ReactiveStringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_MIXED_TCP;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return MixedTransportEnums.decodeByContext(context, redisTemplate);
    }

    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        return MixedTransportEnums.encodeByContext(context, redisTemplate);
    }
}
