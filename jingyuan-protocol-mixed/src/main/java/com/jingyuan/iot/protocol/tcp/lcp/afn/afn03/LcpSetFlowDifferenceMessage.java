package com.jingyuan.iot.protocol.tcp.lcp.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn03.LcpFlowDifference;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 设置流量差量
 *
 * @author: dr
 * @date: 2024-10-31
 * @Version: V1.0
 */
@Slf4j
public class LcpSetFlowDifferenceMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpFlowDifference bean = BeanUtil.toBean(properties, LcpFlowDifference.class);
        byte[] bytes1 = BytesUtils.shortToBytes(bean.getFlowDifference(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(bytes1);
    }
}
