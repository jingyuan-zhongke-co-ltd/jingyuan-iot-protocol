package com.jingyuan.iot.protocol.tcp.cts.codec;

import cn.hutool.core.lang.Assert;
import com.jingyuan.common.protocol.enums.cts.CtsFeatureEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.checksum.ChecksumUtils;
import io.netty.buffer.ByteBuf;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;

/**
 * 京源电信水表消息功能类型枚举
 *
 * @author: dr
 * @date: 2024-08-29
 * @Version: V1.0
 */
@Slf4j
@Getter
public enum CtsFeatureCodecEnums {
    /**
     * 京源电信水表通信协议 消息类型枚举值，具体功能参考实现类
     */
//    属性上报
    DATA_UPLOADS(CtsFeatureEnums.DATA_UPLOADS.getFeatCode(), CtsDataUploadsMessage::new),
    //    阀控应答
    VALVE_REGULATED(CtsFeatureEnums.VALVE_STATE.getFeatCode(), CtsValveRegulatedMessage::new);


    private final String featureType;
    private final Supplier<TcpMessageCodec> forTcp;

    @SuppressWarnings("all")
    CtsFeatureCodecEnums(String featureType,
                         Supplier<? extends TcpMessageCodec> forTcp) {
        this.featureType = featureType;
        this.forTcp = (Supplier) forTcp;
    }

    private static final Map<String, CtsFeatureCodecEnums> VALUES = new HashMap<>();

    static {
        Arrays.stream(values()).forEach(featureEnum -> VALUES.put(featureEnum.featureType, featureEnum));
    }

    public static void read(TcpDecodeHolder decodeHolder, ByteBuf buf, Map<String, Object> properties) {
        IotFeat feature = decodeHolder.getFeature();
        CtsFeatureCodecEnums featureCodecEnum = VALUES.get(feature.getFeatCode());
        TcpMessageCodec messageCodec = featureCodecEnum.forTcp.get();
        messageCodec.read(buf, properties);
    }

    public static void write(TcpEncodeHolder encodeHolder, ByteBuf buf, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        CtsFeatureCodecEnums featureCodecEnum = VALUES.get(feature.getFeatCode());
        Assert.notNull(featureCodecEnum, "发送消息类型不正确");
        TcpMessageCodec messageCodec = featureCodecEnum.forTcp.get();

        messageCodec.write(buf, properties);

        // 计算校验和
        int sum = ChecksumUtils.computeSumSingle(buf.array(), 0, buf.array().length);
        byte lowByte = (byte) (sum & 0xFF);
        buf.writeBytes(new byte[]{lowByte});
        buf.writeBytes(BytesUtils.hexToBytes("16"));
    }
}
