package com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 *  休眠
 *
 * @author: dr
 * @date: 2024-10-24
 * @Version: V1.0
 */
@Slf4j
public class HlmContDormancyMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
    }

}
