package com.jingyuan.iot.protocol.tcp.wac;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.DataDirectionEnums;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.wac.holder.WacTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.wac.wxr.WacMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.Value;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;

/**
 * 京源调节阀
 *
 * @author: cc
 * @date: 2024/12/6 10:59
 * @Version: V1.0
 */
@Slf4j
public class WacDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata JOYO_WAC_CONFIG = new DefaultConfigMetadata(
        "京源调节阀 TCP 配置"
        , "京源调节阀 TCP 配置");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_WAC_TCP;
    }

    private final ReactiveStringRedisTemplate redisTemplate;

    public WacDeviceMessageCodec(ReactiveStringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        return doDecode(context);
    }


    /**
     * 执行解码操作
     *
     * @param context 待解码消息上下文
     * @return org.jetlinks.core.message.DeviceMessage
     */
    private Publisher<? extends Message> doDecode(MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        WacTcpDecodeHolder decodeHolder = WacMessageTypeEnums.read(payload);
        return cachedDecode(decodeHolder, context);
    }

    /**
     * 缓存解码消息
     *
     * @param decodeHolder 解码消息
     * @param context      消息解码上下文
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    public Flux<DeviceMessage> cachedDecode(WacTcpDecodeHolder decodeHolder, MessageDecodeContext context) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        return context.getDevice(message.getDeviceId())
                      // 设置厂商信息
                      .flatMapMany(deviceOperator -> {
                          Map<String, Object> map = new HashMap<>(2);
                          map.put(IotConstants.VENDOR_CODE, decodeHolder.getVendor().getCode());
                          return deviceOperator
                              .setConfigs(map)
                              .thenReturn(message);
                      });
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        return context.getDevice(deviceMessage.getDeviceId())
                      .flatMap(deviceOperator ->
                                   deviceOperator
                                       .getConfig(IotConstants.VENDOR_CODE)
                                       .map(Value::asString)
                                       .map(vendorCode -> {
                                           WacTcpEncodeHolder encodeHolder = WacTcpEncodeHolder.of();
                                           encodeHolder.setVendor(JoyoIotVendor.getByCodeThrowable(vendorCode));
                                           encodeHolder.setFeature(MessageHelper.getHeadFeatThrow(deviceMessage));
                                           encodeHolder.setDir(DataDirectionEnums.down);
                                           encodeHolder.setDeviceMessage(deviceMessage);
                                           return encodeHolder;
                                       })
                                       .flatMap(this::doEncode));
    }


    /**
     * 执行编码操作
     *
     * @param encodeHolder 待编码消息
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(WacTcpEncodeHolder encodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        WacMessageTypeEnums.write(encodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
