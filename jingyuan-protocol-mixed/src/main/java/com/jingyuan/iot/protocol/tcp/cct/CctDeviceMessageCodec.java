package com.jingyuan.iot.protocol.tcp.cct;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.constant.IotCctConstants;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.JoyoIotTransport;
import com.jingyuan.common.protocol.enums.cct.CctAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.cct.CctPrmEnums;
import com.jingyuan.common.protocol.enums.cct.CctSeqConEnums;
import com.jingyuan.common.protocol.enums.cct.CctSlaveFeatureEnums;
import com.jingyuan.common.protocol.enums.cct.fn.CctAfnConfirmRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.wrx.CctMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.Value;
import org.jetlinks.core.device.DeviceOperator;
import org.jetlinks.core.message.ChildDeviceMessageReply;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.*;
import org.jetlinks.core.message.property.WritePropertyMessage;
import org.jetlinks.core.message.property.WritePropertyMessageReply;
import org.jetlinks.core.metadata.DefaultConfigMetadata;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.jingyuan.common.protocol.constant.IotConstants.CACHED_COMMANDS;


/**
 * 京源集采器，采集器通讯协议
 *
 * @author: cc
 * @date: 2024/6/17 10:04
 * @Version: V1.0
 */
@Slf4j
public class CctDeviceMessageCodec implements DeviceMessageCodec {

    public static final DefaultConfigMetadata JOYO_CCT_CONFIG = new DefaultConfigMetadata(
        "京源集采器TCP配置"
        , "京源集采器TCP配置信息");

    @Override
    public Transport getSupportTransport() {
        return JoyoIotTransport.JOYO_CCT_TCP;
    }

    /**
     * 消息解码
     *
     * @param context 待解码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    @NonNull
    @Override
    public Publisher<? extends Message> decode(@NonNull MessageDecodeContext context) {
        ByteBuf payload = context.getMessage().getPayload();
        CctTcpDecodeHolder decodeHolder = CctMessageTypeEnums.read(payload);
        DeviceMessage deviceMessage = decodeHolder.getDeviceMessage();
        String vendorCode = MessageHelper.getHeadVendorThrow(deviceMessage);

        return context.getDevice(deviceMessage.getDeviceId())
                      // 父设备信息缓存
                      .flatMap(deviceOperator -> doParentCache(deviceOperator, deviceMessage, vendorCode))
                      .flatMap(message ->
                                   // 判断消息是否需要确认回复
                                   conWrite(context, decodeHolder)
                                       .collectList()
                                       // 缓存的指令写出
                                       .flatMap(list -> cachedWrite(context, deviceMessage)
                                           .collectList()
                                           .flatMap(messages -> {
                                               messages.stream()
                                                       .filter(msg -> msg != deviceMessage)
                                                       .forEach(list::add);
                                               return Mono.just(list);
                                           })))
                      // 子设备信息缓存
                      .flatMapMany(deviceMessages -> childCache(context, decodeHolder, deviceMessages, vendorCode));
    }

    /**
     * 父设备信息缓存
     *
     * @param message        设备消息
     * @param deviceOperator 设备操作对象
     * @param vendorCode     厂商编号
     * @return reactor.core.publisher.Mono<org.jetlinks.core.message.DeviceMessage>
     */
    private Mono<DeviceMessage> doParentCache(DeviceOperator deviceOperator, DeviceMessage message, String vendorCode) {
        HashMap<String, Object> cacheMap = new HashMap<>(4);
        // 缓存厂商信息
        cacheMap.put(IotConstants.VENDOR_CODE, vendorCode);

        // 缓存行政区划编码
        if (message instanceof WritePropertyMessageReply) {
            Map<String, Object> properties = ((WritePropertyMessageReply) message).getProperties();
            if (CollUtil.isNotEmpty(properties) && properties.containsKey(IotCctConstants.ADCODE)) {
                cacheMap.put(IotCctConstants.ADCODE, Convert.toStr(properties.get(IotCctConstants.ADCODE)));
            }
        }
        return deviceOperator
            .setConfigs(cacheMap)
            .thenReturn(message);
    }

    /**
     * 子设备信息缓存
     *
     * @param context      消息上下文
     * @param decodeHolder 消息解析包装类
     * @param messages     设备消息
     * @param vendorCode   厂商编号
     * @return reactor.core.publisher.Flux<? extends org.jetlinks.core.message.Message>
     */
    private Flux<? extends Message> childCache(MessageDecodeContext context,
                                               CctTcpDecodeHolder decodeHolder,
                                               List<DeviceMessage> messages,
                                               String vendorCode) {
        List<DeviceMessage> childDeviceMessages = decodeHolder.getChildDeviceMessages();
        if (CollUtil.isEmpty(childDeviceMessages)) {
            return Flux.fromIterable(messages);
        }
        messages.addAll(childDeviceMessages);

        return Flux.fromIterable(messages)
                   .flatMap(message -> {
                       if (message instanceof ChildDeviceMessageReply) {
                           return Flux
                               .just((ChildDeviceMessageReply) message)
                               .flatMap(messageReply -> {
                                   Message childDeviceMessage = messageReply.getChildDeviceMessage();
                                   if (childDeviceMessage instanceof WritePropertyMessageReply) {
                                       return doChildCache(context, (WritePropertyMessageReply) childDeviceMessage, vendorCode);
                                   }
                                   return Flux.just(message);
                               });
                       }
                       return Flux.just(message);
                   });
    }

    /**
     * 子设备信息缓存
     *
     * @param context      消息上下文
     * @param messageReply 子设备回复消息
     * @param vendorCode   厂商编号
     * @return reactor.core.publisher.Flux<? extends org.jetlinks.core.message.Message>
     */
    private Flux<? extends Message> doChildCache(MessageDecodeContext context, WritePropertyMessageReply messageReply, String vendorCode) {
        Map<String, Object> properties = messageReply.getProperties();
        if (CollUtil.isEmpty(properties)) {
            return Flux.just(messageReply);
        }
        return context.getDevice(messageReply.getDeviceId())
                      .flatMapMany(deviceOperator -> deviceOperator
                          .setConfig(IotConstants.VENDOR_CODE, vendorCode)
                          .thenReturn(messageReply));
    }


    /**
     * 确认消息写出
     *
     * @param context      消息上下文
     * @param decodeHolder 消息解析包装类
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> conWrite(MessageDecodeContext context, CctTcpDecodeHolder decodeHolder) {
        DeviceMessage message = decodeHolder.getDeviceMessage();
        CctSeqConEnums seqCon = decodeHolder.getSeqCon();
        if (CctSeqConEnums.no_confirm.equals(seqCon)) {
            return Flux.just(message);
        }

        CctTcpEncodeHolder messageEncodeHolder = CctTcpEncodeHolder.of();
        String deviceId = message.getDeviceId();
        messageEncodeHolder.setDeviceId(deviceId);
        messageEncodeHolder.setAfn(CctAfnFeatureEnums.confirm);
        messageEncodeHolder.setAdcode(decodeHolder.getAdcode());
        messageEncodeHolder.setL(CctAfnConfirmRwEnums.confirm.getLength());
        messageEncodeHolder.setFeature(CctAfnConfirmRwEnums.confirm);
        messageEncodeHolder.setPrm(CctPrmEnums.slave_station);
        messageEncodeHolder.setPrmFeature(CctSlaveFeatureEnums.confirm);

        WritePropertyMessage writePropertyMessage = IotMessageType.writePmInstance(deviceId);
        // 添加厂商信息
        IotFeat feature = CctAfnConfirmRwEnums.confirm;
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(writePropertyMessage);

        messageEncodeHolder.setDeviceMessage(writePropertyMessage);
        return doSendAfterDecode(context, messageEncodeHolder, message);
    }


    /**
     * 缓存的消息写出
     *
     * @param context 消息上下文
     * @param message 设备消息
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    @SuppressWarnings("unchecked")
    private Flux<DeviceMessage> cachedWrite(MessageDecodeContext context, DeviceMessage message) {
        return context
            .getDevice(message.getDeviceId())
            .flatMapMany(deviceOperator ->
                             deviceOperator
                                 .getConfig(CACHED_COMMANDS)
                                 .switchIfEmpty(Mono.defer(() -> Mono.just(Value.simple(new HashMap<>()))))
                                 .map(val -> val.as(Map.class))
                                 .flatMapMany(map -> {
                                     if (CollUtil.isEmpty(map)) {
                                         return Flux.just(message);
                                     }
                                     Map<String, Map<String, DeviceMessage>> cachedMap = (Map<String, Map<String, DeviceMessage>>) map;
                                     return Flux.fromIterable(cachedMap.keySet())
                                                .map(cachedMap::get)
                                                .flatMap(cached -> {
                                                    if (CollUtil.isEmpty(cached)) {
                                                        return Flux.just(message);
                                                    }
                                                    return Flux.fromIterable(cached.keySet())
                                                               .flatMap(funcId -> {
                                                                   DeviceMessage deviceMessage = cached.get(funcId);
                                                                   CctTcpEncodeHolder messageEncodeHolder = CctTcpEncodeHolder.of();
                                                                   messageEncodeHolder.setDeviceMessage(cached.get(funcId));
                                                                   CctMessageTypeEnums.convertHeaderToHolder(deviceMessage, messageEncodeHolder);
                                                                   return doSendAfterDecode(context, messageEncodeHolder, message);
                                                               })
                                                               .delayElements(Duration.ofMillis(100))
                                                               .switchIfEmpty(Flux.defer(() -> Flux.just(message)));
                                                });
                                 })
                                 .collectList()
                                 .flatMapMany(messages -> {
                                     if (messages.size() > 1) {
                                         return deviceOperator.setConfig(CACHED_COMMANDS, new HashMap<>())
                                                              .thenMany(Flux.fromIterable(messages));
                                     } else {
                                         return Flux.fromIterable(messages);
                                     }
                                 })
            );
    }


    /**
     * 解码后消息发送
     *
     * @param context       消息上下文
     * @param encodeHolder  消息编码包装类
     * @param decodeMessage 解码后消息
     * @return reactor.core.publisher.Flux<org.jetlinks.core.message.DeviceMessage>
     */
    private Flux<DeviceMessage> doSendAfterDecode(MessageDecodeContext context, CctTcpEncodeHolder encodeHolder, DeviceMessage decodeMessage) {
        return doEncode(encodeHolder)
            .flatMap(encodedMessage -> ((FromDeviceMessageContext) context).getSession().send(encodedMessage))
            .flatMapMany(aBoolean -> {
                if (aBoolean) {
                    return Flux.just(decodeMessage, encodeHolder.getDeviceMessage());
                } else {
                    return Flux.just(decodeMessage);
                }
            });
    }


    /**
     * 消息编码
     *
     * @param context 待编码消息上下文
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.codec.EncodedMessage>
     */
    @NonNull
    @Override
    public Publisher<? extends EncodedMessage> encode(@NonNull MessageEncodeContext context) {
        DeviceMessage deviceMessage = ((DeviceMessage) context.getMessage());
        CctTcpEncodeHolder messageEncodeHolder = CctTcpEncodeHolder.of();
        messageEncodeHolder.setDeviceMessage(deviceMessage);

        // DeviceMessage 数据转换到 CctMessageEncodeHolder
        CctMessageTypeEnums.convertHeaderToHolder(deviceMessage, messageEncodeHolder);
        return doEncode(messageEncodeHolder);
    }


    /**
     * 执行编码操作
     *
     * @param messageEncodeHolder 消息编码包装类
     * @return org.jetlinks.core.message.codec.EncodedMessage
     */
    private Mono<EncodedMessage> doEncode(CctTcpEncodeHolder messageEncodeHolder) {
        ByteBuf buffer = Unpooled.buffer();
        CctMessageTypeEnums.write(messageEncodeHolder, buffer);
        return Mono.just(EncodedMessage.simple(buffer));
    }
}
