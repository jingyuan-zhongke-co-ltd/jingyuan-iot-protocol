package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710ReadMeasureDateBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 读开启/结束计量日期
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class Wac710ReadMeasureDate implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 开启月日
        String beginDay = BytesUtils.byteToHex(buf.readByte());
        String beginMonth = BytesUtils.byteToHex(buf.readByte());
        properties.put("beginMeasureDate", beginMonth + "月" + beginDay + "日");
        // 结束月日
        String endDay = BytesUtils.byteToHex(buf.readByte());
        String endMonth = BytesUtils.byteToHex(buf.readByte());
        properties.put("endMeasureDate", endMonth + "月" + endDay + "日");
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710ReadMeasureDateBo measureDateBo = BeanUtil.toBean(properties, Wac710ReadMeasureDateBo.class);
        if (measureDateBo.isBroadcast()) {
            buf.setBytes(5, new byte[]{(byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA, (byte) 0xAA});
        }
    }
}
