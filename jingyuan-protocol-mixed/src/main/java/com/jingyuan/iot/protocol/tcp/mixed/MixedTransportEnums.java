package com.jingyuan.iot.protocol.tcp.mixed;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.MessageHelper;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.cct.CctDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.wrx.CctMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.cts.CtsDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.cts.wxr.CtsMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.cz.CzDzpDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.cz.wrx.CzDzpMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.hlm.HlmDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.hlm.holder.HlmTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.hlm.wrx.HlmMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.lcp.LcpDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.wrx.LcpMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.nom.JzNomDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.nom.wrx.JzNomMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.rs.RsDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.rs.wrx.RsMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.runda.RundaDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.runda.wrx.RundaMessageTypeEnums;
import com.jingyuan.iot.protocol.tcp.spr.SprChildVendorEnums;
import com.jingyuan.iot.protocol.tcp.spr.SprDeviceMessageCodec;
import com.jingyuan.iot.protocol.tcp.spr.holder.SprTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.spr.wrx.SprMessageTypeEnums;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.Value;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.Message;
import org.jetlinks.core.message.codec.DeviceMessageCodec;
import org.jetlinks.core.message.codec.EncodedMessage;
import org.jetlinks.core.message.codec.MessageDecodeContext;
import org.jetlinks.core.message.codec.MessageEncodeContext;
import org.reactivestreams.Publisher;
import org.springframework.data.redis.core.ReactiveStringRedisTemplate;
import reactor.core.publisher.Mono;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;


/**
 * 传输协议类型枚举
 *
 * @author: cc
 * @date: 2024/7/29 16:47
 * @Version: V1.0
 */
@Slf4j
@AllArgsConstructor
public enum MixedTransportEnums {

    JOYO_SPR_TCP(JoyoIotVendor.JOYO_SPR.getCode(),
                 SprDeviceMessageCodec::new,
                 SprMessageTypeEnums::read,
                 (encodeHolder, buf) -> SprMessageTypeEnums.write((SprTcpEncodeHolder) encodeHolder, buf)),
    JOYO_SPR_THIRD_TCP(JoyoIotVendor.JOYO_SPR_THIRD.getCode(),
                       SprDeviceMessageCodec::new,
                       SprMessageTypeEnums::read,
                       (encodeHolder, buf) -> SprMessageTypeEnums.write((SprTcpEncodeHolder) encodeHolder, buf)),
    JOYO_CCT_TCP(JoyoIotVendor.JOYO_CCT.getCode(),
                 redisTemplate -> new CctDeviceMessageCodec(),
                 CctMessageTypeEnums::read,
                 (encodeHolder, buf) -> CctMessageTypeEnums.write((CctTcpEncodeHolder) encodeHolder, buf)),
    HLM_UC_TCP(JoyoIotVendor.HLM_US.getCode(),
               redisTemplate -> new HlmDeviceMessageCodec(),
               HlmMessageTypeEnums::read,
               (encodeHolder, buf) -> HlmMessageTypeEnums.write((HlmTcpEncodeHolder) encodeHolder, buf)),
    JOYO_CTS_TCP(JoyoIotVendor.JOYO_CTS.getCode(),
                 redisTemplate -> new CtsDeviceMessageCodec(),
                 CtsMessageTypeEnums::read,
                 CtsMessageTypeEnums::write),
    RUNDA_WP_TCP(JoyoIotVendor.RUNDA_WP.getCode(),
                 RundaDeviceMessageCodec::new,
                 RundaMessageTypeEnums::read,
                 RundaMessageTypeEnums::write),
    JZ_NOM(JoyoIotVendor.JZ_NOM.getCode(),
           redisTemplate -> new JzNomDeviceMessageCodec(),
           JzNomMessageTypeEnums::read,
           JzNomMessageTypeEnums::write),
    CZ_DZP(JoyoIotVendor.CZ_DZP.getCode(),
           redisTemplate -> new CzDzpDeviceMessageCodec(),
           CzDzpMessageTypeEnums::read,
           CzDzpMessageTypeEnums::write),
    JOYO_LCP(JoyoIotVendor.JOYO_LCP.getCode(),
             redisTemplate -> new LcpDeviceMessageCodec(),
             LcpMessageTypeEnums::read,
             (encodeHolder, buf) -> LcpMessageTypeEnums.write((LcpTcpEncodeHolder) encodeHolder, buf)),
    JOYO_RS(JoyoIotVendor.JOYO_RS.getCode(),
            redisTemplate -> new RsDeviceMessageCodec(),
            RsMessageTypeEnums::read,
            (encodeHolder, buf) -> RsMessageTypeEnums.write((TcpEncodeHolder) encodeHolder, buf)),
    ;

    /**
     * 厂商编号
     */
    private final String vendorCode;
    /**
     * 编解码操作, 依托于 DeviceMessageCodec
     */
    private final Function<ReactiveStringRedisTemplate, DeviceMessageCodec> codec;
    /**
     * 解码操作，对传递过来的的字符串类型二进制数据解析
     */
    private final Function<ByteBuf, TcpDecodeHolder> doDecode;
    /**
     * 编码操作，对传递过来的的字符串类型二进制数据解析
     */
    private final BiConsumer<TcpEncodeHolder, ByteBuf> doEncode;

    /**
     * 厂商与编解码操作业务对应关系
     */
    private static final Map<String, MixedTransportEnums> maps = new HashMap<>(4);

    static {
        for (MixedTransportEnums value : values()) {
            maps.put(value.vendorCode, value);
        }
    }

    /**
     * 根据消息上下文解码
     *
     * @param context 写入数据
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static Publisher<? extends Message> decodeByContext(@NonNull MessageDecodeContext context, ReactiveStringRedisTemplate redisTemplate) {
        String rawDataHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(context.getMessage().getPayload()));
        MixedTransportEnums transportEnum = getByRawDataHex(rawDataHex);
        return transportEnum.codec.apply(redisTemplate).decode(context);
    }

    /**
     * 根据 byteBuf 解码
     *
     * @param hexStr 原始数据帧
     * @return org.reactivestreams.Publisher<? extends org.jetlinks.core.message.Message>
     */
    public static Mono<DeviceMessage> decodeByHexStr(String hexStr, String vendorCode) {
        // 根据厂商编号或原始数据帧获取实际编解码处理类
        MixedTransportEnums transportEnum = StrUtil.isBlank(vendorCode) ? getByRawDataHex(hexStr) : maps.get(vendorCode);
        ByteBuf byteBuf = Unpooled.buffer();
        byteBuf.writeBytes(BytesUtils.hexToBytes(hexStr));
        TcpDecodeHolder decodeHolder = transportEnum.doDecode.apply(byteBuf);
        return Mono.just(decodeHolder.getDeviceMessage());
    }

    /**
     * 根据消息上下文编码
     *
     * @param context 写出数据
     * @return io.netty.buffer.ByteBuf
     */
    public static Publisher<? extends EncodedMessage> encodeByContext(@NonNull MessageEncodeContext context, ReactiveStringRedisTemplate redisTemplate) {
        return getByEncodeContext(context)
            .switchIfEmpty(Mono.error(() -> new UnsupportedOperationException("不支持的传输协议类型")))
            .flatMapMany(transportEnum -> transportEnum.codec.apply(redisTemplate).encode(context));
    }


    /**
     * 根据设备消息编码
     *
     * @param deviceMessage 设备消息
     * @return io.netty.buffer.ByteBuf
     */
    public static ByteBuf encodeByMessage(@NonNull DeviceMessage deviceMessage) {
        String vendorCode = MessageHelper.getHeadVendorThrow(deviceMessage);
        MixedTransportEnums transportEnum = getByVendorCode(vendorCode);
        ByteBuf byteBuf = Unpooled.buffer();

        TcpEncodeHolder encodeHolder;
        if (JoyoIotVendor.JOYO_SPR_THIRD.isSame(vendorCode) || JoyoIotVendor.JOYO_SPR.isSame(vendorCode)) {
            SprTcpEncodeHolder sprTcpEncodeHolder = SprTcpEncodeHolder.of();
            sprTcpEncodeHolder.setChildVendorEnum(SprChildVendorEnums.getByCode(vendorCode));
            encodeHolder = sprTcpEncodeHolder;
        } else {
            encodeHolder = TcpEncodeHolder.of();
        }
        encodeHolder.setFeature(MessageHelper.getHeadFeatThrow(deviceMessage));
        encodeHolder.setDeviceMessage(deviceMessage);

        transportEnum.doEncode.accept(encodeHolder, byteBuf);
        return byteBuf;
    }


    /**
     * 根据编码上下文对象获取
     *
     * @param context 编码上下文
     * @return reactor.core.publisher.Mono<com.jingyuan.iot.protocol.tcp.mixed.MixedTransportEnums>
     */
    private static Mono<MixedTransportEnums> getByEncodeContext(MessageEncodeContext context) {
        DeviceMessage message = ((DeviceMessage) context.getMessage());
        String vendorCode = MessageHelper.getHeadVendor(message);
        return Mono.just(vendorCode)
                   .switchIfEmpty(Mono.just(StrUtil.EMPTY))
                   .flatMap(code -> {
                       if (StrUtil.isBlank(vendorCode)) {
                           return context
                               .getDevice(message.getDeviceId())
                               .flatMap(operator -> operator.getConfig(IotConstants.VENDOR_CODE))
                               .switchIfEmpty(Mono.error(() -> new UnsupportedOperationException("不支持的传输协议类型")))
                               .map(Value::asString)
                               .map(MixedTransportEnums::getByVendorCode);
                       } else {
                           return Mono.just(getByVendorCode(code));
                       }
                   });
    }

    /**
     * 根据原始数据帧获取
     *
     * @param rawDataHex 原始数据帧
     * @return com.jingyuan.iot.protocol.tcp.mixed.MixedTransportEnums
     */
    private static MixedTransportEnums getByRawDataHex(String rawDataHex) {
        rawDataHex = rawDataHex.toLowerCase();
        JoyoIotVendor vendor = JoyoIotVendor.getByRawData(rawDataHex.toLowerCase());
        MixedTransportEnums transportEnum = maps.get(vendor.getCode());
        Assert.notNull(transportEnum, "不支持的厂商类型，原始数据帧：【" + rawDataHex + "】");
        return transportEnum;
    }

    /**
     * 根据厂商编号获取
     *
     * @param vendorCode 厂商编号
     * @return com.jingyuan.iot.protocol.tcp.mixed.MixedTransportEnums
     */
    private static MixedTransportEnums getByVendorCode(String vendorCode) {
        JoyoIotVendor vendor = JoyoIotVendor.getByCodeThrowable(vendorCode);
        MixedTransportEnums transportEnum = maps.get(vendor.getCode());
        Assert.notNull(transportEnum, "不支持的传输协议类型");
        return transportEnum;
    }

}
