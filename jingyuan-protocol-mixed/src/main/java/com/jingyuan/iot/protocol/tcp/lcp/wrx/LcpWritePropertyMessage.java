package com.jingyuan.iot.protocol.tcp.lcp.wrx;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.lcp.codec.LcpAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.WritePropertyMessage;

/**
 * 京源测控终端协议 - 数据上报
 *
 * @author: dr
 * @date: 2024-10-25
 * @Version: V1.0
 */
@Slf4j
public class LcpWritePropertyMessage implements TcpMessageAdapter<WritePropertyMessage> {

    private WritePropertyMessage message;


    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
        LcpAfnFeatCodecEnums.write(buf, (LcpTcpEncodeHolder) encodeHolder, message.getProperties());
    }


    @Override
    public void setMessage(WritePropertyMessage message) {
        this.message = message;
    }


    @Override
    public WritePropertyMessage getMessage() {
        return message;
    }
}
