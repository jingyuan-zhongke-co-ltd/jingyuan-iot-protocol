package com.jingyuan.iot.protocol.tcp.rs.codec;

import com.jingyuan.common.protocol.enums.rs.RsValveReplyEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控阀应答
 *
 * @author: 李立强
 * @date: 2024/12/26 16:24
 * @Version: V1.0
 */
@Slf4j
public class RsValveReplyMessage implements TcpMessageCodec {
    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        RsValveReplyEnums valveState = RsValveReplyEnums.getByByte(buf.getByte(14));
        properties.put("valveState", valveState.getCode());
    }
}
