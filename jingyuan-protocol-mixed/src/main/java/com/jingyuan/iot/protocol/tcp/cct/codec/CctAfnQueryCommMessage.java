package com.jingyuan.iot.protocol.tcp.cct.codec;

import com.jingyuan.common.protocol.enums.cct.fn.CctAfnQueryCommRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询参数
 *
 * @author: cc
 * @date: 2024/6/19 9:29
 * @Version: V1.0
 */
@Slf4j
public class CctAfnQueryCommMessage implements CctAfnMessage {

    @Override
    public void read(ByteBuf buf, CctTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String afn = String.valueOf(buf.readByte());
        CctAfnQueryCommRwEnums queryCommRwEnum = CctAfnQueryCommRwEnums.getByFeatCode(afn);
        decodeHolder.setFeature(queryCommRwEnum);

        FeatCodecs.read(queryCommRwEnum, buf, properties);
    }

    @Override
    public void write(ByteBuf buf, CctTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof CctAfnQueryCommRwEnums;
        FeatCodecs.write(feature, buf, properties);
    }
}
