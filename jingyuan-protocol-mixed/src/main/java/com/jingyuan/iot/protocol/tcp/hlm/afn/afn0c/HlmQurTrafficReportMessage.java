package com.jingyuan.iot.protocol.tcp.hlm.afn.afn0c;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

import static com.jingyuan.iot.protocol.tcp.hlm.afn.afn0a.HlmUltAfnDataUploadsMessage.unitNumber;


/**
 * 休眠
 *
 * @author: dr
 * @date: 2024-10-24
 * @Version: V1.0
 */
@Slf4j
public class HlmQurTrafficReportMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
    }

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        //        数据包标识
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
//        开启或关闭流量异常上报	BIN	0~1	1	0x00关闭，0x01开启。
        properties.put("trafficReportExSta", BytesUtils.byteToHex(buf.readByte()));

//        监控时段小于多少流量L值上报	BIN		2	x0.1表示实际监控L值。
        byte[] bytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes);
        String monitorLValue = unitNumber(new BigDecimal(BytesUtils.bytesToShort(bytes)), "2D");
        properties.put("monitorLValue", monitorLValue);

//        监控流量时间点1234	BCD		2	时分。
        for (int i = 1; i < 5; i++) {
            properties.put("monitoringTimePoint" + i, BytesUtils.byteToHex(buf.readByte()) + ":" + BytesUtils.byteToHex(buf.readByte()));
        }
    }
}
