package com.jingyuan.iot.protocol.tcp.cz.wrx;

import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * 沧州定制 - 属性上报
 *
 * @author: 李立强
 * @date: 2024/10/08 15:53
 * @Version: V1.0
 */
public class CzDzpReportPropertyMessage implements TcpMessageAdapter<ReportPropertyMessage> {
    private ReportPropertyMessage message;

    /**
     * 设置消息
     *
     * @param message 消息
     */
    @Override
    public void setMessage(ReportPropertyMessage message) {
        this.message = message;
    }

    /**
     * 获取消息
     *
     * @return T 消息
     */
    @Override
    public ReportPropertyMessage getMessage() {
        return message;
    }
    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new ReportPropertyMessage();
        message.setDeviceId(decodeHolder.getDeviceId());
        Map<String, Object> map = initReadMap(buf, decodeHolder);
        FeatCodecs.read(decodeHolder, buf, map);

        message.setProperties(map);
    }

    public static Map<String, Object> initReadMap(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.CZ_DZP.getCode());
        return map;
    }



}
