package com.jingyuan.iot.protocol.tcp.lcp.codec;

import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnTwoRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 请求2类数据（历史数据）
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public class LcpAfnHistoryDataMessage implements LcpAfnMessage {

    @Override
    public void read(ByteBuf buf, LcpTcpDecodeHolder decodeHolder, Map<String, Object> properties) {
        String afn = String.valueOf(buf.readByte());
        LcpAfnTwoRwEnums historyRwEnum = LcpAfnTwoRwEnums.getByFeatCode(afn);
        decodeHolder.setFeature(historyRwEnum);

        FeatCodecs.read(historyRwEnum, buf, properties);
    }

    @Override
    public void write(ByteBuf buf, LcpTcpEncodeHolder encodeHolder, Map<String, Object> properties) {
        IotFeat feature = encodeHolder.getFeature();
        assert feature instanceof LcpAfnHistoryDataMessage;

        FeatCodecs.write(feature, buf, properties);
    }
}
