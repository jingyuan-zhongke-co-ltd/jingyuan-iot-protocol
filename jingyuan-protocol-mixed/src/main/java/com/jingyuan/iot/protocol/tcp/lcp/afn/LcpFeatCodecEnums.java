package com.jingyuan.iot.protocol.tcp.lcp.afn;

import com.jingyuan.common.protocol.enums.lcp.fn.*;
import com.jingyuan.common.protocol.feature.DefaultFeat;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn03.*;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn04.LcpContContTimingMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn04.LcpContValveMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a.LcpQueryCollectionCycleMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a.LcpQueryFlowDifferenceMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a.LcpQueryPressureLimitsMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a.LcpQueryPulseRatioMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b.LcpIntegratedDataMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b.LcpTerminalClockMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0b.LcpTerminalVersionMessage;
import com.jingyuan.iot.protocol.tcp.lcp.afn.afn0c.LcpHistoricalDataMessage;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * 设置命令
 *
 * @author: cc
 * @date: 2024/6/21 11:03
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum LcpFeatCodecEnums implements FeatCodec {
    /**
     * ===================================      查询命令      =======================================
     * 查询采集周期和上报周期
     */
    query_data_collection_cycle(LcpAfnQueryCommRwEnums.query_data_collection_cycle, new LcpQueryCollectionCycleMessage()),
    /**
     * 查询压力上下限
     */
    query_pressure_limits(LcpAfnQueryCommRwEnums.query_pressure_limits, new LcpQueryPressureLimitsMessage()),
    /**
     * 查询流量差值上报
     */
    query_flow_difference(LcpAfnQueryCommRwEnums.query_flow_difference, new LcpQueryFlowDifferenceMessage()),
    /**
     * 查询脉冲变比上报
     */
    query_pulse_ratio(LcpAfnQueryCommRwEnums.query_pulse_ratio, new LcpQueryPulseRatioMessage()),

    /**
     * ===================================      请求1类数据(实时数据)命令      =======================================
     * 终端版本信息
     */
    terminal_version_info(LcpAfnOneRwEnums.terminal_version_info, new LcpTerminalVersionMessage()),
    /**
     * 终端日历时钟
     */
    terminal_clock(LcpAfnOneRwEnums.terminal_clock, new LcpTerminalClockMessage()),

    /**
     * 一体数据
     */
    integrated_data(LcpAfnOneRwEnums.integrated_data, new LcpIntegratedDataMessage()),


    /**
     * ===================================      请求2类数据(历史数据)命令      =======================================
     * 一体历史数据
     */
    query_integrated_historical_data(LcpAfnTwoRwEnums.query_integrated_historical_data, new LcpHistoricalDataMessage()),


    /**
     * ===================================      设置命令      =======================================
     * 定时采集周期，定时上报周期
     */
    set_data_collection_cycle(LcpAfnSetCommWriteEnums.set_data_collection_cycle, new LcpDataCollectionMessage()),
    /**
     * 设定大表底数
     */
    set_large_meter_base(LcpAfnSetCommWriteEnums.set_large_meter_base, new LcpSetLargeMeterReadingMessage()),
    /**
     * 设置压力上下限值
     */
    set_pressure_limits(LcpAfnSetCommWriteEnums.set_pressure_limits, new LcpSetPressureLimitsMessage()),
    /**
     * 设置流量差量
     */
    set_flow_difference(LcpAfnSetCommWriteEnums.set_flow_difference, new LcpSetFlowDifferenceMessage()),
    /**
     * 设置脉冲变比
     */
    set_pulse_ratio(LcpAfnSetCommWriteEnums.set_pulse_ratio, new LcpSetPulseRatioMessage()),


    /**
     * ===================================      控制命令      =======================================
     * 对时命令
     */
    cont_timing(LcpAfnContCommWriteEnums.cont_timing, new LcpContContTimingMessage()),
    /**
     * 开关阀命令
     */
    cont_valve_close(LcpAfnContCommWriteEnums.cont_valve_close, new LcpContValveMessage()),
    ;

    private final IotFeat feat;

    private final TcpMessageCodec codec;
}
