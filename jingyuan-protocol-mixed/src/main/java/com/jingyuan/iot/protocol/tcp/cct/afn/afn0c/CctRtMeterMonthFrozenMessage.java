package com.jingyuan.iot.protocol.tcp.cct.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.codec.BCD;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.text.CharPool;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.CctMeterDataVo;
import com.jingyuan.common.protocol.model.cct.afn0c.CctRtMeterMonthFrozen;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.json.JsonUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Map;

/**
 * 读计量表内月冻结数据
 *
 * @author: dr
 * @date: 2024-12-24
 * @Version: V1.0
 */
@Slf4j
public class CctRtMeterMonthFrozenMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        CctMeterDataVo cctMeterDataVo = new CctMeterDataVo();
        // 采集器号
        byte[] coolAddr = ByteBufUtils.readBytes(buf, 6);
        cctMeterDataVo.setCoolAddr(Integer.valueOf(BytesUtils.binary(10, coolAddr[5])));
        // 计量表序号
        cctMeterDataVo.setMeterSerialNum(Convert.toInt(buf.readByte()));
        // 计量表通信号
        byte[] nbAddr = ByteBufUtils.readBytes(buf, 6);
        BytesUtils.bytesReverse(nbAddr);
        cctMeterDataVo.setMeterAddr(BCD.bcdToStr(nbAddr));
//        起始序号
        cctMeterDataVo.setMonthsAgoNow(Convert.toInt(buf.readByte()));
//        表底数
        byte[] bytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(bytes);
        String res = BCD.bcdToStr(bytes);
        res = StrUtil.subPre(res, res.length() - 2) + CharPool.DOT + StrUtil.subSuf(res, res.length() - 2);
        cctMeterDataVo.setReading(Double.parseDouble(res));
//        剩余金额
        double amount = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
        cctMeterDataVo.setAmount(amount);

        // 中继深度
        String relayDepth = getRelayDepth(buf);
        cctMeterDataVo.setRelayDepth(relayDepth);
        // 中继路径
        String substring = getRelayPath(buf);
        cctMeterDataVo.setRelayPath(substring);
        properties.put("meterData", JsonUtils.toJsonString(Collections.singletonList(cctMeterDataVo)));

    }

    /**
     * 中继深度
     * @param buf
     * @return
     */
    static String getRelayDepth(ByteBuf buf) {
        String relayDepth = BytesUtils.byteToHex(buf.readByte());
        if (relayDepth.equals("直接通讯")) {
            relayDepth = "";
        } else if (relayDepth.equals("0b")) {
            relayDepth = "尚未搜寻路径";
        } else if (relayDepth.equals("ee")) {
            relayDepth = "未搜到路径";
        } else {
            relayDepth = "中继深度:" + Convert.toInt(relayDepth);
        }
        return relayDepth;
    }

    /**
     * 中继路径
     * @param buf
     * @return
     */
    static String getRelayPath(ByteBuf buf) {
        byte[] bytes1 = ByteBufUtils.readBytes(buf, 10);
        BytesUtils.bytesReverse(bytes1);
        String s = BytesUtils.bytesToHex(bytes1);
        StringBuilder stringBuilder = new StringBuilder();
        for (int j = 0; j < 5; j = j + 2) {
            String s1 = s.substring(j, j + 2);
            if ("ee".equals(s1)) break;
            stringBuilder.append(s1).append("->");
        }
        String substring = null;
        if (stringBuilder.length() > 0) {
             substring = stringBuilder.substring(0, stringBuilder.length() - 2);
        }
        return substring;
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctRtMeterMonthFrozen bean = BeanUtil.toBean(properties, CctRtMeterMonthFrozen.class);
        // 采集器通信地址
        buf.writeBytes(new byte[]{bean.getCoolAddr().byteValue(), 0x00, 0x00, 0x00, 0x00, 0x00});
        // 计量表序号
        buf.writeByte(bean.getMeterSerialNum());
        // 计量表通信地址
        byte[] nbAddrBytes = BCD.strToBcd(bean.getMeterAddr());
        Assert.isTrue(ArrayUtil.isNotEmpty(nbAddrBytes) && nbAddrBytes.length == 6, "计量表地址错误");
        BytesUtils.bytesReverse(nbAddrBytes);
        buf.writeBytes(nbAddrBytes);
//        起始序号
//            结束序号
        buf.writeByte(bean.getMonthsAgoNow());
        buf.writeByte(bean.getMonthsAgoNow());
        // 中继深度
        buf.writeByte(bean.getRelayDepth());
        // 中继路径
        byte[] relayPathBytes = BytesUtils.hexToBytes(bean.getRelayPath());
        Assert.isTrue(ArrayUtil.isNotEmpty(relayPathBytes) && relayPathBytes.length == 10, "中继路径错误");
        BytesUtils.bytesReverse(relayPathBytes);
        buf.writeBytes(relayPathBytes);

    }
}
