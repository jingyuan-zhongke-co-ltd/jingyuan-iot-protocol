package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0a;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询采集周期和上报周期
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpQueryPressureLimitsMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        压力上限	Int	0~65535	2
//        压力下限	Int	0~65535	2
        byte[] pressureCap = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(pressureCap);
        properties.put("pressureUpperLimit", BytesUtils.bytesToShort(pressureCap));
        byte[] lowerPressure = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(lowerPressure);
        properties.put("pressureLowerLimit", BytesUtils.bytesToShort(lowerPressure));

    }
}
