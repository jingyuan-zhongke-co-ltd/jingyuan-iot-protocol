package com.jingyuan.iot.protocol.tcp.cz.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.enums.czdzp.CzDzpFeatEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.czdzp.CzDzpValveStateBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;

import java.util.Map;

/**
 * @author 李立强
 */
public class CzDzpValveStateMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(CzDzpFeatEnums.VALVE_STATE.getFeatCode()));
        buf.writeBytes(BytesUtils.hexToBytes("0b00"));
        CzDzpValveStateBo valveStateBo = BeanUtil.toBean(properties, CzDzpValveStateBo.class);
        IotValveStateEnums valveStateEnum = IotValveStateEnums.getByCode(valveStateBo.getValveState());
        // 开阀或管阀状态
        buf.writeByte(BytesUtils.hexToBytes(valveStateEnum.getName())[0]);
    }
}
