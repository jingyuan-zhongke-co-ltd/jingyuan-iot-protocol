package com.jingyuan.iot.protocol.tcp.cz.codec;

import com.jingyuan.common.protocol.enums.spr.JoyoSprUnitEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;

import java.util.Map;

/**
 * @author 李立强
 */
public class CzDzpDataReportMessage implements TcpMessageCodec {
    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param map 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {

        // 跳过表号、控制码、长度
        buf.readBytes(11);

        // 日冻结日期（年月日）
        byte[] freezeTimeDayBytes = ByteBufUtils.readBytes(buf, 3);
        BytesUtils.bytesReverse(freezeTimeDayBytes);
        String freezeTimeDay = BytesUtils.bytesToHex(freezeTimeDayBytes);
        map.put("freezeTimeDay", freezeTimeDay);

        // 当天 0 点冻结累计流量
        byte[] freezeDataBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(freezeDataBytes);
        String freezeData = BytesUtils.binary(10, freezeDataBytes);
        map.put("freezeData", freezeData);

        // 当天48个点累计流量（每半小时占一位）
        everyFreezeNum(buf, map);

        // 当前累计流量
        byte[] readingBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(readingBytes);
        String reading = BytesUtils.binary(10, readingBytes);
        map.put("reading", JoyoSprUnitEnums.KG_1.calculateToTon(reading));


        // 预留
        buf.readBytes(4);

        // ICCID号
        byte[] iccidBytes = ByteBufUtils.readBytes(buf, 10);
        String iccid = BytesUtils.bytesToHex(iccidBytes);
        map.put("ICCID", iccid);

        // IMEI号
        byte[] imeiBytes = ByteBufUtils.readBytes(buf, 8);
        String imei = BytesUtils.bytesToHex(imeiBytes);
        imei = imei.replaceFirst("^0+", "");
        map.put("imei", imei);


        // 当前正向累计流量
        byte[] forwardFlowBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(forwardFlowBytes);
        String forwardFlow = BytesUtils.binary(10, forwardFlowBytes);
        map.put("forwardFlow", forwardFlow);

        // 当前反向累计流量
        byte[] reverseFlowBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(reverseFlowBytes);
        String reverseFlow = BytesUtils.binary(10, reverseFlowBytes);
        map.put("reverseFlow", reverseFlow);

        // 跳过当天48个点增量符号标识
        buf.readBytes(1);

        // 预留
        buf.readBytes(1);

        // 48个点当天24点的冻结累计流量
        byte[] freezeDataSumBytes = ByteBufUtils.readBytes(buf, 4);
        BytesUtils.bytesReverse(freezeDataSumBytes);
        String freezeDataSum = BytesUtils.binary(10, freezeDataSumBytes);
        map.put("freezeDataSum", freezeDataSum);

        // 预留
        buf.readBytes(2);

        // 固定 0xEEEEEEEE
        buf.readBytes(4);

        // 公共数据区解析
        readComm(buf, map);

    }

    /**
     * 公共区解码
     * @param buf
     * @param map
     */
    public static void readComm(ByteBuf buf, Map<String, Object> map) {
        // 终端时钟
        byte[] timeBytes = ByteBufUtils.readBytes(buf, 5);
        String time = BytesUtils.bytesToHex(timeBytes);
        map.put("time", time);
        // 电池电压
        byte[] moduleVoltageBytes = ByteBufUtils.readBytes(buf, 1);
        int voltageHex = moduleVoltageBytes[0] & 0xFF;
        double moduleVoltage = voltageHex * 0.1;
        moduleVoltage = Math.round(moduleVoltage * 10) / 10.0;
        map.put("voltage", moduleVoltage);
        // 信号质量
        map.put("CSQ", buf.readUnsignedByte());
        // 版本号
        byte[] version = ByteBufUtils.readBytes(buf, 5);
        map.put("programVersion", BytesUtils.bytesToAscStr(version));
        // ST0
        st0StatusRead(buf, map);
        // ST1
        st1StatusRead(buf, map);
        // RSRP(信号强度)
        byte[] rsrpBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(rsrpBytes);
        String rsrpString = BytesUtils.bytesToHex(rsrpBytes);
        short rsrp = BytesUtils.bytesToShort(BytesUtils.hexToBytes(rsrpString));
        map.put("RSRP", rsrp / 10.0);
        // SNR(信噪比)
        byte[] snrBytes = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(snrBytes);
        String snrString = BytesUtils.bytesToHex(snrBytes);
        short snr = BytesUtils.bytesToShort(BytesUtils.hexToBytes(snrString));
        map.put("SNR", snr / 10.0);
        // ECL(覆盖等级)
        byte[] eclBytes = ByteBufUtils.readBytes(buf, 1);
        String ecl = BytesUtils.binary(10, eclBytes);
        map.put("ECL", ecl);
        // ST2
        st2StatusRead(buf, map);
        // 网络制式
        netRead(buf, map);
        // 备用字节(固定0xEE)，跳过
        buf.readBytes(3);
    }

    /**
     * 网络制式解码
     *
     * @param buf
     */
    private static void netRead(ByteBuf buf, Map<String, Object> map) {
        byte[] netBytes = ByteBufUtils.readBytes(buf, 1);
        int voltageHex = netBytes[0] & 0xFF;
        map.put("net", voltageHex);
    }

    /**
     * ST0状态解析
     * @param buf
     * @param map
     */
    private static void st0StatusRead(ByteBuf buf, Map<String, Object> map) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int byteValue = bytes[0] & 0xFF;
        // 提取各个状态位
        // 提取 Bit1 和 Bit0
        int valveStatus = byteValue & 0b00000011;
        // 提取 Bit4
        int magneticAttack = (byteValue >> 3) & 0b00000001;
        // 提取 Bit6
        int batteryLow = (byteValue >> 5) & 0b00000001;
        // 输出解析结果
        map.put("valveState", valveStatus);
        map.put("magneticAttack", magneticAttack);
        map.put("batteryLow", batteryLow);
    }

    /**
     * ST1状态解析
     * @param buf
     * @param map
     */
    private static void st1StatusRead(ByteBuf buf, Map<String, Object> map) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 1);
        int byteValue = bytes[0] & 0xFF;
        // 提取强制开阀状态 (Bit2)
        int forceOpenValveStatus = (byteValue >> 2) & 0b00000001;
        // 输出解析结果
        map.put("forceOpenValveStatus", forceOpenValveStatus);
    }

    /**
     * ST2状态解析
     * @param buf
     * @param map
     */
    private static void st2StatusRead(ByteBuf buf, Map<String, Object> map) {
        byte[] bytes = ByteBufUtils.readBytes(buf, 1);
        int byteValue = bytes[0] & 0xFF;
        // 提取电池无电状态 (Bit4)
        int batteryNoPowerStatus = (byteValue >> 4) & 0b00000001;
        // 解析电池无电状态
        String batteryNoPowerStatusStr = (batteryNoPowerStatus == 1) ? "无电（二级欠压）" : "正常";
        // 输出解析结果
        map.put("batteryNoPowerStatus", batteryNoPowerStatusStr);
    }

    private static void everyFreezeNum(ByteBuf buf, Map<String, Object> map) {
        // 获取 bytesToHex 用于判断是否为 0xAA
        String bytesToHex = ByteBufUtils.getBytesToHex(buf, 150, 1).toUpperCase();
        byte[] bytes = ByteBufUtils.readBytes(buf, 96);
        // 96个半小时的增量数据
        double[] incrementData = new double[96];
        if (("AA").equals(bytesToHex)) {
            for (int i = 0; i < 96; i++) {
                // 直接将字节放入数组，不进行任何转换
                incrementData[i] = bytes[i];
            }
        } else {
            for (int i = 0; i < 96; i++) {
                // 使用 & 0xFF 将字节转为无符号整数
                incrementData[i] = bytes[i] & 0xFF;
            }
        }
        map.put("everyFreeze", incrementData);
    }

}
