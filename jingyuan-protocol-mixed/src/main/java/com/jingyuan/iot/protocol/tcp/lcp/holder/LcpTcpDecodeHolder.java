package com.jingyuan.iot.protocol.tcp.lcp.holder;

import com.jingyuan.common.protocol.enums.cct.*;
import com.jingyuan.common.protocol.enums.lcp.LcpAfnFeatureEnums;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * 设备消息解码包装类
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Data
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor(staticName = "of")
@AllArgsConstructor(staticName = "of")
public class LcpTcpDecodeHolder extends TcpDecodeHolder {
    /**
     * 控制码
     */
    private String controlCode;

    /**
     * 应用层子功能码（FN）
     */
    private String fn;

    /**
     * 应用层功能码（AFN）
     */
    private LcpAfnFeatureEnums afn;
    /**
     * 帧序列域 FIR、FIN
     */
    private CctSeqFfEnums seqFf;
    /**
     * 请求确认标志位
     * 帧序列域 CON
     */
    private CctSeqConEnums seqCon;
}
