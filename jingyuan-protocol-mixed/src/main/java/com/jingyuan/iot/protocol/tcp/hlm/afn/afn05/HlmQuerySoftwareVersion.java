package com.jingyuan.iot.protocol.tcp.hlm.afn.afn05;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询软件版本
 *
 * @author: dr
 * @date: 2024-12-03
 * @Version: V1.0
 */
@Slf4j
public class HlmQuerySoftwareVersion implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
//        数据包标识	BIN	0 ~ 255	1	设备收到下发命令包中的数据包标识
        int logotype = BytesUtils.byteToUnInt(buf.readByte());
//        软件版本	BIN	0 ~ 255	1
        properties.put("SoftwareVersion", BytesUtils.byteToUnInt(buf.readByte()));

    }
}
