package com.jingyuan.iot.protocol.tcp.cct.wrx;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cct.codec.CctAfnFeatCodecEnums;
import com.jingyuan.iot.protocol.tcp.cct.holder.CctTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.ChildDeviceMessage;

import java.util.HashMap;

/**
 * 京源IOT分体阀控协议 - 数据上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class CctChildDeviceMessage implements TcpMessageAdapter<ChildDeviceMessage> {

    private ChildDeviceMessage message;

    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
        CctAfnFeatCodecEnums.write(buf, (CctTcpEncodeHolder) encodeHolder, new HashMap<>(0));
    }

    @Override
    public void setMessage(ChildDeviceMessage message) {
        this.message = message;
    }

    @Override
    public ChildDeviceMessage getMessage() {
        return message;
    }
}
