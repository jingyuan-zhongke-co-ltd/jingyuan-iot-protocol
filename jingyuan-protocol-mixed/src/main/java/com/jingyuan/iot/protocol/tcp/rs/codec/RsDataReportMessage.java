package com.jingyuan.iot.protocol.tcp.rs.codec;

import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;

import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 李立强
 */
public class RsDataReportMessage implements TcpMessageCodec {
    /**
     * 从 ByteBuf 读数据
     *
     * @param buf 数据
     * @param map 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {
        // 表号
        byte[] deviceIdBytes = ByteBufUtils.readBytes(buf, 8);
        String deviceId = BytesUtils.bytesToHex(deviceIdBytes);
        map.put("meterAddr", deviceId);
        // 时间
        byte[] currentTimeBytes = ByteBufUtils.readBytes(buf, 7);
        String currentTime = BytesUtils.bytesToHex(currentTimeBytes);
        map.put("currentTime", currentTime);
        // 上线帧指示 1
        onlineFrame(buf, map);
        // 判断是否需要上线帧指示 2
        Integer onlineFrameIndication = Convert.toInt(map.get("onlineFrameIndication"));
        if (onlineFrameIndication == 1) {
            buf.readBytes(1);
        }
        // 电池电压
        byte[] voltageBytes = ByteBufUtils.readBytes(buf, 2);
        short voltage = BytesUtils.bytesToShort(voltageBytes, ByteOrder.LITTLE_ENDIAN);
        map.put("voltage", voltage * 0.01);
        // 累计上线成功次数
        byte[] onlineSuccessNumBytes = ByteBufUtils.readBytes(buf, 2);
        int onlineSuccessNum = ((onlineSuccessNumBytes[1] & 0xFF) << 8) | (onlineSuccessNumBytes[0] & 0xFF);
        map.put("onlineSuccessNum", onlineSuccessNum);
        // 累计上线失败次数
        byte[] onlineDefaultNumBytes = ByteBufUtils.readBytes(buf, 2);
        int onlineDefaultNum = ((onlineDefaultNumBytes[1] & 0xFF) << 8) | (onlineDefaultNumBytes[0] & 0xFF);
        map.put("onlineDefaultNum", onlineDefaultNum);
        // 状态字 ST1
        st1Read(buf, map);
        // 状态字 ST2
        st2Read(buf, map);
        // 状态字 ST3
        st3Read(buf, map);
        // 状态字 ST4
        st4Read(buf, map);
        // RSRP
        byte[] rsrpBytes = ByteBufUtils.readBytes(buf, 2);
        short rsrp = BytesUtils.bytesToShort(rsrpBytes, ByteOrder.LITTLE_ENDIAN);
        if (rsrp == -999) {
            map.put("rsrp", "读取过 rsrp");
        } else if (rsrp == -998) {
            map.put("rsrp", "读取 rsrp 出错");
        } else if (rsrp == -997) {
            map.put("rsrp", "模组断开信号");
        } else {
            // 正常的 RSRP 值（有符号整数）
            map.put("rsrp", rsrp);
        }
        // 信噪比
        byte[] snrBytes = ByteBufUtils.readBytes(buf, 2);
        short snr = BytesUtils.bytesToShort(snrBytes, ByteOrder.LITTLE_ENDIAN);
        if (snr == -999) {
            map.put("snr", "读取过 snr");
        } else if (snr == -998) {
            map.put("snr", "读取 snr 出错");
        } else if (snr == -997) {
            map.put("snr", "模组断开信号");
        } else {
            // 正常的 RSRP 值（有符号整数）
            map.put("snr", snr);
        }
        // 信号覆盖等级
        byte[] eclBytes = ByteBufUtils.readBytes(buf, 1);
        int eclValue = eclBytes[0] & 0xFF;
        if (eclValue == 9) {
            map.put("ecl", "未读取信号覆盖等级");
        } else if (eclValue == 8) {
            map.put("ecl", "读取信号覆盖等级出错");
        } else if (eclValue == 7) {
            map.put("ecl", "模组断开信号");
        } else if (eclValue <= 2) {
            map.put("ecl", "有效值：" + eclValue);
        } else {
            map.put("ecl", "无效值");
        }
        // PCI 和 内部温度
        Integer pciTempReport = Convert.toInt(map.get("pciTempReport"));
        if (pciTempReport == 1) {
            byte[] bytes = ByteBufUtils.readBytes(buf, 2);
            short pciValue = BytesUtils.bytesToShort(bytes, ByteOrder.LITTLE_ENDIAN);
            map.put("pci", pciValue); // 将 PCI 值格式化为 16 进制上报
            byte[] heatBytes = ByteBufUtils.readBytes(buf, 2);
            short heat = BytesUtils.bytesToShort(heatBytes, ByteOrder.LITTLE_ENDIAN);
            map.put("temperature", heat);
        } else {
            map.put("pci", "不上报");
            map.put("temperature", "不上报");
        }
        // 当前水压
        Integer pressureReport = Convert.toInt(map.get("pressureReport"));
        if (pressureReport == 1) {
            byte[] pressureBytes = ByteBufUtils.readBytes(buf, 2);
            short pressure = BytesUtils.bytesToShort(pressureBytes, ByteOrder.LITTLE_ENDIAN);
            map.put("pressure", pressure * 0.001);
            if (pressure == BytesUtils.bytesToShort(BytesUtils.hexToBytes(String.valueOf(0x7FFF)))) {
                map.put("pressure", "传感器断线或未接");
            }
        } else {
            map.put("pressure", "传感器断线或未接");
        }
        // 软件版本号（主版本）
        byte[] majorVersionBytes = ByteBufUtils.readBytes(buf, 2);
        short majorVersion = BytesUtils.bytesToShort(majorVersionBytes, ByteOrder.LITTLE_ENDIAN);
        map.put("majorVersion", majorVersion);
        // 软件版本号（子版本）
        map.put("subversionVersion", buf.readUnsignedByte());
        // IMSI
        byte[] imsiBytes = ByteBufUtils.readBytes(buf, 8);
        String imsi = BytesUtils.bytesToHex(imsiBytes);
        imsi = imsi.replaceFirst("^0+", "");
        map.put("imsi", imsi);
        // IMEI号
        Integer imeiReport = Convert.toInt(map.get("imeiReport"));
        if (imeiReport == 1) {
            byte[] imeiBytes = ByteBufUtils.readBytes(buf, 8);
            String imei = BytesUtils.bytesToHex(imeiBytes);
            imei = imei.replaceFirst("^0+", "");
            map.put("imei", imei);
        } else {
            map.put("imei", "未上报");
        }
        // 当前累计流量
        byte[] readingBytes = ByteBufUtils.readBytes(buf, 5);
        short reading = BytesUtils.bytesToShort(readingBytes, ByteOrder.LITTLE_ENDIAN);
        map.put("reading", reading * 0.1);
        // 当前累计逆流量
        byte[] reverseReadingBytes = ByteBufUtils.readBytes(buf, 5);
        short reverseReading = BytesUtils.bytesToShort(reverseReadingBytes, ByteOrder.LITTLE_ENDIAN);
        map.put("reverseReading", reverseReading * 0.1);
        // 日结数据组数
        short dataNum = buf.readUnsignedByte();
        map.put("dataNum", dataNum);
        // 日结数据指示
        dataIndication(buf, map);
        // 日结累计流量
        dailyCumulativeTraffic(buf, map, dataNum);
        // 间隔流量组数
        short intervalNum = buf.readUnsignedByte();
        map.put("intervalNum", intervalNum);
        // 间隔流量指示
        dataInterval(buf, map);
        // 间隔流量
        intervalTraffic(buf, map, intervalNum);
        // 密集采样数据组数
        short denseNum = buf.readUnsignedByte();
        map.put("denseNum", denseNum);
        // 密集采样流量指示
        denseInterval(buf, map);
        // 密集采样起始时间
        byte[] denseTimeDayBytes = ByteBufUtils.readBytes(buf, 5);
        String denseTimeDay = BytesUtils.bytesToHex(denseTimeDayBytes);
        map.put("denseTimeDay", denseTimeDay);
        // 密集采样正向流量
        denseNumTraffic(buf, map, denseNum);
        // 压力值组数
        short pressureNum = 0;
        if (pressureReport == 1) {
            pressureNum = buf.readUnsignedByte();
            map.put("pressureNum", pressureNum);
        }
        // 压力值指示
        pressureInterval(buf, map);
        // 压力值
        if (pressureReport == 1) {
            pressureNumTraffic(buf, map, pressureNum);
        }
        // 需报警条数
        short alarmNum = buf.readUnsignedByte();
        map.put("alarmNum", alarmNum);
        // 当前报警条数
        short nowAlarmNum = buf.readUnsignedByte();
        map.put("nowAlarmNum", nowAlarmNum);
        // 报警记录
        alarmNumTraffic(buf, map, alarmNum);
        // 瞬时流速最大值
        byte[] speedBytes = ByteBufUtils.readBytes(buf, 4);
        int instantaneousFlow = ((speedBytes[3] & 0xFF) << 24) |
            ((speedBytes[2] & 0xFF) << 16) |
            ((speedBytes[1] & 0xFF) << 8) |
            (speedBytes[0] & 0xFF);
        map.put("instantaneousFlow", instantaneousFlow);
        // 对应时间
        byte[] correspondingTimeDayBytes = ByteBufUtils.readBytes(buf, 3);
        String correspondingTimeDay = BytesUtils.bytesToHex(correspondingTimeDayBytes);
        map.put("correspondingTimeDay", correspondingTimeDay);
    }

    private static void onlineFrame(ByteBuf buf, Map<String, Object> map) {
        // 读取 1 个字节
        byte[] bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int byteValue = bytes[0] & 0xFF;
        // 提取各个位
        // BIT0 -> 上线类型为 1 天，0 其他
        int onlineType = byteValue & 0b00000001;
        // BIT1-3 -> 无线模组型号 (按规则可以从 BIT1 到 BIT3 提取)
        int moduleType = (byteValue >> 1) & 0b00000111;
        // BIT4 -> PCI 和内部温度上报使能 1 上报 0 不上报
        int pciTempReport = (byteValue >> 4) & 0b00000001;
        // BIT5 -> 是否上报 IMEI 号 1 上报 0 不上报
        int imeiReport = (byteValue >> 5) & 0b00000001;
        // BIT6 -> 是否上报压力值 1 上报 0 不上报
        int pressureReport = (byteValue >> 6) & 0b00000001;
        // BIT7 -> 是否存在上线帧指示 1 存在 0 不存在
        int onlineFrameIndication = (byteValue >> 7) & 0b00000001;
        // 输出解析结果到 map
        // 上线类型
        map.put("onlineType", onlineType);
        // 无线模组型号
        map.put("moduleType", moduleType);
        // PCI 和内部温度上报使能
        map.put("pciTempReport", pciTempReport);
        // 是否上报 IMEI 号
        map.put("imeiReport", imeiReport);
        // 是否上报压力值
        map.put("pressureReport", pressureReport);
        // 是否存在上线帧指示
        map.put("onlineFrameIndication", onlineFrameIndication);
    }

    private static void st1Read(ByteBuf buf, Map<String, Object> map) {
        // 读取 1 个字节
        byte[] st1Bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int st1Value = st1Bytes[0] & 0xFF; // 保证无符号
        // 提取信号强度 CSQ（ST1.3~7），5 位
        int csq = (st1Value >> 3) & 0x1F; // 高 5 位
        // 提取存储故障标志（ST1.2）
        int storageFault = (st1Value >> 2) & 0x01; // 第 2 位
        // 提取按键异常标志（ST1.1）
        int keyFault = (st1Value >> 1) & 0x01; // 第 1 位
        // 提取逆流标志（ST1.0）
        int reverseFlow = st1Value & 0x01; // 第 0 位
        // 输出解析结果到 map
        map.put("csq", csq);
        map.put("storageFault", storageFault);      // 存储故障标志
        map.put("keyFault", keyFault);              // 按键异常标志
        map.put("reverseFlow", reverseFlow);        // 逆流标志
    }

    private static void st2Read(ByteBuf buf, Map<String, Object> map) {
        // 读取 1 个字节（ST2 字节）
        byte[] st2Bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int st2Value = st2Bytes[0] & 0xFF;
        // 提取阀门状态（ST2.7）
        int valveState = (st2Value >> 7) & 0x01;
        // 提取阀门异常（ST2.6）
        int valveFault = (st2Value >> 6) & 0x01;
        // 提取流量通讯异常标志（ST2.5）
        int flowCommFault = (st2Value >> 5) & 0x01;
        // 提取磁干扰标志（ST2.4）
        int magneticInterference = (st2Value >> 4) & 0x01;
        // 提取 2 级低压标志（ST2.3）
        int lowPressure2 = (st2Value >> 3) & 0x01;
        // 提取 1 级低压标志（ST2.2）
        int lowPressure1 = (st2Value >> 2) & 0x01;
        // 提取防拆标志（ST2.1）
        int antiTamper = (st2Value >> 1) & 0x01;
        // 提取脉冲异常标志（ST2.0）
        int pulseFault = st2Value & 0x01;
        // 将解析结果存入 map
        map.put("valveState", valveState);
        map.put("valveFault", valveFault);
        map.put("flowCommFault", flowCommFault);
        map.put("magneticInterference", magneticInterference);
        map.put("lowPressure2", lowPressure2);
        map.put("lowPressure1", lowPressure1);
        map.put("antiTamper", antiTamper);
        map.put("pulseFault", pulseFault);
    }

    private static void st3Read(ByteBuf buf, Map<String, Object> map) {
        // 读取 1 个字节（ST3 字节）
        byte[] st3Bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int st3Value = st3Bytes[0] & 0xFF; // 确保无符号
        // 提取光电模块组合位（ST3.5~7）
        int photoelectricModule = (st3Value >> 5) & 0x07;
        // 提取应急按键状态（ST3.4）
        int emergencyButton = (st3Value >> 4) & 0x01;
        // 提取高瞬时流速（ST3.3）
        int highInstantaneousFlow = (st3Value >> 3) & 0x01;
        // 提取无磁水表磁干扰标志（ST3.2）
        int noMagneticInterference = (st3Value >> 2) & 0x01;
        // 提取预留位（ST3.1 和 ST3.0），不需要处理
        // 将解析结果存入 map
        map.put("photoelectricModuleStatus", photoelectricModule);
        map.put("emergencyButton", emergencyButton == 0);
        map.put("highInstantaneousFlow", highInstantaneousFlow);
        map.put("noMagneticInterference", noMagneticInterference);
    }

    private static void st4Read(ByteBuf buf, Map<String, Object> map) {
        // 读取 1 个字节（ST4 字节）
        byte[] st4Bytes = ByteBufUtils.readBytes(buf, 1);
        // 将字节转换为无符号整数
        int st4Value = st4Bytes[0] & 0xFF; // 确保无符号
        // 0-1 位默认是 0，赋值为 "STA"
        String stTypeStr = "STA";  // 默认值为 STA
        // 提取小无线通讯异常标志（ST4.3）
        int smallWirelessCommError = (st4Value >> 3) & 0x01;
        // 将解析结果存入 map
        map.put("stType", stTypeStr);
        map.put("smallWirelessCommError", smallWirelessCommError);
    }

    private static void dataIndication(ByteBuf buf, Map<String, Object> map) {
        byte[] dataIndicationBytes = ByteBufUtils.readBytes(buf, 1);
        int dataIndication = dataIndicationBytes[0] & 0xFF;
        int flowFormat = dataIndication & 0x03;
        map.put("flowFormat", flowFormat);
        int dataContent = (dataIndication >> 2) & 0x03;  // 提取 BIT2~3
        map.put("dataContent", dataContent);

    }

    private static void dailyCumulativeTraffic(ByteBuf buf, Map<String, Object> map, int groupCount) {
        int totalLength = 5 * groupCount;
        byte[] flowBytes = ByteBufUtils.readBytes(buf, totalLength);
        List<Double> dailyFlows = new ArrayList<>();
        Integer flowFormat = Convert.toInt(map.get("flowFormat"));
        for (int i = 0; i < groupCount; i++) {
            int startIndex = i * 5;
            int flow = ((flowBytes[startIndex + 2] & 0xFF) << 16) |
                ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                (flowBytes[startIndex] & 0xFF);
            if (flowFormat == 0) {
                short reverseReading = Convert.toShort(flow); // 转为 short 类型
                dailyFlows.add(Convert.toDouble(reverseReading));
            } else if (flowFormat == 1) {
                double actualFlow = flow * 0.1; // 转换单位
                dailyFlows.add(actualFlow);
            } else if (flowFormat == 2) {
                short reverseReading = Convert.toShort(flow); // 按照协议处理
                dailyFlows.add(Convert.toDouble(reverseReading));
            }
        }
        map.put("dailyFlows", dailyFlows);
    }

    private static void dataInterval(ByteBuf buf, Map<String, Object> map) {
        byte[] dataIntervalBytes = ByteBufUtils.readBytes(buf, 1);
        int dataInterval = dataIntervalBytes[0] & 0xFF;
        // 直接解析并存入 map
        map.put("enabled", (dataInterval & 0x01) == 1);
        map.put("intervalFlowFormat", (dataInterval >> 1) & 0x7F);
    }

    private static void intervalTraffic(ByteBuf buf, Map<String, Object> map, int groupCount) {
        // 获取流量总长度
        int intervalFlowFormat = Convert.toInt(map.get("intervalFlowFormat")); // 获取格式
        int groupLength;
        switch (intervalFlowFormat) {
            case 0:
                groupLength = 3;
                break;
            case 1:
                groupLength = 2;
                break;
            case 2:
                groupLength = 1;
                break;
            case 3:
                groupLength = 2;
                break;
            case 4:
                groupLength = 1;
                break;
            case 5:
                groupLength = 2;
                break;
            case 6:
                groupLength = 10;
                break;
            default:
                throw new IllegalArgumentException("不支持的类型: " + intervalFlowFormat);
        }
        int totalLength = groupLength * groupCount;
        // 读取字节流
        byte[] flowBytes = ByteBufUtils.readBytes(buf, totalLength);
        List<Double> intervalFlows = new ArrayList<>();
        for (int i = 0; i < groupCount; i++) {
            int startIndex = i * groupLength;
            double intervalFlow;
            switch (intervalFlowFormat) {
                // 格式 0，无符号，每组 3 字节，单位 0.001L
                case 0:
                    int flow0 = ((flowBytes[startIndex + 2] & 0xFF) << 16) |
                        ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF);
                    if (flow0 == 0xFFFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow0 * 0.001; // 单位 0.001L
                    }
                    break;
                // 格式 1，每组 2 字节无符号，单位 1L
                case 1:
                    int flow1 = ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF);
                    if (flow1 == 0xFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow1 * 1.0; // 单位 1L
                    }
                    break;
                // 格式 2，每组 1 字节无符号，单位 0.1 立方米
                case 2:
                    int flow2 = flowBytes[startIndex] & 0xFF;
                    if (flow2 == 0xFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow2 * 0.1; // 单位 0.1立方米
                    }
                    break;
                // 格式 3，每组 2 字节有符号，单位 1L
                case 3:
                    short flow3 = (short) (((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF));
                    if (flow3 == (short) 0xFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow3 * 1.0; // 单位 1L
                    }
                    break;
                // 格式 4，每组 1 字节有符号，单位 0.1 立方米
                case 4:
                    byte flow4 = flowBytes[startIndex];
                    if (flow4 == (byte) 0xFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow4 * 0.1; // 单位 0.1立方米
                    }
                    break;
                // 格式 5，每组 2 字节有符号，单位 0.1 立方米
                case 5:
                    short flow5 = (short) (((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF));
                    if (flow5 == (short) 0xFFFF) {
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow5 * 0.1;
                    }
                    break;
                // 格式 6，参考1110
                case 6:
                    short flow6 = (short) (((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF));
                    if (flow6 == (short) 0xFFFF) {
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow6 * 0.1;
                    }
                    break;
                default:
                    throw new IllegalArgumentException("不支持的类型:" + intervalFlowFormat);
            }
            intervalFlows.add(intervalFlow);
        }
        map.put("intervalFlows", intervalFlows);
    }

    private static void denseInterval(ByteBuf buf, Map<String, Object> map) {
        byte[] denseIntervalBytes = ByteBufUtils.readBytes(buf, 1);
        byte denseIntervalByte = denseIntervalBytes[0];
        // BIT0 密集采样流量上报使能
        boolean reportEnabled = (denseIntervalByte & 0x01) == 1;
        map.put("denseReportEnabled", reportEnabled);
        // BIT1~3 密集采样流量格式
        int denseFlowFormat = (denseIntervalByte >> 1) & 0x07; // 提取 BIT1~3
        map.put("denseFlowFormat", denseFlowFormat);
        // BIT4~6 密集采样间隔单位
        int intervalUnit = (denseIntervalByte >> 4) & 0x07; // 提取 BIT4~6
        map.put("intervalUnit", intervalUnit);
        // BIT7 保留位
        boolean bit7Reserved = (denseIntervalByte & 0x80) != 0; // 提取 BIT7
        map.put("bit7Reserved", bit7Reserved);
    }

    private static void denseNumTraffic(ByteBuf buf, Map<String, Object> map, int groupCount) {
        // 获取流量总长度
        int denseFlowFormat = Convert.toInt(map.get("denseFlowFormat")); // 获取格式
        int groupLength;
        switch (denseFlowFormat) {
            case 0:
                groupLength = 3;
                break;
            case 1:
                groupLength = 2;
                break;
            case 2:
                groupLength = 1;
                break;
            case 3:
                groupLength = 2;
                break;
            case 4:
                groupLength = 2;
                break;
            default:
                throw new IllegalArgumentException("不支持的类型: " + denseFlowFormat);
        }
        int totalLength = groupLength * groupCount;
        // 读取字节流
        byte[] flowBytes = ByteBufUtils.readBytes(buf, totalLength);
        List<Double> denseFlows = new ArrayList<>();
        for (int i = 0; i < groupCount; i++) {
            int startIndex = i * groupLength;
            double intervalFlow;
            switch (denseFlowFormat) {
                // 格式 0，无符号，每组 3 字节，单位 0.001L
                case 0:
                    int flow0 = ((flowBytes[startIndex + 2] & 0xFF) << 16) |
                        ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF);
                    if (flow0 == 0xFFFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow0 * 0.001; // 单位 0.001L
                    }
                    break;
                // 格式 1，每组 2 字节无符号，单位 1L
                case 1:
                    int flow1 = ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF);
                    if (flow1 == 0xFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow1 * 1.0; // 单位 1L
                    }
                    break;
                // 格式 2，每组 1 字节无符号，单位 0.1 立方米
                case 2:
                    int flow2 = flowBytes[startIndex] & 0xFF;
                    if (flow2 == 0xFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow2 * 0.1; // 单位 0.1立方米
                    }
                    break;
                // 格式 3，每组 2 字节有符号，单位 1L
                case 3:
                    short flow3 = (short) (((flowBytes[startIndex + 1] & 0xFF) << 8) |
                        (flowBytes[startIndex] & 0xFF));
                    if (flow3 == (short) 0xFFFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow3 * 1.0; // 单位 1L
                    }
                    break;
                // 格式 4，每组 1 字节有符号，单位 0.1 立方米
                case 4:
                    byte flow4 = flowBytes[startIndex];
                    if (flow4 == (byte) 0xFF) { // 特殊值无效判断
                        intervalFlow = Double.NaN;
                    } else {
                        intervalFlow = flow4 * 0.1; // 单位 0.1立方米
                    }
                    break;
                default:
                    throw new IllegalArgumentException("不支持的类型:" + denseFlowFormat);
            }
            denseFlows.add(intervalFlow);
        }
        map.put("denseFlows", denseFlows);
    }

    private static void pressureInterval(ByteBuf buf, Map<String, Object> map) {
        byte[] pressureIntervalBytes = ByteBufUtils.readBytes(buf, 1);
        int pressureInterval = pressureIntervalBytes[0] & 0xFF;
        int pressureFormat = pressureInterval >> 1;
        pressureFormat &= 0x7F;
        map.put("pressureFormat", pressureFormat);
    }

    private static void pressureNumTraffic(ByteBuf buf, Map<String, Object> map, int groupCount) {
        int totalLength = 2 * groupCount;
        byte[] flowBytes = ByteBufUtils.readBytes(buf, totalLength);
        List<Double> pressureFlows = new ArrayList<>();
        Integer pressureFormat = Convert.toInt(map.get("pressureFormat"));
        for (int i = 0; i < groupCount; i++) {
            int startIndex = i * 2;
            // 正确的字节拼接方式：只使用 flowBytes[startIndex] 和 flowBytes[startIndex + 1]
            int flow = ((flowBytes[startIndex + 1] & 0xFF) << 8) |
                (flowBytes[startIndex] & 0xFF);

            if (pressureFormat == 0) {
                double pressureFlow = flow * 0.01; // 转换单位
                pressureFlows.add(pressureFlow);
            } else {
                pressureFlows.add(Convert.toDouble(flow));
            }
        }
        map.put("pressureFlows", pressureFlows);
    }

    private static void alarmNumTraffic(ByteBuf buf, Map<String, Object> map, int groupCount) {
        int totalLength = 4 * groupCount;  // 每个报警纪录占 4 字节
        byte[] flowBytes = ByteBufUtils.readBytes(buf, totalLength);  // 读取报警记录字节数据
        List<Map<String, Object>> alarmFlows = new ArrayList<>();  // 用于存储解析后的报警信息
        for (int i = 0; i < groupCount; i++) {
            int startIndex = i * 4;  // 每组报警记录的起始位置
            // 获取报警时间（BCD 格式：时、分、秒）
            byte hour = flowBytes[startIndex];  // 第 1 字节：小时
            byte minute = flowBytes[startIndex + 1];  // 第 2 字节：分钟
            byte second = flowBytes[startIndex + 2];  // 第 3 字节：秒
            // 解析报警类型和状态（第 4 字节）
            byte alarmInfo = flowBytes[startIndex + 3];
            int alarmType = alarmInfo & 0x1F;  // 取 BIT0~4，表示故障告警类型
            boolean alarmStatus = (alarmInfo & 0x20) != 0;  // BIT5：0-警告消失，1-警告产生
            boolean auxiliaryFlag = (alarmInfo & 0x40) != 0;  // BIT6：辅助标志
            boolean reservedFlag = (alarmInfo & 0x80) != 0;  // BIT7：暂未定义
            // 将解析结果存入 Map
            Map<String, Object> alarmRecord = new HashMap<>();
            alarmRecord.put("time", String.format("%02d:%02d:%02d", hour, minute, second));  // 格式化时间
            alarmRecord.put("alarmType", alarmType);  // 故障告警类型
            alarmRecord.put("alarmStatus", alarmStatus);  // 警告产生或消失
            alarmRecord.put("auxiliaryFlag", auxiliaryFlag);  // 辅助标志
            alarmRecord.put("reservedFlag", reservedFlag);  // 暂未定义
            alarmFlows.add(alarmRecord);  // 将每条报警记录添加到列表中
        }
        // 将所有的报警记录存储到 map 中
        map.put("alarmFlows", alarmFlows);
    }
}

