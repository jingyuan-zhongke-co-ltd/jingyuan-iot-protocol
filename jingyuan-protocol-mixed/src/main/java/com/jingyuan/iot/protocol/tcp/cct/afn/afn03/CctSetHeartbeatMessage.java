package com.jingyuan.iot.protocol.tcp.cct.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.cct.afn03.CctSetHeartbeatBo;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 设置集采器心跳周期
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class CctSetHeartbeatMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctSetHeartbeatBo setHeartbeatBo = BeanUtil.toBean(properties, CctSetHeartbeatBo.class);
        buf.writeByte(setHeartbeatBo.getHeartbeat());
    }
}
