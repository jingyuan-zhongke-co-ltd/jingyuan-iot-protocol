package com.jingyuan.iot.protocol.tcp.lcp.afn.afn0c;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn0c.IntegratedHistoricalData;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Date;
import java.util.Map;

/**
 * 一体数据
 *
 * @author: dr
 * @date: 2024-11-01
 * @Version: V1.0
 */
@Slf4j
public class LcpHistoricalDataMessage implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> map) {
        String rawDataHex = BytesUtils.bytesToHex(ByteBufUtils.getAll(buf));
        int userLength = (rawDataHex.length() - 10 * 2) / 2;
//        表地址	BCD	6个字节
        String BCD = BytesUtils.bcdToStr(ByteBufUtils.readBytes(buf, 6));
        map.put("BCD", BCD);
//                第1个点数据体
        for (int i = userLength, n = 1; i > 32; i = i - 32, n++) {
//        时间（从1970-01-01开始的秒数)	td2类型的 umix 时间	4个字节
            String freezeTime = BytesUtils.bytesToHex(ByteBufUtils.readBytes(buf, 4));
            map.put("freezeTime", DateUtils.parseLocalDateTime(freezeTime));
            // 瞬时流量 Float 4个字节
            float instantaneousFlow = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
            map.put("instantFlowUnit", instantaneousFlow);

            // 正向累计流量整数部分 Long 4个字节
            long forwardAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
            // 正向累计流量小数部分 Float 4个字节
            float forwardAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
            map.put("forwardFlow", forwardAccumulatedFlowInt+","+forwardAccumulatedFlowDec);

            // 反向累计流量整数部分 Long 4个字节
            long reverseAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
            // 反向累计流量小数部分 Float 4个字节
            float reverseAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
            map.put("reverseFlow", reverseAccumulatedFlowInt+","+reverseAccumulatedFlowDec);

            // 第1路模拟量通道采集数据 Int 2个字节
            int analogChannel1 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel1", analogChannel1);

            // 第2路模拟量通道采集数据 Int 2个字节
            int analogChannel2 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("analogChannel2", analogChannel2);

            // 告警状态 Byte 1个字节
            byte alarmStatus = buf.readByte();
            map.put("alarmStatus", alarmStatus);

            // 电池电压 Int 2个字节
            int batteryVoltage = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
            map.put("voltage", batteryVoltage);

            // 网络信号 Byte 1个字节
            byte networkSignal = buf.readByte();
            map.put("signal", networkSignal);
        }
//        for (int i = userLength, n = 1; i > 32; i = i - 32, n++) {
//            // 时间（从1970-01-01开始的秒数)	td2类型的 umix 时间	4个字节
//            String freezeTime = BytesUtils.bytesToHex(ByteBufUtils.readBytes(buf, 4));
//            map.put("freezeTime" + n, freezeTime);
//
//            // 瞬时流量 Float 4个字节
//            float instantaneousFlow = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 4));
//            map.put("instantaneousFlow" + n, instantaneousFlow);
//
//            // 正向累计流量整数部分 Long 4个字节
//            long forwardAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
//            map.put("forwardAccumulatedFlowInt" + n, forwardAccumulatedFlowInt);
//
//            // 正向累计流量小数部分 Float 4个字节
//            float forwardAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
//            map.put("forwardAccumulatedFlowDec" + n, forwardAccumulatedFlowDec);
//
//            // 反向累计流量整数部分 Long 4个字节
//            long reverseAccumulatedFlowInt = BytesUtils.bytesToLong(ByteBufUtils.readBytes(buf, 4));
//            map.put("reverseAccumulatedFlowInt" + n, reverseAccumulatedFlowInt);
//
//            // 反向累计流量小数部分 Float 4个字节
//            float reverseAccumulatedFlowDec = BytesUtils.bytesToFloat(ByteBufUtils.readBytes(buf, 4));
//            map.put("reverseAccumulatedFlowDec" + n, reverseAccumulatedFlowDec);
//
//            // 第1路模拟量通道采集数据 Int 2个字节
//            int analogChannel1 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("analogChannel1" + n, analogChannel1);
//
//            // 第2路模拟量通道采集数据 Int 2个字节
//            int analogChannel2 = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("analogChannel2" + n, analogChannel2);
//
//            // 告警状态 Byte 1个字节
//            byte alarmStatus = buf.readByte();
//            map.put("alarmStatus" + n, alarmStatus);
//
//            // 电池电压 Int 2个字节
//            int batteryVoltage = BytesUtils.bytesToShort(ByteBufUtils.readBytes(buf, 2));
//            map.put("batteryVoltage" + n, batteryVoltage);
//
//            // 网络信号 Byte 1个字节
//            byte networkSignal = buf.readByte();
//            map.put("networkSignal" + n, networkSignal);
//        }

    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        IntegratedHistoricalData bean = BeanUtil.toBean(properties, IntegratedHistoricalData.class);
        Date time = bean.getTimestamp();
        byte[] bytes = BytesUtils.longToBytes(time.getTime() / 1000, ByteOrder.LITTLE_ENDIAN);
        for (int i = 0; i < 4; i++) {
            buf.writeByte(bytes[i]);
        }
        byte quantity = bean.getQuantity();
        buf.writeByte(quantity);
    }
}
