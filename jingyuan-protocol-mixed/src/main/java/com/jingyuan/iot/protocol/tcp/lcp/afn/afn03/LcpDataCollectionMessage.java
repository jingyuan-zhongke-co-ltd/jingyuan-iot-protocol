package com.jingyuan.iot.protocol.tcp.lcp.afn.afn03;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.lcp.afn03.LcpCollectionCycle;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;


/**
 * 定时采集周期，定时上报周期
 *
 * @author: dr
 * @date: 2024-10-31
 * @Version: V1.0
 */
@Slf4j
public class LcpDataCollectionMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        LcpCollectionCycle bean = BeanUtil.toBean(properties, LcpCollectionCycle.class);
        byte[] bytes1 = BytesUtils.shortToBytes(bean.getCollectionCycle(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(bytes1);

        byte[] bytes2 = BytesUtils.shortToBytes(bean.getReportCycle(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(bytes2);

    }

}
