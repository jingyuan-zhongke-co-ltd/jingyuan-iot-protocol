package com.jingyuan.iot.protocol.http.codec.ctwing;


import com.jingyuan.common.utils.bytes.BytesUtils;
import lombok.extern.slf4j.Slf4j;

import java.util.Base64;
import java.util.Optional;

/**
 * @author 代荣
 */
@Slf4j
public class AppDataDecryptUtils {

    public static void main(String[] args) {
        // base64解密->16进制字符串
        Optional<String> decrypt = decrypt("GhCAkGixfXT0ABGa+I91dGdPkLxoNYNicniQkGiQdXRyeJCQaJB1dHJ4kJBokHV0cniQkGiQdXRyeJCQaJB1dHJ4kJBokHV0cniQkGiQdXRyeN6hI8o2RkFJoJBokRhAREihoVugQU1BTqGmUKlNQkFMoKlYpU1DR02op16DdWI=");
        decrypt.ifPresent(p -> System.out.println("解密后" + p));

        // 16进制字符串->base64编码
        String s = "68681000002108008678810a901f00001537002c00a5f616000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004e314b5a43323331300000016d343630313133303439333631363839383633343039303538373535383736130016";
        String s1 = Base64.getEncoder().encodeToString(BytesUtils.hexToBytes(s));
        System.out.println("base64编码: " + s1);

        // 16进制字符串解密->-16进制字符串
        String hexDataFrame = "7E7E027797001118123432002A020001240531154924F1F1779700111849F0F02405311549FF01450000000003100000FF15280000000000031D5F";
        byte[] decryptedBytes = decryptCtsPayload(BytesUtils.hexToBytes(hexDataFrame));
        System.out.println("解密后的十六进制数据帧: " + BytesUtils.bytesToHex(decryptedBytes));

        // 16进制字符串加密->16进制字符串&base64编码
        String hexDataFrame1 = "68681000002108008678810a901f00001537002c00a5f616000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000004e314b5a43323331300000016d343630313133303439333631363839383633343039303538373535383736130016";
        byte[] bytes = decryptCtsPayload(BytesUtils.hexToBytes(hexDataFrame1));
        System.out.println("加密后的十六进制数据帧: " + BytesUtils.bytesToHex(bytes));
        System.out.println("加密后的base64编码: " + Base64.getEncoder().encodeToString(bytes));

    }

    public static Optional<String> decrypt(String base64Str) {
        Optional<byte[]> decodedBytesOpt = Optional
            .ofNullable(base64Str)
            // 解码 Base64 编码的字符串
            .map(str -> Base64.getDecoder().decode(str));
        // 将字节数组转换为十六进制字符串
        return decodedBytesOpt
            .map(BytesUtils::bytesToHex)
            .filter(hexString -> hexString.startsWith("68") || hexString.startsWith("a568") || hexString.startsWith("a168") || hexString.startsWith("a268"))
            .map(Optional::of)
            .orElseGet(() -> {
                // 解密解码后的数据
                byte[] decryptedBytes = decryptCtsPayload(decodedBytesOpt.orElse(new byte[0]));
                // 解密后的结果的十六进制表示
                return Optional.of(BytesUtils.bytesToHex(decryptedBytes));
            });
    }

    private static byte[] decryptCtsPayload(byte[] payload) {
        byte[] key = {(byte) 0x72, (byte) 0x78, (byte) 0x90, (byte) 0x90, (byte) 0x68, (byte) 0x90, (byte) 0x75, (byte) 0x74};

        if (payload.length > 9) {
            int j = 0;
            for (int i = 0; i < payload.length; i++) {
                if (j == 8) {
                    j = 0;
                }
                payload[i] = (byte) (payload[i] ^ key[j]);
                j++;
            }
        }
        return payload;
    }
}


