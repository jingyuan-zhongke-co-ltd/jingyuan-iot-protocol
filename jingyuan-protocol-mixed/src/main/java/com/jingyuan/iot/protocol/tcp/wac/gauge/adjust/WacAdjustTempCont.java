package com.jingyuan.iot.protocol.tcp.wac.gauge.adjust;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.adjust.WacAdjustReturnConfBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;
import java.util.Map;

/**
 * 0x02:回温调节
 * 0x03:温差调节模式
 *
 * @author: cc
 * @date: 2024/12/5 11:06
 * @Version: V1.0
 */
@Slf4j
public class WacAdjustTempCont implements TcpMessageCodec {

    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 目标调节值，表示0.5摄氏度温度（BCD码）
        byte[] bytes1 = ByteBufUtils.readBytes(buf, 2);
        BytesUtils.bytesReverse(bytes1);
        // 目标调节值，目标调节值，表示0.5摄氏度温度。
        properties.put("adjustTemp", BigDecimal.valueOf(Convert.toInt(BytesUtils.bytesToHex(bytes1))).movePointLeft(2));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        WacAdjustReturnConfBo tempConfBo = BeanUtil.toBean(properties, WacAdjustReturnConfBo.class);
        short adjustTemp = (short) new BigDecimal(Convert.toStr(tempConfBo.getAdjustTemp()))
            .movePointRight(2)
            .intValue();
        // 暂不确定温度的编码形式，先采用与开度相同的方式
        byte[] bytes = BytesUtils.hexToBytes(Convert.toStr(adjustTemp));
        BytesUtils.bytesReverse(bytes);
        buf.writeBytes(bytes);
        if (bytes.length == 1) {
            buf.writeByte(0);
        }
    }
}
