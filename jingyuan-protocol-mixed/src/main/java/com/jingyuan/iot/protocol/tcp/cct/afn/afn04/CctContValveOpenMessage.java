package com.jingyuan.iot.protocol.tcp.cct.afn.afn04;

import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 遥控开阀（合闸）
 *
 * @author: cc
 * @date: 2024/7/26 10:08
 * @Version: V1.0
 */
@Slf4j
public class CctContValveOpenMessage implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        CctContValveCloseMessage.contValve(buf, properties);
    }
}
