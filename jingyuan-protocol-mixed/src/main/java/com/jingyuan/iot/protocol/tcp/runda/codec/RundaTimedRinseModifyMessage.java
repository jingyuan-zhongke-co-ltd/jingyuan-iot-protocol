package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpTimedRinseBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 定时冲洗时间读写
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaTimedRinseModifyMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 定时冲洗时间
        byte[] timedRinse = ByteBufUtils.readBytes(buf, 2);
        properties.put("timedRinse", BytesUtils.binary(10, timedRinse));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x02);

        WpTimedRinseBo wpTimedRinseBo = BeanUtil.toBean(properties, WpTimedRinseBo.class);
        Short timedRinse = wpTimedRinseBo.getTimedRinse();
        buf.writeBytes(BytesUtils.shortToBytes(timedRinse));
    }
}
