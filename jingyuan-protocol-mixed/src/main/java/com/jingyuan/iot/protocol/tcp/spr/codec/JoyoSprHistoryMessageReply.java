package com.jingyuan.iot.protocol.tcp.spr.codec;

import com.jingyuan.common.protocol.enums.spr.JoyoSprUnitEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 查询历史数据应答
 *
 * @author: cc
 * @date: 2024/4/1 9:55
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprHistoryMessageReply implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        JoyoSprDataReportMessage.nbFreezeDataRead(buf, properties, JoyoSprUnitEnums.KG_10);
    }
}
