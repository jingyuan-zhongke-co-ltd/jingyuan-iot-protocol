package com.jingyuan.iot.protocol.tcp.cts.wxr;

import com.jingyuan.common.protocol.holder.tcp.TcpEncodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.cts.codec.CtsFeatureCodecEnums;
import io.netty.buffer.ByteBuf;
import org.jetlinks.core.message.property.WritePropertyMessage;

public class CtsWritePropertyMessage implements TcpMessageAdapter<WritePropertyMessage> {

    private WritePropertyMessage message;

    /**
     * 设置消息
     *
     * @param message 消息
     */
    @Override
    public void setMessage(WritePropertyMessage message) {
     this.message=message;
    }

    /**
     * 获取消息
     *
     * @return T 消息
     */
    @Override
    public WritePropertyMessage getMessage() {
        return message;
    }

    @Override
    public void write(ByteBuf buf, TcpEncodeHolder encodeHolder) {
       CtsFeatureCodecEnums.write(encodeHolder, buf, message.getProperties());
    }
}
