package com.jingyuan.iot.protocol.tcp.spr.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.enums.IotValveStateEnums;
import com.jingyuan.common.protocol.enums.spr.JoyoSprFeatureEnums;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.spr.SprValveStateBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 控阀
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class JoyoSprValveStateMessage implements TcpMessageCodec {

    /**
     * 向 ByteBuf 写数据
     *
     * @param buf        数据
     * @param properties 写入的属性值
     */
    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeBytes(BytesUtils.hexToBytes(JoyoSprFeatureEnums.VALVE_STATE.getFeatCode()));
        SprValveStateBo valveStateBo = BeanUtil.toBean(properties, SprValveStateBo.class);
        IotValveStateEnums valveStateEnum = IotValveStateEnums.getByCode(valveStateBo.getValveState());

        if (IotValveStateEnums.CLOSE_TO_OPEN.equals(valveStateEnum)) {
            // 关阀并进入待开阀状态
            buf.writeByte(valveStateBo.getReportCycle().byteValue());
        } else {
            // 开阀或管阀状态
            buf.writeByte(BytesUtils.hexToBytes(valveStateEnum.getName())[0]);
        }
    }
}
