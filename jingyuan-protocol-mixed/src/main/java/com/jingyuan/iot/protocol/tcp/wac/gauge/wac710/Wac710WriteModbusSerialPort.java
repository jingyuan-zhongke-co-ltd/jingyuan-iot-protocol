package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteModbusSerialPortBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 修改modbus本机串口属性
 *
 * @author: dr
 * @date: 2024-12-12
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteModbusSerialPort implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteModbusSerialPortBo bean = BeanUtil.toBean(properties, Wac710WriteModbusSerialPortBo.class);
        buf.writeBytes(BytesUtils.shortToBytes(bean.getSerialBaudRate(), ByteOrder.LITTLE_ENDIAN));
        buf.writeByte(bean.getSerialDataBits());
        buf.writeByte(bean.getSerialCheckBits());
        buf.writeByte(bean.getSerialStopBits());
    }
}
