package com.jingyuan.iot.protocol.tcp.lcp.wrx;

import com.jingyuan.common.protocol.enums.IotMessageType;
import com.jingyuan.common.protocol.enums.cct.*;
import com.jingyuan.common.protocol.enums.lcp.LcpAfnFeatureEnums;
import com.jingyuan.common.protocol.enums.lcp.fn.LcpAfnConfirmRwEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.common.protocol.msgid.MsgIdCacheUtil;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.iot.protocol.tcp.cct.wrx.CctChildDeviceMessage;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpDecodeHolder;
import com.jingyuan.iot.protocol.tcp.lcp.holder.LcpTcpEncodeHolder;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.device.DeviceThingType;
import org.jetlinks.core.message.DeviceMessage;
import org.jetlinks.core.message.HeaderKey;

import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.function.Supplier;


/**
 * 京源测控终端消息类型枚举
 *
 * @author: dr
 * @date: 2024-10-22 15:06
 * @Version: V1.0
 */
@Slf4j
public enum LcpMessageTypeEnums {

    online(IotMessageType.ONLINE.getCode(), null),
    ack(IotMessageType.ACKNOWLEDGE.getCode(), null),
    reportProperty(IotMessageType.REPORT_PROPERTY.getCode(), null),
    readProperty(IotMessageType.READ_PROPERTY.getCode(), null),
    readPropertyReply(IotMessageType.READ_PROPERTY_REPLY.getCode(), null),
    writeProperty(IotMessageType.WRITE_PROPERTY.getCode(), LcpWritePropertyMessage::new),
    writePropertyReply(IotMessageType.WRITE_PROPERTY_REPLY.getCode(), LcpWritePropertyMessageReply::new),
    childDevice(IotMessageType.CHILD.getCode(), CctChildDeviceMessage::new),
    childDeviceReply(IotMessageType.CHILD_REPLY.getCode(), null),
    function(IotMessageType.INVOKE_FUNCTION.getCode(), null),
    functionReply(IotMessageType.INVOKE_FUNCTION_REPLY.getCode(), null),
    event(IotMessageType.EVENT.getCode(), null);

    private final String messageType;
    private final Supplier<TcpMessageAdapter<DeviceMessage>> forTcp;

    @SuppressWarnings("all")
    LcpMessageTypeEnums(String messageType,
                        Supplier<? extends TcpMessageAdapter<?>> forTcp) {
        this.messageType = messageType;
        this.forTcp = (Supplier) forTcp;
    }

    public static final HeaderKey<Integer> HEADER_MSG_SEQ = HeaderKey.of("_seq", 0, Integer.class);
    private static final Map<String, LcpMessageTypeEnums> maps = new HashMap<>(16);

    static {
        for (LcpMessageTypeEnums value : values()) {
            maps.put(value.messageType, value);
        }
    }

    /**
     * 数据写出
     *
     * @param messageEncodeHolder 待写出消息
     * @param buf                 待写入缓冲区
     */
    public static void write(LcpTcpEncodeHolder messageEncodeHolder, ByteBuf buf) {
        DeviceMessage message = messageEncodeHolder.getDeviceMessage();

        int msgId = message.getHeaderOrElse(HEADER_MSG_SEQ, () ->
                MsgIdCacheUtil.takeHolder(message.getDeviceId()).next(message.getMessageId()));
        message.messageId(String.valueOf(msgId));

        IotMessageType messageType = IotMessageType.lookupByMessage(message);
        LcpMessageTypeEnums type = maps.get(messageType.getCode());
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> tcp = type.forTcp.get();
        tcp.setMessage(message);

        // 链路层
        buf.writeByte(0x68);
        byte[] lBytes = BytesUtils.shortToBytes((short) messageEncodeHolder.getL(), ByteOrder.LITTLE_ENDIAN);
        buf.writeBytes(lBytes);
        buf.writeByte(0x68);

        // 控制码
        buf.writeByte(0x20);
        // 地址域
        buf.writeBytes(BytesUtils.hexToBytes(messageEncodeHolder.getDeviceId()));
        // 应用层
        tcp.write(buf, messageEncodeHolder);
        // 缓存消息ID，供设备回复时匹配
        MsgIdCacheUtil.cacheReplyMsgId(message.getDeviceId(), ByteBufUtils.getBytesToHex(buf, 10, 1), msgId);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf 待写出缓冲区
     * @return org.jetlinks.core.message.DeviceMessage
     */
    public static LcpTcpDecodeHolder read(ByteBuf buf) {
        LcpTcpDecodeHolder decodeHolder = LcpTcpDecodeHolder.of();
        decodeHolder.setRawDataHex(BytesUtils.bytesToHex(ByteBufUtils.getAll(buf)));
        // 校验和校验
        LcpAfnConfirmRwEnums.confirm.getCheckResult(decodeHolder.getRawDataHex());

        ByteBufUtils.readBytesToHex(buf, 4);
        // 解析控制码
        decodeHolder.setControlCode(BytesUtils.byteToHex(buf.readByte()));

        // 解析地址域
        decodeHolder.setDeviceId(ByteBufUtils.readBytesToHex(buf, 5));

        // 链路用户数据区
        // 应用层功能码
        decodeHolder.setAfn(LcpAfnFeatureEnums.getByCode(BytesUtils.byteToHex(buf.readByte())));
        // seq
        readSeq(buf.readByte(),decodeHolder);

        DeviceMessage deviceMessage = read(buf, decodeHolder, MsgIdCacheUtil::msgIdCache);

        IotFeat feature = decodeHolder.getFeature();
        // 设备消息日志添加功能信息
        feature.setMessageHeaders(deviceMessage);

        decodeHolder.setDeviceMessage(deviceMessage);
        return decodeHolder;
    }

    private static void readSeq(byte seq, LcpTcpDecodeHolder decodeHolder) {
        CctSeqFfEnums seqFfEnum = CctSeqFfEnums.getBySeq(seq);
        decodeHolder.setSeqFf(seqFfEnum);
        CctSeqConEnums seqConEnum = CctSeqConEnums.getBySeq(seq);
        decodeHolder.setSeqCon(seqConEnum);
    }

    /**
     * ByteBuf 读取数据转为 DeviceMessage
     *
     * @param buf                 待写出缓冲区
     * @param messageDecodeHolder 消息解码数据包装类
     * @param handler             消息ID处理类
     * @return T
     */
    public static <T> T read(ByteBuf buf, LcpTcpDecodeHolder messageDecodeHolder, BiFunction<DeviceMessage, Integer, T> handler) {
        // 创建消息对象
        TcpMessageAdapter<DeviceMessage> nbMessage = LcpMessageTypeEnums.writePropertyReply.forTcp.get();

        // 从ByteBuf读取
        nbMessage.read(buf, messageDecodeHolder);
        DeviceMessage message = nbMessage.getMessage();
        message.thingId(DeviceThingType.device, messageDecodeHolder.getDeviceId());

        // 从消息体中的地址域A解析 MSA -> msgId
        Integer replyMsgId = MsgIdCacheUtil.getReplyMsgId(message.getDeviceId(), messageDecodeHolder.getAfn().getCode());
        return handler.apply(message, replyMsgId);
    }


}
