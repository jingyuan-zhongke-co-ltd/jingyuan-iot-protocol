package com.jingyuan.iot.protocol.tcp.runda.wrx;

import cn.hutool.core.util.StrUtil;
import com.jingyuan.common.protocol.constant.IotConstants;
import com.jingyuan.common.protocol.enums.JoyoIotVendor;
import com.jingyuan.common.protocol.holder.tcp.TcpDecodeHolder;
import com.jingyuan.common.protocol.message.adapter.TcpMessageAdapter;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodecs;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;
import org.jetlinks.core.message.property.ReportPropertyMessage;

import java.util.HashMap;
import java.util.Map;

/**
 * 京源IOT分体阀控协议 - 属性上报
 *
 * @author: cc
 * @date: 2024/3/27 16:24
 * @Version: V1.0
 */
@Slf4j
public class RundaReportPropertyMessage implements TcpMessageAdapter<ReportPropertyMessage> {

    private ReportPropertyMessage message;

    @Override
    public void read(ByteBuf buf, TcpDecodeHolder decodeHolder) {
        message = new ReportPropertyMessage();
        Map<String, Object> map = initReadMap(decodeHolder);
        FeatCodecs.read(decodeHolder, buf, map);
        message.setProperties(map);
    }

    @Override
    public void setMessage(ReportPropertyMessage message) {
        this.message = message;
    }

    @Override
    public ReportPropertyMessage getMessage() {
        return message;
    }


    public static Map<String, Object> initReadMap(TcpDecodeHolder decodeHolder) {
        Map<String, Object> map = new HashMap<>(32);
        map.put(IotConstants.RAW_DATA_HEX, decodeHolder.getRawDataHex());
        map.put(IotConstants.VENDOR_CODE, JoyoIotVendor.RUNDA_WP.getCode());
        map.put(IotConstants.IMEI_LOWER, StrUtil.subPre(decodeHolder.getDeviceId(), 15));
        return map;
    }
}
