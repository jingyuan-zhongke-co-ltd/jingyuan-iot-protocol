package com.jingyuan.iot.protocol.tcp.hlm.codec;

import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;

/**
 * @author 代荣
 */
public class HlmDecodeUtils {
    /**
     * 时间解析
     *
     * @param buf
     * @param l   时间字节数
     * @return
     */
    public static String timeByteRead(ByteBuf buf, int l) {
        byte[] time = BytesUtils.hexToBytes(ByteBufUtils.readBytesToHex(buf, l));
        BytesUtils.bytesReverse(time);
        String output = BytesUtils.bytesToHex(time);
        String substring;
        int i = 0;
        if (l == 6) substring ="20" + output.substring(i, i += 2);// 年
        else substring = output.substring(i, i += 4);// 年
        String formatted = String.format("-%s-%s %s:%s:%s",
                output.substring(i, i += 2), // 月
                output.substring(i, i += 2), // 日
                output.substring(i, i += 2), // 时
                output.substring(i, i += 2), // 分
                output.substring(i, i += 2)); // 秒
        return substring + formatted;
    }

    /**
     * 补齐位
     */
    public static byte[] replenishByte(byte[] original, int length) {

        if (original.length == length) return original;
        int i = original.length;
        byte[] padded = new byte[length]; // 创建一个长度为 l的新数组

        // 复制原始字节数组的内容到新数组
        System.arraycopy(original, 0, padded, 0, Math.min(i, padded.length));

        // 返回补齐后的数组
        return padded;
    }

}
