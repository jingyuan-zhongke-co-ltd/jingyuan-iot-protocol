package com.jingyuan.iot.protocol.tcp.wac.gauge.wac710;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.Wac710WriteWorkingHoursBo;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.nio.ByteOrder;
import java.util.Map;

/**
 * 写累计工作时间
 *
 * @author: dr
 * @date: 2024-12-11
 * @Version: V1.0
 */
@Slf4j
public class Wac710WriteWorkingHours implements TcpMessageCodec {

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        Wac710WriteWorkingHoursBo bean = BeanUtil.toBean(properties, Wac710WriteWorkingHoursBo.class);
        buf.writeBytes(BytesUtils.intToBytes(bean.getCumulateWorkingTime(), ByteOrder.LITTLE_ENDIAN));
    }
}
