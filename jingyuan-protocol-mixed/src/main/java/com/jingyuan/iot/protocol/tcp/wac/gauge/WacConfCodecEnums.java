package com.jingyuan.iot.protocol.tcp.wac.gauge;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.convert.Convert;
import com.jingyuan.common.protocol.enums.wac.conf.WacConfFeatEnums;
import com.jingyuan.common.protocol.feature.IotFeat;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.wac.conf.WacConfGeneralParamBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import com.jingyuan.common.utils.date.DateUtils;
import com.jingyuan.iot.protocol.tcp.codec.FeatCodec;
import com.jingyuan.iot.protocol.tcp.wac.gauge.conf.*;
import io.netty.buffer.ByteBuf;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 写调节阀所有配置参数 编解码枚举
 *
 * @author: dr
 * @date: 2024-12-13
 * @Version: V1.0
 */
@Slf4j
@Getter
@AllArgsConstructor
public enum WacConfCodecEnums implements FeatCodec {

    /**
     * 面板调节模式
     */
    PANELS_CONT(WacConfFeatEnums.PANELS_CONT, new WacConfPanelsCont()),
    /**
     * 远程开度模式
     */
    REMOTE_CONT(WacConfFeatEnums.REMOTE_CONT, new WacConfRemoteCont()),
    /**
     * 回温调节
     */
    RETURN_CONT(WacConfFeatEnums.RETURN_CONT, new WacConfTempCont()),
    /**
     * 温差调节模式
     */
    TEMP_DIFF_CONT(WacConfFeatEnums.TEMP_DIFF_CONT, new WacConfTempCont()),
    /**
     * 分时调节
     */
    TIME_SHARING_MODE(WacConfFeatEnums.TIME_SHARING_MODE, new WacConfTimeShardingMode()),
    /**
     * 刷卡调节
     */
    SWIPE_MODE(WacConfFeatEnums.SWIPE_MODE, new WacConfPanelsCont()),
    /**
     * 瞬时流量调节
     */
    INSTANT_FLOW(WacConfFeatEnums.INSTANT_FLOW, new WacConfInstantFlow()),
    /**
     * 瞬时流量PID调节
     */
    INSTANT_FLOW_PID(WacConfFeatEnums.INSTANT_FLOW_PID, new WacConfPid()),
    /**
     * 热功率PID调节
     */
    THERMAL_POWER_PID(WacConfFeatEnums.THERMAL_POWER_PID, new WacConfPid()),
    ;

    private final IotFeat feat;
    private final TcpMessageCodec codec;

    public static TcpMessageCodec lookup(WacConfFeatEnums wacConfFeatEnums) {
        return Arrays.stream(values())
                     .filter(e -> e.feat.equals(wacConfFeatEnums))
                     .findFirst()
                     .map(val -> val.codec)
                     .orElseThrow(() -> new IllegalArgumentException("不支持的调节模式: " + wacConfFeatEnums));
    }

    public static void read(ByteBuf buf, Map<String, Object> properties) {

        //  供暖期起始日期6月10日
        properties.put("heatStartDate", revertBCDMMhh(buf, 2));
        //  供暖期结束日期6月09日
        properties.put("heatEndDate", revertBCDMMhh(buf, 2));
        //  阀门开度上限 100
        properties.put("openLimitUpper", Convert.toInt(revertBCD(buf, 2)));
        //  阀门开度下限
        properties.put("openLimitLower", Convert.toInt(revertBCD(buf, 2)));
        //  阀门清洗间隔720小时
        properties.put("cleanValveCycle", Convert.toInt(revertBCD(buf, 2)));
        //  调节模式
        String adjustMode = Convert.toStr(buf.readByte());
        WacConfFeatEnums byCode = WacConfFeatEnums.getByCode(adjustMode);
        TcpMessageCodec messageCodec = lookup(byCode);
        properties.put("adjustMode", adjustMode);
        //  是否启用自调节
        properties.put("enSelfAdjust", BytesUtils.byteToHex(buf.readByte()));
        messageCodec.read(buf, properties);
    }

    public static String revertBCD(ByteBuf buf, int length) {
        byte[] bytes = ByteBufUtils.readBytes(buf, length);
        BytesUtils.bytesReverse(bytes);
        return BytesUtils.bcdToStr(bytes);
    }

    private static String revertBCDMMhh(ByteBuf buf, int length) {
        String s = revertBCD(buf, length);
        return s.substring(0, 2) + "月" + s.substring(2) + "日";
    }

    public static void write(ByteBuf buf, Map<String, Object> properties) {
        // 写入通用参数
        WacConfGeneralParamBo wacConfGeneralParamBo = BeanUtil.toBean(properties, WacConfGeneralParamBo.class);
        Date heatStartDate = wacConfGeneralParamBo.getHeatStartDate();
        Date heatEndDate = wacConfGeneralParamBo.getHeatEndDate();
        buf.writeBytes(BytesUtils.hexToBytes(DateUtils.format(heatStartDate, "ddMM")))
           .writeBytes(BytesUtils.hexToBytes(DateUtils.format(heatEndDate, "ddMM")))
           .writeBytes(BcdAndPad(wacConfGeneralParamBo.getOpenLimitUpper(), 2))
           .writeBytes(BcdAndPad(wacConfGeneralParamBo.getOpenLimitLower(), 2))
           .writeBytes(BcdAndPad(wacConfGeneralParamBo.getCleanInterval(), 2));
        buf.writeByte(Convert.toShort(wacConfGeneralParamBo.getAdjustmentMode()));
        buf.writeByte(wacConfGeneralParamBo.isEnSelfAdjust() ? 0 : 1);
        //调节模式
        String adjustMode = Convert.toStr(wacConfGeneralParamBo.getAdjustmentMode());
        WacConfFeatEnums WacConfMode = WacConfFeatEnums.getByCode(adjustMode);
        TcpMessageCodec messageCodec = lookup(WacConfMode);
        messageCodec.write(buf, properties);
    }

    public static byte[] BcdAndPad(Number num, int length) {
        byte[] bytes = BytesUtils.str2Bcd(Convert.toStr(num));
        BytesUtils.bytesReverse(bytes);
        // 如果字节数组的长度小于指定的 length，则进行填充
        if (bytes.length < length) {
            byte[] paddedBytes = new byte[length];
            System.arraycopy(bytes, 0, paddedBytes, 0, bytes.length);
            return paddedBytes;
        }
        return bytes;
    }

    public static byte[] stringToBCDAndPad(Number str, int length) {
        String string = Convert.toStr(str);
        // 如果字符串长度小于目标长度，则在字符串前面补足'0'
        if (string.length() < length) {
            string = String.format("%" + length + "s", str).replace(' ', '0');
        }

        // 将补足后的字符串转换为BCD字节数组
        return BytesUtils.str2Bcd(string);
    }
}
