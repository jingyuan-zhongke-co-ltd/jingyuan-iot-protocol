package com.jingyuan.iot.protocol.tcp.runda.codec;

import cn.hutool.core.bean.BeanUtil;
import com.jingyuan.common.protocol.message.codec.TcpMessageCodec;
import com.jingyuan.common.protocol.model.runda.WpFilterResetBo;
import com.jingyuan.common.utils.bytes.ByteBufUtils;
import com.jingyuan.common.utils.bytes.BytesUtils;
import io.netty.buffer.ByteBuf;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * 滤芯复位与修改：必须绑定套餐后操作，否则无响应。
 *
 * @author: cc
 * @date: 2024/5/7 14:53
 * @Version: V1.0
 */
@Slf4j
public class RundaFilterResetMessage implements TcpMessageCodec {

    /**
     * 从 ByteBuf 读数据
     *
     * @param buf        数据
     * @param properties 读取的属性值
     */
    @Override
    public void read(ByteBuf buf, Map<String, Object> properties) {
        // 数据长度
        buf.readByte();
        // 滤芯复位
        byte[] filterResetHex = ByteBufUtils.readBytes(buf, 3);
        properties.put("filterResetHex", BytesUtils.bytesToHex(filterResetHex));
    }

    @Override
    public void write(ByteBuf buf, Map<String, Object> properties) {
        buf.writeByte(0x03);

        WpFilterResetBo wpFilterResetBo = BeanUtil.toBean(properties, WpFilterResetBo.class);
        String filterResetHex = wpFilterResetBo.getFilterResetHex();
        buf.writeBytes(BytesUtils.hexToBytes(filterResetHex));
    }
}
